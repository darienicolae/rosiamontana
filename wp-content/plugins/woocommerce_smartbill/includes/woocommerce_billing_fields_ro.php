<?php
function woocommerce_billing_fields_ro($checkout)
{
    $wbfr_company_details = empty($_POST) ? false : $checkout->get_value('wbfr_company_details');

    woocommerce_form_field('wbfr_cnp', array(
        'type'          => 'text',
        'class'         => array('form-row-wide'),
        'label'         => __('CNP', 'wc_smartbill'),
        'placeholder'   => __('CNP', 'wc_smartbill'),
        ), $checkout->get_value('wbfr_cnp'));

    echo '<div id="woocommerce_billing_fields_ro">
            <h3 style="margin:0;padding:0;">
                <label class="checkbox" for="wbfr_company_details">'.__('Doriti facturare pe firmă?', 'wc_smartbill').'</label>
                <input id="wbfr_company_details-checkbox" class="input-checkbox" '.checked( $wbfr_company_details, 1 ).' type="checkbox" name="wbfr_company_details" value="1" />
            </h3>
            <div class="wbfr_company_details">';

    woocommerce_form_field('wbfr_cif', array(
        'type'          => 'text',
        'class'         => array('form-row-wide'),
        'label'         => __('CIF', 'wc_smartbill'),
        'placeholder'   => __('Cod de identificare fiscală', 'wc_smartbill'),
        ), $checkout->get_value('wbfr_cif'));

    woocommerce_form_field('wbfr_regcom', array(
        'type'          => 'text',
        'class'         => array('form-row-wide'),
        'label'         => __('Nr. înregistrare Registrul Comerțului', 'wc_smartbill'),
        'placeholder'   => __('Nr. înregistrare Registrul Comerțului', 'wc_smartbill'),
        ), $checkout->get_value('wbfr_regcom'));

    woocommerce_form_field('wbfr_cont_banca', array(
        'type'          => 'text',
        'class'         => array('form-row-wide'),
        'label'         => __('Cont bancar', 'wc_smartbill'),
        'placeholder'   => __('Cont bancar', 'wc_smartbill'),
        ), $checkout->get_value('wbfr_cont_banca'));

    woocommerce_form_field('wbfr_banca', array(
        'type'          => 'text',
        'class'         => array('form-row-wide'),
        'label'         => __('Banca', 'wc_smartbill'),
        'placeholder'   => __('Banca', 'wc_smartbill'),
        ), $checkout->get_value('wbfr_banca'));

    echo '</div></div>
 <script type="text/javascript"><!--//--><![CDATA[//><!--
     try {
        jQuery( "#wbfr_company_details-checkbox" ).on( "change", function(){
            jQuery( "div.wbfr_company_details" ).hide();
            if ( jQuery( this ).is( ":checked" ) ) {
                jQuery( "div.wbfr_company_details" ).slideDown();
            }
        }).trigger( "change" );
     } catch (ex) {}
 //--><!]]></script>
    ';
}

function woocommerce_billing_fields_ro_process()
{
    //global $woocommerce;

    // Check if set, if its not set add an error. This one is only required for companies
    if (isset($_POST['billing_company']) && !empty($_POST['billing_company'])) {
        if ( empty($_POST['wbfr_cif']) ) {
            wc_add_notice(
                __('Nu ai introdus toate câmpurile de la <strong>Date facturare firmă</strong> din partea de jos a paginii', 'wc_smartbill'),
                'error'
            );
        }
    } else {
        /*
        if (empty($_POST['wbfr_cnp'])) {
            wc_add_notice(
                __('Nu ai introdus competat campul <strong>CNP</strong>', 'wc_smartbill'),
                'error'
            );
        }
        */
    }
}

function woocommerce_billing_fields_ro_update_user_meta($userId)
{
    if ($userId && isset($_POST['wbfr_cnp'])) {
        update_user_meta($userId, 'wbfr_cnp', esc_attr($_POST['wbfr_cnp']));
    }
    if ($userId && isset($_POST['wbfr_cif'])) {
        update_user_meta($userId, 'wbfr_cif', esc_attr($_POST['wbfr_cif']));
    }
    if ($userId && isset($_POST['wbfr_regcom'])) {
        update_user_meta($userId, 'wbfr_regcom', esc_attr($_POST['wbfr_regcom']));
    }
    if ($userId && isset($_POST['wbfr_cont_banca'])) {
        update_user_meta($userId, 'wbfr_cont_banca', esc_attr($_POST['wbfr_cont_banca']));
    }
    if ($userId && isset($_POST['wbfr_banca'])) {
        update_user_meta($userId, 'wbfr_banca', esc_attr($_POST['wbfr_banca']));
    }
}

function woocommerce_billing_fields_ro_update_order_meta($orderId)
{
    if (isset($_POST['wbfr_cnp'])) {
        update_post_meta($orderId, 'wbfr_cnp', esc_attr($_POST['wbfr_cnp']));
    }
    if (isset($_POST['wbfr_cif'])) {
        update_post_meta($orderId, 'wbfr_cif', esc_attr($_POST['wbfr_cif']));
    }
    if (isset($_POST['wbfr_regcom'])) {
        update_post_meta($orderId, 'wbfr_regcom', esc_attr($_POST['wbfr_regcom']));
    }
    if (isset($_POST['wbfr_cont_banca'])) {
        update_post_meta($orderId, 'wbfr_cont_banca', esc_attr($_POST['wbfr_cont_banca']));
    }
    if (isset($_POST['wbfr_banca'])) {
        update_post_meta($orderId, 'wbfr_banca', esc_attr($_POST['wbfr_banca']));
    }
}

function woocommerce_billing_fields_ro_order_meta_keys($keys)
{
    $keys[__('CNP', 'wc_smartbill')] = 'wbfr_cnp';
    $keys[__('CIF', 'wc_smartbill')] = 'wbfr_cif';
    $keys[__('Nr. înregistrare Registrul Comerțului', 'wc_smartbill')] = 'wbfr_regcom';
    $keys[__('Cont bancar', 'wc_smartbill')] = 'wbfr_cont_banca';
    $keys[__('Banca', 'wc_smartbill')] = 'wbfr_banca';
    return $keys;
}
