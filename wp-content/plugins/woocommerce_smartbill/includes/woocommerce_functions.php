<?php
function wc_smartbill_order_details_meta_box()
{
    add_meta_box(
        'wc_smartbill_meta_box',
        'Facturare Smartbill',
        'wc_smartbill_order_details_invoice_box',
        'shop_order',
        'side',
        'high'
    );
}

function wc_smartbill_order_details_invoice_box($post)
{
    $series = get_post_meta($post->ID, 'smartbill_series_name', true);
    $number = get_post_meta($post->ID, 'smartbill_document_number', true);
    $link = get_post_meta($post->ID, 'smartbill_private_link', true);
    if (!empty($link)) {
        # vizualizeaza factura
        echo '<h4>'.$series.' '.$number.'</h4>';
        echo '<p>
            <a class="button tips"
               data-tip="'.__('Vizualizeaza in Smart Bill', 'wc_smartbill').'"
               href="'.$link.'" target="_blank">'.__('Vizualizeaza in Smart Bill', 'wc_smartbill').'</a>
        </p>';
    } else {
        # genereaza factura
        echo '<p>
            <a class="button tips"
               data-tip="'.__('Emite in Smart Bill', 'wc_smartbill').'"
               href="'.wc_smartbill_generate_url('smartbill-create', $post->ID).'" target="_blank">'.__('Emite in Smart Bill', 'wc_smartbill').'</a>
        </p>';
    }
}
function wc_smartbill_order_details_invoice_column_button($post)
{
    $link = get_post_meta($post->ID, 'smartbill_private_link', true);
    if (empty($link)) {
        # genereaza factura
        echo '<p>
            <a class="button tips"
               data-tip="'.__('Emite in Smart Bill', 'wc_smartbill').'"
               href="'.wc_smartbill_generate_url('smartbill-create', $post->ID).'" target="_blank"><img src="'.plugin_dir_url(__FILE__).'../assets/images/smb_head.png'.'"/></a>
        </p>';
    }
}

function wc_smartbill_generate_url($arg_name, $orderId)
{
    $action_url = add_query_arg($arg_name, $orderId);
    $complete_url = wp_nonce_url($action_url);

    return esc_url($complete_url);
}

function wc_smartbill_init_plugin_actions() {
    # links
    if (isset($_GET['smartbill-create'])) {
        // enable errors (for "white" page cases)
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        wc_smartbill_create_invoice($_GET['smartbill-create']);
    }

    if (isset($_GET['smartbill_response'])) {
        $input = file_get_contents("php://input");
        if (!empty($input)) {
            $smartbillResponse = SmartBillHelper::decryptData($input);
            if (!empty($smartbillResponse)) {
                if (empty($smartbillResponse['documentNumber']) && empty($smartbillResponse['seriesName'])) {
                    # stergem factura
                    delete_post_meta($smartbillResponse['orderNumber'], 'smartbill_document_number');
                    delete_post_meta($smartbillResponse['orderNumber'], 'smartbill_public_link');
                    delete_post_meta($smartbillResponse['orderNumber'], 'smartbill_series_name');
                    delete_post_meta($smartbillResponse['orderNumber'], 'smartbill_private_link');
                    delete_post_meta($smartbillResponse['orderNumber'], 'smartbill_ecsid');

                    # add order note
                    $order = new WC_Order($smartbillResponse['orderNumber']);
                    $order->add_order_note(__('Documentul SmartBill a fost sters', 'wc_smartbill'));
                } else {
                    # add order note
                    $order = new WC_Order($smartbillResponse['orderNumber']);
                    $order->add_order_note(__('Documentul SmartBill ' . $smartbillResponse['seriesName'] . ' ' . $smartbillResponse['documentNumber'] . ' a fost creat', 'wc_smartbill'));

                    # add meta
                    add_post_meta(
                        $smartbillResponse['orderNumber'],
                        'smartbill_document_number',
                        $smartbillResponse['documentNumber']
                    );
                    add_post_meta(
                        $smartbillResponse['orderNumber'],
                        'smartbill_public_link',
                        $smartbillResponse['externalViewUrl']
                    );
                    add_post_meta(
                        $smartbillResponse['orderNumber'],
                        'smartbill_series_name',
                        $smartbillResponse['seriesName']
                    );
                    add_post_meta(
                        $smartbillResponse['orderNumber'],
                        'smartbill_private_link',
                        $smartbillResponse['url']
                    );
                    add_post_meta(
                        $smartbillResponse['orderNumber'],
                        'smartbill_ecsid',
                        $smartbillResponse['ecsId']
                    );
                }
                // header('Location:' . get_site_url());
                // return;
            }
        }

        echo 'updated';
        exit;
    }
}

function wc_smartbill_create_invoice($orderId)
{
    $order = new WC_Order($orderId);
    $orderMeta = get_post_meta($orderId);

    # build custom fields
    $customer = array(
        'cnp' => isset($orderMeta['wbfr_cnp'])?$orderMeta['wbfr_cnp'][0]:'',
        'cif' => isset($orderMeta['wbfr_cif'])?$orderMeta['wbfr_cif'][0]:'',
        'regcom' => isset($orderMeta['wbfr_regcom'])?$orderMeta['wbfr_regcom'][0]:'',
        'iban' => isset($orderMeta['wbfr_cont_banca'])?$orderMeta['wbfr_cont_banca'][0]:'',
        'banca' => isset($orderMeta['wbfr_banca'])?$orderMeta['wbfr_banca'][0]:'',
        'company' => isset($orderMeta['_billing_company'])?$orderMeta['_billing_company'][0]:'',
        'first_name' => isset($orderMeta['_billing_first_name'])?$orderMeta['_billing_first_name'][0]:'',
        'last_name' => isset($orderMeta['_billing_last_name'])?$orderMeta['_billing_last_name'][0]:'',
        'email' => isset($orderMeta['_billing_email'])?$orderMeta['_billing_email'][0]:'',
        'phone' => isset($orderMeta['_billing_phone'])?$orderMeta['_billing_phone'][0]:'',
        'country' => isset($orderMeta['_billing_country'])?$orderMeta['_billing_country'][0]:'',
        'address1' => isset($orderMeta['_billing_address_1'])?$orderMeta['_billing_address_1'][0]:'',
        'address2' => isset($orderMeta['_billing_address_2'])?$orderMeta['_billing_address_2'][0]:'',
        'city' => isset($orderMeta['_billing_city'])?$orderMeta['_billing_city'][0]:'',
        'state' => isset($orderMeta['_billing_state'])?$orderMeta['_billing_state'][0]:'',
        'postcode' => isset($orderMeta['_billing_postcode'])?$orderMeta['_billing_postcode'][0]:''
    );

    # verificam daca exista deja
    $link = get_post_meta($orderId, 'smartbill_private_link', true);
    if (!empty($link)) {
        header('Location: '.$link);
        exit;
    }

    // get options
    $loginOptions = get_option('wc_smartbill_plugin_options');
    $options = get_option('wc_smartbill_plugin_options_settings');
    $token = $loginOptions['token'];
    $username = $loginOptions['username'];
    $settingsResponse = SmartBillHelper::getSettings($token, $username);
    if (!empty($options) && is_array($options) && isset($options['save_client'])) {
        $save_client = $options['save_client'];
    } else {
        $save_client = 0;
    }
    if (!empty($options) && is_array($options) && isset($options['document_series'])) {
        $document_series = $options['document_series'];
    } else {
        $document_series = 0;
    }
    if (!empty($options) && is_array($options) && isset($options['document_type'])) {
        $document_type = $options['document_type'];
    } else {
        $document_type = 0;
    }
    if (!empty($options) && is_array($options) && isset($options['billing_currency'])) {
        $billing_currency = $options['billing_currency'];
    } else {
        $billing_currency = 0;
    }
    if (!empty($options) && is_array($options) && isset($options['product_currency'])) {
        $product_currency = $options['product_currency'];
    } else {
        $product_currency = '';
    }
    if (!empty($options) && is_array($options) && isset($options['included_vat'])) {
        $included_vat = $options['included_vat'];
    } else {
        $included_vat = 0;
    }
    if (!empty($options) && is_array($options) && isset($options['shipping_included_vat'])) {
        $shipping_included_vat = $options['shipping_included_vat'];
    } else {
        $shipping_included_vat = 0;
    }
    if (!empty($options) && is_array($options) && isset($options['company'])) {
        $company = $options['company'];
    } else {
        $company = '';
    }
    if (!empty($options) && is_array($options) && isset($options['gestiune'])) {
        $gestiune = $options['gestiune'];
    } else {
        $gestiune = 'fara gestiune';
    }
    if (!empty($options) && is_array($options) && isset($options['extra_taxes'])) {
        $extra_taxes = $options['extra_taxes'];
    } else {
        $extra_taxes = array();
    }
    if (!empty($options) && is_array($options) && isset($options['um'])) {
        $um = $options['um'];
    } else {
        $um = '';
    }
    if (!empty($options) && is_array($options) && isset($options['product_vat'])) {
        $product_vat = $options['product_vat'];
    } else {
        $product_vat = '';
    }
    if (!empty($options) && is_array($options) && isset($options['shipping_vat'])) {
        $shipping_vat = $options['shipping_vat'];
    } else {
        $shipping_vat = '';
    }
    if (!empty($options) && is_array($options) && isset($options['include_shipping'])) {
        $include_shipping = $options['include_shipping'];
    } else {
        $include_shipping = 0;
    }

    $companySettings = SmartBillHelper::getCompanySettings($company, $settingsResponse);

    // generam setarile pentru produse
    $productSettings = array(
        'product_currency' => $product_currency,
        'included_vat' => $included_vat,
        'shipping_included_vat' => $shipping_included_vat,
        'um' => $um,
        'saveProductToDb' => $companySettings['saveProductToDb'],
        'product_vat' => $product_vat,
        'isTaxPayer' => $companySettings['isTaxPayer'],
        'warehouse' => $gestiune,
        'extraTaxes' => $extra_taxes,
        'include_shipping' => $include_shipping,
        'taxes' => $order->get_taxes(),
        'companySettings' => $companySettings,
        'shipping_vat' => $shipping_vat
    );

    $orderTotal = (float)$order->get_total();
    $products   = SmartBillDocumentHelper::getOrderProducts($order, $order->get_items(), $productSettings);
    //echo '<pre>'; print_r($products); echo '</pre>'; die();
    $postData = new stdClass;
    $postData->companyVatCode       = !empty($options['company']) ? $options['company'] : '';
    $postData->client               = SmartBillDocumentHelper::getClientData($customer, $save_client);
    $postData->isDraft              = true;
    $postData->issueDate            = date('Y-m-d', time());
    $postData->seriesName           = $document_series;
    if (!$document_type) {
        $postData->type = 'n';
    }
    $postData->currency             = $billing_currency;
    $postData->currency             = $postData->currency=='9998' ? '' : $postData->currency;
    $postData->exchangeRate         = 1;
    $postData->language             = 'RO';
    $postData->precision            = 2;
    $postData->issuerName           = '';
    $postData->issuerCnp            = '';
    $postData->aviz                 = '';
    $postData->dueDate              = '';
    $postData->mentions             = 'comanda online #'.$order->get_order_number();
    $postData->delegateAuto         = '';
    $postData->delegateIdentityCard = '';
    $postData->delegateName         = '';
    $postData->deliveryDate         = '';
    $postData->paymentDate          = '';
    $postData->usePaymentTax        = SmartBillDocumentHelper::getCompanyUsePaymentTax($settingsResponse, $company);
    $postData->paymentTotal         = 0;
    $postData->paymentBase          = 0;
    $postData->colectedTax          = 0;
    $postData->orderNumber          = $order->get_order_number();
    $postData->trackingNumber       = '';
    $postData->products             = $products;
    $postData->documentTotal        = !empty($orderTotal) ? ($orderTotal - (float)$order->get_total_refunded()) : 0;
    if ($document_type==0) {
        $postData->useStock = (bool)$companySettings['isStockEnabled'];
        $warehouseName = $gestiune;
        if ('fara gestiune'==strtolower(trim($warehouseName))) {
            $postData->useStock = false;
        }
    }

    if (empty($postData->currency) && !empty($products[0]->currency)) {
        $postData->currency = $products[0]->currency;
    }

    switch ($document_type) {
        case 1:
            $apiURL = SmartBillHelper::PROFORMA_URL;
            break;

        default:
            $apiURL = SmartBillHelper::INVOICE_URL;
            break;
    }

    $invoiceResponse = SmartBillHelper::curl(
        $token,
        $apiURL,
        $postData,
        array(
            "Content-Type: application/json; charset=utf-8",
            "Accept:application/json",
            "Authorization: ECSDigest ".SmartBillHelper::getAuthorization($token, $username)
        ),
        false,
        false
    );
    $invoiceResponse = json_decode($invoiceResponse);
    if (!empty($invoiceResponse->url)) {
        header('Location: '.$invoiceResponse->url);
        exit;
    } else {
        # error handler
    }

}

function wc_smarbill_add_invoice_column($columns)
{
    $columns['smartbill_invoice'] = "SmartBill";
    return $columns;
}

function wc_smartbill_add_invoice_column_content($column)
{
    global $post;
    switch ($column) {
        case 'smartbill_invoice':
            $series = get_post_meta($post->ID, 'smartbill_series_name', true);
            $number = get_post_meta($post->ID, 'smartbill_document_number', true);
            $link = get_post_meta($post->ID, 'smartbill_private_link', true);

            if (!empty($series) && !empty($number) && !empty($link)) {
                echo '<a href="'.$link.'" target="_blank">'.$series.' '.$number.'</a>';
            }
            break;
        
        case 'wc_actions':
        case 'order_actions':
            wc_smartbill_order_details_invoice_column_button($post);
            break;
    }
}
