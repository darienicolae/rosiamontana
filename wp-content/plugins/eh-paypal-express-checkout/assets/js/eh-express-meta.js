jQuery(function($) {
    jQuery("select#eh_paypal_meta_refund_select").on('change',function() 
    {
        if ( 'full' === $( this ).val() )
        {
                $( "#eh_paypal_meta_refund_amount" ).hide();
                $( '#eh_paypal_meta_refund_button' ).css("width","100%");
        } 
        else
        {
                $( "#eh_paypal_meta_refund_amount" ).show();
                $( '#eh_paypal_meta_refund_button' ).css("width","auto");
        }
    }).change();
    jQuery('#eh_paypal_meta_refund_amount').on('keyup', function()
    {
        if(!($(this).val() === ''))
        {
            var amount=$(this).val();
            if (!$.isNumeric(amount))
            {
                $( "#eh_paypal_meta_refund_amount" ).addClass("eh_pe_number_error");
            }
            else
            {
                $( "#eh_paypal_meta_refund_amount" ).removeClass("eh_pe_number_error");
            }
        }
        else
        {
            $( "#eh_paypal_meta_refund_amount" ).removeClass("eh_pe_number_error");
        }
    });
    jQuery('#eh_paypal_meta_capture_amount').on('keyup', function()
    {
        if(!($(this).val() === ''))
        {
            var amount=$(this).val();
            if (!$.isNumeric(amount))
            {
                $( "#eh_paypal_meta_capture_amount" ).addClass("eh_pe_number_error");
            }
            else
            {
                $( "#eh_paypal_meta_capture_amount" ).removeClass("eh_pe_number_error");
            }
        }
        else
        {
            $( "#eh_paypal_meta_capture_amount" ).removeClass("eh_pe_number_error");
        }
    });
    jQuery("#eh_paypal_meta_capture_button").on('click',function() 
    {
        swal({
            title: "PayPal Alert",
            text: "Making Capture action",
            showCancelButton: true,
            allowOutsideClick: false,
            allowEscapeKey: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Capture!",
            cancelButtonText: "No, Wait!"
        }).then(function() {
            $( "#eh_paypal_meta_capture_amount" ).removeClass("eh_pe_number_error");
            jQuery("#eh_paypal_action_box .loader").css("display", "block");
            jQuery.ajax({
             type   : 'post',
             url    : ajaxurl,
             data   :
                {
                    action    : 'eh_pe_capture_action',
                    order_id  : $("#eh_pe_order_id").val(),
                    _eh_nonce : jQuery('#_eh_meta_action_nonce').val(),
                },
             success: function(response)
                {
                    jQuery("#eh_paypal_action_box .loader").css("display", "none");
                    var parse=jQuery.parseJSON(response);
                    var title='';
                    if(parse.result==='success')
                    {
                        title = '<span style="color:green !important">'+parse.title+'</span>';
                    }
                    else
                    {
                        title = '<span style="color:red !important">'+parse.title+'</span>';
                    }
                    swal({
                        title: title,
                        html : parse.reason
                    });
                },
             error: function(jqXHR, textStatus, errorThrown)
                {                 
                    console.log(textStatus, errorThrown);
                    alert(textStatus+ ' '+errorThrown);   
                }
            });
        });
    });
    jQuery("#eh_paypal_meta_refund_button").on('click',function() 
    {
        var amount='';
        var type='full';
        if('partial' === $( "#eh_paypal_meta_refund_select" ).val())
        {
            amount=$( "#eh_paypal_meta_refund_amount" ).val();
            type='partial';            
        }
        if(type === 'partial')
        {
            if(amount !== '')
            {
                swal({
                    title: "PayPal Alert",
                    text: "Making Refund action",
                    showCancelButton: true,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Refund!",
                    cancelButtonText: "No, Wait!"
                }).then(function() {
                    $( "#eh_paypal_meta_refund_amount" ).removeClass("eh_pe_number_error");
                    jQuery("#eh_paypal_action_box .loader").css("display", "block");
                    jQuery.ajax({
                     type   : 'post',
                     url    : ajaxurl,
                     data   :
                        {
                            action      : 'eh_pe_refund_action',
                            order_id    : $("#eh_pe_order_id").val(),
                            type        : type,
                            amt         : amount,
                            _eh_nonce   : jQuery('#_eh_meta_action_nonce').val(),
                        },
                     success: function(response)
                        {
                            jQuery("#eh_paypal_action_box .loader").css("display", "none");
                            var parse=jQuery.parseJSON(response);
                            var title='';
                            if(parse.result==='success')
                            {
                                title = '<span style="color:green !important">'+parse.title+'</span>';
                            }
                            else
                            {
                                title = '<span style="color:red !important">'+parse.title+'</span>';
                            }
                            swal({
                                title: title,
                                html : parse.reason
                            });
                        },
                     error: function(jqXHR, textStatus, errorThrown)
                        {
                            console.log(textStatus, errorThrown);
                            alert(textStatus+ ' '+errorThrown);   
                        }
                    });
                });
            }
            else
            {
                $( "#eh_paypal_meta_refund_amount" ).addClass("eh_pe_number_error");
            }
        }
        else
        {
            swal({
                title: "PayPal Alert",
                text: "Making Refund action",
                showCancelButton: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Refund!",
                cancelButtonText: "No, Wait!"
            }).then(function() {
                $( "#eh_paypal_meta_refund_amount" ).removeClass("eh_pe_number_error");
                jQuery("#eh_paypal_action_box .loader").css("display", "block");
                jQuery.ajax({
                 type   : 'post',
                 url    : ajaxurl,
                 data   :
                    {
                        action      : 'eh_pe_refund_action',
                        order_id    : $("#eh_pe_order_id").val(),
                        type        : type,
                        amt         : amount,
                        _eh_nonce   : jQuery('#_eh_meta_action_nonce').val(),
                    },
                 success: function(response)
                    {
                        jQuery("#eh_paypal_action_box .loader").css("display", "none");
                        var parse=jQuery.parseJSON(response);
                        var title='';
                        if(parse.result==='success')
                        {
                            title = '<span style="color:green !important">'+parse.title+'</span>';
                        }
                        else
                        {
                            title = '<span style="color:red !important">'+parse.title+'</span>';
                        }
                        swal({
                            title: title,
                            html : parse.reason
                        });
                    },
                 error: function(jqXHR, textStatus, errorThrown)
                    {
                        console.log(textStatus, errorThrown);
                    }
                });
            });
        }
    });
});