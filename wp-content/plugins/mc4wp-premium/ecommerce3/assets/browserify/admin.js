const m = require('mithril');
const QueueProcessor = require('./_queue-processor.js');

// ask for confirmation for elements with [data-confirm] attribute
require('./_confirm-attr.js')();

function fetchJSON(url, args) {
	args.headers = args.headers ?? {};
	args.headers['Accept'] = 'application/json';
	return window.fetch(url, args)
		.then(r => r.json());
}

let forms = document.querySelectorAll('.object-sync');
[].forEach.call(forms, (form) => {
	form.addEventListener('submit', (evt) => {
		evt.preventDefault();

		let formData = new FormData(form)
		let elStatusLine = form.querySelector('.sync-busy');
		let elStatus = elStatusLine.querySelector('.sync-status');
		let elButton = form.querySelector('input[type="submit"]');
		let starting = form.action.indexOf('_start') > -1;
		let statusUrl = form.action.replace('_start', '_status');
		let timeout = null;

		elButton.disabled = true;
		form.parentElement.querySelector('.mc4wp-status-label').style.display = 'none';

		const toggleForm = (start) => {
			if (start) {
				form.action = form.action.replace('_stop', '_start')
				elButton.value = 'Synchronize';
				elStatusLine.style.display = 'none';
				window.clearTimeout(timeout);
			} else {
				form.action = form.action.replace('_start', '_stop')
				elButton.value = 'Cancel';
			}
		}

		fetchJSON(form.action, {
			method: form.method,
			body: formData,
		}).then((data) => {
			elButton.disabled = false;

			// if request failed
			if (data === false) {
				elStatusLine.style.display = 'none';
				return;
			}

			if (starting) {
				elStatusLine.style.display = '';
				elStatus.textContent = Math.round(data * 100) + '%';

				// update status element every 2.4 seconds
				let fetchStatus = () => {
					fetchJSON(statusUrl, {})
						.then((data) => {
							if (data === false) {
								toggleForm(true);
								return;
							}

							elStatus.textContent = Math.round(data * 100) + '%';
							timeout = window.setTimeout(fetchStatus, 2400);
						})
				}
				timeout = window.setTimeout(fetchStatus, 2400);
				toggleForm(false);
			} else {
				toggleForm(true);
			}

		});
	});
})

// queue processor
const queueRootElement = document.getElementById('queue-processor');
if (queueRootElement) {
    m.mount(queueRootElement, QueueProcessor);
}
