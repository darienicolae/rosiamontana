<?php
class SmartBillDocumentHelper
{
    public static function getClientData($customer, $saveClient)
    {
        $client = new stdClass;
        $client->vatCode    = !empty($customer['cif']) ? $customer['cif'] : ( !empty($customer['cnp']) ? $customer['cnp'] : '-' );
        $client->vatCode    = trim($client->vatCode);
        $client->name       = $customer['company'];
        $client->name       = trim($client->name);
        $clientClean        = preg_replace('/[^a-z0-9]+/i', '', $client->name);
        $client->name       = empty($client->name) || empty($clientClean) ? $customer['first_name'].' '.$customer['last_name'] : $client->name;
        $client->code       = ''; // TODO (doesn't exist)
        $client->address    = trim($customer['address1'].' '.$customer['address2']);
        $client->regCom     = $customer['regcom']; // TODO (doesn't exist)
        $client->isTaxPayer = false; // TODO (doesn't exist/might be based on VIES check but not 100% correct)
        $client->contact    = $customer['first_name'] . ' ' . $customer['last_name'];
        $client->phone      = $customer['phone'];
        $client->city       = $customer['city'];
        $client->county     = $customer['state'];
        $client->county     = empty($client->county) ? '-' : $client->county;
        $client->country    = $customer['country'];
        $client->email      = $customer['email'];
        $client->bank       = $customer['banca'];
        $client->iban       = $customer['iban'];
        $client->saveToDb   = (bool)$saveClient; // TODO (cateodata daca este trimis se primeste measj "Clientul exista deja pe server.")

        return $client;
    }

    public static function getCompanyUsePaymentTax($settingsResponse, $vatCode)
    {
        $usePaymentTax = null;

        if (!empty($settingsResponse)) {
            $company = SmartBillHelper::getCompanySettings($vatCode, $settingsResponse);

            if (isset($company['usePaymentTax'])) {
                $usePaymentTax = $company['usePaymentTax'];
            }
        }

        return $usePaymentTax;
    }

    public static function getOrderProducts($order, $orderItems, $productSettings)
    {
        $products = array();

        foreach($orderItems as $sItem) {
            $product = wc_get_product($sItem['product_id']);

            // if($sItem['type'] == "line_item")
            if( !in_array($product->get_type(), array('bundle')) )
            {
                $qty = method_exists($product, 'get_quantity') ? $product->get_quantity() : $sItem['qty'];
                $productV = method_exists($sItem, 'get_product') ? $sItem->get_product() : apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $sItem ), $sItem );

                $products[] = self::createOrderProduct(
                    $productV,
                    $sItem,
                    $qty,
                    $productSettings
                );

                // check if the variation still exists
                // if ( 'product_variation' === get_post_type( $sItem->get_variation_id() ) ) {
                    // cred ca asta calculeaza daca exista reducere
                    $productDiscountBasePrice = self::createOrderProductDiscountFromBasePrice(
                        $productV,
                        $sItem,
                        $qty,
                        $productSettings
                    );
                    if (!empty($productDiscountBasePrice)) {
                        $products[] = $productDiscountBasePrice;
                    }
                // }
            }
        }

        // order discount (refunds)
        $orderDiscounts = self::getOrderDiscounts($order, $sItem, $productSettings);
        if ( !empty($orderDiscounts) ) {
            $products = array_merge($products, $orderDiscounts);
        }        

        // order discount (coupons)
        $orderCoupons = self::getOrderCoupons($order, $sItem, $productSettings);
        if ( !empty($orderCoupons) ) {
            $products = array_merge($products, $orderCoupons);
        }        

        // shipping
        if ($productSettings['include_shipping']) {
            $products[] = self::createOrderTransport('shipping', 'Transport', 1, $order, $productSettings);
        }

        // other taxes
        $otherTaxes = self::getOrderOtherTaxes($order, $productSettings);
        if ( !empty($otherTaxes) ) {
            $products = array_merge($products, $otherTaxes);
        }

        return $products;
    }

    private static function _woo_version_check( $version = '3.0' ) {
        if ( class_exists( 'WooCommerce' ) ) {
            global $woocommerce;
            if ( version_compare( $woocommerce->version, $version, ">=" ) ) {
                return true;
            }
        }
        return false;
    }

    private static function getOrderDiscounts($order, $orderItem, $productSettings) {
        $discounts= $order->get_refunds();
        $products = array();
        $firstTax = !empty($orderItem['line_subtotal_tax']) ? round($orderItem['line_tax'] * 100 / $orderItem['line_total'], 0) : 0;

        if ( !empty($discounts) ) {
            foreach ($discounts as $item) {
                $product = new stdClass;
                $product->code                      = 'discount';
                $product->currency                  = $productSettings['product_currency'];
                $product->exchangeRate              = 1;
                $product->isDiscount                = true;
                $product->discountPercentage        = 0;
                $product->discountValue             = -1*floatval(self::_woo_version_check('3.0') ? $item->get_amount() : $item->get_refund_amount());
                $product->discountType              = 1;
                $product->isTaxIncluded             = (bool)$productSettings['included_vat'];
                $product->measuringUnitName         = $productSettings['um'];
                $product->name                      = 'Cupon discount (' . $item->reason . ')';
                $product->price                     = 0;
                $product->quantity                  = 0;
                $product->numberOfItems             = 0;
                $product->saveToDb                  = false;
                $product->taxName                   = ''; // TODO (daca este trimisa se primeste mesaj "Cota tva a produsului XXX nu a fost gasita pe server!")
                $product->taxPercentage             = floatval($productSettings['product_vat']);
                if ($product->taxPercentage=='' && (int)$productSettings['isTaxPayer']) {
                    $product->taxPercentage = '9999';
                }
                $product->translatedMeasuringUnit   = '';
                $product->translatedName            = '';
                $product->warehouseName             = $productSettings['warehouse'];

                switch ($product->taxPercentage) {
                    // din woocommerce pe produse
                    case 9998:
                        $product->taxPercentage = floatval($firstTax);
                        break;

                    // din smartbill pe produse
                    case 9999:
                        $product->taxName = '@@@@@';
                        $product->taxPercentage = 0;
                        break;
                }
                $products[] = $product;
            }
        }

        return $products;
    }

    private static function getOrderCoupons($order, $orderItem, $productSettings) {
        $data     = method_exists($order, 'get_data') ? $order->get_data() : array('coupon_lines' => '');
        $coupons  = $data['coupon_lines'];
        $products = array();
        $firstTax = !empty($orderItem['line_subtotal_tax']) ? round($orderItem['line_tax'] * 100 / $orderItem['line_total'], 0) : 0;

        if ( !empty($coupons) ) {
            foreach ($coupons as $item) {
                $product = new stdClass;
                $product->code                      = 'coupon_' . $item->get_code();
                $product->currency                  = $productSettings['product_currency'];
                $product->exchangeRate              = 1;
                $product->isDiscount                = true;
                $product->discountPercentage        = 0;
                $product->discountValue             = -1*(floatval($item->get_discount()) + floatval($item->get_discount_tax()));
                $product->discountType              = 1;
                $product->isTaxIncluded             = (bool)$productSettings['included_vat'];
                $product->measuringUnitName         = $productSettings['um'];
                $product->name                      = 'Cupon discount (' . $item->get_name() . ')';
                $product->price                     = 0;
                $product->quantity                  = 0;
                $product->numberOfItems             = 0;
                $product->saveToDb                  = false;
                $product->taxName                   = ''; // TODO (daca este trimisa se primeste mesaj "Cota tva a produsului XXX nu a fost gasita pe server!")
                $product->taxPercentage             = floatval($productSettings['product_vat']);
                if ($product->taxPercentage=='' && (int)$productSettings['isTaxPayer']) {
                    $product->taxPercentage = '9999';
                }
                $product->translatedMeasuringUnit   = '';
                $product->translatedName            = '';
                $product->warehouseName             = $productSettings['warehouse'];

                switch ($product->taxPercentage) {
                    // din woocommerce pe produse
                    case 9998:
                        $product->taxPercentage = floatval($firstTax);
                        break;

                    // din smartbill pe produse
                    case 9999:
                        $product->taxName = '@@@@@';
                        $product->taxPercentage = 0;
                        break;
                }
                $products[] = $product;
            }
        }

        return $products;
    }

    private static function getProductVariationName($productV, $orderItem) {
        return method_exists($productV, 'get_name') ? $productV->get_name() : $orderItem['name'];
        /*
        $variationName = '';

        if ( !empty($orderItem['variation_id']) ) {
            $variation      = wc_get_product($orderItem['variation_id']);
            $variationName  = $variation->get_title();            
        }

        try {
            $wcOrderItemMeta = new WC_Order_Item_Meta($orderItem);
            $itemMeta = $wcOrderItemMeta->display(true, true, '_', ', ');
        } catch (Exception $ex) {}

		$variationName = empty($variationName) ? $orderItem['name'] : $variationName;

        if ( !empty($itemMeta) ) {
            $variationName .= ' - ' . $itemMeta;
        }

        return $variationName;
        */
    }
    private static function getProductVariationSKU($orderItem) {
        $variationName = array();

        if ( !empty($orderItem['variation_id']) ) {
            $product = wc_get_product($orderItem['variation_id']);
        } else {
            $product = wc_get_product($orderItem['product_id']);
        }

        return $product->get_sku();
    }

    private static function createOrderProduct($productV, $orderItem, $quantity, $productSettings) {
        $baseItemPrice = $productV->get_regular_price();
        /*
        // luam detalii despre produs
        $productV = null;
		if ( !empty($orderItem['variation_id']) ) {
			$productV = new WC_Product_Variation($orderItem['variation_id']);
		}
        */

        // alternativa la calculul taxei, still not happy but eh
        $firstTax = !empty($orderItem['line_subtotal_tax']) ? round($orderItem['line_tax'] * 100 / $orderItem['line_total'], 0) : 0;

        /*
        if (!empty($productV)) {
			$baseItemPrice = $productV->get_regular_price();
		} else {
            $baseItemPrice = get_post_meta($orderItem['product_id'], '_regular_price', true);
            if ( empty($baseItemPrice) ) {
                $baseItemPrice = get_post_meta($orderItem['product_id'], '_price', true);
            }
		}
        */

        // convert string (romanian number) to float
        $baseItemPrice = self::_float($baseItemPrice);

        $product = new stdClass;
        $product->code                      = self::getProductVariationSKU($orderItem);
        $product->currency                  = $productSettings['product_currency'];
        $product->exchangeRate              = 1;
        $product->isDiscount                = false;
        $product->isTaxIncluded             = (bool)$productSettings['included_vat'];
        $product->measuringUnitName         = $productSettings['um'];
        $product->measuringUnitName         = trim($product->measuringUnitName)=='' ? 'buc' : $product->measuringUnitName;
        $product->name                      = self::getProductVariationName($productV, $orderItem);
        $product->price                     = $baseItemPrice;
        $product->quantity                  = $quantity;
        $product->saveToDb                  = (bool)$productSettings['saveProductToDb'];
        $product->taxName                   = ''; // TODO (daca este trimisa se primeste mesaj "Cota tva a produsului XXX nu a fost gasita pe server!")
        $product->taxPercentage             = floatval($productSettings['product_vat']);
        if ($product->taxPercentage=='' && (int)$productSettings['isTaxPayer']) {
            $product->taxPercentage = '9999';
        }
        $product->translatedMeasuringUnit   = '';
        $product->translatedName            = '';
        $product->warehouseName             = $productSettings['warehouse'];

        switch ($product->taxPercentage) {
            // din woocommerce pe produse
            case 9998:
                $product->taxPercentage = floatval($firstTax);
                break;

            // din smartbill pe produse
            case 9999:
                $product->taxName = '@@@@@';
                $product->taxPercentage = 0;
                break;
        }

        return $product;
    }

    private static function createOrderProductDiscountFromBasePrice($productV, $orderItem, $quantity, $productSettings)
    {
        $product = null;
        $baseItemPrice = $productV->get_regular_price();
        $thisItemPrice = method_exists($productV, 'get_sale_price') ? $productV->get_sale_price() : $orderItem['subtotal']/$quantity;
        // if ( !(bool)$productSettings['included_vat'] ) {
        //     $thisItemPrice += method_exists($productV, 'get_total') ? $productV->get_total_tax() : $orderItem['subtotal_tax'];
        // }
        // $thisItemPrice/= $quantity;
        /*        
        // luam detalii despre produs
        $productV = null;
        if ( !empty($orderItem['variation_id']) ) {
            $productV = new WC_Product_Variation($orderItem['variation_id']);
        }

        // $baseItemPrice = $productI->regular_price;
        // $thisItemPrice = ($orderItem['line_total']+$orderItem['line_tax'])/$orderItem['qty'];
        $baseItemPrice = get_post_meta($orderItem['product_id'], '_regular_price', true);
        if ( empty($baseItemPrice) ) {
            $baseItemPrice = get_post_meta($orderItem['product_id'], '_price', true);
        }

        // $thisItemPrice = get_post_meta($orderItem['product_id'], '_sale_price', true);

        if ( (bool)$productSettings['included_vat'] ) {
            $thisItemPrice = ($orderItem['line_total'] + $orderItem['line_tax']) / $quantity;
        } else {
            $thisItemPrice = $orderItem['line_total'] / $quantity;
        }

        if ( !empty($productV) ) {
            $baseItemPrice  = $productV->get_regular_price();
        }
        */

        // convert string (romanian number) to float
        $thisItemPrice = self::_float($thisItemPrice);
        $baseItemPrice = self::_float($baseItemPrice);

        $firstTax = !empty($orderItem['line_subtotal_tax']) ? round($orderItem['line_tax'] * 100 / $orderItem['line_total'], 0) : 0;

        if (!empty($baseItemPrice)
         && !empty($thisItemPrice)
         && $baseItemPrice!=$thisItemPrice) {
            $product = new stdClass;
//            $product->comparatie = $baseItemPrice.' / '.$thisItemPrice;
            $product->code                      = self::getProductVariationSKU($orderItem);
            $product->currency                  = $productSettings['product_currency'];
            $product->exchangeRate              = 1;
            $product->isDiscount                = true;
            $product->discountPercentage        = 0;
            $product->discountValue             = $quantity*($thisItemPrice-$baseItemPrice);
            $product->discountType              = 1;
            $product->isTaxIncluded             = (bool)$productSettings['included_vat'];
            $product->measuringUnitName         = 'buc';
            $product->name                      = 'Discount (pret special): '.self::getProductVariationName($productV, $orderItem);
            $product->price                     = 0;
            $product->quantity                  = 1;
            $product->numberOfItems             = 1;
            $product->saveToDb                  = false;
            $product->taxName                   = ''; // TODO (daca este trimisa se primeste mesaj "Cota tva a produsului XXX nu a fost gasita pe server!")
            $product->taxPercentage             = floatval($productSettings['product_vat']);
            if ($product->taxPercentage=='' && (int)$productSettings['isTaxPayer']) {
                $product->taxPercentage = '9999';
            }
            $product->discountTaxValue          = 0;
            if ( '9999' != $product->taxPercentage
              && '9998' != $product->taxPercentage ) {
                $product->discountTaxValue          = $product->discountValue*$product->taxPercentage/100; // TODO: why needed to be calculated on the client side? as long as we have isTaxIncluded :)
            }
            $product->translatedMeasuringUnit   = '';
            $product->translatedName            = '';
            $product->warehouseName             = $productSettings['warehouse'];

            if ($product->isTaxIncluded) {
                $product->discountTaxValue = $product->discountValue-$product->discountValue/(1+$product->taxPercentage/100);
            } else {
                $product->discountValue -= $product->discountTaxValue;
            }

            switch ($product->taxPercentage) {
                // din magento pe produse
                case 9998:
                    $product->taxPercentage = floatval($firstTax);
                    if ($product->isTaxIncluded) {
                        $product->discountTaxValue = $product->discountValue-$product->discountValue/(1+$firstTax/100);
                    } else {
                        $product->discountTaxValue = $product->discountValue*$firstTax/100;
                    }
                    break;

                // din smartbill pe produse
                case 9999:
                    $product->taxName = '@@@@@';
                    $product->taxPercentage = 0;
                    $product->discountTaxValue = 0;
                    break;
            }
        }

        return $product;
    }

    private static function createOrderTransport($code, $name, $quantity, $order, $productSettings)
    {
        $transportTaxDetails = self::getTransportTaxDetails($productSettings);
        $id                  = method_exists($order, 'get_id') ? $order->get_id() : $order->ID;
        $orderShippingTax    = get_post_meta($id, '_order_shipping_tax', true );

        $product = new stdClass;
        $product->code                      = $code;
        $product->currency                  = $productSettings['product_currency'];
        $product->exchangeRate              = 1;
        $product->isDiscount                = false;
        $product->isTaxIncluded             = (bool)$productSettings['shipping_included_vat'];
        $product->measuringUnitName         = 'buc';
        $product->name                      = $name;
        $product->price                     = floatval($order->get_total_shipping()+$orderShippingTax);
        $product->quantity                  = $quantity;
        $product->saveToDb                  = false;
        $product->taxName                   = $transportTaxDetails[0];
        $product->taxPercentage             = floatval($transportTaxDetails[1]);
        $product->translatedMeasuringUnit   = '';
        $product->translatedName            = '';
        $product->warehouseName             = $productSettings['warehouse'];
        $product->isService                 = true;

        return $product;
    }

    private static function getTransportTaxDetails($productSettings)
    {
        $details = array('', null);

        $taxes = self::getTransportTaxes($productSettings['companySettings']);
        if (!empty($taxes[$productSettings['shipping_vat']])) {
            $details = $taxes[$productSettings['shipping_vat']];

            if (is_null($details[1]) || $details[1]==='') {
                $details[1] = '9999';
            }
        }

        return $details;
    }

    private static function getTransportTaxes($companySettings)
    {
        $taxes = array();

        if (!empty($companySettings)) {
            if (!empty($companySettings['taxes'])
             && is_array($companySettings['taxes'])) {
                foreach ($companySettings['taxes'] as $value) {
                    $taxes[$value['id']] = array($value['name'], $value['percentage']);
                }
            }
        }

        return $taxes;
    }

    private static function getOrderOtherTaxes($order, $productSettings) {
        $otherTaxes = array();

        if ( !empty($productSettings['extraTaxes']) ) {
            $taxes = $order->get_taxes();
            if ( !empty($taxes) ) {
                foreach ($taxes as $tax) {
                    if ( !in_array($tax['rate_id'], $productSettings['extraTaxes']) )    return;

                    $product = new stdClass;
                    $product->code                      = $tax['name'];
                    $product->currency                  = $productSettings['product_currency'];
                    $product->exchangeRate              = 1;
                    $product->isDiscount                = false;
                    $product->isTaxIncluded             = (bool)$productSettings['included_vat'];
                    $product->measuringUnitName         = 'buc';
                    $product->name                      = $tax['label'];
                    $product->price                     = floatval($tax['compound']+$tax['tax_amount']+$tax['shipping_tax_amount']);
                    $product->quantity                  = 1;
                    $product->saveToDb                  = false;
                    $product->taxName                   = $tax['label'];
                    $product->taxPercentage             = floatval($productSettings['product_vat']);
                    if ($product->taxPercentage=='' && (int)$productSettings['isTaxPayer']) {
                        $product->taxPercentage = '9999';
                    }
                    $product->translatedMeasuringUnit   = '';
                    $product->translatedName            = '';
                    $product->warehouseName             = $productSettings['warehouse'];
                    $product->isService                 = true;

                    $otherTaxes[] = $product;
                }
            }
        }

        return $otherTaxes;
    }


    private static function _float($float){
        if ( self::_isLocalized($float) ) {
            $find = get_option( 'woocommerce_price_thousand_sep' );
            $float = str_replace($find , "", $float);
            $find = get_option( 'woocommerce_price_decimal_sep' );
            $float = str_replace($find , ".", $float);
        }

        return floatval($float);
    }
    private static function _isLocalized($number) {
        $find = get_option( 'woocommerce_price_decimal_sep' );

        return false !== strpos($number, $find);
    }
}
