<?php

/*
 * Plugin Name: PayPal Express Checkout Payment Gateway for WooCommerce
 * Plugin URI: https://www.webtoffee.com/product/paypal-express-checkout-gateway-for-woocommerce/
 * Description: Simplify your Store's checkout Process using PayPal Express Checkout.
 * Author: WebToffee
 * Author URI: http://www.webtoffee.com/
 * Version: 1.2.6
 * WC tested up to: 4.3.1
 * Text Domain: eh-paypal-express
 */
if (!defined('ABSPATH')) {
    exit;
}
if (!defined('EH_PAYPAL_MAIN_PATH')) {
    define('EH_PAYPAL_MAIN_PATH', plugin_dir_path(__FILE__));
}
if (!defined('EH_PAYPAL_MAIN_URL')) {
    define('EH_PAYPAL_MAIN_URL', plugin_dir_url(__FILE__));
}
if (!defined('EH_PAYPAL_VERSION')) {
    define('EH_PAYPAL_VERSION', '1.2.6');
}


require_once(ABSPATH . "wp-admin/includes/plugin.php");


if(is_plugin_active('express-checkout-paypal-payment-gateway-for-woocommerce/express-checkout-paypal-payment-gateway-for-woocommerce.php'))
{   
    deactivate_plugins(plugin_basename(__FILE__));

    add_action('admin_notices', 'eh_wc_admin_notices', 99);
    
    function eh_wc_admin_notices() {
        is_admin() && add_filter('gettext', function($translated_text, $untranslated_text, $domain) {
                    $old = array(
                        "Plugin <strong>activated</strong>.",
                        "Selected plugins <strong>activated</strong>.",
                        "Plugin activated.",
                        "Selected plugins activated."
                    );
                   
                    $error_text = "BASIC Version of this Plugin Installed. Please uninstall the BASIC Version before activating PREMIUM.";
                    
                    $new = "<span style='color:red'>" . $error_text . "</span>";
                    if (in_array($untranslated_text, $old, true)) {
                        $translated_text = $new;
                    }
                    return $translated_text;
                }, 99, 3);
    }

    return;
} else {
    
    add_action('plugins_loaded', 'eh_paypal_check', 99);

    function eh_paypal_check(){ 

        if( class_exists( 'WooCommerce' )){

            register_activation_hook(__FILE__, 'eh_paypal_express_init_log');
            include(EH_PAYPAL_MAIN_PATH . "includes/log.php");
            
            add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'eh_paypal_express_plugin_action_links');

            function eh_paypal_express_plugin_action_links($links) {
                $setting_link = admin_url('admin.php?page=wc-settings&tab=checkout&section=eh_paypal_express');
                $plugin_links = array(
                    '<a href="' . $setting_link . '">' . __('Settings', 'eh-paypal-express') . '</a>',
                    '<a href="https://www.webtoffee.com/category/documentation/paypal-express-checkout-payment-gateway-for-woocommerce/" target="_blank">' . __('Documentation', 'eh-paypal-express') . '</a>',
                    '<a href="https://www.webtoffee.com/support/" target="_blank">' . __('Support', 'eh-paypal-express') . '</a>'
                );
                return array_merge($plugin_links, $links);
            }

            
        } else {
            add_action('admin_notices', 'eh_paypal_express_wc_admin_notices', 99);
            deactivate_plugins(plugin_basename(__FILE__));
        }
    }

    function eh_paypal_express_wc_admin_notices() {
        is_admin() && add_filter('gettext', function($translated_text, $untranslated_text, $domain) {
                    $old = array(
                        "Plugin <strong>deactivated</strong>.",
                        "Selected plugins <strong>deactivated</strong>.",
                        "Plugin deactivated.",
                        "Selected plugins deactivated.",
                        "Plugin <strong>activated</strong>.",
                        "Selected plugins <strong>activated</strong>.",
                        "Plugin activated.",
                        "Selected plugins activated."
                    );
                    $new = "<span style='color:red'>PayPal Express Payment for WooCommerce (WebToffee)-</span> Plugin Needs WooCommerce to Work.";
                    if (in_array($untranslated_text, $old, true)) {
                        $translated_text = $new;
                    }
                    return $translated_text;
                }, 99, 3);
    }

    function eh_paypal_express_init_log() {
        if (WC()->version >= '2.7.0') {
            $log = wc_get_logger();
            $init_msg = Eh_PayPal_Log::init_log();
            $context = array('source' => 'eh_paypal_express_log');
            $log->log("debug", $init_msg, $context);
        } else {
            $log = new WC_Logger();
            $init_msg = Eh_PayPal_Log::init_log();
            $log->add("eh_paypal_express_log", $init_msg);
        }
    }
    function eh_paypal_express_run() {
        include_once ( 'includes/wf_api_manager/wf-api-manager-config.php' );
        static $eh_paypal_plugin;
        if (!isset($eh_paypal_plugin)) {
            require_once (EH_PAYPAL_MAIN_PATH . "includes/class-eh-paypal-init-handler.php");
            $eh_paypal_plugin = new Eh_Paypal_Express_Handlers();
        }
        return $eh_paypal_plugin;
    }

    eh_paypal_express_run()->express_run();

    add_action('init','eh_load_plugin_textdomain');
    function eh_load_plugin_textdomain() {
        load_plugin_textdomain('eh-paypal-express', false, dirname(plugin_basename(__FILE__)) . '/lang/');
    }

    /*
    *   When Skip Review option disabled, Prevent WC order creation and divert to our order creation process for prevent creating twise order 
    *
    */

    add_action('woocommerce_checkout_process', 'get_order_processed');
    function get_order_processed(){
    
        if(isset(WC()->session->eh_pe_checkout['order_id']) && isset(WC()->session->eh_pe_set['skip_review_disabled']) && (WC()->session->eh_pe_set['skip_review_disabled'] === 'true') ){
            $order_id = WC()->session->eh_pe_checkout['order_id'];
            $order = wc_get_order($order_id);
           
            $eh_paypal_express = new Eh_PayPal_Express_Payment();
            //settings recurring cart item for subscription product is skip review disabled
            if(isset(WC()->session->eh_recurring_carts)){
                WC()->cart->recurring_carts = WC()->session->eh_recurring_carts;
                //creates user account for guest users
                $user_exits = get_post_meta($order_id, '_customer_user', true);
                if($user_exits == 0){
                    $eh_paypal_express->eh_subscription_payment_user_creation($order_id);
                }
                do_action('woocommerce_checkout_order_processed', $order_id, '', $order);
            }

            $eh_paypal_express->process_payment($order_id);
            
            unset(WC()->session->eh_pe_set);
    
        }
    }
    

}
