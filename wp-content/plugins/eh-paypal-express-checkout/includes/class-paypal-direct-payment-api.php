<?php

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Class that handles Paypal Direct Payment Gateway.
 *
 * @extends WC_Payment_Gateway
 * @since 1.2.5
 */
class Eh_Paypal_Direct_Payment_API extends WC_Payment_Gateway {

    public function __construct() {
		$this->id = 'eh_paypal_direct_checkout';
        $this->method_title = __('Paypal Credit Card Checkout', 'eh-paypal-express');
        $this->method_description = sprintf(__("Use the Paypal Credit Card Checkout to accept direct credit card payments on your website from buyers who do not have a PayPal account. Your Paypal account credentials should be entered <a href='%s' target='blank'>here</a>.", 'eh-paypal-express'), admin_url( 'admin.php?page=wc-settings&tab=checkout&section=eh_paypal_express' ));
        $this->has_fields = true;
        $this->supports = array(
            'products', 'refunds'
        );
        $this->init_form_fields();
        $this->init_settings();
        $this->enabled = $this->get_option('enabled');
        $this->title = $this->get_option('title');
		$this->description = $this->get_option('description');
		
		$this->paypal_options = get_option('woocommerce_eh_paypal_express_settings');
		$this->environment = isset($this->paypal_options['environment']) ? $this->paypal_options['environment'] : '';                
        $this->sandbox_username = isset($this->paypal_options['sandbox_username']) ? $this->paypal_options['sandbox_username'] : '';
        $this->sandbox_password = isset($this->paypal_options['sandbox_password']) ? $this->paypal_options['sandbox_password'] : '';
        $this->sandbox_signature = isset($this->paypal_options['sandbox_signature']) ? $this->paypal_options['sandbox_signature'] : '';
        $this->live_username = isset($this->paypal_options['live_username']) ? $this->paypal_options['live_username'] : '';
        $this->live_password = isset($this->paypal_options['live_password']) ? $this->paypal_options['live_password'] : '';
		$this->live_signature = isset($this->paypal_options['live_signature']) ? $this->paypal_options['live_signature'] : '';
		
		if ($this->environment === 'sandbox') {
            $this->api_username = $this->sandbox_username;
            $this->api_password = $this->sandbox_password;
            $this->api_signature = $this->sandbox_signature;
            $this->nvp_url = "https://api-3t.sandbox.paypal.com/nvp";
            $this->scr_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        } else {
            $this->api_username = $this->live_username;
            $this->api_password = $this->live_password;
            $this->api_signature = $this->live_signature;
            $this->nvp_url = "https://api-3t.paypal.com/nvp";
            $this->scr_url = "https://www.paypal.com/cgi-bin/webscr";
		}
		$this->order_button_text = $this->get_option( 'order_button');
		$this->payment_action = isset($this->paypal_options['payment_action']) ? $this->paypal_options['payment_action'] : 'Sale';
		$this->icon = apply_filters( 'eh_direct_checkout_icon', EH_PAYPAL_MAIN_URL . 'assets/img/credit-cards.png' );

	add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( &$this, 'process_admin_options' ) );
	add_action('wp_enqueue_scripts', array($this, 'payment_scripts'));
    }

	/**
	 * Initialize form fields in settings page.
     * @since 1.2.5
	 */
    public function init_form_fields() {
        $this->form_fields = array(
			'enabled'         => array(
                'title'       => __('Paypal Credit Card Checkout', 'eh-paypal-express'),
                'label'       => __('Enable', 'eh-paypal-express'),
                'type'        => 'checkbox',
                'description' => __('Upgrade your business account to a pro account before configuring the gateway.', 'eh-paypal-express'),
                'default'     => 'no',
                'desc_tip'    => __('This option will enable PayPal direct checkout payment method in checkout page.', 'eh-paypal-express'),
            ),
            'title'           => array(
                'title'       => __('Title', 'eh-paypal-express'),
                'type'        => 'text',
                'description' => __('Enter the title of the checkout which the user can see.', 'eh-paypal-express'),
                'default'     => __('Paypal Credit Card Checkout', 'eh-paypal-express'),
                'desc_tip'    => true,
            ),
            'description'      => array(
                'title'        => __('Regular Description', 'eh-paypal-express'),
                'type'         => 'textarea',
                'css'          => 'width:25em',
                'description'  => __('Description which the user sees during checkout.', 'eh-paypal-express'),
                'default'      => __('Allows Credit Card Payments via Paypal Credit Card Checkout gateway', 'eh-paypal-express'),
                'desc_tip'     => true
			),
			'order_button'     => array(
				'title'        => __('Order Button Text', 'eh-paypal-express'),
				'type'         => 'text',
				'description'  => __('You can key in the text of your choice that will appear at the checkout page as the order button text.', 'eh-paypal-express'),
				'default'      => __('Pay via Paypal Credit Card Checkout', 'eh-paypal-express'),
				'desc_tip'     => true
			)
		);   
	}

	/**
	 * Loads scripts.
     * @since 1.2.5
	 */
	public function payment_scripts(){

		wp_register_style('eh-paypal-style', EH_PAYPAL_MAIN_URL . 'assets/css/eh-paypal-style.css',array(),EH_PAYPAL_VERSION);
        wp_enqueue_style( 'eh-paypal-style' );
	}

	/**
	 * Checks if gateway should be available to use.
     * @since 1.2.5
	 */
	public function is_available() {
        if ('yes' === $this->enabled) {
            if (!$this->environment && is_checkout()) {
                return false;
            }
            if ('sandbox' === $this->environment) {
                if (!$this->sandbox_username || !$this->sandbox_password || !$this->sandbox_signature) {
                    return false;
                }
            } else {
                if (!$this->live_username || !$this->live_password || !$this->live_signature) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
	 * Validates the payment fields.
     * @since 1.2.5
	 */
    public function validate_fields() {

		if ( ! $this->eh_is_valid_card_number( $_POST[ 'eh_billing_credircard_number' ] ) ) {
			wc_add_notice( __( 'Credit card number you entered is invalid.', 'eh-paypal-express' ), 'error' );
		}
		if ( ! $this->eh_is_valid_card_type( $_POST[ 'eh_billing_cardtype' ] ) ) {
			wc_add_notice( __( 'Card type is not valid.', 'eh-paypal-express' ), 'error' );
		}
		if ( ! $this->eh_is_valid_expiry( $_POST[ 'eh_billing_expdatemonth' ], $_POST[ 'eh_billing_expdateyear' ] ) ) {
			wc_add_notice( __( 'Card expiration date is not valid.', 'eh-paypal-express' ), 'error' );
		}
		if ( ! $this->eh_is_valid_cvv_number( $_POST[ 'eh_billing_ccvnumber' ] ) ) {
			wc_add_notice( __( 'Card verification number (CVV) is not valid. You can find this number on your credit card.', 'eh-paypal-express' ), 'error' );
		}
    }

	/**
	 * Validates the credit card number entered
     * @since 1.2.5
	 */
	public function eh_is_valid_card_number($card_number){

		if ( ! is_numeric( $card_number ) )
	    return false;

		$strlen	 = strlen( $card_number );
		if ( $strlen < 13 )
		return false;

		return true;
	}

	/**
	 * Validates the credit card type selected
     * @since 1.2.5
	 */
	public function eh_is_valid_card_type($card_type){

		$allowed_cards = array("Visa","MasterCard","Discover","Amex");
		if ( ! in_array( $card_type , $allowed_cards) ){
			return false;
		}
		return $card_type;
	}

	/**
	 * Validates the expiry month and year selected
     * @since 1.2.5
	 */
	public function eh_is_valid_expiry($month,$year){

		$now		 = time();
		$thisYear	 = (int) date( 'Y', $now );
		$thisMonth	 = (int) date( 'm', $now );

		if ( is_numeric( $year ) && is_numeric( $month ) ) {
			$thisDate	 = mktime( 0, 0, 0, $thisMonth, 1, $thisYear );
			$expireDate	 = mktime( 0, 0, 0, $month, 1, $year );

			return $thisDate <= $expireDate;
		}

		return false;
	}

	/**
	 * Validates the card verification number entered
     * @since 1.2.5
	 */
	public function eh_is_valid_cvv_number($cvv_number){

		$length = strlen( $cvv_number );
	    return is_numeric( $cvv_number ) AND $length > 2 AND $length < 5;
	}
	

	/**
	 * Renders the card fields on the checkout page.
     * @since 1.2.5
	 */
    public function payment_fields() {

		echo '<div class="status-box">';

        if ($this->description) {
            echo apply_filters('eh_paypal_direct_checkout_desc', wpautop(wp_kses_post("<span>" . $this->description . "</span>")));
        }
        echo "</div>";
	
	    ?>
		<fieldset id="eh-<?php echo esc_attr( $this->id ); ?>-cc-form" class="eh-paypal-credit-card-form" style="background:transparent;">
			
			<div class="form-row form-row-wide">
				<label for="eh-paypal-card-element"><?php esc_html_e( 'Card Number', 'eh-paypal-express' ); ?> <span class="required">*</span></label>
				<div class="paypal-card-group">
					<div id="eh-paypal-card-element" class="eh-paypal-elements-field">
					
						<input class="input-text eh_billing_credircard_number" type="text"  name="eh_billing_credircard_number" placeholder="<?php echo __( 'Card Number', 'eh-paypal-express' ); ?>" />
					</div>
				</div>
			</div>

			<div class="form-row form-row-wide">
				<label for="eh-paypal-type-element"><?php esc_html_e( 'Card Type', 'eh-paypal-express' ); ?> <span class="required">*</span></label>
				<div class="paypal-card-group">
					<div id="eh-paypal-type-element" class="eh-paypal-elements-field">
					
					<select name="eh_billing_cardtype" id="eh_billing_cardtype" >
						<option value="Visa" selected="selected">Visa</option>
						<option value="MasterCard">MasterCard</option>
						<option value="Discover">Discover</option>
						<option value="Amex">American Express</option>
					</select>
					</div>
				</div>
			</div>

			<div class="form-row form-row-first">
				<label for="eh-paypal-exp-element"><?php esc_html_e( 'Expiry Date', 'eh-paypal-express' ); ?> <span class="required">*</span></label>

				<div id="eh-paypal-exp-element" class="eh-paypal-elements-field">
				<select name="eh_billing_expdatemonth" id="eh_billing_expdatemonth">
					<option value=1>01</option>
					<option value=2>02</option>
					<option value=3>03</option>
					<option value=4>04</option>
					<option value=5>05</option>
					<option value=6>06</option>
					<option value=7>07</option>
					<option value=8>08</option>
					<option value=9>09</option>
					<option value=10>10</option>
					<option value=11>11</option>
					<option value=12>12</option>
				</select>
				<select name="eh_billing_expdateyear" id="eh_billing_expdateyear">
					<?php
					$today				 = (int) date( 'Y', time() );
					for ( $i = 0; $i < 12; $i ++ ) {
						?>
						<option value="<?php echo $today; ?>"><?php echo $today; ?></option>
						<?php
						$today ++;
					}
					?>
				</select>
				</div>
			</div>

			<div class="form-row form-row-last">
				<label for="eh-paypal-cvc-element"><?php esc_html_e( 'Card Code (CVC)','eh-paypal-express' ); ?> <span class="required">*</span></label>
				<div id="eh-paypal-cvc-element" class="eh-paypal-elements-field">
					<input class="input-text eh_billing_ccvnumber" type="text" name="eh_billing_ccvnumber" placeholder="CVV" />
				</div>
			</div>

			<div class="clear"></div>

			
		</fieldset>
	    <?php
	 
    }
    /**
	 * Process Payment.
     * @since 1.2.5
	 */
    public function process_payment( $order_id ) {
      
        $order = wc_get_order($order_id);
		$request_process = new Eh_PE_Process_Request();
		$request_params = $this->new_request()->direct_payment_params
			(
			array
				(
					'method'           => 'DoDirectPayment',
					'payment_action'   => ('sale' === $this->payment_action) ? 'Sale' : 'Authorization',
					'ipaddress'	       => $_SERVER[ 'REMOTE_ADDR' ],
					'creditcardtype'   => $_POST[ 'eh_billing_cardtype' ],
					'cardnumber'	   => $_POST[ 'eh_billing_credircard_number' ],
					'cvvvalue'		   => $_POST[ 'eh_billing_ccvnumber' ],
					'expirydate'	   => sprintf( '%s%s', $_POST[ 'eh_billing_expdatemonth' ], $_POST[ 'eh_billing_expdateyear' ] ),

				),$order
			);
		$response = $request_process->process_request($request_params, $this->nvp_url);
		Eh_PayPal_Log::log_update($response,'Response on DoDirect Payment Request');

		if ($response['ACK'] == 'Success' || $response['ACK'] == 'SuccessWithWarning') {

			$update = array
			(
				'status' => ('sale' === $this->payment_action) ? 'Sale' : 'Authorization',
				'trans_id' => $response['TRANSACTIONID']
			);

			if($update['status'] === 'Sale'){
				$order->add_order_note(__('Payment Status : ' . $response['ACK'] .'.<br>Transaction ID : ' . $response['TRANSACTIONID'], 'eh-paypal-express'));
				$order->payment_complete($response['TRANSACTIONID']);
				
			}else{
				$order->update_status('on-hold');
				$order->add_order_note(__('Payment Status : ' . __('Pending','eh-paypal-express') . '<br>[ ' . '.<br>Transaction ID : ' . $response['TRANSACTIONID'] . '.<br>Reason : ' .__('The payment is pending because it has been authorized but not settled. You must capture the funds first.','eh-paypal-express'), 'eh-paypal-express'));
			}
			add_post_meta($order_id, '_eh_pe_details', $update);

			
			WC()->cart->empty_cart();
			return array(
				'result'	 => 'success',
				'redirect'	 => $this->get_return_url( $order )
				);

		} else {
			if(isset($response['L_LONGMESSAGE0'])){
				wc_add_notice(__($response['L_ERRORCODE0'].' error - '.$response['L_LONGMESSAGE0'], 'eh-paypal-express'), 'error');
			}else{
				wc_add_notice(__('An error occured.Please refresh and try again', 'eh-paypal-express'), 'error');
			}
		}
	
	}
	
	public function new_request() {
        return new Eh_PE_Request_Built($this->api_username, $this->api_password, $this->api_signature);
    }
}
