jQuery(function($) {
    window.paypalCheckoutReady = function() {
		paypal.checkout.setup(
			context_obj.id,
			{
				environment: context_obj.environment,
				button: 'eh_paypal_express_inine_button',
				locale: context_obj.locale,
			}
		);
	}

	$( document ).on( 'click', '#eh_paypal_express_inine_button', function( event ) {
		event.preventDefault();
		paypal.checkout.initXO();
		paypal.checkout.startFlow( context_obj.start_inline );
	} );
});
