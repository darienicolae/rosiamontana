<?php
/**
 * Bulk Table Editor
 *
 * @package BulkTableEditor
 *
 * Plugin Name: Bulk Table Editor for WooCommerce
 * Plugin URI: https://woocommerce.com/products/bulk-table-editor-for-woocommerce/
 * Description: Enables bulk updating of product prices, stock, sale values, sku, tags and other.
 * Version: 2.1.10
 * Author: Consortia
 * Author URI: http://www.consortia.no/en/
 * Text Domain: woo-bulk-table-editor
 * Domain Path: /languages
 *
 * Tested up to: 5.3.2
 * Woo: 4703437:2514ffe2d12bf99178aacbc3755dc518
 * WC requires at least: 3.3
 * WC tested up to: 4.7.0
 *
 * Copyright: © 2018-2020 Consortia AS.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

register_activation_hook( __FILE__, 'wbte_install' );
register_deactivation_hook( __FILE__, 'wbte_deactivate' );

add_action( 'init', 'wbte_init' );
add_action( 'admin_init', 'wbte_store_register_settings' );
add_action( 'admin_menu', 'wbte_woocommerce_submenu_page', 99 );

/**
 * Install
 *
 * @return void
 */
function wbte_install() {

	global $wp_version;

	if ( version_compare( $wp_version, '4.1', '<' ) ) {
		wp_die( 'This plugin require WordPress 4.1 or higher.' );
	}

	$wbte_options_arr = array(
		'wbte_posts_per_page'             => '150',
		'wbte_product_cat'                => '',
		'wbte_date_format'                => 'mm/dd/yy',
		'wbte_sale_no_date'               => false,
		'wbte_no_stock_management'        => false,
		'wbte_error_sale_no_date'         => false,
		'wbte_autosort'                   => '',
		'wbte_no_autofocus'               => '',
		'wbte_custom_price_1'             => '',
		'wbte_custom_price_1_header'      => '',
		'wbte_custom_price_1_visible'     => false,
		'wbte_custom_price_1_normal_calc' => false,
		'wbte_date_format_autofocus'      => false,
		'wbte_use_sku_main_page'          => '',
	);

	update_option( 'wbte_options', $wbte_options_arr );
	
	set_transient( 'wbte-admin-notice-activated', true );

}

/**
 * On activation notice
 */
function wbte_admin_activation_notice_success() {

	$allowed_tags = array(
		'a' => array(
			'class'  => array(),
			'href'   => array(),
			'target' => array(),
			'title'  => array(),
		),
	);
	/* translators: %s: url for documentation */
	$read_doc = __( 'Read the <a href="%1$s" target="_blank"> extension documentation </a> for more information.', 'woo-bulk-table-editor' );
	$out_str  = sprintf( $read_doc, esc_url( 'https://docs.woocommerce.com/document/woocommerce-bulk-table-editor' ) );

	if ( get_transient( 'wbte-admin-notice-activated' ) ) {
		?>
		<div class="updated woocommerce-message">
			<a class="woocommerce-message-close notice-dismiss" href="<?php echo esc_url( wp_nonce_url( add_query_arg( 'wc-hide-notice', 'wbte_admin_activation_notice_success' ), 'woocommerce_hide_notices_nonce', '_wc_notice_nonce' ) ); ?>"><?php esc_html_e( 'Dismiss', 'woo-bulk-table-editor' ); ?></a>
			<p>
				<?php esc_html_e( 'Thank you for installing Bulk Table Editor for WooCommerce. You can now start to bulk update products. ', 'woo-bulk-table-editor' ); ?>
				<?php echo wp_kses( $out_str, $allowed_tags ); ?>
			</p>
			<p class="submit">
				<a href="<?php echo esc_url( admin_url( 'edit.php?post_type=product&page=wbte-products' ) ); ?>" class="button-primary"><?php esc_html_e( 'Start bulk editing products', 'woo-bulk-table-editor' ); ?></a>
				<a href="<?php echo esc_url( admin_url( 'admin.php?page=wc-settings&tab=wbte' ) ); ?>" class="button-secondary"><?php esc_html_e( 'Settings', 'woo-bulk-table-editor' ); ?></a>
			</p>
		</div>

		<?php
		delete_transient( 'wbte-admin-notice-activated' );
	}
}
add_action( 'admin_notices', 'wbte_admin_activation_notice_success' );

/**
 * Deactivate
 *
 * @return void
 */
function wbte_deactivate() {

}

/**
 * Init wbte
 * Add language support
 */
function wbte_init() {

	load_plugin_textdomain( 'woo-bulk-table-editor', false, basename( dirname( __FILE__ ) ) . '/languages' );
	include_once dirname( __FILE__ ) . '/includes/class-wbte-settings.php';

}

/**
 * Create submenu in products
 */
function wbte_woocommerce_submenu_page() {

	add_submenu_page(
		'edit.php?post_type=product',
		__( 'Bulk Table Editor', 'woo-bulk-table-editor' ),
		__( 'Bulk Table Editor', 'woo-bulk-table-editor' ),
		'manage_woocommerce',
		'wbte-products',
		'wbte_products_page'
	);

}


/**
 * Get Bulk Table Editor main page
 */
function wbte_products_page() {

	if ( class_exists( 'WooCommerce' ) ) {
		include_once dirname( __FILE__ ) . '/wbte-products.php';
	}

}


/**
 * Add admin select category
 *
 * @param var $value object.
 */
function wbte_add_admin_field_select( $value ) {

	settings_fields( 'wbte-settings-group' );
	$wbte_options = get_option( 'wbte_options' );

	include_once __DIR__ . '/includes/class-wbte-functions.php';
	$wbte_functions = new WbteFunctions();

	?>
		<tr valign="top">
			<th scope="row">
				<label for="<?php echo esc_attr( $value['id'] ); ?>"><?php echo esc_attr( $value['title'] ); ?></label>
				<span class="woocommerce-help-tip" data-tip="<?php echo esc_attr( $value['desc_tip'] ); ?>"></span>
			</th>
			<td>
				<?php $wbte_functions->wbte_settings_get_categories_select( $wbte_options ); ?>
			</td>
		</tr>
	<?php
}
add_action( 'woocommerce_admin_field_button', 'wbte_add_admin_field_select' );

/**
 * Register settings
 */
function wbte_store_register_settings() {

	register_setting(
		'wbte-settings-group',
		'wbte_options',
		'wbte_sanitize_options'
	);

}

/**
 * Sanitize values
 *
 * @param var $input object.
 */
function wbte_sanitize_options( $input ) {

	$input['wbte_posts_per_page']             = ( ! empty( $input['wbte_posts_per_page'] ) ) ? sanitize_text_field( $input['wbte_posts_per_page'] ) : '';
	$input['wbte_date_format']                = ( ! empty( $input['wbte_date_format'] ) ) ? sanitize_text_field( $input['wbte_date_format'] ) : '';
	$input['wbte_sale_no_date']               = ( ! empty( $input['wbte_sale_no_date'] ) ) ? sanitize_text_field( $input['wbte_sale_no_date'] ) : '';
	$input['wbte_no_stock_management']        = ( ! empty( $input['wbte_no_stock_management'] ) ) ? sanitize_text_field( $input['wbte_no_stock_management'] ) : '';
	$input['wbte_no_autofocus']               = ( ! empty( $input['wbte_no_autofocus'] ) ) ? sanitize_text_field( $input['wbte_no_autofocus'] ) : '';
	$input['wbte_autosort']                   = ( ! empty( $input['wbte_autosort'] ) ) ? sanitize_text_field( $input['wbte_autosort'] ) : '';
	$input['wbte_custom_price_1']             = ( ! empty( $input['wbte_custom_price_1'] ) ) ? sanitize_text_field( $input['wbte_custom_price_1'] ) : '';
	$input['wbte_custom_price_1_header']      = ( ! empty( $input['wbte_custom_price_1_header'] ) ) ? sanitize_text_field( $input['wbte_custom_price_1_header'] ) : '';
	$input['wbte_custom_price_1_visible'] 	  = ( ! empty( $input['wbte_custom_price_1_visible'] ) ) ? sanitize_text_field( $input['wbte_custom_price_1_visible'] ) : '';
	$input['wbte_custom_price_1_normal_calc'] = ( ! empty( $input['wbte_custom_price_1_normal_calc'] ) ) ? sanitize_text_field( $input['wbte_custom_price_1_normal_calc'] ) : '';
	$input['wbte_date_format_autofocus']      = ( ! empty( $input['wbte_date_format_autofocus'] ) ) ? sanitize_text_field( $input['wbte_date_format_autofocus'] ) : '';
	
	return $input;
}

/**
 * Add scripts and style
 *
 * @param var $hook object.
 */
function wbte_header_scripts( $hook ) {

	if ( 'product_page_wbte-products' !== $hook ) {
		return;
	}

	wp_register_script( 'wbte_jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array( 'jquery' ), '1.12.1', true );
	wp_enqueue_script( 'wbte_jquery-ui' );

	wp_register_script( 'wbte_tablecalc', plugins_url( '/js/jquery.tablecalc.min.js', __FILE__ ), array( 'jquery' ), '2.0.8', false );
	wp_enqueue_script( 'wbte_tablecalc' );

	wp_register_script( 'wbte_scripts', plugins_url( '/js/wbte-scripts.js', __FILE__ ), array( 'jquery' ), '1.3.1', true );
	wp_enqueue_script( 'wbte_scripts' );
		
	wp_register_script( 'lozad', 'https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js', array(), '1', false );
	wp_enqueue_script( 'lozad' );

	wp_register_style( 'woocommerce_admin', plugins_url( '../plugins/woocommerce/assets/css/admin.css' ), array(), '1.12.1' );
	wp_enqueue_style( 'woocommerce_admin' );

	wp_register_style( 'fonta', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css', array(), '5.2.0' );
	wp_enqueue_style( 'fonta' );

	wp_register_style( 'jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', array(), '1.12.1', 'all' );
	wp_enqueue_style( 'jquery-ui' );

	wp_register_style( 'wbte_css', plugins_url( '/css/wbte.css', __FILE__ ), array(), '1.3.0' );
	wp_enqueue_style( 'wbte_css' );

}
add_action( 'admin_enqueue_scripts', 'wbte_header_scripts' );


/**
 * Update product (ajax)
 */
function wbte_update_table_rows_ajax_handler() {
	
	check_ajax_referer( 'footer_id', 'nonce' );
	$data = wp_unslash( $_POST );

	if ( apply_filters( 'wbte_update_table_rows_data_async', true ) ) {
		WC()->queue()->add( 'wbte_update_table_rows_data_async', $data, 'wbte_work_queue' );
	} else {
		wbte_update_table_rows_data( $data );
	}

	die();
}
add_action( 'wp_ajax_wbte_update_table_rows_data', 'wbte_update_table_rows_ajax_handler' );
add_action( 'wp_ajax_nopriv_wbte_update_table_rows_data', 'wbte_update_table_rows_ajax_handler' );

/**
 * Update product (ajax)
 */
function wbte_update_table_rows_ext_ajax_handler() {
	
	check_ajax_referer( 'footer_id_ext', 'nonce' );
	$data = wp_unslash( $_POST );

	if ( apply_filters( 'wbte_update_table_rows_ext_data_async', true ) ) {
		WC()->queue()->add( 'wbte_update_table_rows_ext_data_async', $data, 'wbte_work_queue' );
	} else {
		wbte_update_table_rows_ext_data( $data );
	}

	die();
}
add_action( 'wp_ajax_wbte_update_table_rows_ext_data', 'wbte_update_table_rows_ext_ajax_handler' );
add_action( 'wp_ajax_nopriv_wbte_update_table_rows_ext_data', 'wbte_update_table_rows_ext_ajax_handler' );

/**
 * Update products
 * 
 * @param var $data data.
 */
function wbte_update_table_rows_data( $data ) {

	include_once dirname( __FILE__ ) . '/includes/class-wbte-functions.php';
	$wbte = new WbteFunctions();
	
	foreach ( $data['rows'] as $row ) {
		$wbte->wbte_update_product( $row );
	}
	
}
add_action( 'wbte_update_table_rows_data_async', 'wbte_update_table_rows_data', 5 );

/**
 * Update products
 * 
 * @param var $data data.
 */
function wbte_update_table_rows_ext_data( $data ) {

	include_once dirname( __FILE__ ) . '/includes/class-wbte-functions.php';
	$wbte = new WbteFunctions();
	
	foreach ( $data['rows'] as $row ) {
		$wbte->wbte_update_product_ext( $row );
	}	
	
}
add_action( 'wbte_update_table_rows_ext_data_async', 'wbte_update_table_rows_ext_data', 5 );


/**
 * Delete product (ajax)
 */
function wbte_delete_table_rows_ajax_handler() {
	
	check_ajax_referer( 'footer_id', 'nonce' );
	$data = wp_unslash( $_POST );

	if ( apply_filters( 'wbte_delete_table_rows_data_async', true ) ) {
		WC()->queue()->add( 'wbte_delete_table_rows_data_async', $data, 'wbte_work_queue' );
	} else {
		wbte_delete_table_rows_data( $data );
	}

	die();
}
add_action( 'wp_ajax_wbte_delete_table_rows_data', 'wbte_delete_table_rows_ajax_handler' );
add_action( 'wp_ajax_nopriv_wbte_delete_table_rows_data', 'wbte_delete_table_rows_ajax_handler' );


/**
 * Delete products
 * 
 * @param var $data data.
 */
function wbte_delete_table_rows_data( $data ) {

	include_once dirname( __FILE__ ) . '/includes/class-wbte-functions.php';
	$wbte = new WbteFunctions();

	foreach ( $data['rows'] as $row ) {
		$wbte->wbte_move_to_trash( $row );
	}

}
add_action( 'wbte_delete_table_rows_data_async', 'wbte_delete_table_rows_data' );


/**
 * Allow infinity sale message
 */
function wbte_infinity_sale_info() {

	$allowed_tags = array(
		'a' => array(
			'class'  => array(),
			'href'   => array(),
			'target' => array(),
			'title'  => array(),
		),
	);
	/* translators: %s: url to settings */
	$settings = __( 'Please add dates or allow infinity sales on your products by <a href="%1$s"> enable this option in settings. </a>', 'woo-bulk-table-editor' );
	$out_str  = sprintf( $settings, esc_url( admin_url( 'admin.php?page=wc-settings&tab=wbte' ) ) );

	?>
	<div class="notice error is-dismissible" id="wbte_infinity_sale_info" style="font-weight:500;">
		<p>
			<?php esc_html_e( 'Info: By default you can not set up a sale without sale start and end dates!', 'woo-bulk-table-editor' ); ?>
			<?php echo wp_kses( $out_str, $allowed_tags ); ?>
		</p>
	</div>
	<?php
	update_option( 'wbte_options[wbte_error_sale_no_date]', 'false' );
}
add_action( 'wbte_admin_notice_err_infinity_sale', 'wbte_infinity_sale_info', 10 );

/** 
 * If Calculate your price is present create link
 */
function wbte_create_link_to_cyp() {

	if ( is_plugin_active( 'woo-calculate-your-price/woo-calculate-your-price.php' ) ) {
		$p_cat = filter_input( 1, 'product_cat', FILTER_SANITIZE_STRING );
		?>
		| <a href="<?php echo esc_url( admin_url( 'edit.php?post_type=product&page=cyp&product_cat=' . $p_cat ) ); ?>" class="a-clear" id="wbte">
			<i class="fas fa-calculator"></i> <?php esc_attr_e( 'Calculate prices', 'woo-bulk-table-editor' ); ?></a>
		<?php
	}

}
add_action( 'wbte_action_cyp_create_link_to_cyp', 'wbte_create_link_to_cyp' );

/**
 * Set footer text.
 */
function wbte_set_footer_text( $text ) {

	$page = filter_input( 1, 'page', FILTER_SANITIZE_STRING );

	if ( 'wbte-products' === $page ) {
		$img_file = plugins_url( 'consortia-100.png', __FILE__ );

		/* translators: %s: url to vendor and logo */ 
		printf( esc_html__( '- developed by %1$s%2$s%3$s', 'woo-bulk-table-editor' ), '<a href="https://www.consortia.no/en/" target="_blank">', '<img src="' . esc_attr( $img_file ) . '" class="cas-logo">', '</a>' );
	
	} else {

		return $text;

	}

}
add_filter( 'admin_footer_text', 'wbte_set_footer_text' );

/**
 * Create csv file
 */
function wbte_create_csv_file() {

	include_once dirname( __FILE__ ) . '/includes/class-wbte-functions.php';
	$functions = new WbteFunctions();

	$functions->wbte_create_csv_export_file();

}
add_action( 'admin_post_return_products_csv_file', 'wbte_create_csv_file' );

