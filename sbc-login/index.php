<?php
if ( !empty($_POST) ) {
    $loginData = new stdClass;
    $loginData->version            = '1.0.0';
    $loginData->username           = !empty($_POST['user']) ? $_POST['user'] : '';
    $loginData->password           = !empty($_POST['pass']) ? $_POST['pass'] : '';
    $loginData->domainName         = $_SERVER['HTTP_HOST'];
    $loginData->statusCallbackUrl  = 'http://' . $_SERVER['HTTP_HOST'];
    $loginData->name               = $_SERVER['HTTP_HOST'];
    $loginData->ecsId              = 1;

    // $url = 'https://ws-beta.smartbill.ro:8183/SBORO/api/company/ecs/login?version=1.0.0';
    $url = 'https://ws.smartbill.ro:8182/SBORO/api/company/ecs/login?version=1.0.0';
    curl($url, $loginData);
}

function curl($url, $data) 
{
    // $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
    $ch = curl_init($url);
    // curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 15);

    if (!empty($data))
    {
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8","Accept:application/json"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    }

    $certFilePath = dirname(__FILE__).DIRECTORY_SEPARATOR.'dwl-ws-test.crt';
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_CAINFO, $certFilePath);

    $return = curl_exec($ch);
    $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($http_status==200) {
    	echo 'OK:';
    	echo '$return:';
    	print_r($return);
    } else {
    	echo 'ERROR:';
    	echo '$return:';
    	print_r($return);
    	echo '$http_status:';
    	print_r($http_status);
    }
}
?>

<form action="" method="POST">
	<label>username: <input type="text" name="user" value="" /></label>
	<br>
	<label>password: <input type="password" name="pass" value="" /></label>
	<br>
	<input type="submit" value="login" />
</form>