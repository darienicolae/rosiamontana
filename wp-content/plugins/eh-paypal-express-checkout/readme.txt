=== PayPal Express Checkout Payment Gateway for WooCommerce ===
Contributors: webtoffee
Version: 1.2.6
Tags: PayPal, Express Checkout, WooCommerce, Wordpress
Requires at least: 3.0.1
Tested up to: 5.4
== Description ==
== Screenshots ==
== Changelog ==

Please refer our product page for above details -->  https://www.webtoffee.com/product/paypal-express-checkout-gateway-for-woocommerce/

= Contact Us =
Support: https://www.webtoffee.com/support/
Or make use of questions and comments section in individual product page.

== Installation ==
https://www.webtoffee.com/how-to-download-install-update-woocommerce-plugin/

== Tutorial / Manual ==
https://www.webtoffee.com/category/documentation/paypal-express-checkout-payment-gateway-for-woocommerce/
