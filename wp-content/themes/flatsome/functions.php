<?php
/**
 * Flatsome functions and definitions
 *
 * @package flatsome
 */

require get_template_directory() . '/inc/init.php';

/**
 * Note: It's not recommended to add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * Learn more here: http://codex.wordpress.org/Child_Themes
 */

function upload_url() {
    return 'https://rosiamontana.s3.eu-central-1.amazonaws.com/wp-content/uploads';
}

add_filter( 'pre_option_upload_url_path', 'upload_url' );
