<?php
function wc_smartbill_settings()
{
    // verificare daca este autentificat sau nu
    $options = get_option('wc_smartbill_plugin_options');
    if (!empty($options) && is_array($options) && !empty($options['token'])) {
        $token = $options['token'];
        $username = $options['username'];

        // luam setarile json
        $settingsResponse = SmartBillHelper::getSettings($token, $username);
//        echo '<pre>'; print_r($settingsResponse); echo '</pre>';

        // curatam cache-ul si salvam noile valori
        if (!empty($settingsResponse)) {
            wp_cache_set('wc_smartbill_settings', $settingsResponse);
        }
    } else {
        $token = '';
    }
    ?>
    <div class="wrap">
        <h2><?=__('Smart Bill - Setari', 'wc_smartbill')?></h2>
        <?php settings_errors(); ?>
        <form action="options.php" method="post">
            <?php settings_fields('wc_smartbill_plugin_options_settings'); ?>
            <?php do_settings_sections('wc_smartbill_plugin_settings'); ?>
            <?php if (!empty($token)): ?>
            <input name="Submit" type="submit" value="<?=__('Salveaza modificarile', 'wc_smartbill')?>" />
            <?php endif; ?>
        </form>
    </div>
    <?php
}

function wc_smartbill_plugin_settings_section_text()
{
    // verificare daca este autentificat sau nu
    $options = get_option('wc_smartbill_plugin_options');
    if (!empty($options) && is_array($options) && !empty($options['token'])) {
        $token = $options['token'];
    } else {
        $token = '';
    }

    if (empty($token)) {
        ?>
        <p><?=__('Nu sunteti conectat la Smart Bill Cloud', 'wc_smartbill')?>.</p>
        <p><?=__('Accesati sectiunea Smart Bill', 'wc_smartbill')?> > <a href="<?=admin_url('admin.php')?>?page=wc_smartbill_login"><?=__('Autentificare', 'wc_smartbill')?></a>.</p>
        <p><?=__('Conectati-va cu datele contului de Smart Bill Cloud', 'wc_smartbill')?>.</p>
        <br />
        <p><?=__('Nu aveti cont Smart Bill Cloud?', 'wc_smartbill')?><br /><?=__('Inregistrati-va GRATUIT <a href="https://cloud.smartbill.ro/inregistrare-cont/" target="_blank">aici</a>', 'wc_smartbill')?>.</p>
        <?php
    }
}

function wc_smartbill_plugin_settings_vat_section_text()
{
    // nothing here
}

function wc_smartbill_plugin_settings_documents_section_text()
{
    // nothing here
}

function wc_smartbill_settings_display_company()
{
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['company'])) {
        $company = $options['company'];
    } else {
        $company = '';
    }

    $settings = wp_cache_get('wc_smartbill_settings');

    echo '
    <select name="wc_smartbill_plugin_options_settings[company]" style="width: 400px;">
    ';
    foreach ($settings['companies'] as $c) {
        if ($c['vatCode'] == $company) {
            echo '<option value="'.$c['vatCode'].'" selected>'.$c['name'].'</option>';
        } else {
            echo '<option value="' . $c['vatCode'] . '">' . $c['name'] . '</option>';
        }
    }
    echo '</select>';
    echo '<p class="description">'.__('Daca ai mai multe firme pe cont, vei emite pe cea selectata', 'wc_smartbill').'</p>';
}

function wc_smartbill_settings_display_included_vat()
{
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['included_vat'])) {
        $included_vat = $options['included_vat'];
    } else {
        $included_vat = 0;
    }

    echo '
    <select name="wc_smartbill_plugin_options_settings[included_vat]" style="width: 400px;">
        <option value="1" '.(!empty($included_vat)?'selected':'').'>'.__('Yes', 'wc_smartbill').'</option>
        <option value="0" '.(empty($included_vat)?'selected':'').'>'.__('No', 'wc_smartbill').'</option>
    </select>';
    echo '<p class="description">'.__('Daca vrei ca preturile sa fie transmise din WooCommerce catre Smart Bill cu TVA inclus', 'wc_smartbill').'</p>';
}

function wc_smartbill_settings_display_shipping_included_vat()
{
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['shipping_included_vat'])) {
        $shipping_included_vat = $options['shipping_included_vat'];
    } else {
        $shipping_included_vat = 0;
    }

    echo '
    <select name="wc_smartbill_plugin_options_settings[shipping_included_vat]" style="width: 400px;">
        <option value="1" '.(!empty($shipping_included_vat)?'selected':'').'>'.__('Yes', 'wc_smartbill').'</option>
        <option value="0" '.(empty($shipping_included_vat)?'selected':'').'>'.__('No', 'wc_smartbill').'</option>
    </select>';
    echo '<p class="description">'.__('Daca vrei ca transportul sa fie transmis din WooCommerce catre Smart Bill cu TVA inclus', 'wc_smartbill').'</p>';
}

function wc_smartbill_settings_display_product_vat()
{
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['product_vat'])) {
        $product_vat = $options['product_vat'];
    } else {
        $product_vat = 0;
    }

    // get company
    if (!empty($options) && is_array($options) && isset($options['company'])) {
        $company = $options['company'];
    } else {
        $company = '';
    }

    // get settings for selected company
    $settings = wp_cache_get('wc_smartbill_settings');
    if (empty($company)) {
        $settings = $settings['companies'][0];
    } else {
        $settings = SmartBillHelper::getCompanySettings($company, $settings);
    }

    echo '
    <select name="wc_smartbill_plugin_options_settings[product_vat]" style="width: 400px;">
        <option value="'.$settings['defaultVatCode'].'" '.($product_vat==$settings['defaultVatCode']?'selected':'').'>'.$settings['defaultVatCode'].'%</option>
        <option value="9998" '.($product_vat==9998?'selected':'').'>'.__('Preluat din WooCommerce, pe produse', 'wc_smartbill').'</option>
        <option value="9999" '.($product_vat==9999?'selected':'').'>'.__('Preluat din SmartBill, pe produse', 'wc_smartbill').'</option>
    </select>';
    echo '<p class="description">'.__('Ce cota TVA se va aplica produselor pe documentul emis in Smart Bill', 'wc_smartbill').'</p>';
}

function wc_smartbill_settings_display_shipping_vat()
{
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['shipping_vat'])) {
        $shipping_vat = $options['shipping_vat'];
    } else {
        $shipping_vat = '';
    }

    // get company
    if (!empty($options) && is_array($options) && isset($options['company'])) {
        $company = $options['company'];
    } else {
        $company = '';
    }

    // get settings for selected company
    $settings = wp_cache_get('wc_smartbill_settings');
    if (empty($company)) {
        $settings = $settings['companies'][0];
    } else {
        $settings = SmartBillHelper::getCompanySettings($company, $settings);
    }

    echo '
    <select name="wc_smartbill_plugin_options_settings[shipping_vat]" style="width: 400px;">
        <option value="" '.(empty($shipping_vat)?'selected':'').'>'.__('Alegeti cota TVA', 'wc_smartbill').'</option>
    ';
    foreach ($settings['taxes'] as $tax) {
        echo '<option value="'.$tax['id'].'" '.($shipping_vat==$tax['id']?'selected':'').'>'.$tax['name'].' ('.$tax['percentage'].'%)</option>';
    }
    echo '</select>';
}

function wc_smartbill_settings_display_document_type()
{
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['document_type'])) {
        $document_type = $options['document_type'];
    } else {
        $document_type = 0;
    }

    if (!empty($options) && is_array($options) && isset($options['document_series'])) {
        $document_series = $options['document_series'];
    } else {
        $document_series = '';
    }

    // get company
    if (!empty($options) && is_array($options) && isset($options['company'])) {
        $company = $options['company'];
    } else {
        $company = '';
    }

    // get settings for selected company
    $settings = wp_cache_get('wc_smartbill_settings');
    if (empty($company)) {
        $settings = $settings['companies'][0];
    } else {
        $settings = SmartBillHelper::getCompanySettings($company, $settings);
    }

    $htmlFactura = '<option value="" '.(empty($document_series)?'selected':'').'>'.__('Alegeti seria', 'wc_smartbill').'</option>';
    $htmlProforma = '<option value="" '.(empty($document_series)?'selected':'').'>'.__('Alegeti seria', 'wc_smartbill').'</option>';

    if ( !empty($settings['invoiceSeries']) ) {
        foreach ($settings['invoiceSeries'] as $s) {
            $htmlFactura .= '<option value="'.$s.'" '.($document_series==$s?'selected':'').'>'.$s.'</option>';
        }
    }

    if ( !empty($settings['estimateSeries']) ) {
        foreach ($settings['estimateSeries'] as $s) {
            $htmlProforma .= '<option value="'.$s.'" '.($document_series==$s?'selected':'').'>'.$s.'</option>';
        }
    }
    ?>
    <script type="text/javascript">
        function changeSmartBillDocumentType(type)
        {
            var htmlFactura = '<?=$htmlFactura?>';
            var htmlProforma = '<?=$htmlProforma?>';
            if (type==0) {
                document.getElementById('wc_smartbill_plugin_options_settings_document_series').innerHTML = htmlFactura;
            } else {
                document.getElementById('wc_smartbill_plugin_options_settings_document_series').innerHTML = htmlProforma;
            }
        }
    </script>
    <?php

    echo '
    <select id="wc_smartbill_plugin_options_settings_document_type" name="wc_smartbill_plugin_options_settings[document_type]" onchange="changeSmartBillDocumentType(this.value)" style="width: 400px;">
        <option value="0" '.(empty($document_type)?'selected':'').'>'.__('Factura', 'wc_smartbill').'</option>
        <option value="1" '.(!empty($document_type)?'selected':'').'>'.__('Proforma', 'wc_smartbill').'</option>
    </select>';
}

function wc_smartbill_settings_display_document_series()
{
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['document_series'])) {
        $document_series = $options['document_series'];
    } else {
        $document_series = '';
    }

    // get company
    if (!empty($options) && is_array($options) && isset($options['company'])) {
        $company = $options['company'];
    } else {
        $company = '';
    }

    // tip document
    if (!empty($options) && is_array($options) && isset($options['document_type'])) {
        $document_type = $options['document_type'];
    } else {
        $document_type = 0;
    }

    // get settings for selected company
    $settings = wp_cache_get('wc_smartbill_settings');
    if (empty($company)) {
        $settings = $settings['companies'][0];
    } else {
        $settings = SmartBillHelper::getCompanySettings($company, $settings);
    }

    echo '
    <select id="wc_smartbill_plugin_options_settings_document_series" name="wc_smartbill_plugin_options_settings[document_series]" style="width: 400px;">
        <option value="" '.(empty($document_series)?'selected':'').'>'.__('Alegeti seria', 'wc_smartbill').'</option>
    ';
    if (empty($document_type)) {
        foreach ($settings['invoiceSeries'] as $s) {
            echo '<option value="'.$s.'" '.($document_series==$s?'selected':'').'>'.$s.'</option>';
        }
    } else {
        foreach ($settings['estimateSeries'] as $s) {
            echo '<option value="'.$s.'" '.($document_series==$s?'selected':'').'>'.$s.'</option>';
        }
    }
    echo '</select>';
}

function wc_smartbill_settings_display_um()
{
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['um'])) {
        $um = $options['um'];
    } else {
        $um = '';
    }

    // get company
    if (!empty($options) && is_array($options) && isset($options['company'])) {
        $company = $options['company'];
    } else {
        $company = '';
    }

    // get settings for selected company
    $settings = wp_cache_get('wc_smartbill_settings');
    if (empty($company)) {
        $settings = $settings['companies'][0];
    } else {
        $settings = SmartBillHelper::getCompanySettings($company, $settings);
    }

    echo '
    <select name="wc_smartbill_plugin_options_settings[um]" style="width: 400px;">
        <option value="" '.(empty($um)?'selected':'').'>'.__('Alegeti unitatea de masura', 'wc_smartbill').'</option>
        <option value="@@@@@" '.($um=='@@@@@'?'selected':'').'>'.__('Preluat din SmartBill, pe produse', 'wc_smartbill').'</option>
    ';
    foreach ($settings['measureUnits'] as $m) {
        echo '<option value="'.$m.'" '.($m==$um?'selected':'').'>'.$m.'</option>';
    }
    echo '</select>';
    echo '<p class="description">'.__('Ce unitate de masura se va aplica produselor pe documentul emis in Smart Bill', 'wc_smartbill').'</p>';
}

function wc_smartbill_settings_display_product_currency()
{
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['product_currency'])) {
        $product_currency = $options['product_currency'];
    } else {
        $product_currency = '';
    }

    // get company
    if (!empty($options) && is_array($options) && isset($options['company'])) {
        $company = $options['company'];
    } else {
        $company = '';
    }

    // get settings for selected company
    $settings = wp_cache_get('wc_smartbill_settings');
    if (empty($company)) {
        $settings = $settings['companies'][0];
    } else {
        $settings = SmartBillHelper::getCompanySettings($company, $settings);
    }

    echo '
    <select name="wc_smartbill_plugin_options_settings[product_currency]" style="width: 400px;">
        <option value="" '.(empty($product_currency)?'selected':'').'>'.__('Alegeti moneda', 'wc_smartbill').'</option>
        <option value="auto" '.($product_currency=='auto'?'selected':'').'>'.__('Auto', 'wc_smartbill').'</option>
    ';
    foreach ($settings['currencies'] as $currency) {
        echo '<option value="'.$currency['symbol'].'" '.($currency['symbol']==$product_currency?'selected':'').'>'.$currency['symbol'].' - '.$currency['name'].'</option>';
    }
    echo '</select>';
    echo '<p class="description">'.__('Moneda aceasta se va prelua pe documentul emis in Smart Bill', 'wc_smartbill').'</p>';
}

function wc_smartbill_settings_display_billing_currency()
{
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['billing_currency'])) {
        $billing_currency = $options['billing_currency'];
    } else {
        $billing_currency = 9998;
    }

    echo '
    <select name="wc_smartbill_plugin_options_settings[billing_currency]" style="width: 400px;">
        <option value="9998" '.($billing_currency==9998?'selected':'').'>'.__('Moneda produselor', 'wc_smartbill').'</option>
        <option value="RON" '.($billing_currency=='RON'?'selected':'').'>RON - Leu</option>
    </select>';
}

function wc_smartbill_settings_display_include_shipping()
{
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['include_shipping'])) {
        $include_shipping = $options['include_shipping'];
    } else {
        $include_shipping = 0;
    }

    echo '
    <select name="wc_smartbill_plugin_options_settings[include_shipping]" style="width: 400px;">
        <option value="1" '.(!empty($include_shipping)?'selected':'').'>'.__('Yes', 'wc_smartbill').'</option>
        <option value="0" '.(empty($include_shipping)?'selected':'').'>'.__('No', 'wc_smartbill').'</option>
    </select>';
}

function wc_smartbill_settings_display_save_client()
{
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['save_client'])) {
        $save_client = $options['save_client'];
    } else {
        $save_client = 0;
    }

    echo '
    <select name="wc_smartbill_plugin_options_settings[save_client]" style="width: 400px;">
        <option value="1" '.(!empty($save_client)?'selected':'').'>'.__('Yes', 'wc_smartbill').'</option>
        <option value="0" '.(empty($save_client)?'selected':'').'>'.__('No', 'wc_smartbill').'</option>
    </select>';
    echo '<p class="description">'.__('Salvand clientul in Smart Bill, vei avea datele lui disponibile pentru emiteri ulterioare', 'wc_smartbill').'</p>';
}

function wc_smartbill_settings_display_gestiune()
{
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['gestiune'])) {
        $gestiune = $options['gestiune'];
    } else {
        $gestiune = 'Fara gestiune';
    }

    // get company
    if (!empty($options) && is_array($options) && isset($options['company'])) {
        $company = $options['company'];
    } else {
        $company = '';
    }

    // get settings for selected company
    $settings = wp_cache_get('wc_smartbill_settings');
    if (empty($company)) {
        $settings = $settings['companies'][0];
    } else {
        $settings = SmartBillHelper::getCompanySettings($company, $settings);
    }

    echo '<select name="wc_smartbill_plugin_options_settings[gestiune]" style="width: 400px;">';
//    echo '<option value="Fara gestiune" '.($gestiune=='Fara gestiune'?'selected':'').'>Fara gestiune</option>';
    foreach ($settings['warehouses'] as $warehouse) {
        echo '<option value="'.$warehouse.'" '.($gestiune==$warehouse?'selected':'').'>'.$warehouse.'</option>';
    }
    echo '</select>';
}

function wc_smartbill_settings_display_extra_taxes()
{
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['extra_taxes'])) {
        $extra_taxes = $options['extra_taxes'];
    } else {
        $extra_taxes = array();
    }

    $tax_classes = array_merge(array(''), WC_Tax::get_tax_classes());
    foreach ($tax_classes as $tax_class) {
        $taxes = WC_Tax::get_rates_for_tax_class($tax_class);
        foreach ($taxes as $tax) {
            echo '<label><input type="checkbox" name="wc_smartbill_plugin_options_settings[extra_taxes][]" value="'.$tax->tax_rate_id.'" '. (in_array($tax->tax_rate_id, $extra_taxes) ? 'checked="checked"':'').'> '.$tax->tax_rate_name.' ('.(int)$tax->tax_rate.'%)</label><br>';
        }
    }
}

function wc_smartbill_plugin_options_settings_validate($input)
{
    // luam firma curenta ca sa vedem daca s-a schimbat
    $options = get_option('wc_smartbill_plugin_options_settings');
    if (!empty($options) && is_array($options) && isset($options['company'])) {
        $company = $options['company'];
    } else {
        $company = '';
    }

    if (!empty($company) && $company != $input['company']) {
        // s-a schimbat compania, resetam toate setarile ce tin de companie
        $input['product_vat'] = '';
        $input['shipping_vat'] = '';
        $input['document_series'] = '';
        $input['um'] = '';
//        $input['gestiune'] = '';
        add_settings_error('wc_smartbill_settings_company', '', __('Firma a fost schimbata. Va rugam sa revizuiti setarile de mai jos pentru noua selectie.', 'wc_smartbill'), 'updated');
    }

    // verificam valorile
    if (empty($input['company'])) {
        add_settings_error('wc_smartbill_settings_shipping_vat', '', __('Trebuie sa alegi o valoare pentru "Compania implicita"', 'wc_smartbill'), 'error');
    }

    if (!in_array($input['included_vat'], array(0, 1))) {
        $input['included_vat'] = 0;
    }

    if (isset($input['shipping_vat']) && empty($input['shipping_vat'])) {
        add_settings_error('wc_smartbill_settings_shipping_vat', '', __('Trebuie sa alegi o valoare pentru "Cota TVA transport"', 'wc_smartbill'), 'error');
    } else {
        $input['shipping_vat'] = (int)$input['shipping_vat'];
    }

    if (!in_array($input['document_type'], array(0, 1))) {
        $input['document_type'] = 0;
    }

    if (empty($input['document_series'])) {
        add_settings_error('wc_smartbill_settings_document_series', '', __('Trebuie sa alegi o valoare pentru "Serie implicita document emis"', 'wc_smartbill'), 'error');
    }

    if (empty($input['um'])) {
        add_settings_error('wc_smartbill_settings_um', '', __('Trebuie sa alegi o valoare pentru "Unitatea de masura implicita"', 'wc_smartbill'), 'error');
    }

    if (empty($input['product_currency'])) {
        add_settings_error('wc_smartbill_settings_product_currency', '', __('Trebuie sa alegi o valoare pentru "Preturile produselor din WooCommerce sunt in"', 'wc_smartbill'), 'error');
    }

    if (!in_array($input['billing_currency'], array(9998, 'RON'))) {
        $input['billing_currency'] = 9998;
    }

    if (!in_array($input['include_shipping'], array(0, 1))) {
        $input['include_shipping'] = 0;
    }

    if (!in_array($input['save_client'], array(0, 1))) {
        $input['save_client'] = 0;
    }

    add_settings_error('wc_smartbill_settings_saved', '', __('Setarile au fost salvate', 'wc_smartbill'), 'updated');
    return $input;
}
