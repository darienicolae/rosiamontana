<?php
/**
 * Bulk Table Editor extended templates
 *
 * @package BulkTableEditor/includes
 */

defined( 'ABSPATH' ) || exit;

require_once __DIR__ . '/class-wbte-functions.php';

/**
 * Class for extended templates
 */
class WbteExtended {

	/**
	 * WbteFunctions
	 *
	 * @var var $functions.
	 */
	public $functions;

	/**
	 * WbteTemplates
	 *
	 * @var var $templates.
	 */
	public $templates;

	/**
	 * WbteOptions
	 *
	 * @var var $wbte_options.
	 */
	public $wbte_options;


	/**
	 * Constructor
	 */
	public function __construct() {
		$this->functions    = new WbteFunctions();
		$this->wbte_options = get_option( 'wbte_options' );
	}

	/**
	 * Get the extended table
	 *
	 */
	public function wbte_get_table() {
		?>
		<div class="display-desktop"> <!-- Table -->
			<table class="wp-list-table widefat fixed striped posts" id="wbtetable_ext" style="width:100%;">
				<thead>
					<?php $this->wbte_get_table_head(); ?>
					<?php $this->wbte_get_table_head_info(); ?>
				</thead>
				<tbody id="the-list"> 
					<?php $this->functions->wbte_loop_products(); ?>
				</tbody>
					<?php $this->wbte_get_table_footer(); ?>
			</table>
			<div id="wbte-paging-bottom" class="div-paging" style="margin-top: 6px;">
			</div>

			</div>
		<div>
		<?php

	}

	/**
	 * Get table head
	 */
	public function wbte_get_table_head() {

		?>
		<tr>
			<th class="th-first"><input type="checkbox" id="checkall" onclick="checkVisible();"></th>
			<th style="width:18%;"><a href="#" onclick="sort(1,'text');"><?php esc_html_e( 'Name', 'woo-bulk-table-editor' ); ?></a> <i id="s1" class="fas fa-sort"></i></th>
			<th class="th-center" style="width: 90px;"><i class="fas fa-star" data-tip="Featured"></i></th>
			<th class="th-center" style="width:12%;"><a href="#" onclick="sort(3,'text');"><?php esc_html_e( 'SKU', 'woo-bulk-table-editor' ); ?></a> <i id="s3" class="fas fa-sort"></i></th>
			<th class="th-center" style="width:12%;"><a href="#" onclick="sort(4,'text');"><?php esc_html_e( 'Tags', 'woo-bulk-table-editor' ); ?></a> <i id="s4" class="fas fa-sort"></i></th>
			<th class="th-center"><?php esc_html_e( 'Backorders', 'woo-bulk-table-editor' ); ?></th>
			<th class="th-center"><?php esc_html_e( 'In stock?', 'woo-bulk-table-editor' ); ?></th>
			<th class="th-center"><a href="#" onclick="sort(7,'number');"><?php esc_html_e( 'Weight', 'woo-bulk-table-editor' ); ?></a> <i id="s7" class="fas fa-sort"></i></th>
			<th class="th-center"><a href="#" onclick="sort(8,'number');"><?php esc_html_e( 'Length', 'woo-bulk-table-editor' ); ?></a> <i id="s8" class="fas fa-sort"></i></th>
			<th class="th-center"><a href="#" onclick="sort(9,'number');"><?php esc_html_e( 'Width', 'woo-bulk-table-editor' ); ?></a> <i id="s9" class="fas fa-sort"></i></th>
			<th class="th-center"><a href="#" onclick="sort(10,'number');"><?php esc_html_e( 'Height', 'woo-bulk-table-editor' ); ?></a> <i id="s10" class="fas fa-sort"></i></th>
			<th class="th-center" style="width:100px;"><i class="fas fa-image"></i></th>
	
		</tr>

		<?php
	}

	/**
	 * Get table head info
	 */
	public function wbte_get_table_head_info() {

		$auto_date  = 'date';
		$auto_focus = '';
		?>
			<tr class="th-info">
				<th></th>
				<th style="vertical-align: middle;"><strong><?php esc_html_e( 'Bulk Editor', 'woo-bulk-table-editor' ); ?></strong><br /><span class="th-desc"><?php esc_html_e( 'Set or select values to apply to all visible products', 'woo-bulk-table-editor' ); ?></span></th>
				<th style="vertical-align: top;">
					<select name="featured_select_type" id="featured_select_type" class="input-select" onchange="bulkSetValues('featured_select', 'ext');">
						<?php $this->functions->wbte_get_bulk_options( 'featured' ); ?>
					</select>
				</th>
				<th style="vertical-align: top;"><input type="text" name="sku_select" id="sku_select" class="input-txt" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
				<br/>
					<select name="sku_select_type" id="sku_select_type" class="input-select" onchange="bulkSetValues('sku_select', 'ext');">
						<?php $this->functions->wbte_get_bulk_options( 'sku' ); ?>
					</select>
				</th>
				<th style="vertical-align: top;"><input type="text" name="tags_select" id="tags_select" class="input-txt" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
				<br/>
					<select name="tags_select_type" id="tags_select_type" class="input-select" onchange="bulkSetValues('tags_select', 'ext');">
						<?php $this->functions->wbte_get_bulk_options( 'tags' ); ?>
					</select>
				</th>
				<th style="vertical-align: top;">
					<select name="backorder_select_type" id="backorder_select_type" class="input-select" onchange="bulkSetValues('backorder_select', 'ext');">
						<?php $this->functions->wbte_get_bulk_options( 'backorder' ); ?>
					</select>
				</th>
				
				<th style="vertical-align: top;">
					<select name="instock_select_type" id="instock_select_type" class="input-select" onchange="bulkSetValues('instock_select', 'ext');">
						<?php $this->functions->wbte_get_bulk_options( 'instock' ); ?>
					</select>
				</th>
				<th style="vertical-align: top;"><input type="number" name="weight_select" id="weight_select" class="input-txt" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
				<br/>
					<select name="weight_select_type" id="weight_select_type" class="input-select" onchange="bulkSetValues('weight_select', 'ext');">
						<?php $this->functions->wbte_get_bulk_options( 'numbers' ); ?>
					</select>
				</th>
				<th style="vertical-align: top;"><input type="number" name="length_select" id="length_select" class="input-txt" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
				<br/>
					<select name="length_select_type" id="length_select_type" class="input-select" onchange="bulkSetValues('length_select', 'ext');">
						<?php $this->functions->wbte_get_bulk_options( 'numbers' ); ?>
					</select>
				</th>
				<th style="vertical-align: top;"><input type="number" name="width_select" id="width_select" class="input-txt" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
				<br/>
					<select name="width_select_type" id="width_select_type" class="input-select" onchange="bulkSetValues('width_select', 'ext');">
						<?php $this->functions->wbte_get_bulk_options( 'numbers' ); ?>
					</select>
				</th>
				<th style="vertical-align: top;"><input type="number" name="height_select" id="height_select" class="input-txt" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
				<br/>
					<select name="height_select_type" id="height_select_type" class="input-select" onchange="bulkSetValues('height_select', 'ext');">
						<?php $this->functions->wbte_get_bulk_options( 'numbers' ); ?>
					</select>
				</th>
				<th style="vertical-align: top;">
					<button type="button" id="saveallExt" class="button action" onclick='saveAllExt();' style="width:100%;"><i class="fas fa-database"></i> <?php esc_html_e( 'Save all', 'woo-bulk-table-editor' ); ?></button>
					<div class="wbte-saving" id="wbte-saving">
						<progress id="pbar-saving" class="pbar-saving" value="0" max="100"></progress>
					</div>
				</th>
			</tr>

		<?php
	}

	/**
	 * Get table row
	 *
	 * @param var $product object.
	 * @param var $has_attributes bool.
	 * @param var $parent_id int.
	 */
	public function wbte_get_table_row( $product, $has_attributes, $parent_id ) {

		$product_name = $product->get_title();
		$product_id   = $product->get_id();
		
		
		if ( $has_attributes ) {

			$attributes = $product->get_attributes();

			if ( isset( $attributes ) && is_array( $attributes ) ) {
				foreach ( $attributes as $key => $val ) {
					if ( isset( $val ) && is_string( $val ) && strlen( $val ) > 0 ) {
						$product_name .= ', ' . $val;
					}
				}
			}

			$product_id = $product->get_parent_id();
			
			if ( 0 === $product_id ) {
				$product_id = $product->get_id();
			}
		}

		$auto_focus = '';
		$tags_arr   = get_the_terms( $product->get_id(), 'product_tag' );
		$tags       = '';
		
		if ( $tags_arr ) {
			foreach ( $tags_arr as $obj ) {
				$tags .= $obj->name . ', ';
			}
			$tags = substr( $tags, 0, strlen( $tags ) - 2 );
		}

		?>
		<tr class="lozad">
			<td id="<?php echo esc_attr( $product->get_id() ); ?>">
				<form id="frm_<?php echo esc_attr( $product->get_id() ); ?>_<?php echo esc_attr( wp_rand( 1, 50000 ) ); ?>" method="post">
				<input name="id" value="<?php echo esc_attr( $product->get_id() ); ?>" type="checkbox">
			</td>
			<td>
				<?php
				if ( ! $has_attributes ) {
					?>
					<input type="text" class="input-txt-name" name="product_name" value="<?php echo esc_html( $product_name ); ?>" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
					<input type="hidden" name="name_update" value="true">
					<?php
				} else {
					?>
					<input type="hidden" name="product_name" value="<?php echo esc_html( $product_name ); ?>">
					<input type="hidden" name="name_update" value="false">
					<?php echo esc_html( $product_name ); ?>
					
					<?php
				}
				?>
				<a href="<?php echo esc_url( get_admin_url() . 'post.php?post=' . $product_id . '&action=edit' ); ?>">
					<i class="fas fa-edit" style="float:right;"></i>
				</a>
			</td>
			<td class="th-center">
				<?php
				$featured         = ( $product->get_featured() ) ? '1' : '0';
				$featured_checked = '';
				if ( '1' === $featured ) {
					$featured_checked = 'checked="checked"';
				}
				?>
				<input type="checkbox" name="featured" <?php echo esc_attr( $featured_checked ); ?> value="<?php echo esc_attr( $product->get_featured() ); ?>"  onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
			</td>
			<td>
				<input type="text" class="input-txt" name="sku" value="<?php echo esc_attr( $this->functions->wbte_get_product_sku( $product ) ); ?>" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
			</td>
			<td>
			<input type="text" class="input-txt" name="tags" value="<?php echo esc_attr( $tags ); ?>" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
			</td>
			<td>
				<select name="backorder" class="input-select" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
					<?php $this->functions->wbte_get_bulk_options( 'backorder', $product->get_backorders() ); ?>
				</select>
			</td>
			<td>
				<select name="instock" class="input-select" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
					<?php $this->functions->wbte_get_bulk_options( 'instock', $product->get_stock_status() ); ?>
				</select>
			</td>
			<td>
				<input type="number" class="input-txt" name="weight" value="<?php echo esc_attr( $product->get_weight() ); ?>" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
			</td>
			<td>
				<input type="number" class="input-txt" name="_length" value="<?php echo esc_attr( $product->get_length() ); ?>" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
			</td>
			<td>
				<input type="number" class="input-txt" name="width" value="<?php echo esc_attr( $product->get_width() ); ?>" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
			</td>
			<td>
				<input type="number" class="input-txt" name="height" value="<?php echo esc_attr( $product->get_height() ); ?>" onmouseover="<?php echo esc_js( $this->functions->wbte_get_autofocus_value( $auto_focus ) ); ?>">
			</form>
			</td>
			<td class="th-center">
				<?php echo wp_kses( get_the_post_thumbnail( $product_id, array( '28', '28' ), array( 'class' => 'wbte-thumb' ) ), $this->functions->wbte_get_allowed_html() ); ?>
			</td>
		</tr>
		<?php

	}

	/**
	 * Get table footer
	 */
	public function wbte_get_table_footer() {
		?>
		<tfoot>
			<tr>
				<td><?php wp_nonce_field( 'footer_id_ext' ); ?></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>
				<button type="button" id="saveall_foot_ext" class="button action" onclick='saveAllExt();' style="width:100%;"><i class="fas fa-database"></i> <?php esc_html_e( 'Save all', 'woo-bulk-table-editor' ); ?></button>
				</td>
			</tr>
		</tfoot>
		<?php
	}
}


