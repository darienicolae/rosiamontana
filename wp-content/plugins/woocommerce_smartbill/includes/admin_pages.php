<?php
add_action('admin_menu', 'wc_smartbill_add_menu');

function wc_smartbill_add_menu()
{
    add_menu_page(
        __('Smart Bill', 'wc_smartbill'),
        __('Smart Bill', 'wc_smartbill'),
        'manage_options',
        'wc_smartbill_main',
        'wc_smartbill_main',
        plugin_dir_url(__FILE__).'../assets/images/smb_head.png'
    );

    add_submenu_page(
        'wc_smartbill_main',
        __('Autentificare', 'wc_smartbill'),
        __('Autentificare', 'wc_smartbill'),
        'manage_options',
        'wc_smartbill_login',
        'wc_smartbill_login'
    );

    add_submenu_page(
        'wc_smartbill_main',
        __('Setari', 'wc_smartbill'),
        __('Setari', 'wc_smartbill'),
        'manage_options',
        'wc_smartbill_settings',
        'wc_smartbill_settings'
    );

    add_submenu_page(
        'wc_smartbill_main',
        __('Ajutor', 'wc_smartbill'),
        __('Ajutor', 'wc_smartbill'),
        'manage_options',
        'wc_smartbill_help',
        'wc_smartbill_help'
    );
}
