<?php
class SmartBillHelper
{
    const VERSION               = '1.0.0';

    const DOMAIN_VALID          = '/.+(smartbill.ro)$/i';
    const SSL_CERT_URL          = 'ssl://ws.smartbill.ro';
    const LOGIN_URL             = 'https://ws.smartbill.ro/SBORO/api/company/ecs/login?version=';
    const SETTINGS_URL          = 'https://ws.smartbill.ro/SBORO/api/company/ecs/info?version=';
    const INVOICE_URL           = 'https://ws.smartbill.ro/SBORO/api/invoice/ecs?version=';
    const PROFORMA_URL          = 'https://ws.smartbill.ro/SBORO/api/estimate/ecs?version=';

    const DOCUMENT_DATE_NOW     = 0;
    const DOCUMENT_DATE_ORDER   = 1;

    // const DEBUG_EMAIL_FROM      = 'support@smartbill.ro';
    const DEBUG_EMAIL_SUBJECT   = 'DEBUG %s (woocommerce Smart Bill)';
    const SESSION_NAME          = 'IntelligentIT_SmartBill_SessionName';
    const PUBLIC_KEY_FILENAME   = 'sbc_public_key.pem';

    const DEFAULT_LOGIN_ERROR = 'Autentificare esuata. Va rugam verificati datele si incercati din nou.';
    const CERT_URL_ERROR = 'Accesul catre serviciul Smart Bill Cloud este restrictionat. Va rugam verificati configuratia serverului si incercati din nou.';
    const SERVER_ERROR = 'A intervenit o eroare la comunicarea cu Smart Bill Cloud. Va rugam verificati datele de conectare / reincercati o noua autentificare cu datele existente.';

    private static function getSSLCertificate()
    {
        $g = stream_context_create(array(
            "ssl" => array(
                "capture_peer_cert" => true,
                "verify_peer"=>false,
                "verify_peer_name"=>false,
                "allow_self_signed"=>true
            )
        ));
        $r = stream_socket_client(self::SSL_CERT_URL, $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $g);
        $cont = stream_context_get_params($r);

        $sss = "_";
        openssl_x509_export($cont["options"]["ssl"]["peer_certificate"], $sss);

        if ( '_' !== $sss ) {
            $file = fopen(dirname(__FILE__).DIRECTORY_SEPARATOR.'dwl-ws-test.crt', "w");
            fwrite($file, $sss);
            fclose($file);
        }
    }

    public static function curl(
        $loginToken,
        $url,
        $data = null,
        $httpHeaders = null,
        $noSSL = false,
        $checkToken = true,
        $throwError = true
    ) {
        if (empty($url) || ($url!=self::LOGIN_URL && empty($loginToken) && $checkToken)) {
            return false;
        }

        $urlToCall = $url;
        switch ($url) {
            case self::LOGIN_URL:
            case self::SETTINGS_URL:
            case self::INVOICE_URL:
            case self::PROFORMA_URL:
                $urlToCall .= self::VERSION;
                break;
        }

        $cacheGroup = 'sbc_curl';
        $cacheKey = md5(serialize(array($loginToken,$url,$data,$httpHeaders,$noSSL,$checkToken,$throwError)));
        $return = wp_cache_get($cacheKey, $cacheGroup);
        if ( !empty($return) ) {
            return $return;
        }

        // $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
        $ch = curl_init($urlToCall);
        // curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);

        if (!empty($data)) {
            // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json; charset=utf-8","Accept:application/json"
            ));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        if (!empty($httpHeaders)
         && is_array($httpHeaders)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeaders);
        }

        /*
        if (!$noSSL) {
            $certFilePath = dirname(__FILE__).DIRECTORY_SEPARATOR.'dwl-ws-test.crt';
            if ($url==self::LOGIN_URL) {
                self::getSSLCertificate();
            }

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_CAINFO, $certFilePath);
        }
        */

        $return = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($http_status==200) {
        } else {
            $errorMessage = $return;
            try {
                $returnJSON = json_decode($return);
                $errorMessage = !empty($returnJSON) ? $returnJSON->errorText : $errorMessage;
            } catch (Exception $e) {}
            $errorMessage = empty($errorMessage) ? self::SERVER_ERROR : $errorMessage;

            switch ($url) {
                case self::LOGIN_URL:
                    if (!$throwError) {
                        return false;
                    }

                    if (empty($http_status)) {
                        return json_encode(array('error' => self::CERT_URL_ERROR));
                    } elseif ($http_status==401) {
                        return json_encode(array('error' => self::DEFAULT_LOGIN_ERROR));
                    } else {
                        return json_encode(array('error' => $errorMessage));
                    }
                    break;

                case self::SETTINGS_URL:
                    return json_encode(array('error' => $errorMessage));
//                    $cache->save('', self::CACHE_KEY_SETTINGS, array(self::CACHE_KEY_SETTINGS), self::CACHE_LIFE_SETTINGS);
                    break;

                default:
                    if (!$throwError) {
                        return false;
                    }

                    // $http_status = empty($http_status) ? '' : $http_status;
                    break;
            }
            wc_smartbill_display_errors($return);
            //throw new Exception($return);

            // empty response
            $return = '';
        }

        wp_cache_set($cacheKey, $return, $cacheGroup, 1);

        return $return;
    }

    public static function prepareLoginData($data, $store)
    {
        $loginData = new stdClass;
        $loginData->version            = self::VERSION;
        $loginData->username           = $data['username'];
        $loginData->password           = $data['password'];
        $loginData->domainName         = $store['url'];
        $loginData->statusCallbackUrl  = $store['callback'];
        $loginData->name               = $store['name'];
        $loginData->ecsId              = 1;

        return $loginData;
    }

    public static function getStoreURL($url)
    {
        $url = str_replace(array('http://', 'https://'), '', $url);
        $url = $url[strlen($url)-1]=='/' ? substr($url, 0, -1) : $url;

        return $url;
    }

    public static function getCallbackURL()
    {
        $url = get_site_url().'/?smartbill_response=1';
        return $url;
    }

    public static function getAuthorization($token = '', $username = '')
    {
        return base64_encode($username.':'.$token);
    }

    public static function getSettings($token, $username)
    {
        $settingsResponse = self::curl(
            $token,
            self::SETTINGS_URL,
            null,
            array(
                "Content-Type: application/json; charset=utf-8",
                "Accept:application/json",
                "Authorization: ECSDigest ".self::getAuthorization($token, $username)
            ),
            false,
            true
        );
        $settingsResponse = json_decode($settingsResponse, true);
        return $settingsResponse;
    }

    public static function getCompanySettings($companyVat, $settings)
    {
        if ( empty($settings)
          || empty($settings['companies']) ) return;

        foreach ($settings['companies'] as $company) {
            if ($company['vatCode'] == $companyVat) {
                return $company;
            }
        }
    }

    public static function decryptData($data)
    {
        $decrypted = '';

        try {
            $publicKey = self::getPublicKey();
            $res = openssl_get_publickey($publicKey);
            openssl_public_decrypt(self::hex2bin($data), $decrypted, $res);
            $decrypted = json_decode($decrypted, true);
        } catch (Exception $e) {

        }

        return $decrypted;
    }

    private static function getPublicKey()
    {
        $publicKey = file_get_contents(dirname(__FILE__).'/'.self::PUBLIC_KEY_FILENAME);

        return $publicKey;
    }

    private static function hex2bin($hexdata)
    {
        $bindata = "";

        for ($i = 0; $i < strlen($hexdata); $i += 2) {
            $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
        }

        return $bindata;
    }
}
