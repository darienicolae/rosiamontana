<?php
// Add custom Theme Functions here


function upload_url() {
    return 'https://rosiamontana.s3.eu-central-1.amazonaws.com/wp-content/uploads';
}

add_filter( 'pre_option_upload_url_path', 'upload_url' );

