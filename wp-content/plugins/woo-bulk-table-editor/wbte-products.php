<?php
/**
 * Bulk Table Editor
 *
 * @package BulkTableEditor
 */

defined( 'ABSPATH' ) || exit;

global $woocommerce;
global $templates;

require_once __DIR__ . '/includes/class-wbte-templates.php';
require_once __DIR__ . '/includes/class-wbte-functions.php';

$wbte_functions     = new WbteFunctions();
$templates          = new WbteTemplates();
$wbte_options       = get_option( 'wbte_options' );
$show_sku           = ( strlen( $wbte_options[ 'wbte_use_sku_main_page' ] ) > 0 ) ? $wbte_options[ 'wbte_use_sku_main_page' ] : 'no';
$date_format_picker = $wbte_options['wbte_date_format'];
$custom_price       = $wbte_functions->wbte_get_custom_price_info();
$view               = ( strlen ( filter_input( 1, 'view', FILTER_SANITIZE_STRING ) ) > 0 ) ? filter_input( 1, 'view', FILTER_SANITIZE_STRING ) : 'prod';

$sum_col           = 8;
$custom_col        = '';
$custom_col_active = 'false';
		
if ( 'yes' === $custom_price['active'] ) {
	$sum_col           = 9;
	$custom_col        = ', 8 ';
	$custom_col_active = 'true';
}
?>

<div class="wbte-main">
<?php
	/**
	 * Get the content of the page
	 */
	$templates->wbte_load();
?>
</div>    

<script>
	var tbl               = '#wbtetable';
	var tbl_ext           = '#wbtetable_ext';
	var $                 = jQuery;
	var sum_col           = <?php echo esc_js( $sum_col ); ?>;
	var custom_col_active = <?php echo esc_js( $custom_col_active ); ?>;
	var page_view         = '<?php echo esc_js( $view ); ?>';
	var show_sku_home     = '<?php echo esc_js( $show_sku ); ?>';

	jQuery(document).ready(function ( $ ) {

		if ( page_view === 'ext' ) {
			$( tbl_ext ).tableCalc({
				calcColumns: [ 6, 7, 8, 9 ], 
				calcColumn_sum: 10,
				rowId: 0, 
				textColumns: [ 1, 2, 3, 4, 5 ],
				calcType: 'c', 
				calcCustom: '',
				decimals: 2, 
				calcOnLoad: false, 
				onEvent: '', 
			});

			$( tbl_ext +' tr td input' ).on( 'change', extChanged );
			$( tbl_ext +' tr td select' ).on( 'change', extChanged );
			

		} else {
			$( tbl ).tableCalc({
				calcColumns: [ 2, 3, 4, 7<?php echo esc_js( $custom_col ); ?>], 
				calcColumn_sum: sum_col,
				rowId: 0, 
				textColumns: [ 5, 6 ], 
				calcType: 'c', 
				calcCustom: '(0:0 * 0:1)',
				decimals: 2, 
				calcOnLoad: true, 
				onEvent: 'change', 
			});

			$( tbl +' tr td input' ).on( 'change', calcChanged );
			calcChanged( true );
		}

		$( '#product_search' ).on( 'keyup', rowSearch );
		$( "input[name='stype']" ).on( 'click', radioChange );
		
		if($('#product_search').val().length > 0){
			rowSearch();
		}

		var observer = lozad(); // lazy loads elements
		observer.observe();

		$('#datep_from').datepicker();
		$('#datep_to').datepicker();
		$('input[name="salefrom"]').datepicker();
		$('input[name="saleto"]').datepicker();
		$('#datep_from').datepicker( 'option', 'dateFormat', <?php echo "'" . esc_js( $date_format_picker ) . "'"; ?> );
		$('#datep_to').datepicker( 'option', 'dateFormat', <?php echo "'" . esc_js( $date_format_picker ) . "'"; ?> );
		$('#saveall').css( 'color', 'black' );


		// Add paging bottom
		$( '#wbte-paging-bottom' ).empty();
		$( '#wbte-paging-bottom' ).append( paging_buttons );
	});

</script>
