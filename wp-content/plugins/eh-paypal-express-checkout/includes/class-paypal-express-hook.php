<?php
if (!defined('ABSPATH')) {
    exit;
}
class Eh_Paypal_Express_Hooks
{
    protected $eh_paypal_express_options;
    function __construct() {
        $this->eh_paypal_express_options=get_option('woocommerce_eh_paypal_express_settings');
        $priority=(isset($this->eh_paypal_express_options['position_checkout']) && $this->eh_paypal_express_options['position_checkout']==='below')?20:0;
        add_action('woocommerce_proceed_to_checkout',array($this,'eh_express_checkout_hook'),$priority);
        if($priority===20)
        {
            add_action('woocommerce_after_add_to_cart_button',array($this,'eh_express_checkout_hook'),20);
        }
        else
        {
            add_action('woocommerce_before_add_to_cart_button',array($this,'eh_express_checkout_hook'),0);
        }
        add_action('woocommerce_after_mini_cart',array($this,'eh_express_minicart_hook'),20);
        add_action('wp', array($this, 'unset_express'));
        add_action('woocommerce_cart_emptied', array($this,'unset_expres_cart_empty'));
        add_action('add_meta_boxes', array($this,'add_paypal_action_metabox'));
    }
    public function add_paypal_action_metabox()
    {
        global $post;
        $order = wc_get_order($post->ID);
        if($order)
        {
            $payment_method = (WC()->version < '2.7.0')?$order->payment_method:$order->get_payment_method();
            //adds meta boxes to Paypal Direct Checkout Payment Gateway
            if(($payment_method === 'eh_paypal_express' || $payment_method === 'eh_paypal_direct_checkout') && (in_array($order->get_status(),array('on-hold','processing','completed'))))
            {
                wp_register_style('eh-express-meta', EH_PAYPAL_MAIN_URL . 'assets/css/eh-express-meta.css',array(),EH_PAYPAL_VERSION);
                wp_enqueue_style('eh-express-meta');
                wp_register_script('eh-express-meta', EH_PAYPAL_MAIN_URL . 'assets/js/eh-express-meta.js',array(),EH_PAYPAL_VERSION);
                wp_enqueue_script('eh-express-meta');
                wp_register_style('eh-alert-style', EH_PAYPAL_MAIN_URL.'/assets/css/sweetalert2.css',array(),EH_PAYPAL_VERSION);
                wp_enqueue_style('eh-alert-style');
                wp_register_script('eh-alert-jquery', EH_PAYPAL_MAIN_URL.'/assets/js/sweetalert2.min.js',array(),EH_PAYPAL_VERSION);
                wp_enqueue_script('eh-alert-jquery');
                add_meta_box
                            (
                                'eh_paypal_action_box', __('PayPal Express Actions', 'eh-paypal-express'),
                                array
                                    (
                                        $this,
                                        'paypal_metabox_html'
                                    ) ,
                                'shop_order',
                                'side',
                                'high'
                            );
            }
        }
    }
    public function paypal_metabox_html()
    {
        global $post;
        $order = wc_get_order($post->ID);

        wp_nonce_field('eh_meta_action_nonce', '_eh_meta_action_nonce');

        ?>
        <input type="hidden" value="<?php echo ((WC()->version < '2.7.0')?$order->id:$order->get_id()); ?>" id="eh_pe_order_id">
        <div class="loader"></div>
        <table class="eh_pe_meta_table">
            <?php
                $status= get_post_meta($post->ID,'_eh_pe_details',true);
                if($status['status'] === 'Authorization')
                {
                    ?>
                    <tr>
                        <td>
                            <?php _e('Capture Actions','eh-paypal-express'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select class="wc-enhanced-select" id="eh_paypal_meta_capture_select" style="width: 100%;">
                                <option value="full">
                                    <?php _e('Full Capture','eh-paypal-express'); ?>
                                </option>
                            </select>
                            <div  class="div_eh_pe_capture_meta">
                                <span class="button button-primary" id="eh_paypal_meta_capture_button">
                                    <?php _e('Capture','eh-paypal-express'); ?>
                                </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr>
                        </td>
                    </tr>
                    <?php
                }
                elseif ($status['status'] === 'Sale')
                {
                    ?>
                    <tr>
                        <td>
                            <?php _e('Refund Actions','eh-paypal-express'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select class="wc-enhanced-select" id="eh_paypal_meta_refund_select" style="width: 100%;">
                                <option value="partial">
                                    <?php _e('Partial Refund','eh-paypal-express'); ?>
                                </option>
                                <option value="full">
                                    <?php _e('Full Refund','eh-paypal-express'); ?>
                                </option>
                            </select>
                            <div  class="div_eh_pe_refund_meta">
                                <input type="text" class="text" placeholder="Amount" id="eh_paypal_meta_refund_amount">
                                <span class="button button-primary" id="eh_paypal_meta_refund_button">
                                    <?php _e('Refund','eh-paypal-express'); ?>
                                </span>
                            </div>
                        </td>
                    </tr>
                    <?php
                }
            ?>
            
    </table>
        <?php
    }
    public function unset_express()
    {
        if( ( isset($_REQUEST['cancel_express_checkout']) && ($_REQUEST['cancel_express_checkout'] === 'cancel') ) )
        {
            if(isset(WC()->session->eh_pe_billing)){
                unset(WC()->session->eh_pe_billing);
            }

            if(isset(WC()->session->eh_recurring_carts)){
                unset(WC()->session->eh_recurring_carts);
            }

            if(isset(WC()->session->eh_pe_checkout))
            {
                unset(WC()->session->eh_pe_checkout);
                wc_clear_notices();
                wc_add_notice(__('You have cancelled PayPal Express Checkout. Please try to process your order again.', 'eh-paypal-express'), 'notice');
            }
        }
    }
    public function unset_expres_cart_empty()
    {
        if(isset(WC()->session->eh_pe_billing)){
            unset(WC()->session->eh_pe_billing);
        }
        if(isset(WC()->session->eh_pe_checkout))
        {
            unset(WC()->session->eh_pe_checkout);
        }
        if(isset(WC()->session->eh_recurring_carts)){
            unset(WC()->session->eh_recurring_carts);
        }
    }
    public function express_run()
    {
        if($this->eh_paypal_express_options['enabled']==="yes")
        {            
            $this->check_express();
        }        
    }
    protected function check_express()
    {
        if($this->eh_paypal_express_options['express_enabled']==='yes')
        {
            if (($this->eh_paypal_express_options['express_on_cart_page']==='yes') &&  is_cart()) 
            {
                $show_button_in_cart_page = apply_filters('wt_show_paypal_express_button_in_cart_page', TRUE);
                if($show_button_in_cart_page){
                    $this->checkout_button_include();
                    $this->eh_payment_scripts();
                }
            }
            if (($this->eh_paypal_express_options['express_on_product_page']==='yes') && is_product()) 
            {
                $show_button_in_product_page = apply_filters('wt_show_paypal_express_button_in_product_page', TRUE);
                if($show_button_in_product_page){
                    $this->checkout_button_include();
                    $this->eh_payment_scripts();
                }
            }
            
        }
    }
    public function eh_express_minicart_hook(){

        if($this->eh_paypal_express_options['enabled']==="yes")
        {            
            if (isset($this->eh_paypal_express_options['express_on_mini_cart_page']) && ($this->eh_paypal_express_options['express_on_mini_cart_page']==='yes') &&  (0 != WC()->cart->get_cart_contents_count()) && ! is_cart() && ! is_product()) 
            {
                $show_button_in_mini_cart = apply_filters('wt_show_paypal_express_button_in_mini_cart', TRUE);
                if($show_button_in_mini_cart){
                    $this->checkout_button_include();
                    $this->eh_payment_scripts();
                }
            }
        }  
        
    }
    
    public function eh_express_checkout_hook() 
    {
        require_once (EH_PAYPAL_MAIN_PATH . "includes/functions.php");
        eh_paypal_express_hook_init();
    }
    public function checkout_button_include()
    {
        $page= get_page_link();
        $desc="";
        if($this->eh_paypal_express_options['express_description'] !=='')
        {
            $desc = '<div class="eh_paypal_express_description" ><small>-- '.$this->eh_paypal_express_options['express_description'].' --</small></div>';
        }
        if($this->eh_paypal_express_options['type_checkout'] === 'inline' && is_cart())
        {
            $express_id='id="eh_paypal_express_inine_button"';
            $express_url='href="' . esc_url( add_query_arg( array( 'express_start_inline' => 'true' ), add_query_arg('p', $page, $this->make_express_url('express_start')))) . '"';
        }
        else
        {
            $express_id='';
            $express_url='href="' . esc_url(add_query_arg('p', $page, $this->make_express_url('express_start'))) . '"';
        }
        $rel = apply_filters('eh_paypal_express_button_rel_attribute','rel="nofollow" ');

        $ex_button_output = '<center><div class="eh_payapal_express_checkout_button">'.$desc;      
        if($this->eh_paypal_express_options['style_checkout']==='paypal')
        {          
            $express_button   = apply_filters("eh_paypal_express_checkout_button", EH_PAYPAL_MAIN_URL ."assets/img/checkout-".$this->eh_paypal_express_options['button_size'].".svg");
            $express_ccbutton = apply_filters("eh_paypal_express_checkout_ccbutton", EH_PAYPAL_MAIN_URL ."assets/img/paypalcredit-".$this->eh_paypal_express_options['button_size'].".svg");
            $ex_button_output .= '<a '.$rel.$express_url.' class="single_add_to_cart_button eh_paypal_express_link" '.$express_id.'><img src="'.$express_button.'" style="width:auto;height:auto;" class="eh_paypal_express_image" alt="' . __('Check out with PayPal', 'eh-paypal-express') . '" /></a>';

            if($this->eh_paypal_express_options['credit_checkout']==='yes')
            {
                $ex_button_output .= '<a '.$rel.'href="' . esc_url(add_query_arg('p', $page, $this->make_express_url('credit_start'))) . '" class="single_add_to_cart_button eh_paypal_express_link"><img src="'.$express_ccbutton.'" style="width:auto;height:auto;" class="eh_paypal_express_image" alt="' . __('Check out with PayPal Credit', 'eh-paypal-express') . '" /></a>';
            }
        }
        else
        {
            $ex_button_output .= '<a '.$rel.$express_url.' class="single_add_to_cart_button eh_paypal_express_woo_style button alt eh_paypal_express_link" '.$express_id.'>'. __('PayPal CHECKOUT', 'eh-paypal-express') .'</a>';
            if($this->eh_paypal_express_options['credit_checkout']==='yes')
            {
                $ex_button_output .= '<a '.$rel.'href="' . esc_url(add_query_arg('p', $page, $this->make_express_url('credit_start'))) . '" class="single_add_to_cart_button button eh_paypal_express_woo_style alt eh_paypal_express_link">'. __('PayPal CREDIT', 'eh-paypal-express') .'</a>';
            }
        }
        $ex_button_output .= "</div></center>";
        echo $ex_button_output;
    }
    public function make_express_url($action) {
        return add_query_arg('c', $action, WC()->api_request_url('Eh_PayPal_Express_Payment'));
    }
    public function eh_payment_scripts()
    {
        if(is_product())
        {
            wp_register_style('eh-express-style', EH_PAYPAL_MAIN_URL . 'assets/css/eh-express-style.css',array(),EH_PAYPAL_VERSION);
            wp_enqueue_style('eh-express-style');
            wp_register_script('eh-express-js', EH_PAYPAL_MAIN_URL . 'assets/js/eh-express-script.js',array(),EH_PAYPAL_VERSION);
            wp_enqueue_script('eh-express-js');
            global $product;
            $is_var_product = FALSE;
            if ($product->is_type('variable')) {
                $is_var_product = TRUE;
            }
            wp_localize_script('eh-express-js','eh_express_checkout_params', array('page_name' => 'product','is_var_product' => $is_var_product));
 
        }elseif (is_cart())
        {
            wp_register_style('eh-express-style', EH_PAYPAL_MAIN_URL . 'assets/css/eh-express-style.css',array(),EH_PAYPAL_VERSION);
            wp_enqueue_style('eh-express-style');
            wp_register_script('eh-express-js', EH_PAYPAL_MAIN_URL . 'assets/js/eh-express-script.js',array(),EH_PAYPAL_VERSION);
            wp_enqueue_script('eh-express-js');
            wp_localize_script('eh-express-js','eh_express_checkout_params', array('page_name' => 'cart'));

            if($this->eh_paypal_express_options['type_checkout'] === 'inline')
            {
                $page= get_permalink();
                wp_enqueue_script( 'paypal-checkout-incontext-js', 'https://www.paypalobjects.com/api/checkout.js', array(), '1.0', true );
                wp_enqueue_script( 'eh-express-context', EH_PAYPAL_MAIN_URL . 'assets/js/eh-express-context.js', array( 'jquery' ), EH_PAYPAL_VERSION, true );
                wp_localize_script( 'eh-express-context', 'context_obj',
                        array(
                                'id'    => get_option('eh_paypal_express_payer_id'),
                                'environment'  => $this->eh_paypal_express_options['environment'],
                                'locale'       => ($this->eh_paypal_express_options['paypal_locale']==='yes') ? $this->get_locale(get_locale()) : $this->eh_paypal_express_options['user_locale_code'] ,
                                'start_inline'  => esc_url( add_query_arg( array( 'express_start_inline' => 'true' ),add_query_arg('p', $page, $this->make_express_url('express_start')))),
                        )
                );
            }
        }
    }
    public function get_locale($locale)
    {
        $safe_locales = array(
        'da_DK',
		'de_DE',
		'en_AU',
		'en_GB',
		'en_US',
		'es_ES',
		'fr_CA',
		'fr_FR',
		'he_IL',
		'id_ID',
		'it_IT',
		'ja_JP',
		'nl_NL',
		'pl_PL',
		'pt_BR',
		'pt_PT',
		'ru_RU',
		'sv_SE',
		'th_TH',
		'tr_TR',
		'zh_CN',
		'zh_HK',
        'zh_TW',
        );
        if (!in_array($locale, $safe_locales)) {
            $locale = 'en_US';
        }
        return $locale;
    }
  
}