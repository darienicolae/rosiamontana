<?php
function wc_smartbill_display_errors($error)
{
    $errorConverted = json_decode(strip_tags($error));
    ?>
        <!DOCTYPE html>
        <html lang="en">
          <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Eroare Smart Bill</title>
            <!-- Bootstrap core CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
            <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
          </head>
          <body>
            <div class="container">
              <div class="page-header">
                <h1><img src="<?=plugin_dir_url(__FILE__)?>../assets/images/logo.png" style="width: 320px" /></h1>
                <div class="alert alert-danger" role="alert">A fost intampinata o eroare</div>
              </div>
                <pre>
                    <?php
                    if (is_object($errorConverted)) {
                        print_r($errorConverted);
                    } else {
                        echo $error;
                    }
                    ?>
                </pre>
            </div> <!-- /container -->
          </body>
        </html>
    <?php
    exit;
}
