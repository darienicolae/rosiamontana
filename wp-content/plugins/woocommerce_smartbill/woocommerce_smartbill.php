<?php
/*
Plugin Name: Smart Bill Cloud - WooCommerce
Plugin URI: https://cloud.smartbill.ro/
Description: Acest modul permite generarea automata de facturi prin Smartbill (https://cloud.smartbill.ro)
Version: 1.0.18
Author: Intelligent IT S.R.L.
Author URI: https://cloud.smartbill.ro
*/
$pluginDir = plugin_dir_path(__FILE__);
$pluginDirUrl = plugin_dir_url(__FILE__);

# helpers
require_once $pluginDir.'includes/SmartBillHelper.php';
require_once $pluginDir.'includes/SmartBillDocumentHelper.php';

# menus
include $pluginDir.'includes/admin_pages.php';


function wc_smartbill_admin_get_settings() {
    $settings = false;

    $options = get_option('wc_smartbill_plugin_options');
    if (!empty($options) && is_array($options) && isset($options['token'])) {
        $token = $options['token'];
        $username = $options['username'];

        // luam setarile json
        $settingsData = SmartBillHelper::getSettings($token, $username);

        $options = get_option('wc_smartbill_plugin_options_settings');

        // get company
        if (!empty($options) && is_array($options) && isset($options['company'])) {
            $company = $options['company'];
        } else {
            $company = '';
        }

        // get settings for selected company
        if (empty($company)) {
            $settings = $settingsData['companies'][0];
        } else {
            $settings = SmartBillHelper::getCompanySettings($company, $settingsData);

            if ( empty($settings) ) {
                $settings = $settingsData['companies'][0];
            }
        }
    }

    return $settings;
}
# options
add_action('admin_init', 'wc_smartbill_admin_init');
function wc_smartbill_admin_init()
{
    if ( !empty($_GET['page'])
      && in_array($_GET['page'], array('wc_smartbill_settings')) ) {
        $settings = wc_smartbill_admin_get_settings();
    }

    # for login
    register_setting(
        'wc_smartbill_plugin_options',
        'wc_smartbill_plugin_options',
        'wc_smartbill_plugin_options_validate'
    );
    add_settings_section(
        'wc_smartbill_plugin_login',
        '',
        'wc_smartbill_plugin_login_section_text',
        'wc_smartbill_plugin'
    );
    add_settings_field(
        'wc_smartbill_plugin_options_username',
        __('Nume utilizator / adresa email', 'wc_smartbill'),
        'wc_smartbill_settings_display_username',
        'wc_smartbill_plugin',
        'wc_smartbill_plugin_login'
    );
    add_settings_field(
        'wc_smartbill_plugin_options_password',
        __('Parola', 'wc_smartbill'),
        'wc_smartbill_settings_display_password',
        'wc_smartbill_plugin',
        'wc_smartbill_plugin_login'
    );
    add_settings_field(
        'wc_smartbill_plugin_options_token',
        '',
        'wc_smartbill_settings_display_token',
        'wc_smartbill_plugin',
        'wc_smartbill_plugin_login'
    );

    # for settings
    register_setting(
        'wc_smartbill_plugin_options_settings',
        'wc_smartbill_plugin_options_settings',
        'wc_smartbill_plugin_options_settings_validate'
    );
    add_settings_section(
        'wc_smartbill_plugin_settings',
        '',
        'wc_smartbill_plugin_settings_section_text',
        'wc_smartbill_plugin_settings'
    );
    if ( !empty($settings['isTaxPayer']) ) {
        add_settings_section(
            'wc_smartbill_plugin_settings_vat',
            'Setari TVA',
            'wc_smartbill_plugin_settings_vat_section_text',
            'wc_smartbill_plugin_settings'
        );
    }
    if ( !empty($settings) ) {
        add_settings_section(
            'wc_smartbill_plugin_settings_documents',
            'Setari emitere documente',
            'wc_smartbill_plugin_settings_documents_section_text',
            'wc_smartbill_plugin_settings'
        );
        add_settings_field(
            'wc_smartbill_plugin_options_settings_company',
            __('Compania implicita', 'wc_smartbill'),
            'wc_smartbill_settings_display_company',
            'wc_smartbill_plugin_settings',
            'wc_smartbill_plugin_settings'
        );
    }


    if ( !empty($settings['isTaxPayer']) ) {
        add_settings_field(
            'wc_smartbill_plugin_options_settings_included_vat',
            __('Preturile includ TVA?', 'wc_smartbill'),
            'wc_smartbill_settings_display_included_vat',
            'wc_smartbill_plugin_settings',
            'wc_smartbill_plugin_settings_vat'
        );
        add_settings_field(
            'wc_smartbill_plugin_options_settings_product_vat',
            __('Cota TVA produse', 'wc_smartbill'),
            'wc_smartbill_settings_display_product_vat',
            'wc_smartbill_plugin_settings',
            'wc_smartbill_plugin_settings_vat'
        );
        add_settings_field(
            'wc_smartbill_plugin_options_settings_shipping_included_vat',
            __('Transportul include TVA?', 'wc_smartbill'),
            'wc_smartbill_settings_display_shipping_included_vat',
            'wc_smartbill_plugin_settings',
            'wc_smartbill_plugin_settings_vat'
        );
        add_settings_field(
            'wc_smartbill_plugin_options_settings_shipping_vat',
            __('Cota TVA transport', 'wc_smartbill'),
            'wc_smartbill_settings_display_shipping_vat',
            'wc_smartbill_plugin_settings',
            'wc_smartbill_plugin_settings_vat'
        );        
    }


    if ( !empty($settings) ) {
        add_settings_field(
            'wc_smartbill_plugin_options_settings_document_type',
            __('Tipul de document emis in Smart Bill', 'wc_smartbill'),
            'wc_smartbill_settings_display_document_type',
            'wc_smartbill_plugin_settings',
            'wc_smartbill_plugin_settings_documents'
        );
        add_settings_field(
            'wc_smartbill_plugin_options_settings_document_series',
            __('Serie implicita document emis', 'wc_smartbill'),
            'wc_smartbill_settings_display_document_series',
            'wc_smartbill_plugin_settings',
            'wc_smartbill_plugin_settings_documents'
        );
        add_settings_field(
            'wc_smartbill_plugin_options_settings_um',
            __('Unitatea de masura implicita', 'wc_smartbill'),
            'wc_smartbill_settings_display_um',
            'wc_smartbill_plugin_settings',
            'wc_smartbill_plugin_settings_documents'
        );
        add_settings_field(
            'wc_smartbill_plugin_options_settings_product_currency',
            __('Preturile produselor din WooCommerce sunt in', 'wc_smartbill'),
            'wc_smartbill_settings_display_product_currency',
            'wc_smartbill_plugin_settings',
            'wc_smartbill_plugin_settings_documents'
        );
        add_settings_field(
            'wc_smartbill_plugin_options_settings_billing_currency',
            __('Moneda documentului emis in Smart Bill', 'wc_smartbill'),
            'wc_smartbill_settings_display_billing_currency',
            'wc_smartbill_plugin_settings',
            'wc_smartbill_plugin_settings_documents'
        );
        add_settings_field(
            'wc_smartbill_plugin_options_settings_include_shipping',
            __('Include transportul in factura?', 'wc_smartbill'),
            'wc_smartbill_settings_display_include_shipping',
            'wc_smartbill_plugin_settings',
            'wc_smartbill_plugin_settings_documents'
        );
        add_settings_field(
            'wc_smartbill_plugin_options_settings_save_client',
            __('Salveaza clientul in Smart Bill', 'wc_smartbill'),
            'wc_smartbill_settings_display_save_client',
            'wc_smartbill_plugin_settings',
            'wc_smartbill_plugin_settings_documents'
        );
        add_settings_field(
            'wc_smartbill_plugin_options_settings_gestiune',
            __('Gestiune', 'wc_smartbill'),
            'wc_smartbill_settings_display_gestiune',
            'wc_smartbill_plugin_settings',
            'wc_smartbill_plugin_settings_documents'
        );
        add_settings_field(
            'wc_smartbill_plugin_options_settings_extra_taxes',
            __('Taxe aditionale incluse la emitere document in Smart Bill', 'wc_smartbill'),
            'wc_smartbill_settings_display_extra_taxes',
            'wc_smartbill_plugin_settings',
            'wc_smartbill_plugin_settings_documents'
        );
    }
}

# option pages
include $pluginDir.'includes/options_main_page.php';
include $pluginDir.'includes/options_login_page.php';
include $pluginDir.'includes/options_settings_page.php';
include $pluginDir.'includes/options_help_page.php';

# woocommerce stuff
include $pluginDir.'includes/woocommerce_functions.php';
include $pluginDir.'includes/woocommerce_billing_fields_ro.php';

# error displaying
include $pluginDir.'includes/display_errors.php';

# check if woocommerce is active
if (in_array(
    'woocommerce/woocommerce.php',
    apply_filters('active_plugins', get_option('active_plugins'))
)) {
    # afisare buton de emitere factura in comenzi
    add_action('init', 'wc_smartbill_init_plugin_actions');
    add_action('add_meta_boxes', 'wc_smartbill_order_details_meta_box');

    # afisare coloana cu factura in lista de comenzi
    add_filter('manage_edit-shop_order_columns', 'wc_smarbill_add_invoice_column', 11);
    add_action('manage_shop_order_posts_custom_column', 'wc_smartbill_add_invoice_column_content', 11, 2);

    # billing fields RO
    add_action('woocommerce_after_order_notes', 'woocommerce_billing_fields_ro');
    add_action('woocommerce_checkout_process', 'woocommerce_billing_fields_ro_process');
    add_action('woocommerce_checkout_update_user_meta', 'woocommerce_billing_fields_ro_update_user_meta');
    add_action('woocommerce_checkout_update_order_meta', 'woocommerce_billing_fields_ro_update_order_meta');
    add_filter('woocommerce_email_order_meta_keys', 'woocommerce_billing_fields_ro_order_meta_keys');
}
