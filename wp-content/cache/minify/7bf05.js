jQuery(document).ready(function(){jQuery(document).on('click','.wcml_currency_switcher a',wcml_switch_currency_handler)});var wcml_switch_currency_handler=function(event){event.preventDefault();if(jQuery(this).is(':disabled')||jQuery(this).parent().hasClass('wcml-cs-active-currency')||jQuery(this).hasClass('wcml-cs-active-currency')){return!1}else{jQuery(this).off(event)}
wcml_load_currency(jQuery(this).attr('rel'))}
function wcml_load_currency(currency,force_switch){var ajax_loader=jQuery('<img class=\"wcml-spinner\" width=\"16\" heigth=\"16\" src=\"'+wcml_mc_settings.wcml_spinner+'\" />');jQuery('.wcml_currency_switcher').append(ajax_loader);if(typeof force_switch==='undefined')force_switch=0;jQuery.ajax({type:'post',url:woocommerce_params.ajax_url,dataType:"json",data:{action:'wcml_switch_currency',currency:currency,force_switch:force_switch,params:window.location.search.substr(1)},success:function(response){if(typeof response.error!=='undefined'){alert(response.error)}else if(typeof response.data.prevent_switching!=='undefined'){jQuery('body').append(response.data.prevent_switching)}else{var target_location=window.location.href;if(-1!==target_location.indexOf('#')||wcml_mc_settings.cache_enabled){var url_dehash=target_location.split('#');var hash=url_dehash.length>1?'#'+url_dehash[1]:'';target_location=url_dehash[0].replace(/&wcmlc(\=[^&]*)?(?=&|$)|wcmlc(\=[^&]*)?(&|$)/,'').replace(/\?$/,'');var url_glue=target_location.indexOf('?')!=-1?'&':'?';target_location+=url_glue+'wcmlc='+currency+hash}
wcml_reset_cart_fragments();target_location=wcml_maybe_adjust_widget_price(target_location,response.data);window.location=target_location}}})}
function wcml_maybe_adjust_widget_price(target_location,response){if(typeof response.min_price!=='undefined'){target_location=target_location.replace(/(min_price=)(\d+)/,"$1"+response.min_price)}
if(typeof response.max_price!=='undefined'){target_location=target_location.replace(/(max_price=)(\d+)/,"$1"+response.max_price)}
return target_location};
/*!
 * jQuery blockUI plugin
 * Version 2.70.0-2014.11.23
 * Requires jQuery v1.7 or later
 *
 * Examples at: http://malsup.com/jquery/block/
 * Copyright (c) 2007-2013 M. Alsup
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * Thanks to Amir-Hossein Sobhi for some excellent contributions!
 */
;(function(){
/*jshint eqeqeq:false curly:false latedef:false */
"use strict";

	function setup($) {
		$.fn._fadeIn = $.fn.fadeIn;

		var noOp = $.noop || function() {};

		// this bit is to ensure we don't call setExpression when we shouldn't (with extra muscle to handle
		// confusing userAgent strings on Vista)
		var msie = /MSIE/.test(navigator.userAgent);
		var ie6  = /MSIE 6.0/.test(navigator.userAgent) && ! /MSIE 8.0/.test(navigator.userAgent);
		var mode = document.documentMode || 0;
		var setExpr = $.isFunction( document.createElement('div').style.setExpression );

		// global $ methods for blocking/unblocking the entire page
		$.blockUI   = function(opts) { install(window, opts); };
		$.unblockUI = function(opts) { remove(window, opts); };

		// convenience method for quick growl-like notifications  (http://www.google.com/search?q=growl)
		$.growlUI = function(title, message, timeout, onClose) {
			var $m = $('<div class="growlUI"></div>');
			if (title) $m.append('<h1>'+title+'</h1>');
			if (message) $m.append('<h2>'+message+'</h2>');
			if (timeout === undefined) timeout = 3000;

			// Added by konapun: Set timeout to 30 seconds if this growl is moused over, like normal toast notifications
			var callBlock = function(opts) {
				opts = opts || {};

				$.blockUI({
					message: $m,
					fadeIn : typeof opts.fadeIn  !== 'undefined' ? opts.fadeIn  : 700,
					fadeOut: typeof opts.fadeOut !== 'undefined' ? opts.fadeOut : 1000,
					timeout: typeof opts.timeout !== 'undefined' ? opts.timeout : timeout,
					centerY: false,
					showOverlay: false,
					onUnblock: onClose,
					css: $.blockUI.defaults.growlCSS
				});
			};

			callBlock();
			var nonmousedOpacity = $m.css('opacity');
			$m.mouseover(function() {
				callBlock({
					fadeIn: 0,
					timeout: 30000
				});

				var displayBlock = $('.blockMsg');
				displayBlock.stop(); // cancel fadeout if it has started
				displayBlock.fadeTo(300, 1); // make it easier to read the message by removing transparency
			}).mouseout(function() {
				$('.blockMsg').fadeOut(1000);
			});
			// End konapun additions
		};

		// plugin method for blocking element content
		$.fn.block = function(opts) {
			if ( this[0] === window ) {
				$.blockUI( opts );
				return this;
			}
			var fullOpts = $.extend({}, $.blockUI.defaults, opts || {});
			this.each(function() {
				var $el = $(this);
				if (fullOpts.ignoreIfBlocked && $el.data('blockUI.isBlocked'))
					return;
				$el.unblock({ fadeOut: 0 });
			});

			return this.each(function() {
				if ($.css(this,'position') == 'static') {
					this.style.position = 'relative';
					$(this).data('blockUI.static', true);
				}
				this.style.zoom = 1; // force 'hasLayout' in ie
				install(this, opts);
			});
		};

		// plugin method for unblocking element content
		$.fn.unblock = function(opts) {
			if ( this[0] === window ) {
				$.unblockUI( opts );
				return this;
			}
			return this.each(function() {
				remove(this, opts);
			});
		};

		$.blockUI.version = 2.70; // 2nd generation blocking at no extra cost!

		// override these in your code to change the default behavior and style
		$.blockUI.defaults = {
			// message displayed when blocking (use null for no message)
			message:  '<h1>Please wait...</h1>',

			title: null,		// title string; only used when theme == true
			draggable: true,	// only used when theme == true (requires jquery-ui.js to be loaded)

			theme: false, // set to true to use with jQuery UI themes

			// styles for the message when blocking; if you wish to disable
			// these and use an external stylesheet then do this in your code:
			// $.blockUI.defaults.css = {};
			css: {
				padding:	0,
				margin:		0,
				width:		'30%',
				top:		'40%',
				left:		'35%',
				textAlign:	'center',
				color:		'#000',
				border:		'3px solid #aaa',
				backgroundColor:'#fff',
				cursor:		'wait'
			},

			// minimal style set used when themes are used
			themedCSS: {
				width:	'30%',
				top:	'40%',
				left:	'35%'
			},

			// styles for the overlay
			overlayCSS:  {
				backgroundColor:	'#000',
				opacity:			0.6,
				cursor:				'wait'
			},

			// style to replace wait cursor before unblocking to correct issue
			// of lingering wait cursor
			cursorReset: 'default',

			// styles applied when using $.growlUI
			growlCSS: {
				width:		'350px',
				top:		'10px',
				left:		'',
				right:		'10px',
				border:		'none',
				padding:	'5px',
				opacity:	0.6,
				cursor:		'default',
				color:		'#fff',
				backgroundColor: '#000',
				'-webkit-border-radius':'10px',
				'-moz-border-radius':	'10px',
				'border-radius':		'10px'
			},

			// IE issues: 'about:blank' fails on HTTPS and javascript:false is s-l-o-w
			// (hat tip to Jorge H. N. de Vasconcelos)
			/*jshint scripturl:true */
			iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank',

			// force usage of iframe in non-IE browsers (handy for blocking applets)
			forceIframe: false,

			// z-index for the blocking overlay
			baseZ: 1000,

			// set these to true to have the message automatically centered
			centerX: true, // <-- only effects element blocking (page block controlled via css above)
			centerY: true,

			// allow body element to be stetched in ie6; this makes blocking look better
			// on "short" pages.  disable if you wish to prevent changes to the body height
			allowBodyStretch: true,

			// enable if you want key and mouse events to be disabled for content that is blocked
			bindEvents: true,

			// be default blockUI will supress tab navigation from leaving blocking content
			// (if bindEvents is true)
			constrainTabKey: true,

			// fadeIn time in millis; set to 0 to disable fadeIn on block
			fadeIn:  200,

			// fadeOut time in millis; set to 0 to disable fadeOut on unblock
			fadeOut:  400,

			// time in millis to wait before auto-unblocking; set to 0 to disable auto-unblock
			timeout: 0,

			// disable if you don't want to show the overlay
			showOverlay: true,

			// if true, focus will be placed in the first available input field when
			// page blocking
			focusInput: true,

            // elements that can receive focus
            focusableElements: ':input:enabled:visible',

			// suppresses the use of overlay styles on FF/Linux (due to performance issues with opacity)
			// no longer needed in 2012
			// applyPlatformOpacityRules: true,

			// callback method invoked when fadeIn has completed and blocking message is visible
			onBlock: null,

			// callback method invoked when unblocking has completed; the callback is
			// passed the element that has been unblocked (which is the window object for page
			// blocks) and the options that were passed to the unblock call:
			//	onUnblock(element, options)
			onUnblock: null,

			// callback method invoked when the overlay area is clicked.
			// setting this will turn the cursor to a pointer, otherwise cursor defined in overlayCss will be used.
			onOverlayClick: null,

			// don't ask; if you really must know: http://groups.google.com/group/jquery-en/browse_thread/thread/36640a8730503595/2f6a79a77a78e493#2f6a79a77a78e493
			quirksmodeOffsetHack: 4,

			// class name of the message block
			blockMsgClass: 'blockMsg',

			// if it is already blocked, then ignore it (don't unblock and reblock)
			ignoreIfBlocked: false
		};

		// private data and functions follow...

		var pageBlock = null;
		var pageBlockEls = [];

		function install(el, opts) {
			var css, themedCSS;
			var full = (el == window);
			var msg = (opts && opts.message !== undefined ? opts.message : undefined);
			opts = $.extend({}, $.blockUI.defaults, opts || {});

			if (opts.ignoreIfBlocked && $(el).data('blockUI.isBlocked'))
				return;

			opts.overlayCSS = $.extend({}, $.blockUI.defaults.overlayCSS, opts.overlayCSS || {});
			css = $.extend({}, $.blockUI.defaults.css, opts.css || {});
			if (opts.onOverlayClick)
				opts.overlayCSS.cursor = 'pointer';

			themedCSS = $.extend({}, $.blockUI.defaults.themedCSS, opts.themedCSS || {});
			msg = msg === undefined ? opts.message : msg;

			// remove the current block (if there is one)
			if (full && pageBlock)
				remove(window, {fadeOut:0});

			// if an existing element is being used as the blocking content then we capture
			// its current place in the DOM (and current display style) so we can restore
			// it when we unblock
			if (msg && typeof msg != 'string' && (msg.parentNode || msg.jquery)) {
				var node = msg.jquery ? msg[0] : msg;
				var data = {};
				$(el).data('blockUI.history', data);
				data.el = node;
				data.parent = node.parentNode;
				data.display = node.style.display;
				data.position = node.style.position;
				if (data.parent)
					data.parent.removeChild(node);
			}

			$(el).data('blockUI.onUnblock', opts.onUnblock);
			var z = opts.baseZ;

			// blockUI uses 3 layers for blocking, for simplicity they are all used on every platform;
			// layer1 is the iframe layer which is used to supress bleed through of underlying content
			// layer2 is the overlay layer which has opacity and a wait cursor (by default)
			// layer3 is the message content that is displayed while blocking
			var lyr1, lyr2, lyr3, s;
			if (msie || opts.forceIframe)
				lyr1 = $('<iframe class="blockUI" style="z-index:'+ (z++) +';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="'+opts.iframeSrc+'"></iframe>');
			else
				lyr1 = $('<div class="blockUI" style="display:none"></div>');

			if (opts.theme)
				lyr2 = $('<div class="blockUI blockOverlay ui-widget-overlay" style="z-index:'+ (z++) +';display:none"></div>');
			else
				lyr2 = $('<div class="blockUI blockOverlay" style="z-index:'+ (z++) +';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>');

			if (opts.theme && full) {
				s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage ui-dialog ui-widget ui-corner-all" style="z-index:'+(z+10)+';display:none;position:fixed">';
				if ( opts.title ) {
					s += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title || '&nbsp;')+'</div>';
				}
				s += '<div class="ui-widget-content ui-dialog-content"></div>';
				s += '</div>';
			}
			else if (opts.theme) {
				s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement ui-dialog ui-widget ui-corner-all" style="z-index:'+(z+10)+';display:none;position:absolute">';
				if ( opts.title ) {
					s += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title || '&nbsp;')+'</div>';
				}
				s += '<div class="ui-widget-content ui-dialog-content"></div>';
				s += '</div>';
			}
			else if (full) {
				s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage" style="z-index:'+(z+10)+';display:none;position:fixed"></div>';
			}
			else {
				s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement" style="z-index:'+(z+10)+';display:none;position:absolute"></div>';
			}
			lyr3 = $(s);

			// if we have a message, style it
			if (msg) {
				if (opts.theme) {
					lyr3.css(themedCSS);
					lyr3.addClass('ui-widget-content');
				}
				else
					lyr3.css(css);
			}

			// style the overlay
			if (!opts.theme /*&& (!opts.applyPlatformOpacityRules)*/)
				lyr2.css(opts.overlayCSS);
			lyr2.css('position', full ? 'fixed' : 'absolute');

			// make iframe layer transparent in IE
			if (msie || opts.forceIframe)
				lyr1.css('opacity',0.0);

			//$([lyr1[0],lyr2[0],lyr3[0]]).appendTo(full ? 'body' : el);
			var layers = [lyr1,lyr2,lyr3], $par = full ? $('body') : $(el);
			$.each(layers, function() {
				this.appendTo($par);
			});

			if (opts.theme && opts.draggable && $.fn.draggable) {
				lyr3.draggable({
					handle: '.ui-dialog-titlebar',
					cancel: 'li'
				});
			}

			// ie7 must use absolute positioning in quirks mode and to account for activex issues (when scrolling)
			var expr = setExpr && (!$.support.boxModel || $('object,embed', full ? null : el).length > 0);
			if (ie6 || expr) {
				// give body 100% height
				if (full && opts.allowBodyStretch && $.support.boxModel)
					$('html,body').css('height','100%');

				// fix ie6 issue when blocked element has a border width
				if ((ie6 || !$.support.boxModel) && !full) {
					var t = sz(el,'borderTopWidth'), l = sz(el,'borderLeftWidth');
					var fixT = t ? '(0 - '+t+')' : 0;
					var fixL = l ? '(0 - '+l+')' : 0;
				}

				// simulate fixed position
				$.each(layers, function(i,o) {
					var s = o[0].style;
					s.position = 'absolute';
					if (i < 2) {
						if (full)
							s.setExpression('height','Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.support.boxModel?0:'+opts.quirksmodeOffsetHack+') + "px"');
						else
							s.setExpression('height','this.parentNode.offsetHeight + "px"');
						if (full)
							s.setExpression('width','jQuery.support.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"');
						else
							s.setExpression('width','this.parentNode.offsetWidth + "px"');
						if (fixL) s.setExpression('left', fixL);
						if (fixT) s.setExpression('top', fixT);
					}
					else if (opts.centerY) {
						if (full) s.setExpression('top','(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"');
						s.marginTop = 0;
					}
					else if (!opts.centerY && full) {
						var top = (opts.css && opts.css.top) ? parseInt(opts.css.top, 10) : 0;
						var expression = '((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + '+top+') + "px"';
						s.setExpression('top',expression);
					}
				});
			}

			// show the message
			if (msg) {
				if (opts.theme)
					lyr3.find('.ui-widget-content').append(msg);
				else
					lyr3.append(msg);
				if (msg.jquery || msg.nodeType)
					$(msg).show();
			}

			if ((msie || opts.forceIframe) && opts.showOverlay)
				lyr1.show(); // opacity is zero
			if (opts.fadeIn) {
				var cb = opts.onBlock ? opts.onBlock : noOp;
				var cb1 = (opts.showOverlay && !msg) ? cb : noOp;
				var cb2 = msg ? cb : noOp;
				if (opts.showOverlay)
					lyr2._fadeIn(opts.fadeIn, cb1);
				if (msg)
					lyr3._fadeIn(opts.fadeIn, cb2);
			}
			else {
				if (opts.showOverlay)
					lyr2.show();
				if (msg)
					lyr3.show();
				if (opts.onBlock)
					opts.onBlock.bind(lyr3)();
			}

			// bind key and mouse events
			bind(1, el, opts);

			if (full) {
				pageBlock = lyr3[0];
				pageBlockEls = $(opts.focusableElements,pageBlock);
				if (opts.focusInput)
					setTimeout(focus, 20);
			}
			else
				center(lyr3[0], opts.centerX, opts.centerY);

			if (opts.timeout) {
				// auto-unblock
				var to = setTimeout(function() {
					if (full)
						$.unblockUI(opts);
					else
						$(el).unblock(opts);
				}, opts.timeout);
				$(el).data('blockUI.timeout', to);
			}
		}

		// remove the block
		function remove(el, opts) {
			var count;
			var full = (el == window);
			var $el = $(el);
			var data = $el.data('blockUI.history');
			var to = $el.data('blockUI.timeout');
			if (to) {
				clearTimeout(to);
				$el.removeData('blockUI.timeout');
			}
			opts = $.extend({}, $.blockUI.defaults, opts || {});
			bind(0, el, opts); // unbind events

			if (opts.onUnblock === null) {
				opts.onUnblock = $el.data('blockUI.onUnblock');
				$el.removeData('blockUI.onUnblock');
			}

			var els;
			if (full) // crazy selector to handle odd field errors in ie6/7
				els = $(document.body).children().filter('.blockUI').add('body > .blockUI');
			else
				els = $el.find('>.blockUI');

			// fix cursor issue
			if ( opts.cursorReset ) {
				if ( els.length > 1 )
					els[1].style.cursor = opts.cursorReset;
				if ( els.length > 2 )
					els[2].style.cursor = opts.cursorReset;
			}

			if (full)
				pageBlock = pageBlockEls = null;

			if (opts.fadeOut) {
				count = els.length;
				els.stop().fadeOut(opts.fadeOut, function() {
					if ( --count === 0)
						reset(els,data,opts,el);
				});
			}
			else
				reset(els, data, opts, el);
		}

		// move blocking element back into the DOM where it started
		function reset(els,data,opts,el) {
			var $el = $(el);
			if ( $el.data('blockUI.isBlocked') )
				return;

			els.each(function(i,o) {
				// remove via DOM calls so we don't lose event handlers
				if (this.parentNode)
					this.parentNode.removeChild(this);
			});

			if (data && data.el) {
				data.el.style.display = data.display;
				data.el.style.position = data.position;
				data.el.style.cursor = 'default'; // #59
				if (data.parent)
					data.parent.appendChild(data.el);
				$el.removeData('blockUI.history');
			}

			if ($el.data('blockUI.static')) {
				$el.css('position', 'static'); // #22
			}

			if (typeof opts.onUnblock == 'function')
				opts.onUnblock(el,opts);

			// fix issue in Safari 6 where block artifacts remain until reflow
			var body = $(document.body), w = body.width(), cssW = body[0].style.width;
			body.width(w-1).width(w);
			body[0].style.width = cssW;
		}

		// bind/unbind the handler
		function bind(b, el, opts) {
			var full = el == window, $el = $(el);

			// don't bother unbinding if there is nothing to unbind
			if (!b && (full && !pageBlock || !full && !$el.data('blockUI.isBlocked')))
				return;

			$el.data('blockUI.isBlocked', b);

			// don't bind events when overlay is not in use or if bindEvents is false
			if (!full || !opts.bindEvents || (b && !opts.showOverlay))
				return;

			// bind anchors and inputs for mouse and key events
			var events = 'mousedown mouseup keydown keypress keyup touchstart touchend touchmove';
			if (b)
				$(document).bind(events, opts, handler);
			else
				$(document).unbind(events, handler);

		// former impl...
		//		var $e = $('a,:input');
		//		b ? $e.bind(events, opts, handler) : $e.unbind(events, handler);
		}

		// event handler to suppress keyboard/mouse events when blocking
		function handler(e) {
			// allow tab navigation (conditionally)
			if (e.type === 'keydown' && e.keyCode && e.keyCode == 9) {
				if (pageBlock && e.data.constrainTabKey) {
					var els = pageBlockEls;
					var fwd = !e.shiftKey && e.target === els[els.length-1];
					var back = e.shiftKey && e.target === els[0];
					if (fwd || back) {
						setTimeout(function(){focus(back);},10);
						return false;
					}
				}
			}
			var opts = e.data;
			var target = $(e.target);
			if (target.hasClass('blockOverlay') && opts.onOverlayClick)
				opts.onOverlayClick(e);

			// allow events within the message content
			if (target.parents('div.' + opts.blockMsgClass).length > 0)
				return true;

			// allow events for content that is not being blocked
			return target.parents().children().filter('div.blockUI').length === 0;
		}

		function focus(back) {
			if (!pageBlockEls)
				return;
			var e = pageBlockEls[back===true ? pageBlockEls.length-1 : 0];
			if (e)
				e.focus();
		}

		function center(el, x, y) {
			var p = el.parentNode, s = el.style;
			var l = ((p.offsetWidth - el.offsetWidth)/2) - sz(p,'borderLeftWidth');
			var t = ((p.offsetHeight - el.offsetHeight)/2) - sz(p,'borderTopWidth');
			if (x) s.left = l > 0 ? (l+'px') : '0';
			if (y) s.top  = t > 0 ? (t+'px') : '0';
		}

		function sz(el, p) {
			return parseInt($.css(el,p),10)||0;
		}

	}


	/*global define:true */
	if (typeof define === 'function' && define.amd && define.amd.jQuery) {
		define(['jquery'], setup);
	} else {
		setup(jQuery);
	}

})();;
/*!
 * JavaScript Cookie v2.1.4
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
;(function (factory) {
	var registeredInModuleLoader = false;
	if (typeof define === 'function' && define.amd) {
		define(factory);
		registeredInModuleLoader = true;
	}
	if (typeof exports === 'object') {
		module.exports = factory();
		registeredInModuleLoader = true;
	}
	if (!registeredInModuleLoader) {
		var OldCookies = window.Cookies;
		var api = window.Cookies = factory();
		api.noConflict = function () {
			window.Cookies = OldCookies;
			return api;
		};
	}
}(function () {
	function extend () {
		var i = 0;
		var result = {};
		for (; i < arguments.length; i++) {
			var attributes = arguments[ i ];
			for (var key in attributes) {
				result[key] = attributes[key];
			}
		}
		return result;
	}

	function init (converter) {
		function api (key, value, attributes) {
			var result;
			if (typeof document === 'undefined') {
				return;
			}

			// Write

			if (arguments.length > 1) {
				attributes = extend({
					path: '/'
				}, api.defaults, attributes);

				if (typeof attributes.expires === 'number') {
					var expires = new Date();
					expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
					attributes.expires = expires;
				}

				// We're using "expires" because "max-age" is not supported by IE
				attributes.expires = attributes.expires ? attributes.expires.toUTCString() : '';

				try {
					result = JSON.stringify(value);
					if (/^[\{\[]/.test(result)) {
						value = result;
					}
				} catch (e) {}

				if (!converter.write) {
					value = encodeURIComponent(String(value))
						.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
				} else {
					value = converter.write(value, key);
				}

				key = encodeURIComponent(String(key));
				key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
				key = key.replace(/[\(\)]/g, escape);

				var stringifiedAttributes = '';

				for (var attributeName in attributes) {
					if (!attributes[attributeName]) {
						continue;
					}
					stringifiedAttributes += '; ' + attributeName;
					if (attributes[attributeName] === true) {
						continue;
					}
					stringifiedAttributes += '=' + attributes[attributeName];
				}
				return (document.cookie = key + '=' + value + stringifiedAttributes);
			}

			// Read

			if (!key) {
				result = {};
			}

			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling "get()"
			var cookies = document.cookie ? document.cookie.split('; ') : [];
			var rdecode = /(%[0-9A-Z]{2})+/g;
			var i = 0;

			for (; i < cookies.length; i++) {
				var parts = cookies[i].split('=');
				var cookie = parts.slice(1).join('=');

				if (cookie.charAt(0) === '"') {
					cookie = cookie.slice(1, -1);
				}

				try {
					var name = parts[0].replace(rdecode, decodeURIComponent);
					cookie = converter.read ?
						converter.read(cookie, name) : converter(cookie, name) ||
						cookie.replace(rdecode, decodeURIComponent);

					if (this.json) {
						try {
							cookie = JSON.parse(cookie);
						} catch (e) {}
					}

					if (key === name) {
						result = cookie;
						break;
					}

					if (!key) {
						result[name] = cookie;
					}
				} catch (e) {}
			}

			return result;
		}

		api.set = api;
		api.get = function (key) {
			return api.call(api, key);
		};
		api.getJSON = function () {
			return api.apply({
				json: true
			}, [].slice.call(arguments));
		};
		api.defaults = {};

		api.remove = function (key, attributes) {
			api(key, '', extend(attributes, {
				expires: -1
			}));
		};

		api.withConverter = init;

		return api;
	}

	return init(function () {});
}));
;
/* global Cookies */
jQuery( function( $ ) {
	// Orderby
	$( '.woocommerce-ordering' ).on( 'change', 'select.orderby', function() {
		$( this ).closest( 'form' ).submit();
	});

	// Target quantity inputs on product pages
	$( 'input.qty:not(.product-quantity input.qty)' ).each( function() {
		var min = parseFloat( $( this ).attr( 'min' ) );

		if ( min >= 0 && parseFloat( $( this ).val() ) < min ) {
			$( this ).val( min );
		}
	});

	var noticeID   = $( '.woocommerce-store-notice' ).data( 'notice-id' ) || '',
		cookieName = 'store_notice' + noticeID;

	// Check the value of that cookie and show/hide the notice accordingly
	if ( 'hidden' === Cookies.get( cookieName ) ) {
		$( '.woocommerce-store-notice' ).hide();
	} else {
		$( '.woocommerce-store-notice' ).show();
	}

	// Set a cookie and hide the store notice when the dismiss button is clicked
	$( '.woocommerce-store-notice__dismiss-link' ).click( function( event ) {
		Cookies.set( cookieName, 'hidden', { path: '/' } );
		$( '.woocommerce-store-notice' ).hide();
		event.preventDefault();
	});

	// Make form field descriptions toggle on focus.
	if ( $( '.woocommerce-input-wrapper span.description' ).length ) {
		$( document.body ).on( 'click', function() {
			$( '.woocommerce-input-wrapper span.description:visible' ).prop( 'aria-hidden', true ).slideUp( 250 );
		} );
	}

	$( '.woocommerce-input-wrapper' ).on( 'click', function( event ) {
		event.stopPropagation();
	} );

	$( '.woocommerce-input-wrapper :input' )
		.on( 'keydown', function( event ) {
			var input       = $( this ),
				parent      = input.parent(),
				description = parent.find( 'span.description' );

			if ( 27 === event.which && description.length && description.is( ':visible' ) ) {
				description.prop( 'aria-hidden', true ).slideUp( 250 );
				event.preventDefault();
				return false;
			}
		} )
		.on( 'click focus', function() {
			var input       = $( this ),
				parent      = input.parent(),
				description = parent.find( 'span.description' );

			parent.addClass( 'currentTarget' );

			$( '.woocommerce-input-wrapper:not(.currentTarget) span.description:visible' ).prop( 'aria-hidden', true ).slideUp( 250 );

			if ( description.length && description.is( ':hidden' ) ) {
				description.prop( 'aria-hidden', false ).slideDown( 250 );
			}

			parent.removeClass( 'currentTarget' );
		} );

	// Common scroll to element code.
	$.scroll_to_notices = function( scrollElement ) {
		if ( scrollElement.length ) {
			$( 'html, body' ).animate( {
				scrollTop: ( scrollElement.offset().top - 100 )
			}, 1000 );
		}
	};

	// Show password visiblity hover icon on woocommerce forms
	$( '.woocommerce form .woocommerce-Input[type="password"]' ).wrap( '<span class="password-input"></span>' );
	// Add 'password-input' class to the password wrapper in checkout page.
	$( '.woocommerce form input' ).filter(':password').parent('span').addClass('password-input');
	$( '.password-input' ).append( '<span class="show-password-input"></span>' );

	$( '.show-password-input' ).click(
		function() {
			$( this ).toggleClass( 'display-password' );
			if ( $( this ).hasClass( 'display-password' ) ) {
				$( this ).siblings( ['input[type="password"]'] ).prop( 'type', 'text' );
			} else {
				$( this ).siblings( 'input[type="text"]' ).prop( 'type', 'password' );
			}
		}
	);
});
;
/*global wc_country_select_params */
jQuery( function( $ ) {

	// wc_country_select_params is required to continue, ensure the object exists
	if ( typeof wc_country_select_params === 'undefined' ) {
		return false;
	}

	// Select2 Enhancement if it exists
	if ( $().selectWoo ) {
		var getEnhancedSelectFormatString = function() {
			return {
				'language': {
					errorLoading: function() {
						// Workaround for https://github.com/select2/select2/issues/4355 instead of i18n_ajax_error.
						return wc_country_select_params.i18n_searching;
					},
					inputTooLong: function( args ) {
						var overChars = args.input.length - args.maximum;

						if ( 1 === overChars ) {
							return wc_country_select_params.i18n_input_too_long_1;
						}

						return wc_country_select_params.i18n_input_too_long_n.replace( '%qty%', overChars );
					},
					inputTooShort: function( args ) {
						var remainingChars = args.minimum - args.input.length;

						if ( 1 === remainingChars ) {
							return wc_country_select_params.i18n_input_too_short_1;
						}

						return wc_country_select_params.i18n_input_too_short_n.replace( '%qty%', remainingChars );
					},
					loadingMore: function() {
						return wc_country_select_params.i18n_load_more;
					},
					maximumSelected: function( args ) {
						if ( args.maximum === 1 ) {
							return wc_country_select_params.i18n_selection_too_long_1;
						}

						return wc_country_select_params.i18n_selection_too_long_n.replace( '%qty%', args.maximum );
					},
					noResults: function() {
						return wc_country_select_params.i18n_no_matches;
					},
					searching: function() {
						return wc_country_select_params.i18n_searching;
					}
				}
			};
		};

		var wc_country_select_select2 = function() {
			$( 'select.country_select:visible, select.state_select:visible' ).each( function() {
				var select2_args = $.extend({
					placeholder: $( this ).attr( 'data-placeholder' ) || $( this ).attr( 'placeholder' ) || '',
					width: '100%'
				}, getEnhancedSelectFormatString() );

				$( this )
					.on( 'select2:select', function() {
						$( this ).focus(); // Maintain focus after select https://github.com/select2/select2/issues/4384
					} )
					.selectWoo( select2_args );
			});
		};

		wc_country_select_select2();

		$( document.body ).bind( 'country_to_state_changed', function() {
			wc_country_select_select2();
		});
	}

	/* State/Country select boxes */
	var states_json       = wc_country_select_params.countries.replace( /&quot;/g, '"' ),
		states            = $.parseJSON( states_json ),
		wrapper_selectors = '.woocommerce-billing-fields,' +
			'.woocommerce-shipping-fields,' +
			'.woocommerce-address-fields,' +
			'.woocommerce-shipping-calculator';

	$( document.body ).on( 'change refresh', 'select.country_to_state, input.country_to_state', function() {
		// Grab wrapping element to target only stateboxes in same 'group'
		var $wrapper = $( this ).closest( wrapper_selectors );

		if ( ! $wrapper.length ) {
			$wrapper = $( this ).closest('.form-row').parent();
		}

		var country     = $( this ).val(),
			$statebox     = $wrapper.find( '#billing_state, #shipping_state, #calc_shipping_state' ),
			$parent       = $statebox.closest( '.form-row' ),
			input_name    = $statebox.attr( 'name' ),
			input_id      = $statebox.attr('id'),
			input_classes = $statebox.attr('data-input-classes'),
			value         = $statebox.val(),
			placeholder   = $statebox.attr( 'placeholder' ) || $statebox.attr( 'data-placeholder' ) || '',
			$newstate;

		if ( states[ country ] ) {
			if ( $.isEmptyObject( states[ country ] ) ) {
				$newstate = $( '<input type="hidden" />' )
					.prop( 'id', input_id )
					.prop( 'name', input_name )
					.prop( 'placeholder', placeholder )
					.attr( 'data-input-classes', input_classes )
					.addClass( 'hidden ' + input_classes );
				$parent.hide().find( '.select2-container' ).remove();
				$statebox.replaceWith( $newstate );
				$( document.body ).trigger( 'country_to_state_changed', [ country, $wrapper ] );
			} else {
				var state          = states[ country ],
					$defaultOption = $( '<option value=""></option>' ).text( wc_country_select_params.i18n_select_state_text );

				if ( ! placeholder ) {
					placeholder = wc_country_select_params.i18n_select_state_text;
				}

				$parent.show();

				if ( $statebox.is( 'input' ) ) {
					$newstate = $( '<select></select>' )
						.prop( 'id', input_id )
						.prop( 'name', input_name )
						.data( 'placeholder', placeholder )
						.attr( 'data-input-classes', input_classes )
						.addClass( 'state_select ' + input_classes );
					$statebox.replaceWith( $newstate );
					$statebox = $wrapper.find( '#billing_state, #shipping_state, #calc_shipping_state' );
				}

				$statebox.empty().append( $defaultOption );

				$.each( state, function( index ) {
					var $option = $( '<option></option>' )
						.prop( 'value', index )
						.text( state[ index ] );
					$statebox.append( $option );
				} );

				$statebox.val( value ).change();

				$( document.body ).trigger( 'country_to_state_changed', [country, $wrapper ] );
			}
		} else {
			if ( $statebox.is( 'select, input[type="hidden"]' ) ) {
				$newstate = $( '<input type="text" />' )
					.prop( 'id', input_id )
					.prop( 'name', input_name )
					.prop('placeholder', placeholder)
					.attr('data-input-classes', input_classes )
					.addClass( 'input-text  ' + input_classes );
				$parent.show().find( '.select2-container' ).remove();
				$statebox.replaceWith( $newstate );
				$( document.body ).trigger( 'country_to_state_changed', [country, $wrapper ] );
			}
		}

		$( document.body ).trigger( 'country_to_state_changing', [country, $wrapper ] );
	});

	$( document.body ).on( 'wc_address_i18n_ready', function() {
		// Init country selects with their default value once the page loads.
		$( wrapper_selectors ).each( function() {
			var $country_input = $( this ).find( '#billing_country, #shipping_country, #calc_shipping_country' );

			if ( 0 === $country_input.length || 0 === $country_input.val().length ) {
				return;
			}

			$country_input.trigger( 'refresh' );
		});
	});
});
;
/*global wc_address_i18n_params */
jQuery( function( $ ) {

	// wc_address_i18n_params is required to continue, ensure the object exists
	if ( typeof wc_address_i18n_params === 'undefined' ) {
		return false;
	}

	var locale_json = wc_address_i18n_params.locale.replace( /&quot;/g, '"' ), locale = $.parseJSON( locale_json );

	function field_is_required( field, is_required ) {
		if ( is_required ) {
			field.find( 'label .optional' ).remove();
			field.addClass( 'validate-required' );

			if ( field.find( 'label .required' ).length === 0 ) {
				field.find( 'label' ).append(
					'&nbsp;<abbr class="required" title="' +
					wc_address_i18n_params.i18n_required_text +
					'">*</abbr>'
				);
			}
		} else {
			field.find( 'label .required' ).remove();
			field.removeClass( 'validate-required woocommerce-invalid woocommerce-invalid-required-field' );

			if ( field.find( 'label .optional' ).length === 0 ) {
				field.find( 'label' ).append( '&nbsp;<span class="optional">(' + wc_address_i18n_params.i18n_optional_text + ')</span>' );
			}
		}
	}

	// Handle locale
	$( document.body )
		.bind( 'country_to_state_changing', function( event, country, wrapper ) {
			var thisform = wrapper, thislocale;

			if ( typeof locale[ country ] !== 'undefined' ) {
				thislocale = locale[ country ];
			} else {
				thislocale = locale['default'];
			}

			var $postcodefield = thisform.find( '#billing_postcode_field, #shipping_postcode_field' ),
				$cityfield     = thisform.find( '#billing_city_field, #shipping_city_field' ),
				$statefield    = thisform.find( '#billing_state_field, #shipping_state_field' );

			if ( ! $postcodefield.attr( 'data-o_class' ) ) {
				$postcodefield.attr( 'data-o_class', $postcodefield.attr( 'class' ) );
				$cityfield.attr( 'data-o_class', $cityfield.attr( 'class' ) );
				$statefield.attr( 'data-o_class', $statefield.attr( 'class' ) );
			}

			var locale_fields = $.parseJSON( wc_address_i18n_params.locale_fields );

			$.each( locale_fields, function( key, value ) {

				var field       = thisform.find( value ),
					fieldLocale = $.extend( true, {}, locale['default'][ key ], thislocale[ key ] );

				// Labels.
				if ( typeof fieldLocale.label !== 'undefined' ) {
					field.find( 'label' ).html( fieldLocale.label );
				}

				// Placeholders.
				if ( typeof fieldLocale.placeholder !== 'undefined' ) {
					field.find( ':input' ).attr( 'placeholder', fieldLocale.placeholder );
					field.find( ':input' ).attr( 'data-placeholder', fieldLocale.placeholder );
					field.find( '.select2-selection__placeholder' ).text( fieldLocale.placeholder );
				}

				// Use the i18n label as a placeholder if there is no label element and no i18n placeholder.
				if (
					typeof fieldLocale.placeholder === 'undefined' &&
					typeof fieldLocale.label !== 'undefined' &&
					! field.find( 'label' ).length
				) {
					field.find( ':input' ).attr( 'placeholder', fieldLocale.label );
					field.find( ':input' ).attr( 'data-placeholder', fieldLocale.label );
					field.find( '.select2-selection__placeholder' ).text( fieldLocale.label );
				}

				// Required.
				if ( typeof fieldLocale.required !== 'undefined' ) {
					field_is_required( field, fieldLocale.required );
				} else {
					field_is_required( field, false );
				}

				// Priority.
				if ( typeof fieldLocale.priority !== 'undefined' ) {
					field.data( 'priority', fieldLocale.priority );
				}

				// Hidden fields.
				if ( 'state' !== key ) {
					if ( typeof fieldLocale.hidden !== 'undefined' && true === fieldLocale.hidden ) {
						field.hide().find( ':input' ).val( '' );
					} else {
						field.show();
					}
				}

				// Class changes.
				if ( Array.isArray( fieldLocale.class ) ) {
					field.removeClass( 'form-row-first form-row-last form-row-wide' );
					field.addClass( fieldLocale.class.join( ' ' ) );
				}
			});

			var fieldsets = $(
				'.woocommerce-billing-fields__field-wrapper,' +
				'.woocommerce-shipping-fields__field-wrapper,' +
				'.woocommerce-address-fields__field-wrapper,' +
				'.woocommerce-additional-fields__field-wrapper .woocommerce-account-fields'
			);

			fieldsets.each( function( index, fieldset ) {
				var rows    = $( fieldset ).find( '.form-row' );
				var wrapper = rows.first().parent();

				// Before sorting, ensure all fields have a priority for bW compatibility.
				var last_priority = 0;

				rows.each( function() {
					if ( ! $( this ).data( 'priority' ) ) {
							$( this ).data( 'priority', last_priority + 1 );
					}
					last_priority = $( this ).data( 'priority' );
				} );

				// Sort the fields.
				rows.sort( function( a, b ) {
					var asort = parseInt( $( a ).data( 'priority' ), 10 ),
						bsort = parseInt( $( b ).data( 'priority' ), 10 );

					if ( asort > bsort ) {
						return 1;
					}
					if ( asort < bsort ) {
						return -1;
					}
					return 0;
				});

				rows.detach().appendTo( wrapper );
			});
		})
		.trigger( 'wc_address_i18n_ready' );
});
;
/* global wc_checkout_params */
jQuery( function( $ ) {

	// wc_checkout_params is required to continue, ensure the object exists
	if ( typeof wc_checkout_params === 'undefined' ) {
		return false;
	}

	$.blockUI.defaults.overlayCSS.cursor = 'default';

	var wc_checkout_form = {
		updateTimer: false,
		dirtyInput: false,
		selectedPaymentMethod: false,
		xhr: false,
		$order_review: $( '#order_review' ),
		$checkout_form: $( 'form.checkout' ),
		init: function() {
			$( document.body ).bind( 'update_checkout', this.update_checkout );
			$( document.body ).bind( 'init_checkout', this.init_checkout );

			// Payment methods
			this.$checkout_form.on( 'click', 'input[name="payment_method"]', this.payment_method_selected );

			if ( $( document.body ).hasClass( 'woocommerce-order-pay' ) ) {
				this.$order_review.on( 'click', 'input[name="payment_method"]', this.payment_method_selected );
				this.$order_review.on( 'submit', this.submitOrder );
				this.$order_review.attr( 'novalidate', 'novalidate' );
			}

			// Prevent HTML5 validation which can conflict.
			this.$checkout_form.attr( 'novalidate', 'novalidate' );

			// Form submission
			this.$checkout_form.on( 'submit', this.submit );

			// Inline validation
			this.$checkout_form.on( 'input validate change', '.input-text, select, input:checkbox', this.validate_field );

			// Manual trigger
			this.$checkout_form.on( 'update', this.trigger_update_checkout );

			// Inputs/selects which update totals
			this.$checkout_form.on( 'change', 'select.shipping_method, input[name^="shipping_method"], #ship-to-different-address input, .update_totals_on_change select, .update_totals_on_change input[type="radio"], .update_totals_on_change input[type="checkbox"]', this.trigger_update_checkout ); // eslint-disable-line max-len
			this.$checkout_form.on( 'change', '.address-field select', this.input_changed );
			this.$checkout_form.on( 'change', '.address-field input.input-text, .update_totals_on_change input.input-text', this.maybe_input_changed ); // eslint-disable-line max-len
			this.$checkout_form.on( 'keydown', '.address-field input.input-text, .update_totals_on_change input.input-text', this.queue_update_checkout ); // eslint-disable-line max-len

			// Address fields
			this.$checkout_form.on( 'change', '#ship-to-different-address input', this.ship_to_different_address );

			// Trigger events
			this.$checkout_form.find( '#ship-to-different-address input' ).change();
			this.init_payment_methods();

			// Update on page load
			if ( wc_checkout_params.is_checkout === '1' ) {
				$( document.body ).trigger( 'init_checkout' );
			}
			if ( wc_checkout_params.option_guest_checkout === 'yes' ) {
				$( 'input#createaccount' ).change( this.toggle_create_account ).change();
			}
		},
		init_payment_methods: function() {
			var $payment_methods = $( '.woocommerce-checkout' ).find( 'input[name="payment_method"]' );

			// If there is one method, we can hide the radio input
			if ( 1 === $payment_methods.length ) {
				$payment_methods.eq(0).hide();
			}

			// If there was a previously selected method, check that one.
			if ( wc_checkout_form.selectedPaymentMethod ) {
				$( '#' + wc_checkout_form.selectedPaymentMethod ).prop( 'checked', true );
			}

			// If there are none selected, select the first.
			if ( 0 === $payment_methods.filter( ':checked' ).length ) {
				$payment_methods.eq(0).prop( 'checked', true );
			}

			// Get name of new selected method.
			var checkedPaymentMethod = $payment_methods.filter( ':checked' ).eq(0).prop( 'id' );

			if ( $payment_methods.length > 1 ) {
				// Hide open descriptions.
				$( 'div.payment_box:not(".' + checkedPaymentMethod + '")' ).filter( ':visible' ).slideUp( 0 );
			}

			// Trigger click event for selected method
			$payment_methods.filter( ':checked' ).eq(0).trigger( 'click' );
		},
		get_payment_method: function() {
			return wc_checkout_form.$checkout_form.find( 'input[name="payment_method"]:checked' ).val();
		},
		payment_method_selected: function( e ) {
			e.stopPropagation();

			if ( $( '.payment_methods input.input-radio' ).length > 1 ) {
				var target_payment_box = $( 'div.payment_box.' + $( this ).attr( 'ID' ) ),
					is_checked         = $( this ).is( ':checked' );

				if ( is_checked && ! target_payment_box.is( ':visible' ) ) {
					$( 'div.payment_box' ).filter( ':visible' ).slideUp( 230 );

					if ( is_checked ) {
						target_payment_box.slideDown( 230 );
					}
				}
			} else {
				$( 'div.payment_box' ).show();
			}

			if ( $( this ).data( 'order_button_text' ) ) {
				$( '#place_order' ).text( $( this ).data( 'order_button_text' ) );
			} else {
				$( '#place_order' ).text( $( '#place_order' ).data( 'value' ) );
			}

			var selectedPaymentMethod = $( '.woocommerce-checkout input[name="payment_method"]:checked' ).attr( 'id' );

			if ( selectedPaymentMethod !== wc_checkout_form.selectedPaymentMethod ) {
				$( document.body ).trigger( 'payment_method_selected' );
			}

			wc_checkout_form.selectedPaymentMethod = selectedPaymentMethod;
		},
		toggle_create_account: function() {
			$( 'div.create-account' ).hide();

			if ( $( this ).is( ':checked' ) ) {
				// Ensure password is not pre-populated.
				$( '#account_password' ).val( '' ).change();
				$( 'div.create-account' ).slideDown();
			}
		},
		init_checkout: function() {
			$( document.body ).trigger( 'update_checkout' );
		},
		maybe_input_changed: function( e ) {
			if ( wc_checkout_form.dirtyInput ) {
				wc_checkout_form.input_changed( e );
			}
		},
		input_changed: function( e ) {
			wc_checkout_form.dirtyInput = e.target;
			wc_checkout_form.maybe_update_checkout();
		},
		queue_update_checkout: function( e ) {
			var code = e.keyCode || e.which || 0;

			if ( code === 9 ) {
				return true;
			}

			wc_checkout_form.dirtyInput = this;
			wc_checkout_form.reset_update_checkout_timer();
			wc_checkout_form.updateTimer = setTimeout( wc_checkout_form.maybe_update_checkout, '1000' );
		},
		trigger_update_checkout: function() {
			wc_checkout_form.reset_update_checkout_timer();
			wc_checkout_form.dirtyInput = false;
			$( document.body ).trigger( 'update_checkout' );
		},
		maybe_update_checkout: function() {
			var update_totals = true;

			if ( $( wc_checkout_form.dirtyInput ).length ) {
				var $required_inputs = $( wc_checkout_form.dirtyInput ).closest( 'div' ).find( '.address-field.validate-required' );

				if ( $required_inputs.length ) {
					$required_inputs.each( function() {
						if ( $( this ).find( 'input.input-text' ).val() === '' ) {
							update_totals = false;
						}
					});
				}
			}
			if ( update_totals ) {
				wc_checkout_form.trigger_update_checkout();
			}
		},
		ship_to_different_address: function() {
			$( 'div.shipping_address' ).hide();
			if ( $( this ).is( ':checked' ) ) {
				$( 'div.shipping_address' ).slideDown();
			}
		},
		reset_update_checkout_timer: function() {
			clearTimeout( wc_checkout_form.updateTimer );
		},
		is_valid_json: function( raw_json ) {
			try {
				var json = $.parseJSON( raw_json );

				return ( json && 'object' === typeof json );
			} catch ( e ) {
				return false;
			}
		},
		validate_field: function( e ) {
			var $this             = $( this ),
				$parent           = $this.closest( '.form-row' ),
				validated         = true,
				validate_required = $parent.is( '.validate-required' ),
				validate_email    = $parent.is( '.validate-email' ),
				event_type        = e.type;

			if ( 'input' === event_type ) {
				$parent.removeClass( 'woocommerce-invalid woocommerce-invalid-required-field woocommerce-invalid-email woocommerce-validated' ); // eslint-disable-line max-len
			}

			if ( 'validate' === event_type || 'change' === event_type ) {

				if ( validate_required ) {
					if ( 'checkbox' === $this.attr( 'type' ) && ! $this.is( ':checked' ) ) {
						$parent.removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-required-field' );
						validated = false;
					} else if ( $this.val() === '' ) {
						$parent.removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-required-field' );
						validated = false;
					}
				}

				if ( validate_email ) {
					if ( $this.val() ) {
						/* https://stackoverflow.com/questions/2855865/jquery-validate-e-mail-address-regex */
						var pattern = new RegExp(/^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[0-9a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i); // eslint-disable-line max-len

						if ( ! pattern.test( $this.val()  ) ) {
							$parent.removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid woocommerce-invalid-email' );
							validated = false;
						}
					}
				}

				if ( validated ) {
					$parent.removeClass( 'woocommerce-invalid woocommerce-invalid-required-field woocommerce-invalid-email' ).addClass( 'woocommerce-validated' ); // eslint-disable-line max-len
				}
			}
		},
		update_checkout: function( event, args ) {
			// Small timeout to prevent multiple requests when several fields update at the same time
			wc_checkout_form.reset_update_checkout_timer();
			wc_checkout_form.updateTimer = setTimeout( wc_checkout_form.update_checkout_action, '5', args );
		},
		update_checkout_action: function( args ) {
			if ( wc_checkout_form.xhr ) {
				wc_checkout_form.xhr.abort();
			}

			if ( $( 'form.checkout' ).length === 0 ) {
				return;
			}

			args = typeof args !== 'undefined' ? args : {
				update_shipping_method: true
			};

			var country			 = $( '#billing_country' ).val(),
				state			 = $( '#billing_state' ).val(),
				postcode		 = $( ':input#billing_postcode' ).val(),
				city			 = $( '#billing_city' ).val(),
				address			 = $( ':input#billing_address_1' ).val(),
				address_2		 = $( ':input#billing_address_2' ).val(),
				s_country		 = country,
				s_state			 = state,
				s_postcode		 = postcode,
				s_city			 = city,
				s_address		 = address,
				s_address_2		 = address_2,
				$required_inputs = $( wc_checkout_form.$checkout_form ).find( '.address-field.validate-required:visible' ),
				has_full_address = true;

			if ( $required_inputs.length ) {
				$required_inputs.each( function() {
					if ( $( this ).find( ':input' ).val() === '' ) {
						has_full_address = false;
					}
				});
			}

			if ( $( '#ship-to-different-address' ).find( 'input' ).is( ':checked' ) ) {
				s_country		 = $( '#shipping_country' ).val();
				s_state			 = $( '#shipping_state' ).val();
				s_postcode		 = $( ':input#shipping_postcode' ).val();
				s_city			 = $( '#shipping_city' ).val();
				s_address		 = $( ':input#shipping_address_1' ).val();
				s_address_2		 = $( ':input#shipping_address_2' ).val();
			}

			var data = {
				security        : wc_checkout_params.update_order_review_nonce,
				payment_method  : wc_checkout_form.get_payment_method(),
				country         : country,
				state           : state,
				postcode        : postcode,
				city            : city,
				address         : address,
				address_2       : address_2,
				s_country       : s_country,
				s_state         : s_state,
				s_postcode      : s_postcode,
				s_city          : s_city,
				s_address       : s_address,
				s_address_2     : s_address_2,
				has_full_address: has_full_address,
				post_data       : $( 'form.checkout' ).serialize()
			};

			if ( false !== args.update_shipping_method ) {
				var shipping_methods = {};

				// eslint-disable-next-line max-len
				$( 'select.shipping_method, input[name^="shipping_method"][type="radio"]:checked, input[name^="shipping_method"][type="hidden"]' ).each( function() {
					shipping_methods[ $( this ).data( 'index' ) ] = $( this ).val();
				} );

				data.shipping_method = shipping_methods;
			}

			$( '.woocommerce-checkout-payment, .woocommerce-checkout-review-order-table' ).block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});

			wc_checkout_form.xhr = $.ajax({
				type:		'POST',
				url:		wc_checkout_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'update_order_review' ),
				data:		data,
				success:	function( data ) {

					// Reload the page if requested
					if ( data && true === data.reload ) {
						window.location.reload();
						return;
					}

					// Remove any notices added previously
					$( '.woocommerce-NoticeGroup-updateOrderReview' ).remove();

					var termsCheckBoxChecked = $( '#terms' ).prop( 'checked' );

					// Save payment details to a temporary object
					var paymentDetails = {};
					$( '.payment_box :input' ).each( function() {
						var ID = $( this ).attr( 'id' );

						if ( ID ) {
							if ( $.inArray( $( this ).attr( 'type' ), [ 'checkbox', 'radio' ] ) !== -1 ) {
								paymentDetails[ ID ] = $( this ).prop( 'checked' );
							} else {
								paymentDetails[ ID ] = $( this ).val();
							}
						}
					});

					// Always update the fragments
					if ( data && data.fragments ) {
						$.each( data.fragments, function ( key, value ) {
							if ( ! wc_checkout_form.fragments || wc_checkout_form.fragments[ key ] !== value ) {
								$( key ).replaceWith( value );
							}
							$( key ).unblock();
						} );
						wc_checkout_form.fragments = data.fragments;
					}

					// Recheck the terms and conditions box, if needed
					if ( termsCheckBoxChecked ) {
						$( '#terms' ).prop( 'checked', true );
					}

					// Fill in the payment details if possible without overwriting data if set.
					if ( ! $.isEmptyObject( paymentDetails ) ) {
						$( '.payment_box :input' ).each( function() {
							var ID = $( this ).attr( 'id' );
							if ( ID ) {
								if ( $.inArray( $( this ).attr( 'type' ), [ 'checkbox', 'radio' ] ) !== -1 ) {
									$( this ).prop( 'checked', paymentDetails[ ID ] ).change();
								} else if ( $.inArray( $( this ).attr( 'type' ), [ 'select' ] ) !== -1 ) {
									$( this ).val( paymentDetails[ ID ] ).change();
								} else if ( null !== $( this ).val() && 0 === $( this ).val().length ) {
									$( this ).val( paymentDetails[ ID ] ).change();
								}
							}
						});
					}

					// Check for error
					if ( data && 'failure' === data.result ) {

						var $form = $( 'form.checkout' );

						// Remove notices from all sources
						$( '.woocommerce-error, .woocommerce-message' ).remove();

						// Add new errors returned by this event
						if ( data.messages ) {
							$form.prepend( '<div class="woocommerce-NoticeGroup woocommerce-NoticeGroup-updateOrderReview">' + data.messages + '</div>' ); // eslint-disable-line max-len
						} else {
							$form.prepend( data );
						}

						// Lose focus for all fields
						$form.find( '.input-text, select, input:checkbox' ).trigger( 'validate' ).blur();

						wc_checkout_form.scroll_to_notices();
					}

					// Re-init methods
					wc_checkout_form.init_payment_methods();

					// Fire updated_checkout event.
					$( document.body ).trigger( 'updated_checkout', [ data ] );
				}

			});
		},
		handleUnloadEvent: function( e ) {
			// Modern browsers have their own standard generic messages that they will display.
			// Confirm, alert, prompt or custom message are not allowed during the unload event
			// Browsers will display their own standard messages

			// Check if the browser is Internet Explorer
			if((navigator.userAgent.indexOf('MSIE') !== -1 ) || (!!document.documentMode)) {
				// IE handles unload events differently than modern browsers
				e.preventDefault();
				return undefined;
			}

			return true;
		},
		attachUnloadEventsOnSubmit: function() {
			$( window ).on('beforeunload', this.handleUnloadEvent);
		},
		detachUnloadEventsOnSubmit: function() {
			$( window ).unbind('beforeunload', this.handleUnloadEvent);
		},
		blockOnSubmit: function( $form ) {
			var form_data = $form.data();

			if ( 1 !== form_data['blockUI.isBlocked'] ) {
				$form.block({
					message: null,
					overlayCSS: {
						background: '#fff',
						opacity: 0.6
					}
				});
			}
		},
		submitOrder: function() {
			wc_checkout_form.blockOnSubmit( $( this ) );
		},
		submit: function() {
			wc_checkout_form.reset_update_checkout_timer();
			var $form = $( this );

			if ( $form.is( '.processing' ) ) {
				return false;
			}

			// Trigger a handler to let gateways manipulate the checkout if needed
			// eslint-disable-next-line max-len
			if ( $form.triggerHandler( 'checkout_place_order' ) !== false && $form.triggerHandler( 'checkout_place_order_' + wc_checkout_form.get_payment_method() ) !== false ) {

				$form.addClass( 'processing' );

				wc_checkout_form.blockOnSubmit( $form );

				// Attach event to block reloading the page when the form has been submitted
				wc_checkout_form.attachUnloadEventsOnSubmit();

				// ajaxSetup is global, but we use it to ensure JSON is valid once returned.
				$.ajaxSetup( {
					dataFilter: function( raw_response, dataType ) {
						// We only want to work with JSON
						if ( 'json' !== dataType ) {
							return raw_response;
						}

						if ( wc_checkout_form.is_valid_json( raw_response ) ) {
							return raw_response;
						} else {
							// Attempt to fix the malformed JSON
							var maybe_valid_json = raw_response.match( /{"result.*}/ );

							if ( null === maybe_valid_json ) {
								console.log( 'Unable to fix malformed JSON' );
							} else if ( wc_checkout_form.is_valid_json( maybe_valid_json[0] ) ) {
								console.log( 'Fixed malformed JSON. Original:' );
								console.log( raw_response );
								raw_response = maybe_valid_json[0];
							} else {
								console.log( 'Unable to fix malformed JSON' );
							}
						}

						return raw_response;
					}
				} );

				$.ajax({
					type:		'POST',
					url:		wc_checkout_params.checkout_url,
					data:		$form.serialize(),
					dataType:   'json',
					success:	function( result ) {
						// Detach the unload handler that prevents a reload / redirect
						wc_checkout_form.detachUnloadEventsOnSubmit();

						try {
							if ( 'success' === result.result && $form.triggerHandler( 'checkout_place_order_success' ) !== false ) {
								if ( -1 === result.redirect.indexOf( 'https://' ) || -1 === result.redirect.indexOf( 'http://' ) ) {
									window.location = result.redirect;
								} else {
									window.location = decodeURI( result.redirect );
								}
							} else if ( 'failure' === result.result ) {
								throw 'Result failure';
							} else {
								throw 'Invalid response';
							}
						} catch( err ) {
							// Reload page
							if ( true === result.reload ) {
								window.location.reload();
								return;
							}

							// Trigger update in case we need a fresh nonce
							if ( true === result.refresh ) {
								$( document.body ).trigger( 'update_checkout' );
							}

							// Add new errors
							if ( result.messages ) {
								wc_checkout_form.submit_error( result.messages );
							} else {
								wc_checkout_form.submit_error( '<div class="woocommerce-error">' + wc_checkout_params.i18n_checkout_error + '</div>' ); // eslint-disable-line max-len
							}
						}
					},
					error:	function( jqXHR, textStatus, errorThrown ) {
						// Detach the unload handler that prevents a reload / redirect
						wc_checkout_form.detachUnloadEventsOnSubmit();

						wc_checkout_form.submit_error( '<div class="woocommerce-error">' + errorThrown + '</div>' );
					}
				});
			}

			return false;
		},
		submit_error: function( error_message ) {
			$( '.woocommerce-NoticeGroup-checkout, .woocommerce-error, .woocommerce-message' ).remove();
			wc_checkout_form.$checkout_form.prepend( '<div class="woocommerce-NoticeGroup woocommerce-NoticeGroup-checkout">' + error_message + '</div>' ); // eslint-disable-line max-len
			wc_checkout_form.$checkout_form.removeClass( 'processing' ).unblock();
			wc_checkout_form.$checkout_form.find( '.input-text, select, input:checkbox' ).trigger( 'validate' ).blur();
			wc_checkout_form.scroll_to_notices();
			$( document.body ).trigger( 'checkout_error' );
		},
		scroll_to_notices: function() {
			var scrollElement           = $( '.woocommerce-NoticeGroup-updateOrderReview, .woocommerce-NoticeGroup-checkout' );

			if ( ! scrollElement.length ) {
				scrollElement = $( '.form.checkout' );
			}
			$.scroll_to_notices( scrollElement );
		}
	};

	var wc_checkout_coupons = {
		init: function() {
			$( document.body ).on( 'click', 'a.showcoupon', this.show_coupon_form );
			$( document.body ).on( 'click', '.woocommerce-remove-coupon', this.remove_coupon );
			$( 'form.checkout_coupon' ).hide().submit( this.submit );
		},
		show_coupon_form: function() {
			$( '.checkout_coupon' ).slideToggle( 400, function() {
				$( '.checkout_coupon' ).find( ':input:eq(0)' ).focus();
			});
			return false;
		},
		submit: function() {
			var $form = $( this );

			if ( $form.is( '.processing' ) ) {
				return false;
			}

			$form.addClass( 'processing' ).block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});

			var data = {
				security:		wc_checkout_params.apply_coupon_nonce,
				coupon_code:	$form.find( 'input[name="coupon_code"]' ).val()
			};

			$.ajax({
				type:		'POST',
				url:		wc_checkout_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'apply_coupon' ),
				data:		data,
				success:	function( code ) {
					$( '.woocommerce-error, .woocommerce-message' ).remove();
					$form.removeClass( 'processing' ).unblock();

					if ( code ) {
						$form.before( code );
						$form.slideUp();

						$( document.body ).trigger( 'applied_coupon_in_checkout', [ data.coupon_code ] );
						$( document.body ).trigger( 'update_checkout', { update_shipping_method: false } );
					}
				},
				dataType: 'html'
			});

			return false;
		},
		remove_coupon: function( e ) {
			e.preventDefault();

			var container = $( this ).parents( '.woocommerce-checkout-review-order' ),
				coupon    = $( this ).data( 'coupon' );

			container.addClass( 'processing' ).block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});

			var data = {
				security: wc_checkout_params.remove_coupon_nonce,
				coupon:   coupon
			};

			$.ajax({
				type:    'POST',
				url:     wc_checkout_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'remove_coupon' ),
				data:    data,
				success: function( code ) {
					$( '.woocommerce-error, .woocommerce-message' ).remove();
					container.removeClass( 'processing' ).unblock();

					if ( code ) {
						$( 'form.woocommerce-checkout' ).before( code );

						$( document.body ).trigger( 'removed_coupon_in_checkout', [ data.coupon_code ] );
						$( document.body ).trigger( 'update_checkout', { update_shipping_method: false } );

						// Remove coupon code from coupon field
						$( 'form.checkout_coupon' ).find( 'input[name="coupon_code"]' ).val( '' );
					}
				},
				error: function ( jqXHR ) {
					if ( wc_checkout_params.debug_mode ) {
						/* jshint devel: true */
						console.log( jqXHR.responseText );
					}
				},
				dataType: 'html'
			});
		}
	};

	var wc_checkout_login_form = {
		init: function() {
			$( document.body ).on( 'click', 'a.showlogin', this.show_login_form );
		},
		show_login_form: function() {
			$( 'form.login, form.woocommerce-form--login' ).slideToggle();
			return false;
		}
	};

	var wc_terms_toggle = {
		init: function() {
			$( document.body ).on( 'click', 'a.woocommerce-terms-and-conditions-link', this.toggle_terms );
		},

		toggle_terms: function() {
			if ( $( '.woocommerce-terms-and-conditions' ).length ) {
				$( '.woocommerce-terms-and-conditions' ).slideToggle( function() {
					var link_toggle = $( '.woocommerce-terms-and-conditions-link' );

					if ( $( '.woocommerce-terms-and-conditions' ).is( ':visible' ) ) {
						link_toggle.addClass( 'woocommerce-terms-and-conditions-link--open' );
						link_toggle.removeClass( 'woocommerce-terms-and-conditions-link--closed' );
					} else {
						link_toggle.removeClass( 'woocommerce-terms-and-conditions-link--open' );
						link_toggle.addClass( 'woocommerce-terms-and-conditions-link--closed' );
					}
				} );

				return false;
			}
		}
	};

	wc_checkout_form.init();
	wc_checkout_coupons.init();
	wc_checkout_login_form.init();
	wc_terms_toggle.init();
});
;
 jQuery(document).ready(function($){
     $(document.body).on('change', 'input[name="payment_method"]', function() {
        $('body').trigger('update_checkout');
    });
 });;
/* global wc_add_to_cart_params */
jQuery( function( $ ) {

	if ( typeof wc_add_to_cart_params === 'undefined' ) {
		return false;
	}

	/**
	 * AddToCartHandler class.
	 */
	var AddToCartHandler = function() {
		this.requests   = [];
		this.addRequest = this.addRequest.bind( this );
		this.run        = this.run.bind( this );

		$( document.body )
			.on( 'click', '.add_to_cart_button', { addToCartHandler: this }, this.onAddToCart )
			.on( 'click', '.remove_from_cart_button', { addToCartHandler: this }, this.onRemoveFromCart )
			.on( 'added_to_cart', this.updateButton )
			.on( 'ajax_request_not_sent.adding_to_cart', this.updateButton )
			.on( 'added_to_cart removed_from_cart', { addToCartHandler: this }, this.updateFragments );
	};

	/**
	 * Add add to cart event.
	 */
	AddToCartHandler.prototype.addRequest = function( request ) {
		this.requests.push( request );

		if ( 1 === this.requests.length ) {
			this.run();
		}
	};

	/**
	 * Run add to cart events.
	 */
	AddToCartHandler.prototype.run = function() {
		var requestManager = this,
			originalCallback = requestManager.requests[0].complete;

		requestManager.requests[0].complete = function() {
			if ( typeof originalCallback === 'function' ) {
				originalCallback();
			}

			requestManager.requests.shift();

			if ( requestManager.requests.length > 0 ) {
				requestManager.run();
			}
		};

		$.ajax( this.requests[0] );
	};

	/**
	 * Handle the add to cart event.
	 */
	AddToCartHandler.prototype.onAddToCart = function( e ) {
		var $thisbutton = $( this );

		if ( $thisbutton.is( '.ajax_add_to_cart' ) ) {
			if ( ! $thisbutton.attr( 'data-product_id' ) ) {
				return true;
			}

			e.preventDefault();

			$thisbutton.removeClass( 'added' );
			$thisbutton.addClass( 'loading' );

			// Allow 3rd parties to validate and quit early.
			if ( false === $( document.body ).triggerHandler( 'should_send_ajax_request.adding_to_cart', [ $thisbutton ] ) ) { 
				$( document.body ).trigger( 'ajax_request_not_sent.adding_to_cart', [ false, false, $thisbutton ] );
				return true;
			}

			var data = {};

			// Fetch changes that are directly added by calling $thisbutton.data( key, value )
			$.each( $thisbutton.data(), function( key, value ) {
				data[ key ] = value;
			});

			// Fetch data attributes in $thisbutton. Give preference to data-attributes because they can be directly modified by javascript
			// while `.data` are jquery specific memory stores.
			$.each( $thisbutton[0].dataset, function( key, value ) {
				data[ key ] = value;
			});

			// Trigger event.
			$( document.body ).trigger( 'adding_to_cart', [ $thisbutton, data ] );

			e.data.addToCartHandler.addRequest({
				type: 'POST',
				url: wc_add_to_cart_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'add_to_cart' ),
				data: data,
				success: function( response ) {
					if ( ! response ) {
						return;
					}

					if ( response.error && response.product_url ) {
						window.location = response.product_url;
						return;
					}

					// Redirect to cart option
					if ( wc_add_to_cart_params.cart_redirect_after_add === 'yes' ) {
						window.location = wc_add_to_cart_params.cart_url;
						return;
					}

					// Trigger event so themes can refresh other areas.
					$( document.body ).trigger( 'added_to_cart', [ response.fragments, response.cart_hash, $thisbutton ] );
				},
				dataType: 'json'
			});
		}
	};

	/**
	 * Update fragments after remove from cart event in mini-cart.
	 */
	AddToCartHandler.prototype.onRemoveFromCart = function( e ) {
		var $thisbutton = $( this ),
			$row        = $thisbutton.closest( '.woocommerce-mini-cart-item' );

		e.preventDefault();

		$row.block({
			message: null,
			overlayCSS: {
				opacity: 0.6
			}
		});

		e.data.addToCartHandler.addRequest({
			type: 'POST',
			url: wc_add_to_cart_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'remove_from_cart' ),
			data: {
				cart_item_key : $thisbutton.data( 'cart_item_key' )
			},
			success: function( response ) {
				if ( ! response || ! response.fragments ) {
					window.location = $thisbutton.attr( 'href' );
					return;
				}

				$( document.body ).trigger( 'removed_from_cart', [ response.fragments, response.cart_hash, $thisbutton ] );
			},
			error: function() {
				window.location = $thisbutton.attr( 'href' );
				return;
			},
			dataType: 'json'
		});
	};

	/**
	 * Update cart page elements after add to cart events.
	 */
	AddToCartHandler.prototype.updateButton = function( e, fragments, cart_hash, $button ) {
		$button = typeof $button === 'undefined' ? false : $button;

		if ( $button ) {
			$button.removeClass( 'loading' );
			
			if ( fragments ) {
				$button.addClass( 'added' );
			}

			// View cart text.
			if ( fragments && ! wc_add_to_cart_params.is_cart && $button.parent().find( '.added_to_cart' ).length === 0 ) {
				$button.after( '<a href="' + wc_add_to_cart_params.cart_url + '" class="added_to_cart wc-forward" title="' +
					wc_add_to_cart_params.i18n_view_cart + '">' + wc_add_to_cart_params.i18n_view_cart + '</a>' );
			}

			$( document.body ).trigger( 'wc_cart_button_updated', [ $button ] );
		}
	};

	/**
	 * Update fragments after add to cart events.
	 */
	AddToCartHandler.prototype.updateFragments = function( e, fragments ) {
		if ( fragments ) {
			$.each( fragments, function( key ) {
				$( key )
					.addClass( 'updating' )
					.fadeTo( '400', '0.6' )
					.block({
						message: null,
						overlayCSS: {
							opacity: 0.6
						}
					});
			});

			$.each( fragments, function( key, value ) {
				$( key ).replaceWith( value );
				$( key ).stop( true ).css( 'opacity', '1' ).unblock();
			});

			$( document.body ).trigger( 'wc_fragments_loaded' );
		}
	};

	/**
	 * Init AddToCartHandler.
	 */
	new AddToCartHandler();
});
;
/* global wc_cart_fragments_params, Cookies */
jQuery( function( $ ) {

	// wc_cart_fragments_params is required to continue, ensure the object exists
	if ( typeof wc_cart_fragments_params === 'undefined' ) {
		return false;
	}

	/* Storage Handling */
	var $supports_html5_storage = true,
		cart_hash_key           = wc_cart_fragments_params.cart_hash_key;

	try {
		$supports_html5_storage = ( 'sessionStorage' in window && window.sessionStorage !== null );
		window.sessionStorage.setItem( 'wc', 'test' );
		window.sessionStorage.removeItem( 'wc' );
		window.localStorage.setItem( 'wc', 'test' );
		window.localStorage.removeItem( 'wc' );
	} catch( err ) {
		$supports_html5_storage = false;
	}

	/* Cart session creation time to base expiration on */
	function set_cart_creation_timestamp() {
		if ( $supports_html5_storage ) {
			sessionStorage.setItem( 'wc_cart_created', ( new Date() ).getTime() );
		}
	}

	/** Set the cart hash in both session and local storage */
	function set_cart_hash( cart_hash ) {
		if ( $supports_html5_storage ) {
			localStorage.setItem( cart_hash_key, cart_hash );
			sessionStorage.setItem( cart_hash_key, cart_hash );
		}
	}

	var $fragment_refresh = {
		url: wc_cart_fragments_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'get_refreshed_fragments' ),
		type: 'POST',
		data: {
			time: new Date().getTime()
		},
		timeout: wc_cart_fragments_params.request_timeout,
		success: function( data ) {
			if ( data && data.fragments ) {

				$.each( data.fragments, function( key, value ) {
					$( key ).replaceWith( value );
				});

				if ( $supports_html5_storage ) {
					sessionStorage.setItem( wc_cart_fragments_params.fragment_name, JSON.stringify( data.fragments ) );
					set_cart_hash( data.cart_hash );

					if ( data.cart_hash ) {
						set_cart_creation_timestamp();
					}
				}

				$( document.body ).trigger( 'wc_fragments_refreshed' );
			}
		},
		error: function() {
			$( document.body ).trigger( 'wc_fragments_ajax_error' );
		}
	};

	/* Named callback for refreshing cart fragment */
	function refresh_cart_fragment() {
		$.ajax( $fragment_refresh );
	}

	/* Cart Handling */
	if ( $supports_html5_storage ) {

		var cart_timeout = null,
			day_in_ms    = ( 24 * 60 * 60 * 1000 );

		$( document.body ).on( 'wc_fragment_refresh updated_wc_div', function() {
			refresh_cart_fragment();
		});

		$( document.body ).on( 'added_to_cart removed_from_cart', function( event, fragments, cart_hash ) {
			var prev_cart_hash = sessionStorage.getItem( cart_hash_key );

			if ( prev_cart_hash === null || prev_cart_hash === undefined || prev_cart_hash === '' ) {
				set_cart_creation_timestamp();
			}

			sessionStorage.setItem( wc_cart_fragments_params.fragment_name, JSON.stringify( fragments ) );
			set_cart_hash( cart_hash );
		});

		$( document.body ).on( 'wc_fragments_refreshed', function() {
			clearTimeout( cart_timeout );
			cart_timeout = setTimeout( refresh_cart_fragment, day_in_ms );
		} );

		// Refresh when storage changes in another tab
		$( window ).on( 'storage onstorage', function ( e ) {
			if (
				cart_hash_key === e.originalEvent.key && localStorage.getItem( cart_hash_key ) !== sessionStorage.getItem( cart_hash_key )
			) {
				refresh_cart_fragment();
			}
		});

		// Refresh when page is shown after back button (safari)
		$( window ).on( 'pageshow' , function( e ) {
			if ( e.originalEvent.persisted ) {
				$( '.widget_shopping_cart_content' ).empty();
				$( document.body ).trigger( 'wc_fragment_refresh' );
			}
		} );

		try {
			var wc_fragments = $.parseJSON( sessionStorage.getItem( wc_cart_fragments_params.fragment_name ) ),
				cart_hash    = sessionStorage.getItem( cart_hash_key ),
				cookie_hash  = Cookies.get( 'woocommerce_cart_hash'),
				cart_created = sessionStorage.getItem( 'wc_cart_created' );

			if ( cart_hash === null || cart_hash === undefined || cart_hash === '' ) {
				cart_hash = '';
			}

			if ( cookie_hash === null || cookie_hash === undefined || cookie_hash === '' ) {
				cookie_hash = '';
			}

			if ( cart_hash && ( cart_created === null || cart_created === undefined || cart_created === '' ) ) {
				throw 'No cart_created';
			}

			if ( cart_created ) {
				var cart_expiration = ( ( 1 * cart_created ) + day_in_ms ),
					timestamp_now   = ( new Date() ).getTime();
				if ( cart_expiration < timestamp_now ) {
					throw 'Fragment expired';
				}
				cart_timeout = setTimeout( refresh_cart_fragment, ( cart_expiration - timestamp_now ) );
			}

			if ( wc_fragments && wc_fragments['div.widget_shopping_cart_content'] && cart_hash === cookie_hash ) {

				$.each( wc_fragments, function( key, value ) {
					$( key ).replaceWith(value);
				});

				$( document.body ).trigger( 'wc_fragments_loaded' );
			} else {
				throw 'No fragment';
			}

		} catch( err ) {
			refresh_cart_fragment();
		}

	} else {
		refresh_cart_fragment();
	}

	/* Cart Hiding */
	if ( Cookies.get( 'woocommerce_items_in_cart' ) > 0 ) {
		$( '.hide_cart_widget_if_empty' ).closest( '.widget_shopping_cart' ).show();
	} else {
		$( '.hide_cart_widget_if_empty' ).closest( '.widget_shopping_cart' ).hide();
	}

	$( document.body ).on( 'adding_to_cart', function() {
		$( '.hide_cart_widget_if_empty' ).closest( '.widget_shopping_cart' ).show();
	});

	// Customiser support.
	var hasSelectiveRefresh = (
		'undefined' !== typeof wp &&
		wp.customize &&
		wp.customize.selectiveRefresh &&
		wp.customize.widgetsPreview &&
		wp.customize.widgetsPreview.WidgetPartial
	);
	if ( hasSelectiveRefresh ) {
		wp.customize.selectiveRefresh.bind( 'partial-content-rendered', function() {
			refresh_cart_fragment();
		} );
	}
});
;
;

		jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
			var jetpackLazyImagesLoadEvent;
			try {
				jetpackLazyImagesLoadEvent = new Event( 'jetpack-lazy-images-load', {
					bubbles: true,
					cancelable: true
				} );
			} catch ( e ) {
				jetpackLazyImagesLoadEvent = document.createEvent( 'Event' )
				jetpackLazyImagesLoadEvent.initEvent( 'jetpack-lazy-images-load', true, true );
			}
			jQuery( 'body' ).get( 0 ).dispatchEvent( jetpackLazyImagesLoadEvent );
		} );
	;
var mailchimp,
    mailchimp_cart,
    mailchimp_billing_email,
    mailchimp_username_email,
    mailchimp_registration_email,
    mailchimp_submitted_email = false,
    mailchimpReady = function (a) { /in/.test(document.readyState) ? setTimeout("mailchimpReady(" + a + ")", 9) : a(); };

function mailchimpGetCurrentUserByHash(a) {
    try {
        var b = mailchimp_public_data.ajax_url + "?action=mailchimp_get_user_by_hash&hash=" + a, c = new XMLHttpRequest;
        c.open("POST", b, !0), c.onload = function () {
            if (c.status >= 200 && c.status < 400) {
                var a = JSON.parse(c.responseText);
                if (!a) return;
                mailchimp_cart.valueEmail(a.email) && mailchimp_cart.setEmail(a.email);
            }
        };
        c.onerror = function () {
            console.log("mailchimp.get_email_by_hash.request.error", c.responseText)
        };
        c.setRequestHeader("Content-Type", "application/json");
        c.setRequestHeader("Accept", "application/json");
        c.send();
    } catch (a) {
        console.log("mailchimp.get_email_by_hash.error", a)
    }
}
function mailchimpHandleBillingEmail(selector) {
    try {
        if (!selector) selector = "#billing_email";
        var a = document.querySelector(selector);
        var b = void 0 !== a ? a.value : "";
        if (!mailchimp_cart.valueEmail(b) || mailchimp_submitted_email === b) { return false; }
        mailchimp_cart.setEmail(b);
        var c = mailchimp_public_data.ajax_url + "?action=mailchimp_set_user_by_email&email=" + b + "&mc_language=" + mailchimp_public_data.language;
        var d = new XMLHttpRequest;
        d.open("POST", c, !0);
        d.onload = function () {
            var successful = d.status >= 200 && d.status < 400;
            var msg = successful ? "mailchimp.handle_billing_email.request.success" : "mailchimp.handle_billing_email.request.error";
            if (successful) {
                mailchimp_submitted_email = b;
            }
            console.log(msg, d.responseText);
        };
        d.onerror = function () {
            console.log("mailchimp.handle_billing_email.request.error", d.responseText)
        };
        d.setRequestHeader("Content-Type", "application/json");
        d.setRequestHeader("Accept", "application/json");
        d.send();
        return true;
    } catch (a) {
        console.log("mailchimp.handle_billing_email.error", a); mailchimp_submitted_email = !1
    }
}

!function () {
    "use strict";

    function mailchimpCart() {

        this.email_types = "input[type=email]";
        this.regex_email = /^([A-Za-z0-9_+\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        this.current_email = null;
        this.previous_email = null;
        this.expireUser = function () {
            this.current_email = null;
            mailchimp.storage.expire("mailchimp.cart.current_email");
        };
        this.expireSaved = function () {
            mailchimp.storage.expire("mailchimp.cart.items");
        };
        this.setEmail = function (a) {
            if (!this.valueEmail(a)) return false;
            this.setPreviousEmail(this.getEmail());
            mailchimp.storage.set("mailchimp.cart.current_email", this.current_email = a);
        };
        this.getEmail = function () {
            if (this.current_email) return this.current_email;
            var a = mailchimp.storage.get("mailchimp.cart.current_email", !1);
            if (!a || !this.valueEmail(a)) return false;
            return this.current_email = a;
        };
        this.setPreviousEmail = function (a) {
            if (!this.valueEmail(a)) return false;
            mailchimp.storage.set("mailchimp.cart.previous_email", this.previous_email = a);
        };
        this.valueEmail = function (a) {
            return this.regex_email.test(a);
        };
        return this;
    }

    var g = {
        extend: function (a, b) {
            for (var c in b || {})b.hasOwnProperty(c) && (a[c] = b[c]);
            return a
        }, getQueryStringVars: function () {
            var a = window.location.search || "", b = [], c = {};
            if (a = a.substr(1), a.length) {
                b = a.split("&");
                for (var d in b) {
                    var e = b[d];
                    if ("string" == typeof e) {
                        var f = e.split("="), g = f[0], h = f[1];
                        g.length && ("undefined" == typeof c[g] && (c[g] = []), c[g].push(h))
                    }
                }
            }
            return c
        }, unEscape: function (a) {
            return decodeURIComponent(a)
        }, escape: function (a) {
            return encodeURIComponent(a)
        }, createDate: function (a, b) {
            a || (a = 0);
            var c = new Date, d = b ? c.getDate() - a : c.getDate() + a;
            return c.setDate(d), c
        }, arrayUnique: function (a) {
            for (var b = a.concat(), c = 0; c < b.length; ++c)for (var d = c + 1; d < b.length; ++d)b[c] === b[d] && b.splice(d, 1);
            return b
        }, objectCombineUnique: function (a) {
            for (var b = a[0], c = 1; c < a.length; c++) {
                var d = a[c];
                for (var e in d)b[e] = d[e]
            }
            return b
        }
    }, h = function (a, b) {
        var c = function (a, b, d) {
            return 1 === arguments.length ? c.get(a) : c.set(a, b, d)
        };
        return c.get = function (b, d) {
            return a.cookie !== c._cacheString && c._populateCache(), void 0 == c._cache[b] ? d : c._cache[b]
        }, c.defaults = {path: "/"}, c.set = function (d, e, f) {
            switch (f = {
                path: f && f.path || c.defaults.path,
                domain: f && f.domain || c.defaults.domain,
                expires: f && f.expires || c.defaults.expires,
                secure: f && f.secure !== b ? f.secure : c.defaults.secure
            }, e === b && (f.expires = -1), typeof f.expires) {
                case"number":
                    f.expires = new Date((new Date).getTime() + 1e3 * f.expires);
                    break;
                case"string":
                    f.expires = new Date(f.expires)
            }
            return d = encodeURIComponent(d) + "=" + (e + "").replace(/[^!#-+\--:<-\[\]-~]/g, encodeURIComponent), d += f.path ? ";path=" + f.path : "", d += f.domain ? ";domain=" + f.domain : "", d += f.expires ? ";expires=" + f.expires.toGMTString() : "", d += f.secure ? ";secure" : "", a.cookie = d, c
        }, c.expire = function (a, d) {
            return c.set(a, b, d)
        }, c._populateCache = function () {
            c._cache = {};
            try {
                c._cacheString = a.cookie;
                for (var d = c._cacheString.split("; "), e = 0; e < d.length; e++) {
                    var f = d[e].indexOf("="), g = decodeURIComponent(d[e].substr(0, f)), f = decodeURIComponent(d[e].substr(f + 1));
                    c._cache[g] === b && (c._cache[g] = f)
                }
            } catch (a) {
                console.log(a)
            }
        }, c.enabled = function () {
            var a = "1" === c.set("cookies.js", "1").get("cookies.js");
            return c.expire("cookies.js"), a;
        }(), c
    }(document);

    mailchimp = {storage: h, utils: g};
    mailchimp_cart = new mailchimpCart;
}();

mailchimpReady(function () {

    if (void 0 === a) {
        var a = { site_url: document.location.origin, defaulted: !0, ajax_url: document.location.origin + "/wp-admin?admin-ajax.php" };
    }

    try {
        var b = mailchimp.utils.getQueryStringVars();
        void 0 !== b.mc_cart_id && mailchimpGetCurrentUserByHash(b.mc_cart_id);

        mailchimp_username_email = document.querySelector("#username");
        mailchimp_billing_email = document.querySelector("#billing_email");
        mailchimp_registration_email = document.querySelector("#reg_email");

        if (mailchimp_billing_email) {
            mailchimp_billing_email.onblur = function () { mailchimpHandleBillingEmail('#billing_email'); };
            mailchimp_billing_email.onfocus = function () { mailchimpHandleBillingEmail('#billing_email'); }
        }

        if (mailchimp_username_email) {
            mailchimp_username_email.onblur = function () { mailchimpHandleBillingEmail('#username'); };
            mailchimp_username_email.onfocus = function () { mailchimpHandleBillingEmail('#username'); }
        }

        if (mailchimp_registration_email) {
            mailchimp_registration_email.onblur = function () { mailchimpHandleBillingEmail('#reg_email'); };
            mailchimp_registration_email.onfocus = function () { mailchimpHandleBillingEmail('#reg_email'); }
        }

    } catch (e) {
        console.log('mailchimp ready error', e);
    }
});
;
(function () { var require = undefined; var define = undefined; (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

var Cookies = require('js-cookie');

var triggers = ['mc_cid', 'mc_eid', 'mc_tc'];

function getUrlValue(key) {
  var regex = new RegExp(key + '=([^&]+)');
  var matches = regex.exec(window.location.search);

  if (matches) {
    return matches[1];
  }

  return '';
} // set mc_cid, mc_eid & mc_tc cookies if url params are set


for (var i = 0; i < triggers.length; i++) {
  var paramName = triggers[i];
  var paramValue = getUrlValue(paramName);

  if (paramValue !== '') {
    Cookies.set(paramName, paramValue, {
      expires: 14
    });
  }
} // store landing site in mc_landing_site cookie, if not set


if (!Cookies.get('mc_landing_site')) {
  Cookies.set('mc_landing_site', window.location.href, {
    expires: 7
  });
}

},{"js-cookie":2}],2:[function(require,module,exports){
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
 * JavaScript Cookie v2.2.1
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
;

(function (factory) {
  var registeredInModuleLoader;

  if (typeof define === 'function' && define.amd) {
    define(factory);
    registeredInModuleLoader = true;
  }

  if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object') {
    module.exports = factory();
    registeredInModuleLoader = true;
  }

  if (!registeredInModuleLoader) {
    var OldCookies = window.Cookies;
    var api = window.Cookies = factory();

    api.noConflict = function () {
      window.Cookies = OldCookies;
      return api;
    };
  }
})(function () {
  function extend() {
    var i = 0;
    var result = {};

    for (; i < arguments.length; i++) {
      var attributes = arguments[i];

      for (var key in attributes) {
        result[key] = attributes[key];
      }
    }

    return result;
  }

  function decode(s) {
    return s.replace(/(%[0-9A-Z]{2})+/g, decodeURIComponent);
  }

  function init(converter) {
    function api() {}

    function set(key, value, attributes) {
      if (typeof document === 'undefined') {
        return;
      }

      attributes = extend({
        path: '/'
      }, api.defaults, attributes);

      if (typeof attributes.expires === 'number') {
        attributes.expires = new Date(new Date() * 1 + attributes.expires * 864e+5);
      } // We're using "expires" because "max-age" is not supported by IE


      attributes.expires = attributes.expires ? attributes.expires.toUTCString() : '';

      try {
        var result = JSON.stringify(value);

        if (/^[\{\[]/.test(result)) {
          value = result;
        }
      } catch (e) {}

      value = converter.write ? converter.write(value, key) : encodeURIComponent(String(value)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
      key = encodeURIComponent(String(key)).replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent).replace(/[\(\)]/g, escape);
      var stringifiedAttributes = '';

      for (var attributeName in attributes) {
        if (!attributes[attributeName]) {
          continue;
        }

        stringifiedAttributes += '; ' + attributeName;

        if (attributes[attributeName] === true) {
          continue;
        } // Considers RFC 6265 section 5.2:
        // ...
        // 3.  If the remaining unparsed-attributes contains a %x3B (";")
        //     character:
        // Consume the characters of the unparsed-attributes up to,
        // not including, the first %x3B (";") character.
        // ...


        stringifiedAttributes += '=' + attributes[attributeName].split(';')[0];
      }

      return document.cookie = key + '=' + value + stringifiedAttributes;
    }

    function get(key, json) {
      if (typeof document === 'undefined') {
        return;
      }

      var jar = {}; // To prevent the for loop in the first place assign an empty array
      // in case there are no cookies at all.

      var cookies = document.cookie ? document.cookie.split('; ') : [];
      var i = 0;

      for (; i < cookies.length; i++) {
        var parts = cookies[i].split('=');
        var cookie = parts.slice(1).join('=');

        if (!json && cookie.charAt(0) === '"') {
          cookie = cookie.slice(1, -1);
        }

        try {
          var name = decode(parts[0]);
          cookie = (converter.read || converter)(cookie, name) || decode(cookie);

          if (json) {
            try {
              cookie = JSON.parse(cookie);
            } catch (e) {}
          }

          jar[name] = cookie;

          if (key === name) {
            break;
          }
        } catch (e) {}
      }

      return key ? jar[key] : jar;
    }

    api.set = set;

    api.get = function (key) {
      return get(key, false
      /* read as raw */
      );
    };

    api.getJSON = function (key) {
      return get(key, true
      /* read as json */
      );
    };

    api.remove = function (key, attributes) {
      set(key, '', extend(attributes, {
        expires: -1
      }));
    };

    api.defaults = {};
    api.withConverter = init;
    return api;
  }

  return init(function () {});
});

},{}]},{},[1]);
 })();;
/**
*  Ajax Autocomplete for jQuery, version 1.2.24
*  (c) 2014 Tomas Kirda
*
*  Ajax Autocomplete for jQuery is freely distributable under the terms of an MIT-style license.
*  For details, see the web site: https://github.com/devbridge/jQuery-Autocomplete
*/
!function(a){"use strict";"function"==typeof define&&define.amd?define(["jquery"],a):a("object"==typeof exports&&"function"==typeof require?require("jquery"):jQuery)}(function(a){"use strict";function b(c,d){var e=function(){},f=this,g={ajaxSettings:{},autoSelectFirst:!1,appendTo:document.body,serviceUrl:null,lookup:null,onSelect:null,width:"auto",minChars:1,maxHeight:300,deferRequestBy:0,params:{},formatResult:b.formatResult,delimiter:null,zIndex:9999,type:"GET",noCache:!1,onSearchStart:e,onSearchComplete:e,onSearchError:e,preserveInput:!1,containerClass:"autocomplete-suggestions",tabDisabled:!1,dataType:"text",currentRequest:null,triggerSelectOnValidInput:!0,preventBadQueries:!0,lookupFilter:function(a,b,c){return-1!==a.value.toLowerCase().indexOf(c)},paramName:"query",transformResult:function(b){return"string"==typeof b?a.parseJSON(b):b},showNoSuggestionNotice:!1,noSuggestionNotice:"No results",orientation:"bottom",forceFixPosition:!1};f.element=c,f.el=a(c),f.suggestions=[],f.badQueries=[],f.selectedIndex=-1,f.currentValue=f.element.value,f.intervalId=0,f.cachedResponse={},f.onChangeInterval=null,f.onChange=null,f.isLocal=!1,f.suggestionsContainer=null,f.noSuggestionsContainer=null,f.options=a.extend({},g,d),f.classes={selected:"autocomplete-selected",suggestion:"autocomplete-suggestion"},f.hint=null,f.hintValue="",f.selection=null,f.initialize(),f.setOptions(d)}var c=function(){return{escapeRegExChars:function(a){return a.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g,"\\$&")},createNode:function(a){var b=document.createElement("div");return b.className=a,b.style.position="absolute",b.style.display="none",b}}}(),d={ESC:27,TAB:9,RETURN:13,LEFT:37,UP:38,RIGHT:39,DOWN:40};b.utils=c,a.Autocomplete=b,b.formatResult=function(a,b){var d="("+c.escapeRegExChars(b)+")";return a.value.replace(new RegExp(d,"gi"),"<strong>$1</strong>").replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/"/g,"&quot;").replace(/&lt;(\/?strong)&gt;/g,"<$1>")},b.prototype={killerFn:null,initialize:function(){var c,d=this,e="."+d.classes.suggestion,f=d.classes.selected,g=d.options;d.element.setAttribute("autocomplete","off"),d.killerFn=function(b){0===a(b.target).closest("."+d.options.containerClass).length&&(d.killSuggestions(),d.disableKillerFn())},d.noSuggestionsContainer=a('<div class="autocomplete-no-suggestion"></div>').html(this.options.noSuggestionNotice).get(0),d.suggestionsContainer=b.utils.createNode(g.containerClass),c=a(d.suggestionsContainer),c.appendTo(g.appendTo),"auto"!==g.width&&c.width(g.width),c.on("mouseover.autocomplete",e,function(){d.activate(a(this).data("index"))}),c.on("mouseout.autocomplete",function(){d.selectedIndex=-1,c.children("."+f).removeClass(f)}),c.on("click.autocomplete",e,function(){d.select(a(this).data("index"))}),d.fixPositionCapture=function(){d.visible&&d.fixPosition()},a(window).on("resize.autocomplete",d.fixPositionCapture),d.el.on("keydown.autocomplete",function(a){d.onKeyPress(a)}),d.el.on("keyup.autocomplete",function(a){d.onKeyUp(a)}),d.el.on("blur.autocomplete",function(){d.onBlur()}),d.el.on("focus.autocomplete",function(){d.onFocus()}),d.el.on("change.autocomplete",function(a){d.onKeyUp(a)}),d.el.on("input.autocomplete",function(a){d.onKeyUp(a)})},onFocus:function(){var a=this;a.fixPosition(),0===a.options.minChars&&0===a.el.val().length&&a.onValueChange()},onBlur:function(){this.enableKillerFn()},abortAjax:function(){var a=this;a.currentRequest&&(a.currentRequest.abort(),a.currentRequest=null)},setOptions:function(b){var c=this,d=c.options;a.extend(d,b),c.isLocal=a.isArray(d.lookup),c.isLocal&&(d.lookup=c.verifySuggestionsFormat(d.lookup)),d.orientation=c.validateOrientation(d.orientation,"bottom"),a(c.suggestionsContainer).css({"max-height":d.maxHeight+"px",width:d.width+"px","z-index":d.zIndex})},clearCache:function(){this.cachedResponse={},this.badQueries=[]},clear:function(){this.clearCache(),this.currentValue="",this.suggestions=[]},disable:function(){var a=this;a.disabled=!0,clearInterval(a.onChangeInterval),a.abortAjax()},enable:function(){this.disabled=!1},fixPosition:function(){var b=this,c=a(b.suggestionsContainer),d=c.parent().get(0);if(d===document.body||b.options.forceFixPosition){var e=b.options.orientation,f=c.outerHeight(),g=b.el.outerHeight(),h=b.el.offset(),i={top:h.top,left:h.left};if("auto"===e){var j=a(window).height(),k=a(window).scrollTop(),l=-k+h.top-f,m=k+j-(h.top+g+f);e=Math.max(l,m)===l?"top":"bottom"}if("top"===e?i.top+=-f:i.top+=g,d!==document.body){var n,o=c.css("opacity");b.visible||c.css("opacity",0).show(),n=c.offsetParent().offset(),i.top-=n.top,i.left-=n.left,b.visible||c.css("opacity",o).hide()}"auto"===b.options.width&&(i.width=b.el.outerWidth()-2+"px"),c.css(i)}},enableKillerFn:function(){var b=this;a(document).on("click.autocomplete",b.killerFn)},disableKillerFn:function(){var b=this;a(document).off("click.autocomplete",b.killerFn)},killSuggestions:function(){var a=this;a.stopKillSuggestions(),a.intervalId=window.setInterval(function(){a.visible&&(a.el.val(a.currentValue),a.hide()),a.stopKillSuggestions()},50)},stopKillSuggestions:function(){window.clearInterval(this.intervalId)},isCursorAtEnd:function(){var a,b=this,c=b.el.val().length,d=b.element.selectionStart;return"number"==typeof d?d===c:document.selection?(a=document.selection.createRange(),a.moveStart("character",-c),c===a.text.length):!0},onKeyPress:function(a){var b=this;if(!b.disabled&&!b.visible&&a.which===d.DOWN&&b.currentValue)return void b.suggest();if(!b.disabled&&b.visible){switch(a.which){case d.ESC:b.el.val(b.currentValue),b.hide();break;case d.RIGHT:if(b.hint&&b.options.onHint&&b.isCursorAtEnd()){b.selectHint();break}return;case d.TAB:if(b.hint&&b.options.onHint)return void b.selectHint();if(-1===b.selectedIndex)return void b.hide();if(b.select(b.selectedIndex),b.options.tabDisabled===!1)return;break;case d.RETURN:if(-1===b.selectedIndex)return void b.hide();b.select(b.selectedIndex);break;case d.UP:b.moveUp();break;case d.DOWN:b.moveDown();break;default:return}a.stopImmediatePropagation(),a.preventDefault()}},onKeyUp:function(a){var b=this;if(!b.disabled){switch(a.which){case d.UP:case d.DOWN:return}clearInterval(b.onChangeInterval),b.currentValue!==b.el.val()&&(b.findBestHint(),b.options.deferRequestBy>0?b.onChangeInterval=setInterval(function(){b.onValueChange()},b.options.deferRequestBy):b.onValueChange())}},onValueChange:function(){var b=this,c=b.options,d=b.el.val(),e=b.getQuery(d);return b.selection&&b.currentValue!==e&&(b.selection=null,(c.onInvalidateSelection||a.noop).call(b.element)),clearInterval(b.onChangeInterval),b.currentValue=d,b.selectedIndex=-1,c.triggerSelectOnValidInput&&b.isExactMatch(e)?void b.select(0):void(e.length<c.minChars?b.hide():b.getSuggestions(e))},isExactMatch:function(a){var b=this.suggestions;return 1===b.length&&b[0].value.toLowerCase()===a.toLowerCase()},getQuery:function(b){var c,d=this.options.delimiter;return d?(c=b.split(d),a.trim(c[c.length-1])):b},getSuggestionsLocal:function(b){var c,d=this,e=d.options,f=b.toLowerCase(),g=e.lookupFilter,h=parseInt(e.lookupLimit,10);return c={suggestions:a.grep(e.lookup,function(a){return g(a,b,f)})},h&&c.suggestions.length>h&&(c.suggestions=c.suggestions.slice(0,h)),c},getSuggestions:function(b){var c,d,e,f,g=this,h=g.options,i=h.serviceUrl;if(h.params[h.paramName]=b,d=h.ignoreParams?null:h.params,h.onSearchStart.call(g.element,h.params)!==!1){if(a.isFunction(h.lookup))return void h.lookup(b,function(a){g.suggestions=a.suggestions,g.suggest(),h.onSearchComplete.call(g.element,b,a.suggestions)});g.isLocal?c=g.getSuggestionsLocal(b):(a.isFunction(i)&&(i=i.call(g.element,b)),e=i+"?"+a.param(d||{}),c=g.cachedResponse[e]),c&&a.isArray(c.suggestions)?(g.suggestions=c.suggestions,g.suggest(),h.onSearchComplete.call(g.element,b,c.suggestions)):g.isBadQuery(b)?h.onSearchComplete.call(g.element,b,[]):(g.abortAjax(),f={url:i,data:d,type:h.type,dataType:h.dataType},a.extend(f,h.ajaxSettings),g.currentRequest=a.ajax(f).done(function(a){var c;g.currentRequest=null,c=h.transformResult(a,b),g.processResponse(c,b,e),h.onSearchComplete.call(g.element,b,c.suggestions)}).fail(function(a,c,d){h.onSearchError.call(g.element,b,a,c,d)}))}},isBadQuery:function(a){if(!this.options.preventBadQueries)return!1;for(var b=this.badQueries,c=b.length;c--;)if(0===a.indexOf(b[c]))return!0;return!1},hide:function(){var b=this,c=a(b.suggestionsContainer);a.isFunction(b.options.onHide)&&b.visible&&b.options.onHide.call(b.element,c),b.visible=!1,b.selectedIndex=-1,clearInterval(b.onChangeInterval),a(b.suggestionsContainer).hide(),b.signalHint(null)},suggest:function(){if(0===this.suggestions.length)return void(this.options.showNoSuggestionNotice?this.noSuggestions():this.hide());var b,c=this,d=c.options,e=d.groupBy,f=d.formatResult,g=c.getQuery(c.currentValue),h=c.classes.suggestion,i=c.classes.selected,j=a(c.suggestionsContainer),k=a(c.noSuggestionsContainer),l=d.beforeRender,m="",n=function(a,c){var d=a.data[e];return b===d?"":(b=d,'<div class="autocomplete-group"><strong>'+b+"</strong></div>")};return d.triggerSelectOnValidInput&&c.isExactMatch(g)?void c.select(0):(a.each(c.suggestions,function(a,b){e&&(m+=n(b,g,a)),m+='<div class="'+h+'" data-index="'+a+'">'+f(b,g)+"</div>"}),this.adjustContainerWidth(),k.detach(),j.html(m),a.isFunction(l)&&l.call(c.element,j),c.fixPosition(),j.show(),d.autoSelectFirst&&(c.selectedIndex=0,j.scrollTop(0),j.children("."+h).first().addClass(i)),c.visible=!0,void c.findBestHint())},noSuggestions:function(){var b=this,c=a(b.suggestionsContainer),d=a(b.noSuggestionsContainer);this.adjustContainerWidth(),d.detach(),c.empty(),c.append(d),b.fixPosition(),c.show(),b.visible=!0},adjustContainerWidth:function(){var b,c=this,d=c.options,e=a(c.suggestionsContainer);"auto"===d.width&&(b=c.el.outerWidth()-2,e.width(b>0?b:300))},findBestHint:function(){var b=this,c=b.el.val().toLowerCase(),d=null;c&&(a.each(b.suggestions,function(a,b){var e=0===b.value.toLowerCase().indexOf(c);return e&&(d=b),!e}),b.signalHint(d))},signalHint:function(b){var c="",d=this;b&&(c=d.currentValue+b.value.substr(d.currentValue.length)),d.hintValue!==c&&(d.hintValue=c,d.hint=b,(this.options.onHint||a.noop)(c))},verifySuggestionsFormat:function(b){return b.length&&"string"==typeof b[0]?a.map(b,function(a){return{value:a,data:null}}):b},validateOrientation:function(b,c){return b=a.trim(b||"").toLowerCase(),-1===a.inArray(b,["auto","bottom","top"])&&(b=c),b},processResponse:function(a,b,c){var d=this,e=d.options;a.suggestions=d.verifySuggestionsFormat(a.suggestions),e.noCache||(d.cachedResponse[c]=a,e.preventBadQueries&&0===a.suggestions.length&&d.badQueries.push(b)),b===d.getQuery(d.currentValue)&&(d.suggestions=a.suggestions,d.suggest())},activate:function(b){var c,d=this,e=d.classes.selected,f=a(d.suggestionsContainer),g=f.find("."+d.classes.suggestion);return f.find("."+e).removeClass(e),d.selectedIndex=b,-1!==d.selectedIndex&&g.length>d.selectedIndex?(c=g.get(d.selectedIndex),a(c).addClass(e),c):null},selectHint:function(){var b=this,c=a.inArray(b.hint,b.suggestions);b.select(c)},select:function(a){var b=this;b.hide(),b.onSelect(a)},moveUp:function(){var b=this;if(-1!==b.selectedIndex)return 0===b.selectedIndex?(a(b.suggestionsContainer).children().first().removeClass(b.classes.selected),b.selectedIndex=-1,b.el.val(b.currentValue),void b.findBestHint()):void b.adjustScroll(b.selectedIndex-1)},moveDown:function(){var a=this;a.selectedIndex!==a.suggestions.length-1&&a.adjustScroll(a.selectedIndex+1)},adjustScroll:function(b){var c=this,d=c.activate(b);if(d){var e,f,g,h=a(d).outerHeight();e=d.offsetTop,f=a(c.suggestionsContainer).scrollTop(),g=f+c.options.maxHeight-h,f>e?a(c.suggestionsContainer).scrollTop(e):e>g&&a(c.suggestionsContainer).scrollTop(e-c.options.maxHeight+h),c.options.preserveInput||c.el.val(c.getValue(c.suggestions[b].value)),c.signalHint(null)}},onSelect:function(b){var c=this,d=c.options.onSelect,e=c.suggestions[b];c.currentValue=c.getValue(e.value),c.currentValue===c.el.val()||c.options.preserveInput||c.el.val(c.currentValue),c.signalHint(null),c.suggestions=[],c.selection=e,a.isFunction(d)&&d.call(c.element,e)},getValue:function(a){var b,c,d=this,e=d.options.delimiter;return e?(b=d.currentValue,c=b.split(e),1===c.length?a:b.substr(0,b.length-c[c.length-1].length)+a):a},dispose:function(){var b=this;b.el.off(".autocomplete").removeData("autocomplete"),b.disableKillerFn(),a(window).off("resize.autocomplete",b.fixPositionCapture),a(b.suggestionsContainer).remove()}},a.fn.autocomplete=a.fn.devbridgeAutocomplete=function(c,d){var e="autocomplete";return 0===arguments.length?this.first().data(e):this.each(function(){var f=a(this),g=f.data(e);"string"==typeof c?g&&"function"==typeof g[c]&&g[c](d):(g&&g.dispose&&g.dispose(),g=new b(this,c),f.data(e,g))})}});

jQuery(document).ready(function ($) {
    "use strict";

     $('.searchform').each(function(){

         var append = $(this).find('.live-search-results');
         var search_categories = $(this).find('.search_categories');
         var serviceUrl = flatsomeVars.ajaxurl + '?action=flatsome_ajax_search_products';
         var product_cat = '';

        if (search_categories.length && search_categories.val() !== '') {
            serviceUrl += '&product_cat=' + search_categories.val();
        }

         $(this).find('.search-field').devbridgeAutocomplete({
            minChars        : 3,
            appendTo        : append,
            triggerSelectOnValidInput: false,
            serviceUrl      : serviceUrl,
            onSearchStart   : function () {
              $('.submit-button').removeClass('loading');
              $('.submit-button').addClass('loading');
            },
            onSelect        : function (suggestion) {
                if (suggestion.id != -1) {
                    window.location.href = suggestion.url;
                }
            },
            onSearchComplete: function () {
                $('.submit-button').removeClass('loading');
            },
            beforeRender: function (container) {
                $(container).removeAttr('style');
            },
            formatResult: function (suggestion, currentValue) {
                    var pattern = '(' + $.Autocomplete.utils.escapeRegExChars(currentValue) + ')';
                    var html = '';
                    if(suggestion.img) html += '<img class="search-image" src="'+suggestion.img+'">';
                    html += '<div class="search-name">'+suggestion.value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>')+'</div>';
                    if(suggestion.price) html += '<span class="search-price">'+suggestion.price+'<span>';

                    return html;
                }
        });

          if( search_categories.length ){
                var searchForm = $(this).find('.search-field').devbridgeAutocomplete();

                search_categories.on( 'change', function( e ){

                    if( search_categories.val() != '' ) {
                        searchForm.setOptions({
                            serviceUrl:  flatsomeVars.ajaxurl + '?action=flatsome_ajax_search_products&product_cat=' + search_categories.val()
                        });
                    } else{
                        searchForm.setOptions({
                            serviceUrl:  flatsomeVars.ajaxurl + '?action=flatsome_ajax_search_products'
                        });
                    }

                    // update suggestions
                    searchForm.hide();
                    searchForm.onValueChange();
                });
            }
     });


});
;
jQuery(document).ready(function ($) {

    jQuery(document).on( 'click', '.wcml_removed_cart_items_clear', function(e){
        e.preventDefault();

        jQuery.ajax({
            type : 'post',
            url : woocommerce_params.ajax_url,
            data : {
                action: 'wcml_cart_clear_removed_items',
                wcml_nonce: jQuery('#wcml_clear_removed_items_nonce').val()
            },
            success: function(response) {
                window.location = window.location.href;
            }
        });
    });

});

;
jQuery(document).ready(function ($) {

    var empty_cart_hash = sessionStorage.getItem('woocommerce_cart_hash') == '';
    if ( empty_cart_hash || actions.is_lang_switched == 1 || actions.force_reset == 1 ) {
        wcml_reset_cart_fragments();
    }
});

function wcml_reset_cart_fragments(){
    try {
        jQuery(document).ready(function () {
            jQuery(document.body).trigger('wc_fragment_refresh');
            //backward compatibility for WC < 3.0
            sessionStorage.removeItem('wc_fragments');
        });
    } catch(err){}
};
var WPFC_TOOLBAR = {
	ajax_url: false,
	init: function(){
		var self = this;

		if(typeof ajaxurl != "undefined" || typeof wpfc_ajaxurl != "undefined"){
			self.ajax_url = (typeof ajaxurl != "undefined") ? ajaxurl : wpfc_ajaxurl;
		}else{
			alert("AjaxURL has NOT been defined");
		}

		jQuery("body").append('<div id="revert-loader-toolbar"></div>');

		jQuery("#wp-admin-bar-wpfc-toolbar-parent-default li").click(function(e){
		var id = (typeof e.target.id != "undefined" && e.target.id) ? e.target.id : jQuery(e.target).parent("li").attr("id");
		var action = "";
		
		if(id == "wp-admin-bar-wpfc-toolbar-parent-settings"){
			if(jQuery("div[id^='wpfc-modal-toolbarsettings-']").length === 0){
				self.open_settings();
			}
		}else{
			if(id == "wp-admin-bar-wpfc-toolbar-parent-delete-cache"){
				action = "wpfc_delete_cache";
			}else if(id == "wp-admin-bar-wpfc-toolbar-parent-delete-cache-and-minified"){
				action = "wpfc_delete_cache_and_minified";
			}else if(id == "wp-admin-bar-wpfc-toolbar-parent-clear-cache-of-this-page"){
				action = "wpfc_delete_current_page_cache";
			}else if(id == "wp-admin-bar-wpfc-toolbar-parent-clear-cache-of-allsites"){
				action = "wpfc_clear_cache_of_allsites";
			}

			WPFC_TOOLBAR.send({"action": action, "path" : window.location.pathname});
		}
		});
	},
	open_settings: function(){
		var self = this;

		jQuery("#revert-loader-toolbar").show();
		jQuery.ajax({
			type: 'GET',
			url: self.ajax_url,
			data : {"action": "wpfc_toolbar_get_settings", "path" : window.location.pathname},
			dataType : "json",
			cache: false, 
			success: function(data){
				if(data.success){
					var data_json = {"action": "wpfc_toolbar_save_settings", "path" : window.location.pathname, "roles" : {}};

					Wpfc_New_Dialog.dialog("wpfc-modal-toolbarsettings", {
						close: function(){
							Wpfc_New_Dialog.clone.remove();
						},
						finish: function(){
							jQuery("#" + Wpfc_New_Dialog.id).find("input[type='checkbox']:checked").each(function(i, e){
								data_json.roles[jQuery(e).attr("name")] = 1;
							});

							WPFC_TOOLBAR.send(data_json);

							Wpfc_New_Dialog.clone.remove();
					}}, function(dialog){
						jQuery("#" + Wpfc_New_Dialog.id).find("input[type='checkbox']").each(function(i, e){
							if(typeof data.roles[jQuery(e).attr("name")] != "undefined"){
								jQuery(e).attr('checked', true);
							}
						});

						Wpfc_New_Dialog.show_button("close");
						Wpfc_New_Dialog.show_button("finish");

						setTimeout(function(){
							jQuery("#revert-loader-toolbar").hide();
						}, 500);
					});
				}else{
					alert("Toolbar Settings Error!")
				}
			}
		});
	},
	send: function(data_json){
		var self = this;

		if(typeof wpfc_nonce != "undefined" && wpfc_nonce){
			data_json.nonce = wpfc_nonce;
		}

		jQuery("#revert-loader-toolbar").show();
		jQuery.ajax({
			type: 'GET',
			url: self.ajax_url,
			data : data_json,
			dataType : "json",
			cache: false, 
			success: function(data){
				if(data[1] == "error"){
					if(typeof data[2] != "undefined" && data[2] == "alert"){
						alert(data[0]);
					}else{
						Wpfc_New_Dialog.dialog("wpfc-modal-permission", {close: "default"});
						Wpfc_New_Dialog.show_button("close");
					}
				}

				if(typeof WpFcCacheStatics != "undefined"){
					WpFcCacheStatics.update();
				}else{
					jQuery("#revert-loader-toolbar").hide();
				}
			}
		});
	}
};

window.addEventListener('load', function(){
	jQuery(document).ready(function(){
		WPFC_TOOLBAR.init();
	});
});

;
/*!
 * Variation Swatches for WooCommerce v1.1.2 
 * 
 * Author: Emran Ahmed ( emran.bd.08@gmail.com ) 
 * Date: 11/26/2020, 7:20:28 PM
 * Released under the GPLv3 license.
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ({

/***/ 10:
/***/ (function(module, exports, __webpack_require__) {

jQuery(function ($) {

    Promise.resolve().then(function () {
        return __webpack_require__(11);
    }).then(function () {

        // Init on Ajax Popup :)
        $(document).on('wc_variation_form.wvs', '.variations_form:not(.wvs-loaded)', function (event) {
            $(this).WooVariationSwatches();
        });

        // Support for Jetpack's Infinite Scroll,
        $(document.body).on('post-load.wvs', function () {
            $('.variations_form').each(function () {
                $(this).wc_variation_form();
            });
        });

        // Support for Yith Infinite Scroll
        $(document).on('yith_infs_added_elem.wvs', function () {
            $('.variations_form').each(function () {
                $(this).wc_variation_form();
            });
        });

        // Support for Yith Ajax Filter
        $(document).on('yith-wcan-ajax-filtered.wvs', function () {
            $('.variations_form').each(function () {
                $(this).wc_variation_form();
            });
        });

        // Support for Woodmart theme
        $(document).on('wood-images-loaded.wvs', function () {
            $('.variations_form').each(function () {
                $(this).wc_variation_form();
            });
        });

        // Support for berocket ajax filters
        $(document).on('berocket_ajax_products_loaded.wvs', function () {
            $('.variations_form').each(function () {
                $(this).wc_variation_form();
            });
        });

        // Flatsome Infinite Scroll Support
        $('.shop-container .products').on('append.infiniteScroll', function (event, response, path) {
            $('.variations_form').each(function () {
                $(this).wc_variation_form();
            });
        });

        // FacetWP Load More
        $(document).on('facetwp-loaded.wvs', function () {
            $('.variations_form').each(function () {
                $(this).wc_variation_form();
            });
        });

        // WooCommerce Filter Nav
        $('body').on('aln_reloaded.wvs', function () {
            _.delay(function () {
                $('.variations_form').each(function () {
                    $(this).wc_variation_form();
                });
            }, 100);
        });
    });
}); // end of jquery main wrapper

/***/ }),

/***/ 11:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// ================================================================
// WooCommerce Variation Swatches
/*global _, wc_add_to_cart_variation_params, woo_variation_swatches_options */
// ================================================================

var WooVariationSwatches = function ($) {

    var Default = {};

    var WooVariationSwatches = function () {
        function WooVariationSwatches(element, config) {
            _classCallCheck(this, WooVariationSwatches);

            // Assign
            this._element = $(element);
            this._config = $.extend({}, Default, config);
            this._generated = {};
            this._out_of_stock = {};
            this._disabled = {};
            this.product_variations = this._element.data('product_variations') || [];
            this.is_ajax_variation = this.product_variations.length < 1;
            this.product_id = this._element.data('product_id');
            this.reset_variations = this._element.find('.reset_variations');
            /*this.hidden_behaviour       = $('body').hasClass('woo-variation-swatches-attribute-behavior-hide');*/
            this.is_mobile = $('body').hasClass('woo-variation-swatches-on-mobile');
            this.selected_item_template = '<span class="woo-selected-variation-item-name" data-default=""></span>';

            this._element.addClass('wvs-loaded');

            // Call
            this.init();
            this.update();

            // Trigger
            $(document).trigger('woo_variation_swatches', [this._element]);
        }

        _createClass(WooVariationSwatches, [{
            key: 'init',
            value: function init() {
                var _this2 = this;

                var _this = this;

                this._generated = this.product_variations.reduce(function (obj, variation) {

                    Object.keys(variation.attributes).map(function (attribute_name) {
                        if (!obj[attribute_name]) {
                            obj[attribute_name] = [];
                        }

                        if (variation.attributes[attribute_name]) {
                            obj[attribute_name].push(variation.attributes[attribute_name]);
                        }
                    });

                    return obj;
                }, {});

                this._out_of_stock = this.product_variations.reduce(function (obj, variation) {

                    Object.keys(variation.attributes).map(function (attribute_name) {
                        if (!obj[attribute_name]) {
                            obj[attribute_name] = [];
                        }

                        if (variation.attributes[attribute_name] && !variation.is_in_stock) {
                            obj[attribute_name].push(variation.attributes[attribute_name]);
                        }
                    });

                    return obj;
                }, {});

                // Append Selected Item Template
                if (woo_variation_swatches_options.show_variation_label) {
                    this._element.find('.variations .label').each(function (index, el) {
                        $(el).append(_this2.selected_item_template);
                    });
                }

                this._element.find('ul.variable-items-wrapper').each(function (i, el) {

                    $(this).parent().addClass('woo-variation-items-wrapper');

                    var select = $(this).siblings('select.woo-variation-raw-select');
                    var selected = '';

                    var options = $(this).siblings('select.woo-variation-raw-select').find('option');
                    var disabled = $(this).siblings('select.woo-variation-raw-select').find('option:disabled');
                    var current = $(this).siblings('select.woo-variation-raw-select').find('option:selected');
                    var eq = $(this).siblings('select.woo-variation-raw-select').find('option').eq(1);

                    var li = $(this).find('li:not(.woo-variation-swatches-variable-item-more)');
                    var reselect_clear = $(this).hasClass('reselect-clear');

                    var mouse_event_name = 'click.wvs'; // 'touchstart click';

                    var attribute = $(this).data('attribute_name');
                    var attribute_values = _this.is_ajax_variation ? [] : _this._generated[attribute];
                    var out_of_stocks = _this.is_ajax_variation ? [] : _this._out_of_stock[attribute];
                    var selects = [];
                    var disabled_selects = [];
                    var $selected_variation_item = $(this).parent().prev().find('.woo-selected-variation-item-name');

                    // For Avada FIX
                    if (options.length < 1) {
                        select = $(this).parent().find('select.woo-variation-raw-select');
                        options = $(this).parent().find('select.woo-variation-raw-select').find('option');
                        disabled = $(this).parent().find('select.woo-variation-raw-select').find('option:disabled');
                        current = $(this).parent().find('select.woo-variation-raw-select').find('option:selected');
                        eq = $(this).parent().find('select.woo-variation-raw-select').find('option').eq(1);
                    }

                    options.each(function () {
                        if ($(this).val() !== '') {
                            selects.push($(this).val());
                            selected = current ? current.val() : eq.val();
                        }
                    });

                    disabled.each(function () {
                        if ($(this).val() !== '') {
                            disabled_selects.push($(this).val());
                        }
                    });

                    var in_stocks = _.difference(selects, disabled_selects);

                    // Mark Selected
                    li.each(function (index, li) {

                        var attribute_value = $(this).attr('data-value');
                        var attribute_title = $(this).attr('data-title');

                        $(this).removeClass('selected disabled').addClass('disabled');
                        $(this).attr('aria-checked', 'false');
                        $(this).attr('tabindex', '-1');

                        if ($(this).hasClass('radio-variable-item')) {
                            $(this).find('input.wvs-radio-variable-item:radio').prop('disabled', true).prop('checked', false);
                        }

                        // Default Selected
                        // We can't use es6 includes for IE11
                        // in_stocks.includes(attribute_value)
                        // _.contains(in_stocks, attribute_value)

                        if (_.contains(in_stocks, attribute_value)) {

                            $(this).removeClass('selected disabled');
                            $(this).removeAttr('aria-hidden');
                            $(this).attr('tabindex', '0');

                            $(this).find('input.wvs-radio-variable-item:radio').prop('disabled', false);

                            if (attribute_value === selected) {

                                $(this).addClass('selected');
                                $(this).attr('aria-checked', 'true');

                                if (woo_variation_swatches_options.show_variation_label) {
                                    $selected_variation_item.text(': ' + attribute_title);
                                }

                                if ($(this).hasClass('radio-variable-item')) {
                                    $(this).find('input.wvs-radio-variable-item:radio').prop('checked', true);
                                }
                            }
                        }
                    });

                    // Trigger Select event based on list

                    if (reselect_clear) {

                        // Non Selected Item Should Select
                        $(this).on(mouse_event_name, 'li:not(.selected):not(.radio-variable-item):not(.woo-variation-swatches-variable-item-more)', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            var value = $(this).data('value');
                            select.val(value).trigger('change');
                            select.trigger('click');

                            select.trigger('focusin');

                            if (_this.is_mobile) {
                                select.trigger('touchstart');
                            }

                            $(this).trigger('focus'); // Mobile tooltip
                            $(this).trigger('wvs-selected-item', [value, select, _this._element]); // Custom Event for li
                        });

                        // Selected Item Should Non Select
                        $(this).on(mouse_event_name, 'li.selected:not(.radio-variable-item):not(.woo-variation-swatches-variable-item-more)', function (e) {
                            e.preventDefault();
                            e.stopPropagation();

                            var value = $(this).val();

                            select.val('').trigger('change');
                            select.trigger('click');

                            select.trigger('focusin');

                            if (_this.is_mobile) {
                                select.trigger('touchstart');
                            }

                            $(this).trigger('focus'); // Mobile tooltip

                            $(this).trigger('wvs-unselected-item', [value, select, _this._element]); // Custom Event for li
                        });

                        // RADIO

                        // On Click trigger change event on Radio button
                        $(this).on(mouse_event_name, 'input.wvs-radio-variable-item:radio', function (e) {

                            e.stopPropagation();

                            $(this).trigger('change.wvs', { radioChange: true });
                        });

                        $(this).on('change.wvs', 'input.wvs-radio-variable-item:radio', function (e, params) {

                            e.preventDefault();
                            e.stopPropagation();

                            if (params && params.radioChange) {

                                var value = $(this).val();
                                var is_selected = $(this).parent('li.radio-variable-item').hasClass('selected');

                                if (is_selected) {
                                    select.val('').trigger('change');
                                    $(this).parent('li.radio-variable-item').trigger('wvs-unselected-item', [value, select, _this._element]); // Custom Event for li
                                } else {
                                    select.val(value).trigger('change');
                                    $(this).parent('li.radio-variable-item').trigger('wvs-selected-item', [value, select, _this._element]); // Custom Event for li
                                }

                                select.trigger('click');
                                select.trigger('focusin');
                                if (_this.is_mobile) {
                                    select.trigger('touchstart');
                                }
                            }
                        });
                    } else {

                        $(this).on(mouse_event_name, 'li:not(.radio-variable-item):not(.woo-variation-swatches-variable-item-more)', function (event) {

                            event.preventDefault();
                            event.stopPropagation();

                            var value = $(this).data('value');
                            select.val(value).trigger('change');
                            select.trigger('click');
                            select.trigger('focusin');
                            if (_this.is_mobile) {
                                select.trigger('touchstart');
                            }

                            $(this).trigger('focus'); // Mobile tooltip

                            $(this).trigger('wvs-selected-item', [value, select, _this._element]); // Custom Event for li
                        });

                        // Radio
                        $(this).on('change.wvs', 'input.wvs-radio-variable-item:radio', function (event) {
                            event.preventDefault();
                            event.stopPropagation();

                            var value = $(this).val();

                            select.val(value).trigger('change');
                            select.trigger('click');
                            select.trigger('focusin');

                            if (_this.is_mobile) {
                                select.trigger('touchstart');
                            }

                            // Radio
                            $(this).parent('li.radio-variable-item').removeClass('selected disabled').addClass('selected');
                            $(this).parent('li.radio-variable-item').trigger('wvs-selected-item', [value, select, _this._element]); // Custom Event for li
                        });
                    }

                    // Keyboard Access
                    $(this).on('keydown.wvs', 'li:not(.disabled):not(.woo-variation-swatches-variable-item-more)', function (event) {
                        if (event.keyCode && 32 === event.keyCode || event.key && " " === event.key || event.keyCode && 13 === event.keyCode || event.key && "enter" === event.key.toLowerCase()) {
                            event.preventDefault();
                            $(this).trigger(mouse_event_name);
                        }
                    });
                });

                this._element.trigger('woo_variation_swatches_init', [this, this.product_variations]);

                $(document).trigger('woo_variation_swatches_loaded', [this._element, this.product_variations]);
            }
        }, {
            key: 'update',
            value: function update() {

                var _this = this;

                this._element.on('woocommerce_variation_has_changed.wvs', function (event) {

                    // Don't use any propagation. It will disable composit product functionality
                    // event.stopPropagation();

                    $(this).find('ul.variable-items-wrapper').each(function (index, el) {

                        var select = $(this).siblings('select.woo-variation-raw-select');
                        var selected = '';

                        var options = $(this).siblings('select.woo-variation-raw-select').find('option');
                        var disabled = $(this).siblings('select.woo-variation-raw-select').find('option:disabled');
                        var current = $(this).siblings('select.woo-variation-raw-select').find('option:selected');
                        var eq = $(this).siblings('select.woo-variation-raw-select').find('option').eq(1);
                        var li = $(this).find('li:not(.woo-variation-swatches-variable-item-more)');

                        //let reselect_clear   = $(this).hasClass('reselect-clear');
                        //let is_mobile        = $('body').hasClass('woo-variation-swatches-on-mobile');
                        //let mouse_event_name = 'click.wvs'; // 'touchstart click';

                        var attribute = $(this).data('attribute_name');
                        var attribute_values = _this.is_ajax_variation ? [] : _this._generated[attribute];
                        var out_of_stocks = _this.is_ajax_variation ? [] : _this._out_of_stock[attribute];
                        var selects = [];
                        var disabled_selects = [];
                        var $selected_variation_item = $(this).parent().prev().find('.woo-selected-variation-item-name');

                        // For Avada FIX
                        if (options.length < 1) {
                            select = $(this).parent().find('select.woo-variation-raw-select');
                            options = $(this).parent().find('select.woo-variation-raw-select').find('option');
                            disabled = $(this).parent().find('select.woo-variation-raw-select').find('option:disabled');
                            current = $(this).parent().find('select.woo-variation-raw-select').find('option:selected');
                            eq = $(this).parent().find('select.woo-variation-raw-select').find('option').eq(1);
                        }

                        options.each(function () {
                            if ($(this).val() !== '') {
                                selects.push($(this).val());
                                selected = current ? current.val() : eq.val();
                            }
                        });

                        disabled.each(function () {
                            if ($(this).val() !== '') {
                                disabled_selects.push($(this).val());
                            }
                        });

                        var in_stocks = _.difference(selects, disabled_selects);

                        if (_this.is_ajax_variation) {

                            li.each(function (index, el) {

                                var attribute_value = $(this).attr('data-value');
                                var attribute_title = $(this).attr('data-title');

                                $(this).removeClass('selected disabled');
                                $(this).attr('aria-checked', 'false');

                                // To Prevent blink
                                if (selected.length < 1 && woo_variation_swatches_options.show_variation_label) {
                                    $selected_variation_item.text('');
                                }

                                if (attribute_value === selected) {
                                    $(this).addClass('selected');
                                    $(this).attr('aria-checked', 'true');

                                    if (woo_variation_swatches_options.show_variation_label) {
                                        $selected_variation_item.text(': ' + attribute_title);
                                    }

                                    if ($(this).hasClass('radio-variable-item')) {
                                        $(this).find('input.wvs-radio-variable-item:radio').prop('disabled', false).prop('checked', true);
                                    }
                                }

                                $(this).trigger('wvs-item-updated', [selected, attribute_value, _this]);
                            });
                        } else {

                            li.each(function (index, el) {

                                var attribute_value = $(this).attr('data-value');
                                var attribute_title = $(this).attr('data-title');

                                $(this).removeClass('selected disabled').addClass('disabled');
                                $(this).attr('aria-checked', 'false');
                                $(this).attr('tabindex', '-1');

                                if ($(this).hasClass('radio-variable-item')) {
                                    $(this).find('input.wvs-radio-variable-item:radio').prop('disabled', true).prop('checked', false);
                                }

                                // if (_.contains(selects, value))
                                // if (_.indexOf(selects, value) !== -1)
                                // if (selects.includes(value))

                                // We can't use es6 includes for IE11
                                // in_stocks.includes(attribute_value)
                                // _.contains(in_stocks, attribute_value)

                                // Make Selected // selects.includes(attribute_value) // in_stocks
                                if (_.contains(in_stocks, attribute_value)) {

                                    $(this).removeClass('selected disabled');
                                    $(this).removeAttr('aria-hidden');
                                    $(this).attr('tabindex', '0');

                                    $(this).find('input.wvs-radio-variable-item:radio').prop('disabled', false);

                                    // To Prevent blink
                                    if (selected.length < 1 && woo_variation_swatches_options.show_variation_label) {
                                        $selected_variation_item.text('');
                                    }

                                    if (attribute_value === selected) {

                                        $(this).addClass('selected');
                                        $(this).attr('aria-checked', 'true');

                                        if (woo_variation_swatches_options.show_variation_label) {
                                            $selected_variation_item.text(': ' + attribute_title);
                                        }

                                        if ($(this).hasClass('radio-variable-item')) {
                                            $(this).find('input.wvs-radio-variable-item:radio').prop('checked', true);
                                        }
                                    }
                                }

                                $(this).trigger('wvs-item-updated', [selected, attribute_value, _this]);
                            });
                        }

                        // Items Updated
                        $(this).trigger('wvs-items-updated');
                    });
                });
            }
        }], [{
            key: '_jQueryInterface',
            value: function _jQueryInterface(config) {
                return this.each(function () {
                    new WooVariationSwatches(this, config);
                });
            }
        }]);

        return WooVariationSwatches;
    }();

    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $.fn['WooVariationSwatches'] = WooVariationSwatches._jQueryInterface;
    $.fn['WooVariationSwatches'].Constructor = WooVariationSwatches;
    $.fn['WooVariationSwatches'].noConflict = function () {
        $.fn['WooVariationSwatches'] = $.fn['WooVariationSwatches'];
        return WooVariationSwatches._jQueryInterface;
    };

    return WooVariationSwatches;
}(jQuery);

/* harmony default export */ __webpack_exports__["default"] = (WooVariationSwatches);

/***/ }),

/***/ 9:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(10);


/***/ })

/******/ });;
!function(t){var e={};function i(n){if(e[n])return e[n].exports;var o=e[n]={i:n,l:!1,exports:{}};return t[n].call(o.exports,o,o.exports,i),o.l=!0,o.exports}i.m=t,i.c=e,i.d=function(t,e,n){i.o(t,e)||Object.defineProperty(t,e,{enumerable:!0,get:n})},i.r=function(t){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0})},i.t=function(t,e){if(1&e&&(t=i(t)),8&e)return t;if(4&e&&"object"==typeof t&&t&&t.__esModule)return t;var n=Object.create(null);if(i.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:t}),2&e&&"string"!=typeof t)for(var o in t)i.d(n,o,function(e){return t[e]}.bind(null,o));return n},i.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return i.d(e,"a",e),e},i.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},i.p="",i(i.s=26)}([function(t,e){var i;i=function(){return this}();try{i=i||new Function("return this")()}catch(t){"object"==typeof window&&(i=window)}t.exports=i},function(t,e){t.exports=window.jQuery},function(t,e,i){"use strict";
/*! npm.im/object-fit-images 3.2.4 */var n="bfred-it:object-fit-images",o=/(object-fit|object-position)\s*:\s*([-.\w\s%]+)/g,r="undefined"==typeof Image?{style:{"object-position":1}}:new Image,s="object-fit"in r.style,a="object-position"in r.style,l="background-size"in r.style,c="string"==typeof r.currentSrc,u=r.getAttribute,h=r.setAttribute,d=!1;function p(t,e,i){var n="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='"+(e||1)+"' height='"+(i||0)+"'%3E%3C/svg%3E";u.call(t,"src")!==n&&h.call(t,"src",n)}function f(t,e){t.naturalWidth?e(t):setTimeout(f,100,t,e)}function m(t){var e=function(t){for(var e,i=getComputedStyle(t).fontFamily,n={};null!==(e=o.exec(i));)n[e[1]]=e[2];return n}(t),i=t[n];if(e["object-fit"]=e["object-fit"]||"fill",!i.img){if("fill"===e["object-fit"])return;if(!i.skipTest&&s&&!e["object-position"])return}if(!i.img){i.img=new Image(t.width,t.height),i.img.srcset=u.call(t,"data-ofi-srcset")||t.srcset,i.img.src=u.call(t,"data-ofi-src")||t.src,h.call(t,"data-ofi-src",t.src),t.srcset&&h.call(t,"data-ofi-srcset",t.srcset),p(t,t.naturalWidth||t.width,t.naturalHeight||t.height),t.srcset&&(t.srcset="");try{!function(t){var e={get:function(e){return t[n].img[e||"src"]},set:function(e,i){return t[n].img[i||"src"]=e,h.call(t,"data-ofi-"+i,e),m(t),e}};Object.defineProperty(t,"src",e),Object.defineProperty(t,"currentSrc",{get:function(){return e.get("currentSrc")}}),Object.defineProperty(t,"srcset",{get:function(){return e.get("srcset")},set:function(t){return e.set(t,"srcset")}})}(t)}catch(t){window.console&&console.warn("https://bit.ly/ofi-old-browser")}}!function(t){if(t.srcset&&!c&&window.picturefill){var e=window.picturefill._;t[e.ns]&&t[e.ns].evaled||e.fillImg(t,{reselect:!0}),t[e.ns].curSrc||(t[e.ns].supported=!1,e.fillImg(t,{reselect:!0})),t.currentSrc=t[e.ns].curSrc||t.src}}(i.img),t.style.backgroundImage='url("'+(i.img.currentSrc||i.img.src).replace(/"/g,'\\"')+'")',t.style.backgroundPosition=e["object-position"]||"center",t.style.backgroundRepeat="no-repeat",t.style.backgroundOrigin="content-box",/scale-down/.test(e["object-fit"])?f(i.img,(function(){i.img.naturalWidth>t.width||i.img.naturalHeight>t.height?t.style.backgroundSize="contain":t.style.backgroundSize="auto"})):t.style.backgroundSize=e["object-fit"].replace("none","auto").replace("fill","100% 100%"),f(i.img,(function(e){p(t,e.naturalWidth,e.naturalHeight)}))}function g(t,e){var i=!d&&!t;if(e=e||{},t=t||"img",a&&!e.skipTest||!l)return!1;"img"===t?t=document.getElementsByTagName("img"):"string"==typeof t?t=document.querySelectorAll(t):"length"in t||(t=[t]);for(var o=0;o<t.length;o++)t[o][n]=t[o][n]||{skipTest:e.skipTest},m(t[o]);i&&(document.body.addEventListener("load",(function(t){"IMG"===t.target.tagName&&g(t.target,{skipTest:e.skipTest})}),!0),d=!0,t="img"),e.watchMQ&&window.addEventListener("resize",g.bind(null,t,{skipTest:e.skipTest}))}g.supportsObjectFit=s,g.supportsObjectPosition=a,function(){function t(t,e){return t[n]&&t[n].img&&("src"===e||"srcset"===e)?t[n].img:t}a||(HTMLImageElement.prototype.getAttribute=function(e){return u.call(t(this,e),e)},HTMLImageElement.prototype.setAttribute=function(e,i){return h.call(t(this,e),e,String(i))})}(),t.exports=g},function(t,e,i){(function(e){/*! cookie function. get, set, or forget a cookie. [c]2014 @scottjehl, Filament Group, Inc. Licensed MIT */var i;i=void 0!==e?e:this,t.exports=function(t,e,n){if(void 0===e){var o=("; "+i.document.cookie).split("; "+t+"=");return 2===o.length?o.pop().split(";").shift():null}!1===e&&(n=-1);var r="";if(n){var s=new Date;s.setTime(s.getTime()+24*n*60*60*1e3),r="; expires="+s.toGMTString()}i.document.cookie=t+"="+e+r+"; path=/"}}).call(this,i(0))},function(t,e){function i(e){return"function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?t.exports=i=function(t){return typeof t}:t.exports=i=function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t},i(e)}t.exports=i},function(t,e,i){void 0!==t.exports&&(t.exports=function(t){"use strict";var e=navigator.userAgent.indexOf("Edge/")>=0,i=new Image,n="object-fit"in i.style&&!e,o="object-position"in i.style&&!e,r=/(object-fit|object-position)\s*:\s*([-\w\s%]+)/g;function s(t){for(var e=getComputedStyle(t).fontFamily,i=null,n={};null!==(i=r.exec(e));)n[i[1]]=i[2];return n["object-position"]?function(t){~t["object-position"].indexOf("left")?t["object-position-x"]="left":~t["object-position"].indexOf("right")?t["object-position-x"]="right":t["object-position-x"]="center";~t["object-position"].indexOf("top")?t["object-position-y"]="top":~t["object-position"].indexOf("bottom")?t["object-position-y"]="bottom":t["object-position-y"]="center";return t}(n):n}function a(t,e){if("fill"!==e["object-fit"]){var i=t.style,n=window.getComputedStyle(t),o=document.createElement("object-fit");o.appendChild(t.parentNode.replaceChild(o,t));var r={height:"100%",width:"100%",boxSizing:"content-box",display:"inline-block",overflow:"hidden"};for(var s in"backgroundColor backgroundImage borderColor borderStyle borderWidth bottom fontSize lineHeight left opacity margin position right top visibility".replace(/\w+/g,(function(t){r[t]=n[t]})),r)o.style[s]=r[s];i.border=i.margin=i.padding=0,i.display="block",i.opacity=1,t.addEventListener("loadedmetadata",a),window.addEventListener("optimizedResize",a),t.readyState>=1&&(t.removeEventListener("loadedmetadata",a),a())}function a(){var n=t.videoWidth/t.videoHeight,r=o.clientWidth,s=o.clientHeight,a=r/s,l=0,c=0;i.marginLeft=i.marginTop=0,(n<a?"contain"===e["object-fit"]:"cover"===e["object-fit"])?(l=s*n,c=r/n,i.width=Math.round(l)+"px",i.height=s+"px","left"===e["object-position-x"]?i.marginLeft=0:"right"===e["object-position-x"]?i.marginLeft=Math.round(r-l)+"px":i.marginLeft=Math.round((r-l)/2)+"px"):(c=r/n,i.width=r+"px",i.height=Math.round(c)+"px","top"===e["object-position-y"]?i.marginTop=0:"bottom"===e["object-position-y"]?i.marginTop=Math.round(s-c)+"px":i.marginTop=Math.round((s-c)/2)+"px"),t.autoplay&&t.play()}}n&&o||(function(t){var e=-1;t?"length"in t||(t=[t]):t=document.querySelectorAll("video");for(;t[++e];){var i=s(t[e]);(i["object-fit"]||i["object-position"])&&(i["object-fit"]=i["object-fit"]||"fill",a(t[e],i))}}(t),function(t,e,i){i=i||window;var n=!1,o=null;try{o=new CustomEvent(e)}catch(t){(o=document.createEvent("Event")).initEvent(e,!0,!0)}i.addEventListener(t,(function(){n||(n=!0,requestAnimationFrame((function(){i.dispatchEvent(o),n=!1})))}))}("resize","optimizedResize"))})},function(t,e,i){var n,o;void 0===(o="function"==typeof(n=function(t,e,i){return function(t,e,i,n,o,r){function s(t){return"number"==typeof t&&!isNaN(t)}var a=this;if(a.version=function(){return"1.9.3"},a.options={useEasing:!0,useGrouping:!0,separator:",",decimal:".",easingFn:function(t,e,i,n){return i*(1-Math.pow(2,-10*t/n))*1024/1023+e},formattingFn:function(t){var e,i,n,o,r,s,l=t<0;if(t=Math.abs(t).toFixed(a.decimals),i=(e=(t+="").split("."))[0],n=e.length>1?a.options.decimal+e[1]:"",a.options.useGrouping){for(o="",r=0,s=i.length;r<s;++r)0!==r&&r%3==0&&(o=a.options.separator+o),o=i[s-r-1]+o;i=o}return a.options.numerals.length&&(i=i.replace(/[0-9]/g,(function(t){return a.options.numerals[+t]})),n=n.replace(/[0-9]/g,(function(t){return a.options.numerals[+t]}))),(l?"-":"")+a.options.prefix+i+n+a.options.suffix},prefix:"",suffix:"",numerals:[]},r&&"object"==typeof r)for(var l in a.options)r.hasOwnProperty(l)&&null!==r[l]&&(a.options[l]=r[l]);""===a.options.separator?a.options.useGrouping=!1:a.options.separator=""+a.options.separator;for(var c=0,u=["webkit","moz","ms","o"],h=0;h<u.length&&!window.requestAnimationFrame;++h)window.requestAnimationFrame=window[u[h]+"RequestAnimationFrame"],window.cancelAnimationFrame=window[u[h]+"CancelAnimationFrame"]||window[u[h]+"CancelRequestAnimationFrame"];window.requestAnimationFrame||(window.requestAnimationFrame=function(t,e){var i=(new Date).getTime(),n=Math.max(0,16-(i-c)),o=window.setTimeout((function(){t(i+n)}),n);return c=i+n,o}),window.cancelAnimationFrame||(window.cancelAnimationFrame=function(t){clearTimeout(t)}),a.initialize=function(){return!(!a.initialized&&(a.error="",a.d="string"==typeof t?document.getElementById(t):t,a.d?(a.startVal=Number(e),a.endVal=Number(i),s(a.startVal)&&s(a.endVal)?(a.decimals=Math.max(0,n||0),a.dec=Math.pow(10,a.decimals),a.duration=1e3*Number(o)||2e3,a.countDown=a.startVal>a.endVal,a.frameVal=a.startVal,a.initialized=!0,0):(a.error="[CountUp] startVal ("+e+") or endVal ("+i+") is not a number",1)):(a.error="[CountUp] target is null or undefined",1)))},a.printValue=function(t){var e=a.options.formattingFn(t);"INPUT"===a.d.tagName?this.d.value=e:"text"===a.d.tagName||"tspan"===a.d.tagName?this.d.textContent=e:this.d.innerHTML=e},a.count=function(t){a.startTime||(a.startTime=t),a.timestamp=t;var e=t-a.startTime;a.remaining=a.duration-e,a.options.useEasing?a.countDown?a.frameVal=a.startVal-a.options.easingFn(e,0,a.startVal-a.endVal,a.duration):a.frameVal=a.options.easingFn(e,a.startVal,a.endVal-a.startVal,a.duration):a.countDown?a.frameVal=a.startVal-(a.startVal-a.endVal)*(e/a.duration):a.frameVal=a.startVal+(a.endVal-a.startVal)*(e/a.duration),a.countDown?a.frameVal=a.frameVal<a.endVal?a.endVal:a.frameVal:a.frameVal=a.frameVal>a.endVal?a.endVal:a.frameVal,a.frameVal=Math.round(a.frameVal*a.dec)/a.dec,a.printValue(a.frameVal),e<a.duration?a.rAF=requestAnimationFrame(a.count):a.callback&&a.callback()},a.start=function(t){a.initialize()&&(a.callback=t,a.rAF=requestAnimationFrame(a.count))},a.pauseResume=function(){a.paused?(a.paused=!1,delete a.startTime,a.duration=a.remaining,a.startVal=a.frameVal,requestAnimationFrame(a.count)):(a.paused=!0,cancelAnimationFrame(a.rAF))},a.reset=function(){a.paused=!1,delete a.startTime,a.initialized=!1,a.initialize()&&(cancelAnimationFrame(a.rAF),a.printValue(a.startVal))},a.update=function(t){if(a.initialize()){if(!s(t=Number(t)))return void(a.error="[CountUp] update() - new endVal is not a number: "+t);a.error="",t!==a.frameVal&&(cancelAnimationFrame(a.rAF),a.paused=!1,delete a.startTime,a.startVal=a.frameVal,a.endVal=t,a.countDown=a.startVal>a.endVal,a.rAF=requestAnimationFrame(a.count))}},a.initialize()&&a.printValue(a.startVal)}})?n.call(e,i,e,t):n)||(t.exports=o)},function(t,e){t.exports=function(t,e,i){return e in t?Object.defineProperty(t,e,{value:i,enumerable:!0,configurable:!0,writable:!0}):t[e]=i,t}},,,,,,,,,,,,,,,,,,,function(t,e,i){t.exports=i(27)},function(t,e,i){"use strict";i.r(e),function(t){var e=i(3),n=i.n(e);i(28),i(29),i(30),i(31),i(32),i(33),i(34),i(35),i(36),i(37),i(39),i(40),i(41),i(42),i(43),i(44),i(45),i(46),i(47),i(48),i(49),i(50),i(51),i(52),i(53),i(54),i(55),i(56),i(57),i(58),i(59),i(70),i(60),i(61),i(62);jQuery((function(){return t.Flatsome.attach(document)})),t.cookie=n.a}.call(this,i(0))},function(t,e,i){var n,o,r,s,a,l,c,u,h,d,p,f,m,g,v,y,b,w,x,C,k,S,E,T,_,j,I,P,A;
/*!
 * Flickity PACKAGED v2.2.1
 * Touch, responsive, flickable carousels
 *
 * Licensed GPLv3 for open source use
 * or Flickity Commercial License for commercial use
 *
 * https://flickity.metafizzy.co
 * Copyright 2015-2019 Metafizzy
 */
!function(n,o){P=[i(1)],void 0===(A=function(t){return function(t,e){"use strict";var i=Array.prototype.slice,n=t.console,o=void 0===n?function(){}:function(t){n.error(t)};function r(n,r,a){function l(t,e,i){var r,s="$()."+n+'("'+e+'")';return t.each((function(t,l){var c=a.data(l,n);if(c){var u=c[e];if(u&&"_"!=e.charAt(0)){var h=u.apply(c,i);r=void 0===r?h:r}else o(s+" is not a valid method")}else o(n+" not initialized. Cannot call methods, i.e. "+s)})),void 0!==r?r:t}function c(t,e){t.each((function(t,i){var o=a.data(i,n);o?(o.option(e),o._init()):(o=new r(i,e),a.data(i,n,o))}))}(a=a||e||t.jQuery)&&(r.prototype.option||(r.prototype.option=function(t){a.isPlainObject(t)&&(this.options=a.extend(!0,this.options,t))}),a.fn[n]=function(t){if("string"==typeof t){var e=i.call(arguments,1);return l(this,t,e)}return c(this,t),this},s(a))}function s(t){!t||t&&t.bridget||(t.bridget=r)}return s(e||t.jQuery),r}(n,t)}.apply(e,P))||(t.exports=A)}(window),"undefined"!=typeof window&&window,r={id:"ev-emitter/ev-emitter",exports:{},loaded:!1},n="function"==typeof(o=function(){function t(){}var e=t.prototype;return e.on=function(t,e){if(t&&e){var i=this._events=this._events||{},n=i[t]=i[t]||[];return-1==n.indexOf(e)&&n.push(e),this}},e.once=function(t,e){if(t&&e){this.on(t,e);var i=this._onceEvents=this._onceEvents||{};return(i[t]=i[t]||{})[e]=!0,this}},e.off=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){var n=i.indexOf(e);return-1!=n&&i.splice(n,1),this}},e.emitEvent=function(t,e){var i=this._events&&this._events[t];if(i&&i.length){i=i.slice(0),e=e||[];for(var n=this._onceEvents&&this._onceEvents[t],o=0;o<i.length;o++){var r=i[o];n&&n[r]&&(this.off(t,r),delete n[r]),r.apply(this,e)}return this}},e.allOff=function(){delete this._events,delete this._onceEvents},t})?o.call(r.exports,i,r.exports,r):o,r.loaded=!0,void 0!==n||(n=r.exports),window,l={id:"get-size/get-size",exports:{},loaded:!1},s="function"==typeof(a=function(){"use strict";function t(t){var e=parseFloat(t);return-1==t.indexOf("%")&&!isNaN(e)&&e}var e="undefined"==typeof console?function(){}:function(t){console.error(t)},i=["paddingLeft","paddingRight","paddingTop","paddingBottom","marginLeft","marginRight","marginTop","marginBottom","borderLeftWidth","borderRightWidth","borderTopWidth","borderBottomWidth"],n=i.length;function o(t){var i=getComputedStyle(t);return i||e("Style returned "+i+". Are you running this code in a hidden iframe on Firefox? See https://bit.ly/getsizebug1"),i}var r,s=!1;function a(e){if(function(){if(!s){s=!0;var e=document.createElement("div");e.style.width="200px",e.style.padding="1px 2px 3px 4px",e.style.borderStyle="solid",e.style.borderWidth="1px 2px 3px 4px",e.style.boxSizing="border-box";var i=document.body||document.documentElement;i.appendChild(e);var n=o(e);r=200==Math.round(t(n.width)),a.isBoxSizeOuter=r,i.removeChild(e)}}(),"string"==typeof e&&(e=document.querySelector(e)),e&&"object"==typeof e&&e.nodeType){var l=o(e);if("none"==l.display)return function(){for(var t={width:0,height:0,innerWidth:0,innerHeight:0,outerWidth:0,outerHeight:0},e=0;e<n;e++)t[i[e]]=0;return t}();var c={};c.width=e.offsetWidth,c.height=e.offsetHeight;for(var u=c.isBorderBox="border-box"==l.boxSizing,h=0;h<n;h++){var d=i[h],p=l[d],f=parseFloat(p);c[d]=isNaN(f)?0:f}var m=c.paddingLeft+c.paddingRight,g=c.paddingTop+c.paddingBottom,v=c.marginLeft+c.marginRight,y=c.marginTop+c.marginBottom,b=c.borderLeftWidth+c.borderRightWidth,w=c.borderTopWidth+c.borderBottomWidth,x=u&&r,C=t(l.width);!1!==C&&(c.width=C+(x?0:m+b));var k=t(l.height);return!1!==k&&(c.height=k+(x?0:g+w)),c.innerWidth=c.width-(m+b),c.innerHeight=c.height-(g+w),c.outerWidth=c.width+v,c.outerHeight=c.height+y,c}}return a})?a.call(l.exports,i,l.exports,l):a,l.loaded=!0,void 0!==s||(s=l.exports),function(t,e){"use strict";h={id:"desandro-matches-selector/matches-selector",exports:{},loaded:!1},c="function"==typeof(u=e)?u.call(h.exports,i,h.exports,h):u,h.loaded=!0,void 0!==c||(c=h.exports)}(window,(function(){"use strict";var t=function(){var t=window.Element.prototype;if(t.matches)return"matches";if(t.matchesSelector)return"matchesSelector";for(var e=["webkit","moz","ms","o"],i=0;i<e.length;i++){var n=e[i]+"MatchesSelector";if(t[n])return n}}();return function(e,i){return e[t](i)}})),function(t,i){d=function(e){return function(t,e){var i={extend:function(t,e){for(var i in e)t[i]=e[i];return t},modulo:function(t,e){return(t%e+e)%e}},n=Array.prototype.slice;i.makeArray=function(t){return Array.isArray(t)?t:null==t?[]:"object"==typeof t&&"number"==typeof t.length?n.call(t):[t]},i.removeFrom=function(t,e){var i=t.indexOf(e);-1!=i&&t.splice(i,1)},i.getParent=function(t,i){for(;t.parentNode&&t!=document.body;)if(t=t.parentNode,e(t,i))return t},i.getQueryElement=function(t){return"string"==typeof t?document.querySelector(t):t},i.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},i.filterFindElements=function(t,n){t=i.makeArray(t);var o=[];return t.forEach((function(t){if(t instanceof HTMLElement)if(n){e(t,n)&&o.push(t);for(var i=t.querySelectorAll(n),r=0;r<i.length;r++)o.push(i[r])}else o.push(t)})),o},i.debounceMethod=function(t,e,i){i=i||100;var n=t.prototype[e],o=e+"Timeout";t.prototype[e]=function(){var t=this[o];clearTimeout(t);var e=arguments,r=this;this[o]=setTimeout((function(){n.apply(r,e),delete r[o]}),i)}},i.docReady=function(t){var e=document.readyState;"complete"==e||"interactive"==e?setTimeout(t):document.addEventListener("DOMContentLoaded",t)},i.toDashed=function(t){return t.replace(/(.)([A-Z])/g,(function(t,e,i){return e+"-"+i})).toLowerCase()};var o=t.console;return i.htmlInit=function(e,n){i.docReady((function(){var r=i.toDashed(n),s="data-"+r,a=document.querySelectorAll("["+s+"]"),l=document.querySelectorAll(".js-"+r),c=i.makeArray(a).concat(i.makeArray(l)),u=s+"-options",h=t.jQuery;c.forEach((function(t){var i,r=t.getAttribute(s)||t.getAttribute(u);try{i=r&&JSON.parse(r)}catch(e){return void(o&&o.error("Error parsing "+s+" on "+t.className+": "+e))}var a=new e(t,i);h&&h.data(t,n,a)}))}))},i}(t,e)}.apply(e,P=[c])}(window),window,p=function(t){return function(t,e){function i(t,e){this.element=t,this.parent=e,this.create()}var n=i.prototype;return n.create=function(){this.element.style.position="absolute",this.element.setAttribute("aria-hidden","true"),this.x=0,this.shift=0},n.destroy=function(){this.unselect(),this.element.style.position="";var t=this.parent.originSide;this.element.style[t]=""},n.getSize=function(){this.size=e(this.element)},n.setPosition=function(t){this.x=t,this.updateTarget(),this.renderPosition(t)},n.updateTarget=n.setDefaultTarget=function(){var t="left"==this.parent.originSide?"marginLeft":"marginRight";this.target=this.x+this.size[t]+this.size.width*this.parent.cellAlign},n.renderPosition=function(t){var e=this.parent.originSide;this.element.style[e]=this.parent.getPositionValue(t)},n.select=function(){this.element.classList.add("is-selected"),this.element.removeAttribute("aria-hidden")},n.unselect=function(){this.element.classList.remove("is-selected"),this.element.setAttribute("aria-hidden","true")},n.wrapShift=function(t){this.shift=t,this.renderPosition(this.x+this.parent.slideableWidth*t)},n.remove=function(){this.element.parentNode.removeChild(this.element)},i}(0,t)}.apply(e,P=[s]),window,g={id:"flickity/js/slide",exports:{},loaded:!1},f="function"==typeof(m=function(){"use strict";function t(t){this.parent=t,this.isOriginLeft="left"==t.originSide,this.cells=[],this.outerWidth=0,this.height=0}var e=t.prototype;return e.addCell=function(t){if(this.cells.push(t),this.outerWidth+=t.size.outerWidth,this.height=Math.max(t.size.outerHeight,this.height),1==this.cells.length){this.x=t.x;var e=this.isOriginLeft?"marginLeft":"marginRight";this.firstMargin=t.size[e]}},e.updateTarget=function(){var t=this.isOriginLeft?"marginRight":"marginLeft",e=this.getLastCell(),i=e?e.size[t]:0,n=this.outerWidth-(this.firstMargin+i);this.target=this.x+this.firstMargin+n*this.parent.cellAlign},e.getLastCell=function(){return this.cells[this.cells.length-1]},e.select=function(){this.cells.forEach((function(t){t.select()}))},e.unselect=function(){this.cells.forEach((function(t){t.unselect()}))},e.getCellElements=function(){return this.cells.map((function(t){return t.element}))},t})?m.call(g.exports,i,g.exports,g):m,g.loaded=!0,void 0!==f||(f=g.exports),window,v=function(t){return function(t,e){var i={startAnimation:function(){this.isAnimating||(this.isAnimating=!0,this.restingFrames=0,this.animate())},animate:function(){this.applyDragForce(),this.applySelectedAttraction();var t=this.x;if(this.integratePhysics(),this.positionSlider(),this.settle(t),this.isAnimating){var e=this;requestAnimationFrame((function(){e.animate()}))}},positionSlider:function(){var t=this.x;this.options.wrapAround&&this.cells.length>1&&(t=e.modulo(t,this.slideableWidth),t-=this.slideableWidth,this.shiftWrapCells(t)),this.setTranslateX(t,this.isAnimating),this.dispatchScrollEvent()},setTranslateX:function(t,e){t+=this.cursorPosition,t=this.options.rightToLeft?-t:t;var i=this.getPositionValue(t);this.slider.style.transform=e?"translate3d("+i+",0,0)":"translateX("+i+")"},dispatchScrollEvent:function(){var t=this.slides[0];if(t){var e=-this.x-t.target,i=e/this.slidesWidth;this.dispatchEvent("scroll",null,[i,e])}},positionSliderAtSelected:function(){this.cells.length&&(this.x=-this.selectedSlide.target,this.velocity=0,this.positionSlider())},getPositionValue:function(t){return this.options.percentPosition?.01*Math.round(t/this.size.innerWidth*1e4)+"%":Math.round(t)+"px"},settle:function(t){this.isPointerDown||Math.round(100*this.x)!=Math.round(100*t)||this.restingFrames++,this.restingFrames>2&&(this.isAnimating=!1,delete this.isFreeScrolling,this.positionSlider(),this.dispatchEvent("settle",null,[this.selectedIndex]))},shiftWrapCells:function(t){var e=this.cursorPosition+t;this._shiftCells(this.beforeShiftCells,e,-1);var i=this.size.innerWidth-(t+this.slideableWidth+this.cursorPosition);this._shiftCells(this.afterShiftCells,i,1)},_shiftCells:function(t,e,i){for(var n=0;n<t.length;n++){var o=t[n],r=e>0?i:0;o.wrapShift(r),e-=o.size.outerWidth}},_unshiftCells:function(t){if(t&&t.length)for(var e=0;e<t.length;e++)t[e].wrapShift(0)},integratePhysics:function(){this.x+=this.velocity,this.velocity*=this.getFrictionFactor()},applyForce:function(t){this.velocity+=t},getFrictionFactor:function(){return 1-this.options[this.isFreeScrolling?"freeScrollFriction":"friction"]},getRestingPosition:function(){return this.x+this.velocity/(1-this.getFrictionFactor())},applyDragForce:function(){if(this.isDraggable&&this.isPointerDown){var t=this.dragX-this.x-this.velocity;this.applyForce(t)}},applySelectedAttraction:function(){if((!this.isDraggable||!this.isPointerDown)&&!this.isFreeScrolling&&this.slides.length){var t=(-1*this.selectedSlide.target-this.x)*this.options.selectedAttraction;this.applyForce(t)}}};return i}(0,t)}.apply(e,P=[d]),function(t,i){y=function(e,i,n,o,r,s){return function(t,e,i,n,o,r,s){var a=t.jQuery,l=t.getComputedStyle,c=t.console;function u(t,e){for(t=n.makeArray(t);t.length;)e.appendChild(t.shift())}var h=0,d={};function p(t,e){var i=n.getQueryElement(t);if(i){if(this.element=i,this.element.flickityGUID){var o=d[this.element.flickityGUID];return o.option(e),o}a&&(this.$element=a(this.element)),this.options=n.extend({},this.constructor.defaults),this.option(e),this._create()}else c&&c.error("Bad element for Flickity: "+(i||t))}p.defaults={accessibility:!0,cellAlign:"center",freeScrollFriction:.075,friction:.28,namespaceJQueryEvents:!0,percentPosition:!0,resize:!0,selectedAttraction:.025,setGallerySize:!0},p.createMethods=[];var f=p.prototype;n.extend(f,e.prototype),f._create=function(){var e=this.guid=++h;for(var i in this.element.flickityGUID=e,d[e]=this,this.selectedIndex=0,this.restingFrames=0,this.x=0,this.velocity=0,this.originSide=this.options.rightToLeft?"right":"left",this.viewport=document.createElement("div"),this.viewport.className="flickity-viewport",this._createSlider(),(this.options.resize||this.options.watchCSS)&&t.addEventListener("resize",this),this.options.on){var n=this.options.on[i];this.on(i,n)}p.createMethods.forEach((function(t){this[t]()}),this),this.options.watchCSS?this.watchCSS():this.activate()},f.option=function(t){n.extend(this.options,t)},f.activate=function(){this.isActive||(this.isActive=!0,this.element.classList.add("flickity-enabled"),this.options.rightToLeft&&this.element.classList.add("flickity-rtl"),this.getSize(),u(this._filterFindCellElements(this.element.children),this.slider),this.viewport.appendChild(this.slider),this.element.appendChild(this.viewport),this.reloadCells(),this.options.accessibility&&(this.element.tabIndex=0,this.element.addEventListener("keydown",this)),this.emitEvent("activate"),this.selectInitialIndex(),this.isInitActivated=!0,this.dispatchEvent("ready"))},f._createSlider=function(){var t=document.createElement("div");t.className="flickity-slider",t.style[this.originSide]=0,this.slider=t},f._filterFindCellElements=function(t){return n.filterFindElements(t,this.options.cellSelector)},f.reloadCells=function(){this.cells=this._makeCells(this.slider.children),this.positionCells(),this._getWrapShiftCells(),this.setGallerySize()},f._makeCells=function(t){return this._filterFindCellElements(t).map((function(t){return new o(t,this)}),this)},f.getLastCell=function(){return this.cells[this.cells.length-1]},f.getLastSlide=function(){return this.slides[this.slides.length-1]},f.positionCells=function(){this._sizeCells(this.cells),this._positionCells(0)},f._positionCells=function(t){t=t||0,this.maxCellHeight=t&&this.maxCellHeight||0;var e=0;if(t>0){var i=this.cells[t-1];e=i.x+i.size.outerWidth}for(var n=this.cells.length,o=t;o<n;o++){var r=this.cells[o];r.setPosition(e),e+=r.size.outerWidth,this.maxCellHeight=Math.max(r.size.outerHeight,this.maxCellHeight)}this.slideableWidth=e,this.updateSlides(),this._containSlides(),this.slidesWidth=n?this.getLastSlide().target-this.slides[0].target:0},f._sizeCells=function(t){t.forEach((function(t){t.getSize()}))},f.updateSlides=function(){if(this.slides=[],this.cells.length){var t=new r(this);this.slides.push(t);var e="left"==this.originSide?"marginRight":"marginLeft",i=this._getCanCellFit();this.cells.forEach((function(n,o){if(t.cells.length){var s=t.outerWidth-t.firstMargin+(n.size.outerWidth-n.size[e]);i.call(this,o,s)||(t.updateTarget(),t=new r(this),this.slides.push(t)),t.addCell(n)}else t.addCell(n)}),this),t.updateTarget(),this.updateSelectedSlide()}},f._getCanCellFit=function(){var t=this.options.groupCells;if(!t)return function(){return!1};if("number"==typeof t){var e=parseInt(t,10);return function(t){return t%e!=0}}var i="string"==typeof t&&t.match(/^(\d+)%$/),n=i?parseInt(i[1],10)/100:1;return function(t,e){return e<=(this.size.innerWidth+1)*n}},f._init=f.reposition=function(){this.positionCells(),this.positionSliderAtSelected()},f.getSize=function(){this.size=i(this.element),this.setCellAlign(),this.cursorPosition=this.size.innerWidth*this.cellAlign};var m={center:{left:.5,right:.5},left:{left:0,right:1},right:{right:0,left:1}};f.setCellAlign=function(){var t=m[this.options.cellAlign];this.cellAlign=t?t[this.originSide]:this.options.cellAlign},f.setGallerySize=function(){if(this.options.setGallerySize){var t=this.options.adaptiveHeight&&this.selectedSlide?this.selectedSlide.height:this.maxCellHeight;this.viewport.style.height=t+"px"}},f._getWrapShiftCells=function(){if(this.options.wrapAround){this._unshiftCells(this.beforeShiftCells),this._unshiftCells(this.afterShiftCells);var t=this.cursorPosition,e=this.cells.length-1;this.beforeShiftCells=this._getGapCells(t,e,-1),t=this.size.innerWidth-this.cursorPosition,this.afterShiftCells=this._getGapCells(t,0,1)}},f._getGapCells=function(t,e,i){for(var n=[];t>0;){var o=this.cells[e];if(!o)break;n.push(o),e+=i,t-=o.size.outerWidth}return n},f._containSlides=function(){if(this.options.contain&&!this.options.wrapAround&&this.cells.length){var t=this.options.rightToLeft,e=t?"marginRight":"marginLeft",i=t?"marginLeft":"marginRight",n=this.slideableWidth-this.getLastCell().size[i],o=n<this.size.innerWidth,r=this.cursorPosition+this.cells[0].size[e],s=n-this.size.innerWidth*(1-this.cellAlign);this.slides.forEach((function(t){o?t.target=n*this.cellAlign:(t.target=Math.max(t.target,r),t.target=Math.min(t.target,s))}),this)}},f.dispatchEvent=function(t,e,i){var n=e?[e].concat(i):i;if(this.emitEvent(t,n),a&&this.$element){var o=t+=this.options.namespaceJQueryEvents?".flickity":"";if(e){var r=a.Event(e);r.type=t,o=r}this.$element.trigger(o,i)}},f.select=function(t,e,i){if(this.isActive&&(t=parseInt(t,10),this._wrapSelect(t),(this.options.wrapAround||e)&&(t=n.modulo(t,this.slides.length)),this.slides[t])){var o=this.selectedIndex;this.selectedIndex=t,this.updateSelectedSlide(),i?this.positionSliderAtSelected():this.startAnimation(),this.options.adaptiveHeight&&this.setGallerySize(),this.dispatchEvent("select",null,[t]),t!=o&&this.dispatchEvent("change",null,[t]),this.dispatchEvent("cellSelect")}},f._wrapSelect=function(t){var e=this.slides.length;if(!(this.options.wrapAround&&e>1))return t;var i=n.modulo(t,e),o=Math.abs(i-this.selectedIndex),r=Math.abs(i+e-this.selectedIndex),s=Math.abs(i-e-this.selectedIndex);!this.isDragSelect&&r<o?t+=e:!this.isDragSelect&&s<o&&(t-=e),t<0?this.x-=this.slideableWidth:t>=e&&(this.x+=this.slideableWidth)},f.previous=function(t,e){this.select(this.selectedIndex-1,t,e)},f.next=function(t,e){this.select(this.selectedIndex+1,t,e)},f.updateSelectedSlide=function(){var t=this.slides[this.selectedIndex];t&&(this.unselectSelectedSlide(),this.selectedSlide=t,t.select(),this.selectedCells=t.cells,this.selectedElements=t.getCellElements(),this.selectedCell=t.cells[0],this.selectedElement=this.selectedElements[0])},f.unselectSelectedSlide=function(){this.selectedSlide&&this.selectedSlide.unselect()},f.selectInitialIndex=function(){var t=this.options.initialIndex;if(this.isInitActivated)this.select(this.selectedIndex,!1,!0);else{if(t&&"string"==typeof t)if(this.queryCell(t))return void this.selectCell(t,!1,!0);var e=0;t&&this.slides[t]&&(e=t),this.select(e,!1,!0)}},f.selectCell=function(t,e,i){var n=this.queryCell(t);if(n){var o=this.getCellSlideIndex(n);this.select(o,e,i)}},f.getCellSlideIndex=function(t){for(var e=0;e<this.slides.length;e++){if(-1!=this.slides[e].cells.indexOf(t))return e}},f.getCell=function(t){for(var e=0;e<this.cells.length;e++){var i=this.cells[e];if(i.element==t)return i}},f.getCells=function(t){t=n.makeArray(t);var e=[];return t.forEach((function(t){var i=this.getCell(t);i&&e.push(i)}),this),e},f.getCellElements=function(){return this.cells.map((function(t){return t.element}))},f.getParentCell=function(t){var e=this.getCell(t);return e||(t=n.getParent(t,".flickity-slider > *"),this.getCell(t))},f.getAdjacentCellElements=function(t,e){if(!t)return this.selectedSlide.getCellElements();e=void 0===e?this.selectedIndex:e;var i=this.slides.length;if(1+2*t>=i)return this.getCellElements();for(var o=[],r=e-t;r<=e+t;r++){var s=this.options.wrapAround?n.modulo(r,i):r,a=this.slides[s];a&&(o=o.concat(a.getCellElements()))}return o},f.queryCell=function(t){if("number"==typeof t)return this.cells[t];if("string"==typeof t){if(t.match(/^[#\.]?[\d\/]/))return;t=this.element.querySelector(t)}return this.getCell(t)},f.uiChange=function(){this.emitEvent("uiChange")},f.childUIPointerDown=function(t){"touchstart"!=t.type&&t.preventDefault(),this.focus()},f.onresize=function(){this.watchCSS(),this.resize()},n.debounceMethod(p,"onresize",150),f.resize=function(){if(this.isActive){this.getSize(),this.options.wrapAround&&(this.x=n.modulo(this.x,this.slideableWidth)),this.positionCells(),this._getWrapShiftCells(),this.setGallerySize(),this.emitEvent("resize");var t=this.selectedElements&&this.selectedElements[0];this.selectCell(t,!1,!0)}},f.watchCSS=function(){this.options.watchCSS&&(-1!=l(this.element,":after").content.indexOf("flickity")?this.activate():this.deactivate())},f.onkeydown=function(t){var e=document.activeElement&&document.activeElement!=this.element;if(this.options.accessibility&&!e){var i=p.keyboardHandlers[t.keyCode];i&&i.call(this)}},p.keyboardHandlers={37:function(){var t=this.options.rightToLeft?"next":"previous";this.uiChange(),this[t]()},39:function(){var t=this.options.rightToLeft?"previous":"next";this.uiChange(),this[t]()}},f.focus=function(){var e=t.pageYOffset;this.element.focus({preventScroll:!0}),t.pageYOffset!=e&&t.scrollTo(t.pageXOffset,e)},f.deactivate=function(){this.isActive&&(this.element.classList.remove("flickity-enabled"),this.element.classList.remove("flickity-rtl"),this.unselectSelectedSlide(),this.cells.forEach((function(t){t.destroy()})),this.element.removeChild(this.viewport),u(this.slider.children,this.element),this.options.accessibility&&(this.element.removeAttribute("tabIndex"),this.element.removeEventListener("keydown",this)),this.isActive=!1,this.emitEvent("deactivate"))},f.destroy=function(){this.deactivate(),t.removeEventListener("resize",this),this.allOff(),this.emitEvent("destroy"),a&&this.$element&&a.removeData(this.element,"flickity"),delete this.element.flickityGUID,delete d[this.guid]},n.extend(f,s),p.data=function(t){var e=(t=n.getQueryElement(t))&&t.flickityGUID;return e&&d[e]},n.htmlInit(p,"flickity"),a&&a.bridget&&a.bridget("flickity",p);return p.setJQuery=function(t){a=t},p.Cell=o,p.Slide=r,p}(t,e,i,n,o,r,s)}.apply(e,P=[n,s,d,p,f,v])}(window),
/*!
 * Unipointer v2.3.0
 * base class for doing one thing with pointer event
 * MIT license
 */
function(t,i){b=function(e){return function(t,e){function i(){}var n=i.prototype=Object.create(e.prototype);n.bindStartEvent=function(t){this._bindStartEvent(t,!0)},n.unbindStartEvent=function(t){this._bindStartEvent(t,!1)},n._bindStartEvent=function(e,i){var n=(i=void 0===i||i)?"addEventListener":"removeEventListener",o="mousedown";t.PointerEvent?o="pointerdown":"ontouchstart"in t&&(o="touchstart"),e[n](o,this)},n.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},n.getTouch=function(t){for(var e=0;e<t.length;e++){var i=t[e];if(i.identifier==this.pointerIdentifier)return i}},n.onmousedown=function(t){var e=t.button;e&&0!==e&&1!==e||this._pointerDown(t,t)},n.ontouchstart=function(t){this._pointerDown(t,t.changedTouches[0])},n.onpointerdown=function(t){this._pointerDown(t,t)},n._pointerDown=function(t,e){t.button||this.isPointerDown||(this.isPointerDown=!0,this.pointerIdentifier=void 0!==e.pointerId?e.pointerId:e.identifier,this.pointerDown(t,e))},n.pointerDown=function(t,e){this._bindPostStartEvents(t),this.emitEvent("pointerDown",[t,e])};var o={mousedown:["mousemove","mouseup"],touchstart:["touchmove","touchend","touchcancel"],pointerdown:["pointermove","pointerup","pointercancel"]};return n._bindPostStartEvents=function(e){if(e){var i=o[e.type];i.forEach((function(e){t.addEventListener(e,this)}),this),this._boundPointerEvents=i}},n._unbindPostStartEvents=function(){this._boundPointerEvents&&(this._boundPointerEvents.forEach((function(e){t.removeEventListener(e,this)}),this),delete this._boundPointerEvents)},n.onmousemove=function(t){this._pointerMove(t,t)},n.onpointermove=function(t){t.pointerId==this.pointerIdentifier&&this._pointerMove(t,t)},n.ontouchmove=function(t){var e=this.getTouch(t.changedTouches);e&&this._pointerMove(t,e)},n._pointerMove=function(t,e){this.pointerMove(t,e)},n.pointerMove=function(t,e){this.emitEvent("pointerMove",[t,e])},n.onmouseup=function(t){this._pointerUp(t,t)},n.onpointerup=function(t){t.pointerId==this.pointerIdentifier&&this._pointerUp(t,t)},n.ontouchend=function(t){var e=this.getTouch(t.changedTouches);e&&this._pointerUp(t,e)},n._pointerUp=function(t,e){this._pointerDone(),this.pointerUp(t,e)},n.pointerUp=function(t,e){this.emitEvent("pointerUp",[t,e])},n._pointerDone=function(){this._pointerReset(),this._unbindPostStartEvents(),this.pointerDone()},n._pointerReset=function(){this.isPointerDown=!1,delete this.pointerIdentifier},n.pointerDone=function(){},n.onpointercancel=function(t){t.pointerId==this.pointerIdentifier&&this._pointerCancel(t,t)},n.ontouchcancel=function(t){var e=this.getTouch(t.changedTouches);e&&this._pointerCancel(t,e)},n._pointerCancel=function(t,e){this._pointerDone(),this.pointerCancel(t,e)},n.pointerCancel=function(t,e){this.emitEvent("pointerCancel",[t,e])},i.getPointerPoint=function(t){return{x:t.pageX,y:t.pageY}},i}(t,e)}.apply(e,P=[n])}(window),
/*!
 * Unidragger v2.3.0
 * Draggable base class
 * MIT license
 */
function(t,i){w=function(e){return function(t,e){function i(){}var n=i.prototype=Object.create(e.prototype);n.bindHandles=function(){this._bindHandles(!0)},n.unbindHandles=function(){this._bindHandles(!1)},n._bindHandles=function(e){for(var i=(e=void 0===e||e)?"addEventListener":"removeEventListener",n=e?this._touchActionValue:"",o=0;o<this.handles.length;o++){var r=this.handles[o];this._bindStartEvent(r,e),r[i]("click",this),t.PointerEvent&&(r.style.touchAction=n)}},n._touchActionValue="none",n.pointerDown=function(t,e){this.okayPointerDown(t)&&(this.pointerDownPointer=e,t.preventDefault(),this.pointerDownBlur(),this._bindPostStartEvents(t),this.emitEvent("pointerDown",[t,e]))};var o={TEXTAREA:!0,INPUT:!0,SELECT:!0,OPTION:!0},r={radio:!0,checkbox:!0,button:!0,submit:!0,image:!0,file:!0};return n.okayPointerDown=function(t){var e=o[t.target.nodeName],i=r[t.target.type],n=!e||i;return n||this._pointerReset(),n},n.pointerDownBlur=function(){var t=document.activeElement;t&&t.blur&&t!=document.body&&t.blur()},n.pointerMove=function(t,e){var i=this._dragPointerMove(t,e);this.emitEvent("pointerMove",[t,e,i]),this._dragMove(t,e,i)},n._dragPointerMove=function(t,e){var i={x:e.pageX-this.pointerDownPointer.pageX,y:e.pageY-this.pointerDownPointer.pageY};return!this.isDragging&&this.hasDragStarted(i)&&this._dragStart(t,e),i},n.hasDragStarted=function(t){return Math.abs(t.x)>3||Math.abs(t.y)>3},n.pointerUp=function(t,e){this.emitEvent("pointerUp",[t,e]),this._dragPointerUp(t,e)},n._dragPointerUp=function(t,e){this.isDragging?this._dragEnd(t,e):this._staticClick(t,e)},n._dragStart=function(t,e){this.isDragging=!0,this.isPreventingClicks=!0,this.dragStart(t,e)},n.dragStart=function(t,e){this.emitEvent("dragStart",[t,e])},n._dragMove=function(t,e,i){this.isDragging&&this.dragMove(t,e,i)},n.dragMove=function(t,e,i){t.preventDefault(),this.emitEvent("dragMove",[t,e,i])},n._dragEnd=function(t,e){this.isDragging=!1,setTimeout(function(){delete this.isPreventingClicks}.bind(this)),this.dragEnd(t,e)},n.dragEnd=function(t,e){this.emitEvent("dragEnd",[t,e])},n.onclick=function(t){this.isPreventingClicks&&t.preventDefault()},n._staticClick=function(t,e){this.isIgnoringMouseUp&&"mouseup"==t.type||(this.staticClick(t,e),"mouseup"!=t.type&&(this.isIgnoringMouseUp=!0,setTimeout(function(){delete this.isIgnoringMouseUp}.bind(this),400)))},n.staticClick=function(t,e){this.emitEvent("staticClick",[t,e])},i.getPointerPoint=e.getPointerPoint,i}(t,e)}.apply(e,P=[b])}(window),function(t,i){x=function(e,i,n){return function(t,e,i,n){n.extend(e.defaults,{draggable:">1",dragThreshold:3}),e.createMethods.push("_createDrag");var o=e.prototype;n.extend(o,i.prototype),o._touchActionValue="pan-y";var r="createTouch"in document,s=!1;o._createDrag=function(){this.on("activate",this.onActivateDrag),this.on("uiChange",this._uiChangeDrag),this.on("deactivate",this.onDeactivateDrag),this.on("cellChange",this.updateDraggable),r&&!s&&(t.addEventListener("touchmove",(function(){})),s=!0)},o.onActivateDrag=function(){this.handles=[this.viewport],this.bindHandles(),this.updateDraggable()},o.onDeactivateDrag=function(){this.unbindHandles(),this.element.classList.remove("is-draggable")},o.updateDraggable=function(){">1"==this.options.draggable?this.isDraggable=this.slides.length>1:this.isDraggable=this.options.draggable,this.isDraggable?this.element.classList.add("is-draggable"):this.element.classList.remove("is-draggable")},o.bindDrag=function(){this.options.draggable=!0,this.updateDraggable()},o.unbindDrag=function(){this.options.draggable=!1,this.updateDraggable()},o._uiChangeDrag=function(){delete this.isFreeScrolling},o.pointerDown=function(e,i){this.isDraggable?this.okayPointerDown(e)&&(this._pointerDownPreventDefault(e),this.pointerDownFocus(e),document.activeElement!=this.element&&this.pointerDownBlur(),this.dragX=this.x,this.viewport.classList.add("is-pointer-down"),this.pointerDownScroll=l(),t.addEventListener("scroll",this),this._pointerDownDefault(e,i)):this._pointerDownDefault(e,i)},o._pointerDownDefault=function(t,e){this.pointerDownPointer={pageX:e.pageX,pageY:e.pageY},this._bindPostStartEvents(t),this.dispatchEvent("pointerDown",t,[e])};var a={INPUT:!0,TEXTAREA:!0,SELECT:!0};function l(){return{x:t.pageXOffset,y:t.pageYOffset}}return o.pointerDownFocus=function(t){a[t.target.nodeName]||this.focus()},o._pointerDownPreventDefault=function(t){var e="touchstart"==t.type,i="touch"==t.pointerType,n=a[t.target.nodeName];e||i||n||t.preventDefault()},o.hasDragStarted=function(t){return Math.abs(t.x)>this.options.dragThreshold},o.pointerUp=function(t,e){delete this.isTouchScrolling,this.viewport.classList.remove("is-pointer-down"),this.dispatchEvent("pointerUp",t,[e]),this._dragPointerUp(t,e)},o.pointerDone=function(){t.removeEventListener("scroll",this),delete this.pointerDownScroll},o.dragStart=function(e,i){this.isDraggable&&(this.dragStartPosition=this.x,this.startAnimation(),t.removeEventListener("scroll",this),this.dispatchEvent("dragStart",e,[i]))},o.pointerMove=function(t,e){var i=this._dragPointerMove(t,e);this.dispatchEvent("pointerMove",t,[e,i]),this._dragMove(t,e,i)},o.dragMove=function(t,e,i){if(this.isDraggable){t.preventDefault(),this.previousDragX=this.dragX;var n=this.options.rightToLeft?-1:1;this.options.wrapAround&&(i.x=i.x%this.slideableWidth);var o=this.dragStartPosition+i.x*n;if(!this.options.wrapAround&&this.slides.length){var r=Math.max(-this.slides[0].target,this.dragStartPosition);o=o>r?.5*(o+r):o;var s=Math.min(-this.getLastSlide().target,this.dragStartPosition);o=o<s?.5*(o+s):o}this.dragX=o,this.dragMoveTime=new Date,this.dispatchEvent("dragMove",t,[e,i])}},o.dragEnd=function(t,e){if(this.isDraggable){this.options.freeScroll&&(this.isFreeScrolling=!0);var i=this.dragEndRestingSelect();if(this.options.freeScroll&&!this.options.wrapAround){var n=this.getRestingPosition();this.isFreeScrolling=-n>this.slides[0].target&&-n<this.getLastSlide().target}else this.options.freeScroll||i!=this.selectedIndex||(i+=this.dragEndBoostSelect());delete this.previousDragX,this.isDragSelect=this.options.wrapAround,this.select(i),delete this.isDragSelect,this.dispatchEvent("dragEnd",t,[e])}},o.dragEndRestingSelect=function(){var t=this.getRestingPosition(),e=Math.abs(this.getSlideDistance(-t,this.selectedIndex)),i=this._getClosestResting(t,e,1),n=this._getClosestResting(t,e,-1);return i.distance<n.distance?i.index:n.index},o._getClosestResting=function(t,e,i){for(var n=this.selectedIndex,o=1/0,r=this.options.contain&&!this.options.wrapAround?function(t,e){return t<=e}:function(t,e){return t<e};r(e,o)&&(n+=i,o=e,null!==(e=this.getSlideDistance(-t,n)));)e=Math.abs(e);return{distance:o,index:n-i}},o.getSlideDistance=function(t,e){var i=this.slides.length,o=this.options.wrapAround&&i>1,r=o?n.modulo(e,i):e,s=this.slides[r];if(!s)return null;var a=o?this.slideableWidth*Math.floor(e/i):0;return t-(s.target+a)},o.dragEndBoostSelect=function(){if(void 0===this.previousDragX||!this.dragMoveTime||new Date-this.dragMoveTime>100)return 0;var t=this.getSlideDistance(-this.dragX,this.selectedIndex),e=this.previousDragX-this.dragX;return t>0&&e>0?1:t<0&&e<0?-1:0},o.staticClick=function(t,e){var i=this.getParentCell(t.target),n=i&&i.element,o=i&&this.cells.indexOf(i);this.dispatchEvent("staticClick",t,[e,n,o])},o.onscroll=function(){var t=l(),e=this.pointerDownScroll.x-t.x,i=this.pointerDownScroll.y-t.y;(Math.abs(e)>3||Math.abs(i)>3)&&this._pointerDone()},e}(t,e,i,n)}.apply(e,P=[y,w,d])}(window),window,C=function(t,e,i){return function(t,e,i,n){"use strict";var o="http://www.w3.org/2000/svg";function r(t,e){this.direction=t,this.parent=e,this._create()}r.prototype=Object.create(i.prototype),r.prototype._create=function(){this.isEnabled=!0,this.isPrevious=-1==this.direction;var t=this.parent.options.rightToLeft?1:-1;this.isLeft=this.direction==t;var e=this.element=document.createElement("button");e.className="flickity-button flickity-prev-next-button",e.className+=this.isPrevious?" previous":" next",e.setAttribute("type","button"),this.disable(),e.setAttribute("aria-label",this.isPrevious?"Previous":"Next");var i=this.createSVG();e.appendChild(i),this.parent.on("select",this.update.bind(this)),this.on("pointerDown",this.parent.childUIPointerDown.bind(this.parent))},r.prototype.activate=function(){this.bindStartEvent(this.element),this.element.addEventListener("click",this),this.parent.element.appendChild(this.element)},r.prototype.deactivate=function(){this.parent.element.removeChild(this.element),this.unbindStartEvent(this.element),this.element.removeEventListener("click",this)},r.prototype.createSVG=function(){var t=document.createElementNS(o,"svg");t.setAttribute("class","flickity-button-icon"),t.setAttribute("viewBox","0 0 100 100");var e,i=document.createElementNS(o,"path"),n="string"==typeof(e=this.parent.options.arrowShape)?e:"M "+e.x0+",50 L "+e.x1+","+(e.y1+50)+" L "+e.x2+","+(e.y2+50)+" L "+e.x3+",50  L "+e.x2+","+(50-e.y2)+" L "+e.x1+","+(50-e.y1)+" Z";return i.setAttribute("d",n),i.setAttribute("class","arrow"),this.isLeft||i.setAttribute("transform","translate(100, 100) rotate(180) "),t.appendChild(i),t},r.prototype.handleEvent=n.handleEvent,r.prototype.onclick=function(){if(this.isEnabled){this.parent.uiChange();var t=this.isPrevious?"previous":"next";this.parent[t]()}},r.prototype.enable=function(){this.isEnabled||(this.element.disabled=!1,this.isEnabled=!0)},r.prototype.disable=function(){this.isEnabled&&(this.element.disabled=!0,this.isEnabled=!1)},r.prototype.update=function(){var t=this.parent.slides;if(this.parent.options.wrapAround&&t.length>1)this.enable();else{var e=t.length?t.length-1:0,i=this.isPrevious?0:e;this[this.parent.selectedIndex==i?"disable":"enable"]()}},r.prototype.destroy=function(){this.deactivate(),this.allOff()},n.extend(e.defaults,{prevNextButtons:!0,arrowShape:{x0:10,x1:60,y1:50,x2:70,y2:40,x3:30}}),e.createMethods.push("_createPrevNextButtons");var s=e.prototype;return s._createPrevNextButtons=function(){this.options.prevNextButtons&&(this.prevButton=new r(-1,this),this.nextButton=new r(1,this),this.on("activate",this.activatePrevNextButtons))},s.activatePrevNextButtons=function(){this.prevButton.activate(),this.nextButton.activate(),this.on("deactivate",this.deactivatePrevNextButtons)},s.deactivatePrevNextButtons=function(){this.prevButton.deactivate(),this.nextButton.deactivate(),this.off("deactivate",this.deactivatePrevNextButtons)},e.PrevNextButton=r,e}(0,t,e,i)}.apply(e,P=[y,b,d]),window,k=function(t,e,i){return function(t,e,i,n){function o(t){this.parent=t,this._create()}o.prototype=Object.create(i.prototype),o.prototype._create=function(){this.holder=document.createElement("ol"),this.holder.className="flickity-page-dots",this.dots=[],this.handleClick=this.onClick.bind(this),this.on("pointerDown",this.parent.childUIPointerDown.bind(this.parent))},o.prototype.activate=function(){this.setDots(),this.holder.addEventListener("click",this.handleClick),this.bindStartEvent(this.holder),this.parent.element.appendChild(this.holder)},o.prototype.deactivate=function(){this.holder.removeEventListener("click",this.handleClick),this.unbindStartEvent(this.holder),this.parent.element.removeChild(this.holder)},o.prototype.setDots=function(){var t=this.parent.slides.length-this.dots.length;t>0?this.addDots(t):t<0&&this.removeDots(-t)},o.prototype.addDots=function(t){for(var e=document.createDocumentFragment(),i=[],n=this.dots.length,o=n+t,r=n;r<o;r++){var s=document.createElement("li");s.className="dot",s.setAttribute("aria-label","Page dot "+(r+1)),e.appendChild(s),i.push(s)}this.holder.appendChild(e),this.dots=this.dots.concat(i)},o.prototype.removeDots=function(t){this.dots.splice(this.dots.length-t,t).forEach((function(t){this.holder.removeChild(t)}),this)},o.prototype.updateSelected=function(){this.selectedDot&&(this.selectedDot.className="dot",this.selectedDot.removeAttribute("aria-current")),this.dots.length&&(this.selectedDot=this.dots[this.parent.selectedIndex],this.selectedDot.className="dot is-selected",this.selectedDot.setAttribute("aria-current","step"))},o.prototype.onTap=o.prototype.onClick=function(t){var e=t.target;if("LI"==e.nodeName){this.parent.uiChange();var i=this.dots.indexOf(e);this.parent.select(i)}},o.prototype.destroy=function(){this.deactivate(),this.allOff()},e.PageDots=o,n.extend(e.defaults,{pageDots:!0}),e.createMethods.push("_createPageDots");var r=e.prototype;return r._createPageDots=function(){this.options.pageDots&&(this.pageDots=new o(this),this.on("activate",this.activatePageDots),this.on("select",this.updateSelectedPageDots),this.on("cellChange",this.updatePageDots),this.on("resize",this.updatePageDots),this.on("deactivate",this.deactivatePageDots))},r.activatePageDots=function(){this.pageDots.activate()},r.updateSelectedPageDots=function(){this.pageDots.updateSelected()},r.updatePageDots=function(){this.pageDots.setDots()},r.deactivatePageDots=function(){this.pageDots.deactivate()},e.PageDots=o,e}(0,t,e,i)}.apply(e,P=[y,b,d]),window,S=function(t,e,i){return function(t,e,i){function n(t){this.parent=t,this.state="stopped",this.onVisibilityChange=this.visibilityChange.bind(this),this.onVisibilityPlay=this.visibilityPlay.bind(this)}n.prototype=Object.create(t.prototype),n.prototype.play=function(){"playing"!=this.state&&(document.hidden?document.addEventListener("visibilitychange",this.onVisibilityPlay):(this.state="playing",document.addEventListener("visibilitychange",this.onVisibilityChange),this.tick()))},n.prototype.tick=function(){if("playing"==this.state){var t=this.parent.options.autoPlay;t="number"==typeof t?t:3e3;var e=this;this.clear(),this.timeout=setTimeout((function(){e.parent.next(!0),e.tick()}),t)}},n.prototype.stop=function(){this.state="stopped",this.clear(),document.removeEventListener("visibilitychange",this.onVisibilityChange)},n.prototype.clear=function(){clearTimeout(this.timeout)},n.prototype.pause=function(){"playing"==this.state&&(this.state="paused",this.clear())},n.prototype.unpause=function(){"paused"==this.state&&this.play()},n.prototype.visibilityChange=function(){this[document.hidden?"pause":"unpause"]()},n.prototype.visibilityPlay=function(){this.play(),document.removeEventListener("visibilitychange",this.onVisibilityPlay)},e.extend(i.defaults,{pauseAutoPlayOnHover:!0}),i.createMethods.push("_createPlayer");var o=i.prototype;return o._createPlayer=function(){this.player=new n(this),this.on("activate",this.activatePlayer),this.on("uiChange",this.stopPlayer),this.on("pointerDown",this.stopPlayer),this.on("deactivate",this.deactivatePlayer)},o.activatePlayer=function(){this.options.autoPlay&&(this.player.play(),this.element.addEventListener("mouseenter",this))},o.playPlayer=function(){this.player.play()},o.stopPlayer=function(){this.player.stop()},o.pausePlayer=function(){this.player.pause()},o.unpausePlayer=function(){this.player.unpause()},o.deactivatePlayer=function(){this.player.stop(),this.element.removeEventListener("mouseenter",this)},o.onmouseenter=function(){this.options.pauseAutoPlayOnHover&&(this.player.pause(),this.element.addEventListener("mouseleave",this))},o.onmouseleave=function(){this.player.unpause(),this.element.removeEventListener("mouseleave",this)},i.Player=n,i}(t,e,i)}.apply(e,P=[n,d,y]),window,E=function(t,e){return function(t,e,i){var n=e.prototype;return n.insert=function(t,e){var i=this._makeCells(t);if(i&&i.length){var n=this.cells.length;e=void 0===e?n:e;var o=function(t){var e=document.createDocumentFragment();return t.forEach((function(t){e.appendChild(t.element)})),e}(i),r=e==n;if(r)this.slider.appendChild(o);else{var s=this.cells[e].element;this.slider.insertBefore(o,s)}if(0===e)this.cells=i.concat(this.cells);else if(r)this.cells=this.cells.concat(i);else{var a=this.cells.splice(e,n-e);this.cells=this.cells.concat(i).concat(a)}this._sizeCells(i),this.cellChange(e,!0)}},n.append=function(t){this.insert(t,this.cells.length)},n.prepend=function(t){this.insert(t,0)},n.remove=function(t){var e=this.getCells(t);if(e&&e.length){var n=this.cells.length-1;e.forEach((function(t){t.remove();var e=this.cells.indexOf(t);n=Math.min(e,n),i.removeFrom(this.cells,t)}),this),this.cellChange(n,!0)}},n.cellSizeChange=function(t){var e=this.getCell(t);if(e){e.getSize();var i=this.cells.indexOf(e);this.cellChange(i)}},n.cellChange=function(t,e){var i=this.selectedElement;this._positionCells(t),this._getWrapShiftCells(),this.setGallerySize();var n=this.getCell(i);n&&(this.selectedIndex=this.getCellSlideIndex(n)),this.selectedIndex=Math.min(this.slides.length-1,this.selectedIndex),this.emitEvent("cellChange",[t]),this.select(this.selectedIndex),e&&this.positionSliderAtSelected()},e}(0,t,e)}.apply(e,P=[y,d]),window,T=function(t,e){return function(t,e,i){"use strict";e.createMethods.push("_createLazyload");var n=e.prototype;function o(t,e){this.img=t,this.flickity=e,this.load()}return n._createLazyload=function(){this.on("select",this.lazyLoad)},n.lazyLoad=function(){var t=this.options.lazyLoad;if(t){var e="number"==typeof t?t:0,n=this.getAdjacentCellElements(e),r=[];n.forEach((function(t){var e=function(t){if("IMG"==t.nodeName){var e=t.getAttribute("data-flickity-lazyload"),n=t.getAttribute("data-flickity-lazyload-src"),o=t.getAttribute("data-flickity-lazyload-srcset");if(e||n||o)return[t]}var r=t.querySelectorAll("img[data-flickity-lazyload], img[data-flickity-lazyload-src], img[data-flickity-lazyload-srcset]");return i.makeArray(r)}(t);r=r.concat(e)})),r.forEach((function(t){new o(t,this)}),this)}},o.prototype.handleEvent=i.handleEvent,o.prototype.load=function(){this.img.addEventListener("load",this),this.img.addEventListener("error",this);var t=this.img.getAttribute("data-flickity-lazyload")||this.img.getAttribute("data-flickity-lazyload-src"),e=this.img.getAttribute("data-flickity-lazyload-srcset");this.img.src=t,e&&this.img.setAttribute("srcset",e),this.img.removeAttribute("data-flickity-lazyload"),this.img.removeAttribute("data-flickity-lazyload-src"),this.img.removeAttribute("data-flickity-lazyload-srcset")},o.prototype.onload=function(t){this.complete(t,"flickity-lazyloaded")},o.prototype.onerror=function(t){this.complete(t,"flickity-lazyerror")},o.prototype.complete=function(t,e){this.img.removeEventListener("load",this),this.img.removeEventListener("error",this);var i=this.flickity.getParentCell(this.img),n=i&&i.element;this.flickity.cellSizeChange(n),this.img.classList.add(e),this.flickity.dispatchEvent("lazyLoad",t,n)},e.LazyLoader=o,e}(0,t,e)}.apply(e,P=[y,d]),window,P=[y,x,C,k,S,E,T],_="function"==typeof(j=function(t){return t})?j.apply(e,P):j,window,P=[_,d],void 0===(A="function"==typeof(j=function(t,e){t.createMethods.push("_createAsNavFor");var i=t.prototype;return i._createAsNavFor=function(){this.on("activate",this.activateAsNavFor),this.on("deactivate",this.deactivateAsNavFor),this.on("destroy",this.destroyAsNavFor);var t=this.options.asNavFor;if(t){var e=this;setTimeout((function(){e.setNavCompanion(t)}))}},i.setNavCompanion=function(i){i=e.getQueryElement(i);var n=t.data(i);if(n&&n!=this){this.navCompanion=n;var o=this;this.onNavCompanionSelect=function(){o.navCompanionSelect()},n.on("select",this.onNavCompanionSelect),this.on("staticClick",this.onNavStaticClick),this.navCompanionSelect(!0)}},i.navCompanionSelect=function(t){var e=this.navCompanion&&this.navCompanion.selectedCells;if(e){var i,n,o,r=e[0],s=this.navCompanion.cells.indexOf(r),a=s+e.length-1,l=Math.floor((i=s,n=a,o=this.navCompanion.cellAlign,(n-i)*o+i));if(this.selectCell(l,!1,t),this.removeNavSelectedElements(),!(l>=this.cells.length)){var c=this.cells.slice(s,a+1);this.navSelectedElements=c.map((function(t){return t.element})),this.changeNavSelectedClass("add")}}},i.changeNavSelectedClass=function(t){this.navSelectedElements.forEach((function(e){e.classList[t]("is-nav-selected")}))},i.activateAsNavFor=function(){this.navCompanionSelect(!0)},i.removeNavSelectedElements=function(){this.navSelectedElements&&(this.changeNavSelectedClass("remove"),delete this.navSelectedElements)},i.onNavStaticClick=function(t,e,i,n){"number"==typeof n&&this.navCompanion.selectCell(n)},i.deactivateAsNavFor=function(){this.removeNavSelectedElements()},i.destroyAsNavFor=function(){this.navCompanion&&(this.navCompanion.off("select",this.onNavCompanionSelect),this.off("staticClick",this.onNavStaticClick),delete this.navCompanion)},t})?j.apply(e,P):j)||(t.exports=A),
/*!
 * imagesLoaded v4.1.4
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */
function(t,i){"use strict";I=function(e){return function(t,e){var i=t.jQuery,n=t.console;function o(t,e){for(var i in e)t[i]=e[i];return t}var r=Array.prototype.slice;function s(t,e,a){if(!(this instanceof s))return new s(t,e,a);var l,c=t;("string"==typeof t&&(c=document.querySelectorAll(t)),c)?(this.elements=(l=c,Array.isArray(l)?l:"object"==typeof l&&"number"==typeof l.length?r.call(l):[l]),this.options=o({},this.options),"function"==typeof e?a=e:o(this.options,e),a&&this.on("always",a),this.getImages(),i&&(this.jqDeferred=new i.Deferred),setTimeout(this.check.bind(this))):n.error("Bad element for imagesLoaded "+(c||t))}s.prototype=Object.create(e.prototype),s.prototype.options={},s.prototype.getImages=function(){this.images=[],this.elements.forEach(this.addElementImages,this)},s.prototype.addElementImages=function(t){"IMG"==t.nodeName&&this.addImage(t),!0===this.options.background&&this.addElementBackgroundImages(t);var e=t.nodeType;if(e&&a[e]){for(var i=t.querySelectorAll("img"),n=0;n<i.length;n++){var o=i[n];this.addImage(o)}if("string"==typeof this.options.background){var r=t.querySelectorAll(this.options.background);for(n=0;n<r.length;n++){var s=r[n];this.addElementBackgroundImages(s)}}}};var a={1:!0,9:!0,11:!0};function l(t){this.img=t}function c(t,e){this.url=t,this.element=e,this.img=new Image}return s.prototype.addElementBackgroundImages=function(t){var e=getComputedStyle(t);if(e)for(var i=/url\((['"])?(.*?)\1\)/gi,n=i.exec(e.backgroundImage);null!==n;){var o=n&&n[2];o&&this.addBackground(o,t),n=i.exec(e.backgroundImage)}},s.prototype.addImage=function(t){var e=new l(t);this.images.push(e)},s.prototype.addBackground=function(t,e){var i=new c(t,e);this.images.push(i)},s.prototype.check=function(){var t=this;function e(e,i,n){setTimeout((function(){t.progress(e,i,n)}))}this.progressedCount=0,this.hasAnyBroken=!1,this.images.length?this.images.forEach((function(t){t.once("progress",e),t.check()})):this.complete()},s.prototype.progress=function(t,e,i){this.progressedCount++,this.hasAnyBroken=this.hasAnyBroken||!t.isLoaded,this.emitEvent("progress",[this,t,e]),this.jqDeferred&&this.jqDeferred.notify&&this.jqDeferred.notify(this,t),this.progressedCount==this.images.length&&this.complete(),this.options.debug&&n&&n.log("progress: "+i,t,e)},s.prototype.complete=function(){var t=this.hasAnyBroken?"fail":"done";if(this.isComplete=!0,this.emitEvent(t,[this]),this.emitEvent("always",[this]),this.jqDeferred){var e=this.hasAnyBroken?"reject":"resolve";this.jqDeferred[e](this)}},l.prototype=Object.create(e.prototype),l.prototype.check=function(){this.getIsImageComplete()?this.confirm(0!==this.img.naturalWidth,"naturalWidth"):(this.proxyImage=new Image,this.proxyImage.addEventListener("load",this),this.proxyImage.addEventListener("error",this),this.img.addEventListener("load",this),this.img.addEventListener("error",this),this.proxyImage.src=this.img.src)},l.prototype.getIsImageComplete=function(){return this.img.complete&&this.img.naturalWidth},l.prototype.confirm=function(t,e){this.isLoaded=t,this.emitEvent("progress",[this,this.img,e])},l.prototype.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},l.prototype.onload=function(){this.confirm(!0,"onload"),this.unbindEvents()},l.prototype.onerror=function(){this.confirm(!1,"onerror"),this.unbindEvents()},l.prototype.unbindEvents=function(){this.proxyImage.removeEventListener("load",this),this.proxyImage.removeEventListener("error",this),this.img.removeEventListener("load",this),this.img.removeEventListener("error",this)},c.prototype=Object.create(l.prototype),c.prototype.check=function(){this.img.addEventListener("load",this),this.img.addEventListener("error",this),this.img.src=this.url,this.getIsImageComplete()&&(this.confirm(0!==this.img.naturalWidth,"naturalWidth"),this.unbindEvents())},c.prototype.unbindEvents=function(){this.img.removeEventListener("load",this),this.img.removeEventListener("error",this)},c.prototype.confirm=function(t,e){this.isLoaded=t,this.emitEvent("progress",[this,this.element,e])},s.makeJQueryPlugin=function(e){(e=e||t.jQuery)&&((i=e).fn.imagesLoaded=function(t,e){return new s(this,t,e).jqDeferred.promise(i(this))})},s.makeJQueryPlugin(),s}(t,e)}.apply(e,P=[n])}("undefined"!=typeof window?window:this),window,void 0===(A=function(t,e){return function(t,e,i){"use strict";e.createMethods.push("_createImagesLoaded");var n=e.prototype;return n._createImagesLoaded=function(){this.on("activate",this.imagesLoaded)},n.imagesLoaded=function(){if(this.options.imagesLoaded){var t=this;i(this.slider).on("progress",(function(e,i){var n=t.getParentCell(i.img);t.cellSizeChange(n&&n.element),t.options.freeScroll||t.positionSliderAtSelected()}))}},e}(0,t,e)}.apply(e,P=[_,I]))||(t.exports=A)},function(t,e){!function(){var t=window.MutationObserver||window.WebKitMutationObserver,e="ontouchstart"in window||window.DocumentTouch&&document instanceof DocumentTouch;if(!(void 0!==document.documentElement.style["touch-action"]||document.documentElement.style["-ms-touch-action"])&&e&&t){window.Hammer=window.Hammer||{};var i=/touch-action[:][\s]*(none)[^;'"]*/,n=/touch-action[:][\s]*(manipulation)[^;'"]*/,o=/touch-action/,r=/(iP(ad|hone|od))/.test(navigator.userAgent)&&("indexedDB"in window||!!window.performance);window.Hammer.time={getTouchAction:function(t){return this.checkStyleString(t.getAttribute("style"))},checkStyleString:function(t){if(o.test(t))return i.test(t)?"none":!n.test(t)||"manipulation"},shouldHammer:function(t){var e=t.target.hasParent;return!(!e||r&&!(Date.now()-t.target.lastStart<125))&&e},touchHandler:function(t){var e=this.shouldHammer(t);if("none"===e)this.dropHammer(t);else if("manipulation"===e){var i=t.target.getBoundingClientRect();!(i.top!==this.pos.top||i.left!==this.pos.left)&&this.dropHammer(t)}this.scrolled=!1,delete t.target.lastStart,delete t.target.hasParent},dropHammer:function(t){"touchend"===t.type&&(t.target.focus(),setTimeout((function(){t.target.click()}),0)),t.preventDefault()},touchStart:function(t){this.pos=t.target.getBoundingClientRect(),t.target.hasParent=this.hasParent(t.target),r&&t.target.hasParent&&(t.target.lastStart=Date.now())},styleWatcher:function(t){t.forEach(this.styleUpdater,this)},styleUpdater:function(t){if(t.target.updateNext)t.target.updateNext=!1;else{var e=this.getTouchAction(t.target);e?"none"!==e&&(t.target.hadTouchNone=!1):!e&&(t.oldValue&&this.checkStyleString(t.oldValue)||t.target.hadTouchNone)&&(t.target.hadTouchNone=!0,t.target.updateNext=!1,t.target.setAttribute("style",t.target.getAttribute("style")+" touch-action: none;"))}},hasParent:function(t){for(var e,i=t;i&&i.parentNode;i=i.parentNode)if(e=this.getTouchAction(i))return e;return!1},installStartEvents:function(){document.addEventListener("touchstart",this.touchStart.bind(this)),document.addEventListener("mousedown",this.touchStart.bind(this))},installEndEvents:function(){document.addEventListener("touchend",this.touchHandler.bind(this),!0),document.addEventListener("mouseup",this.touchHandler.bind(this),!0)},installObserver:function(){this.observer=new t(this.styleWatcher.bind(this)).observe(document,{subtree:!0,attributes:!0,attributeOldValue:!0,attributeFilter:["style"]})},install:function(){this.installEndEvents(),this.installStartEvents(),this.installObserver()}},window.Hammer.time.install()}}()},function(t,e,i){
/**
 * sticky-sidebar - A JavaScript plugin for making smart and high performance.
 * @version v3.3.1
 * @link https://github.com/abouolia/sticky-sidebar
 * @author Ahmed Bouhuolia
 * @license The MIT License (MIT)
**/
!function(){"use strict";function t(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}var e=function(){function t(t,e){for(var i=0;i<e.length;i++){var n=e[i];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(t,n.key,n)}}return function(e,i,n){return i&&t(e.prototype,i),n&&t(e,n),e}}(),i=function(){var i=".stickySidebar",n={topSpacing:0,bottomSpacing:0,containerSelector:!1,innerWrapperSelector:".inner-wrapper-sticky",stickyClass:"is-affixed",resizeSensor:!0,minWidth:!1};return function(){function o(e){var i=this,r=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{};if(t(this,o),this.options=o.extend(n,r),this.sidebar="string"==typeof e?document.querySelector(e):e,void 0===this.sidebar)throw new Error("There is no specific sidebar element.");this.sidebarInner=!1,this.container=this.sidebar.parentElement,this.affixedType="STATIC",this.direction="down",this.support={transform:!1,transform3d:!1},this._initialized=!1,this._reStyle=!1,this._breakpoint=!1,this._resizeListeners=[],this.dimensions={translateY:0,topSpacing:0,lastTopSpacing:0,bottomSpacing:0,lastBottomSpacing:0,sidebarHeight:0,sidebarWidth:0,containerTop:0,containerHeight:0,viewportHeight:0,viewportTop:0,lastViewportTop:0},["handleEvent"].forEach((function(t){i[t]=i[t].bind(i)})),this.initialize()}return e(o,[{key:"initialize",value:function(){var t=this;if(this._setSupportFeatures(),this.options.innerWrapperSelector&&(this.sidebarInner=this.sidebar.querySelector(this.options.innerWrapperSelector),null===this.sidebarInner&&(this.sidebarInner=!1)),!this.sidebarInner){var e=document.createElement("div");for(e.setAttribute("class","inner-wrapper-sticky"),this.sidebar.appendChild(e);this.sidebar.firstChild!=e;)e.appendChild(this.sidebar.firstChild);this.sidebarInner=this.sidebar.querySelector(".inner-wrapper-sticky")}if(this.options.containerSelector){var i=document.querySelectorAll(this.options.containerSelector);if((i=Array.prototype.slice.call(i)).forEach((function(e,i){e.contains(t.sidebar)&&(t.container=e)})),!i.length)throw new Error("The container does not contains on the sidebar.")}"function"!=typeof this.options.topSpacing&&(this.options.topSpacing=parseInt(this.options.topSpacing)||0),"function"!=typeof this.options.bottomSpacing&&(this.options.bottomSpacing=parseInt(this.options.bottomSpacing)||0),this._widthBreakpoint(),this.calcDimensions(),this.stickyPosition(),this.bindEvents(),this._initialized=!0}},{key:"bindEvents",value:function(){window.addEventListener("resize",this,{passive:!0,capture:!1}),window.addEventListener("scroll",this,{passive:!0,capture:!1}),this.sidebar.addEventListener("update"+i,this),this.options.resizeSensor&&"undefined"!=typeof ResizeSensor&&(new ResizeSensor(this.sidebarInner,this.handleEvent),new ResizeSensor(this.container,this.handleEvent))}},{key:"handleEvent",value:function(t){this.updateSticky(t)}},{key:"calcDimensions",value:function(){if(!this._breakpoint){var t=this.dimensions;t.containerTop=o.offsetRelative(this.container).top,t.containerHeight=this.container.clientHeight,t.containerBottom=t.containerTop+t.containerHeight,t.sidebarHeight=this.sidebarInner.offsetHeight,t.sidebarWidth=this.sidebar.offsetWidth,t.viewportHeight=window.innerHeight,this._calcDimensionsWithScroll()}}},{key:"_calcDimensionsWithScroll",value:function(){var t=this.dimensions;t.sidebarLeft=o.offsetRelative(this.sidebar).left,t.viewportTop=document.documentElement.scrollTop||document.body.scrollTop,t.viewportBottom=t.viewportTop+t.viewportHeight,t.viewportLeft=document.documentElement.scrollLeft||document.body.scrollLeft,t.topSpacing=this.options.topSpacing,t.bottomSpacing=this.options.bottomSpacing,"function"==typeof t.topSpacing&&(t.topSpacing=parseInt(t.topSpacing(this.sidebar))||0),"function"==typeof t.bottomSpacing&&(t.bottomSpacing=parseInt(t.bottomSpacing(this.sidebar))||0),"VIEWPORT-TOP"===this.affixedType?t.topSpacing<t.lastTopSpacing&&(t.translateY+=t.lastTopSpacing-t.topSpacing,this._reStyle=!0):"VIEWPORT-BOTTOM"===this.affixedType&&t.bottomSpacing<t.lastBottomSpacing&&(t.translateY+=t.lastBottomSpacing-t.bottomSpacing,this._reStyle=!0),t.lastTopSpacing=t.topSpacing,t.lastBottomSpacing=t.bottomSpacing}},{key:"isSidebarFitsViewport",value:function(){return this.dimensions.sidebarHeight<this.dimensions.viewportHeight}},{key:"observeScrollDir",value:function(){var t=this.dimensions;if(t.lastViewportTop!==t.viewportTop){var e="down"===this.direction?Math.min:Math.max;t.viewportTop===e(t.viewportTop,t.lastViewportTop)&&(this.direction="down"===this.direction?"up":"down")}}},{key:"getAffixType",value:function(){var t=this.dimensions,e=!1;this._calcDimensionsWithScroll();var i=t.sidebarHeight+t.containerTop,n=t.viewportTop+t.topSpacing,o=t.viewportBottom-t.bottomSpacing;return"up"===this.direction?n<=t.containerTop?(t.translateY=0,e="STATIC"):n<=t.translateY+t.containerTop?(t.translateY=n-t.containerTop,e="VIEWPORT-TOP"):!this.isSidebarFitsViewport()&&t.containerTop<=n&&(e="VIEWPORT-UNBOTTOM"):this.isSidebarFitsViewport()?t.sidebarHeight+n>=t.containerBottom?(t.translateY=t.containerBottom-i,e="CONTAINER-BOTTOM"):n>=t.containerTop&&(t.translateY=n-t.containerTop,e="VIEWPORT-TOP"):t.containerBottom<=o?(t.translateY=t.containerBottom-i,e="CONTAINER-BOTTOM"):i+t.translateY<=o?(t.translateY=o-i,e="VIEWPORT-BOTTOM"):t.containerTop+t.translateY<=n&&(e="VIEWPORT-UNBOTTOM"),t.translateY=Math.max(0,t.translateY),t.translateY=Math.min(t.containerHeight,t.translateY),t.lastViewportTop=t.viewportTop,e}},{key:"_getStyle",value:function(t){if(void 0!==t){var e={inner:{},outer:{}},i=this.dimensions;switch(t){case"VIEWPORT-TOP":e.inner={position:"fixed",top:i.topSpacing,left:i.sidebarLeft-i.viewportLeft,width:i.sidebarWidth};break;case"VIEWPORT-BOTTOM":e.inner={position:"fixed",top:"auto",left:i.sidebarLeft,bottom:i.bottomSpacing,width:i.sidebarWidth};break;case"CONTAINER-BOTTOM":case"VIEWPORT-UNBOTTOM":var n=this._getTranslate(0,i.translateY+"px");e.inner=n?{transform:n}:{position:"absolute",top:i.translateY,width:i.sidebarWidth}}switch(t){case"VIEWPORT-TOP":case"VIEWPORT-BOTTOM":case"VIEWPORT-UNBOTTOM":case"CONTAINER-BOTTOM":e.outer={height:i.sidebarHeight,position:"relative"}}return e.outer=o.extend({height:"",position:""},e.outer),e.inner=o.extend({position:"relative",top:"",left:"",bottom:"",width:"",transform:this._getTranslate()},e.inner),e}}},{key:"stickyPosition",value:function(t){if(!this._breakpoint){t=this._reStyle||t||!1;var e=this.getAffixType(),n=this._getStyle(e);if((this.affixedType!=e||t)&&e){var r="affix."+e.toLowerCase().replace("viewport-","")+i;for(var s in o.eventTrigger(this.sidebar,r),"STATIC"===e?o.removeClass(this.sidebar,this.options.stickyClass):o.addClass(this.sidebar,this.options.stickyClass),n.outer)this.sidebar.style[s]=n.outer[s];for(var a in n.inner){var l="number"==typeof n.inner[a]?"px":"";this.sidebarInner.style[a]=n.inner[a]+l}var c="affixed."+e.toLowerCase().replace("viewport-","")+i;o.eventTrigger(this.sidebar,c)}else this._initialized&&(this.sidebarInner.style.left=n.inner.left);this.affixedType=e}}},{key:"_widthBreakpoint",value:function(){window.innerWidth<=this.options.minWidth?(this._breakpoint=!0,this.affixedType="STATIC",this.sidebar.removeAttribute("style"),o.removeClass(this.sidebar,this.options.stickyClass),this.sidebarInner.removeAttribute("style")):this._breakpoint=!1}},{key:"updateSticky",value:function(){var t=this,e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};this._running||(this._running=!0,function(e){requestAnimationFrame((function(){switch(e){case"scroll":t._calcDimensionsWithScroll(),t.observeScrollDir(),t.stickyPosition();break;case"resize":default:t._widthBreakpoint(),t.calcDimensions(),t.stickyPosition(!0)}t._running=!1}))}(e.type))}},{key:"_setSupportFeatures",value:function(){var t=this.support;t.transform=o.supportTransform(),t.transform3d=o.supportTransform(!0)}},{key:"_getTranslate",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:0,e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0,i=arguments.length>2&&void 0!==arguments[2]?arguments[2]:0;return this.support.transform3d?"translate3d("+t+", "+e+", "+i+")":!!this.support.translate&&"translate("+t+", "+e+")"}},{key:"destroy",value:function(){window.removeEventListener("resize",this,{caption:!1}),window.removeEventListener("scroll",this,{caption:!1}),this.sidebar.classList.remove(this.options.stickyClass),this.sidebar.style.minHeight="",this.sidebar.removeEventListener("update"+i,this);var t={inner:{},outer:{}};for(var e in t.inner={position:"",top:"",left:"",bottom:"",width:"",transform:""},t.outer={height:"",position:""},t.outer)this.sidebar.style[e]=t.outer[e];for(var n in t.inner)this.sidebarInner.style[n]=t.inner[n];this.options.resizeSensor&&"undefined"!=typeof ResizeSensor&&(ResizeSensor.detach(this.sidebarInner,this.handleEvent),ResizeSensor.detach(this.container,this.handleEvent))}}],[{key:"supportTransform",value:function(t){var e=!1,i=t?"perspective":"transform",n=i.charAt(0).toUpperCase()+i.slice(1),o=document.createElement("support").style;return(i+" "+["Webkit","Moz","O","ms"].join(n+" ")+n).split(" ").forEach((function(t,i){if(void 0!==o[t])return e=t,!1})),e}},{key:"eventTrigger",value:function(t,e,i){try{var n=new CustomEvent(e,{detail:i})}catch(t){(n=document.createEvent("CustomEvent")).initCustomEvent(e,!0,!0,i)}t.dispatchEvent(n)}},{key:"extend",value:function(t,e){var i={};for(var n in t)void 0!==e[n]?i[n]=e[n]:i[n]=t[n];return i}},{key:"offsetRelative",value:function(t){var e={left:0,top:0};do{var i=t.offsetTop,n=t.offsetLeft;isNaN(i)||(e.top+=i),isNaN(n)||(e.left+=n),t="BODY"===t.tagName?t.parentElement:t.offsetParent}while(t);return e}},{key:"addClass",value:function(t,e){o.hasClass(t,e)||(t.classList?t.classList.add(e):t.className+=" "+e)}},{key:"removeClass",value:function(t,e){o.hasClass(t,e)&&(t.classList?t.classList.remove(e):t.className=t.className.replace(new RegExp("(^|\\b)"+e.split(" ").join("|")+"(\\b|$)","gi")," "))}},{key:"hasClass",value:function(t,e){return t.classList?t.classList.contains(e):new RegExp("(^| )"+e+"( |$)","gi").test(t.className)}}]),o}()}();window.StickySidebar=i,function(){if("undefined"!=typeof window){var t=window.$||window.jQuery||window.Zepto;if(t){t.fn.stickySidebar=function(e){return this.each((function(){var n=t(this),o=t(this).data("stickySidebar");if(o||(o=new i(this,"object"==typeof e&&e),n.data("stickySidebar",o)),"string"==typeof e){if(void 0===o[e]&&-1===["destroy","updateSticky"].indexOf(e))throw new Error('No method named "'+e+'"');o[e]()}}))},t.fn.stickySidebar.Constructor=i;var e=t.fn.stickySidebar;t.fn.stickySidebar.noConflict=function(){return t.fn.stickySidebar=e,this}}}}()}()},function(t,e,i){var n,o,r;
/*!
 * jQuery.scrollTo
 * Copyright (c) 2007-2015 Ariel Flesler - aflesler<a>gmail<d>com | http://flesler.blogspot.com
 * Licensed under MIT
 * http://flesler.blogspot.com/2007/10/jqueryscrollto.html
 * @projectDescription Lightweight, cross-browser and highly customizable animated scrolling with jQuery
 * @author Ariel Flesler
 * @version 2.1.2
 */!function(s){"use strict";o=[i(1)],void 0===(r="function"==typeof(n=function(t){var e=t.scrollTo=function(e,i,n){return t(window).scrollTo(e,i,n)};function i(e){return!e.nodeName||-1!==t.inArray(e.nodeName.toLowerCase(),["iframe","#document","html","body"])}function n(e){return t.isFunction(e)||t.isPlainObject(e)?e:{top:e,left:e}}return e.defaults={axis:"xy",duration:0,limit:!0},t.fn.scrollTo=function(o,r,s){"object"==typeof r&&(s=r,r=0),"function"==typeof s&&(s={onAfter:s}),"max"===o&&(o=9e9),s=t.extend({},e.defaults,s),r=r||s.duration;var a=s.queue&&s.axis.length>1;return a&&(r/=2),s.offset=n(s.offset),s.over=n(s.over),this.each((function(){if(null!==o){var l,c=i(this),u=c?this.contentWindow||window:this,h=t(u),d=o,p={};switch(typeof d){case"number":case"string":if(/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(d)){d=n(d);break}d=c?t(d):t(d,u);case"object":if(0===d.length)return;(d.is||d.style)&&(l=(d=t(d)).offset())}var f=t.isFunction(s.offset)&&s.offset(u,d)||s.offset;t.each(s.axis.split(""),(function(t,i){var n="x"===i?"Left":"Top",o=n.toLowerCase(),r="scroll"+n,g=h[r](),v=e.max(u,i);if(l)p[r]=l[o]+(c?0:g-h.offset()[o]),s.margin&&(p[r]-=parseInt(d.css("margin"+n),10)||0,p[r]-=parseInt(d.css("border"+n+"Width"),10)||0),p[r]+=f[o]||0,s.over[o]&&(p[r]+=d["x"===i?"width":"height"]()*s.over[o]);else{var y=d[o];p[r]=y.slice&&"%"===y.slice(-1)?parseFloat(y)/100*v:y}s.limit&&/^\d+$/.test(p[r])&&(p[r]=p[r]<=0?0:Math.min(p[r],v)),!t&&s.axis.length>1&&(g===p[r]?p={}:a&&(m(s.onAfterFirst),p={}))})),m(s.onAfter)}function m(e){var i=t.extend({},s,{queue:!0,duration:r,complete:e&&function(){e.call(u,d,s)}});h.animate(p,i)}}))},e.max=function(e,n){var o="x"===n?"Width":"Height",r="scroll"+o;if(!i(e))return e[r]-t(e)[o.toLowerCase()]();var s="client"+o,a=e.ownerDocument||e.document,l=a.documentElement,c=a.body;return Math.max(l[r],c[r])-Math.min(l[s],c[s])},t.Tween.propHooks.scrollLeft=t.Tween.propHooks.scrollTop={get:function(e){return t(e.elem)[e.prop]()},set:function(e){var i=this.get(e);if(e.options.interrupt&&e._last&&e._last!==i)return t(e.elem).stop();var n=Math.round(e.now);i!==n&&(t(e.elem)[e.prop](n),e._last=this.get(e))}},e})?n.apply(e,o):n)||(t.exports=r)}()},function(t,e,i){var n,o,r;
/*! Magnific Popup - v1.1.0 - 2016-02-20
* http://dimsemenov.com/plugins/magnific-popup/
* Copyright (c) 2016 Dmitry Semenov; */o=[i(1)],void 0===(r="function"==typeof(n=function(t){var e,i,n,o,r,s,a=function(){},l=!!window.jQuery,c=t(window),u=function(t,i){e.ev.on("mfp"+t+".mfp",i)},h=function(e,i,n,o){var r=document.createElement("div");return r.className="mfp-"+e,n&&(r.innerHTML=n),o?i&&i.appendChild(r):(r=t(r),i&&r.appendTo(i)),r},d=function(i,n){e.ev.triggerHandler("mfp"+i,n),e.st.callbacks&&(i=i.charAt(0).toLowerCase()+i.slice(1),e.st.callbacks[i]&&e.st.callbacks[i].apply(e,t.isArray(n)?n:[n]))},p=function(i){return i===s&&e.currTemplate.closeBtn||(e.currTemplate.closeBtn=t(e.st.closeMarkup.replace("%title%",e.st.tClose)),s=i),e.currTemplate.closeBtn},f=function(){t.magnificPopup.instance||((e=new a).init(),t.magnificPopup.instance=e)};a.prototype={constructor:a,init:function(){var i=navigator.appVersion;e.isLowIE=e.isIE8=document.all&&!document.addEventListener,e.isAndroid=/android/gi.test(i),e.isIOS=/iphone|ipad|ipod/gi.test(i),e.supportsTransition=function(){var t=document.createElement("p").style,e=["ms","O","Moz","Webkit"];if(void 0!==t.transition)return!0;for(;e.length;)if(e.pop()+"Transition"in t)return!0;return!1}(),e.probablyMobile=e.isAndroid||e.isIOS||/(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent),n=t(document),e.popupsCache={}},open:function(i){var o;if(!1===i.isObj){e.items=i.items.toArray(),e.index=0;var s,a=i.items;for(o=0;o<a.length;o++)if((s=a[o]).parsed&&(s=s.el[0]),s===i.el[0]){e.index=o;break}}else e.items=t.isArray(i.items)?i.items:[i.items],e.index=i.index||0;if(!e.isOpen){e.types=[],r="",i.mainEl&&i.mainEl.length?e.ev=i.mainEl.eq(0):e.ev=n,i.key?(e.popupsCache[i.key]||(e.popupsCache[i.key]={}),e.currTemplate=e.popupsCache[i.key]):e.currTemplate={},e.st=t.extend(!0,{},t.magnificPopup.defaults,i),e.fixedContentPos="auto"===e.st.fixedContentPos?!e.probablyMobile:e.st.fixedContentPos,e.st.modal&&(e.st.closeOnContentClick=!1,e.st.closeOnBgClick=!1,e.st.showCloseBtn=!1,e.st.enableEscapeKey=!1),e.bgOverlay||(e.bgOverlay=h("bg").on("click.mfp",(function(){e.close()})),e.wrap=h("wrap").attr("tabindex",-1).on("click.mfp",(function(t){e._checkIfClose(t.target)&&e.close()})),e.container=h("container",e.wrap)),e.contentContainer=h("content"),e.st.preloader&&(e.preloader=h("preloader",e.container,e.st.tLoading));var l=t.magnificPopup.modules;for(o=0;o<l.length;o++){var f=l[o];f=f.charAt(0).toUpperCase()+f.slice(1),e["init"+f].call(e)}d("BeforeOpen"),e.st.showCloseBtn&&(e.st.closeBtnInside?(u("MarkupParse",(function(t,e,i,n){i.close_replaceWith=p(n.type)})),r+=" mfp-close-btn-in"):e.wrap.append(p())),e.st.alignTop&&(r+=" mfp-align-top"),e.fixedContentPos?e.wrap.css({overflow:e.st.overflowY,overflowX:"hidden",overflowY:e.st.overflowY}):e.wrap.css({top:c.scrollTop(),position:"absolute"}),(!1===e.st.fixedBgPos||"auto"===e.st.fixedBgPos&&!e.fixedContentPos)&&e.bgOverlay.css({height:n.height(),position:"absolute"}),e.st.enableEscapeKey&&n.on("keyup.mfp",(function(t){27===t.keyCode&&e.close()})),c.on("resize.mfp",(function(){e.updateSize()})),e.st.closeOnContentClick||(r+=" mfp-auto-cursor"),r&&e.wrap.addClass(r);var m=e.wH=c.height(),g={};if(e.fixedContentPos&&e._hasScrollBar(m)){var v=e._getScrollbarSize();v&&(g.marginRight=v)}e.fixedContentPos&&(e.isIE7?t("body, html").css("overflow","hidden"):g.overflow="hidden");var y=e.st.mainClass;return e.isIE7&&(y+=" mfp-ie7"),y&&e._addClassToMFP(y),e.updateItemHTML(),d("BuildControls"),t("html").css(g),e.bgOverlay.add(e.wrap).prependTo(e.st.prependTo||t(document.body)),e._lastFocusedEl=document.activeElement,setTimeout((function(){e.content?(e._addClassToMFP("mfp-ready"),e._setFocus()):e.bgOverlay.addClass("mfp-ready"),n.on("focusin.mfp",e._onFocusIn)}),16),e.isOpen=!0,e.updateSize(m),d("Open"),i}e.updateItemHTML()},close:function(){e.isOpen&&(d("BeforeClose"),e.isOpen=!1,e.st.removalDelay&&!e.isLowIE&&e.supportsTransition?(e._addClassToMFP("mfp-removing"),setTimeout((function(){e._close()}),e.st.removalDelay)):e._close())},_close:function(){d("Close");var i="mfp-removing mfp-ready ";if(e.bgOverlay.detach(),e.wrap.detach(),e.container.empty(),e.st.mainClass&&(i+=e.st.mainClass+" "),e._removeClassFromMFP(i),e.fixedContentPos){var o={marginRight:""};e.isIE7?t("body, html").css("overflow",""):o.overflow="",t("html").css(o)}n.off("keyup.mfp focusin.mfp"),e.ev.off(".mfp"),e.wrap.attr("class","mfp-wrap").removeAttr("style"),e.bgOverlay.attr("class","mfp-bg"),e.container.attr("class","mfp-container"),!e.st.showCloseBtn||e.st.closeBtnInside&&!0!==e.currTemplate[e.currItem.type]||e.currTemplate.closeBtn&&e.currTemplate.closeBtn.detach(),e.st.autoFocusLast&&e._lastFocusedEl&&t(e._lastFocusedEl).focus(),e.currItem=null,e.content=null,e.currTemplate=null,e.prevHeight=0,d("AfterClose")},updateSize:function(t){if(e.isIOS){var i=document.documentElement.clientWidth/window.innerWidth,n=window.innerHeight*i;e.wrap.css("height",n),e.wH=n}else e.wH=t||c.height();e.fixedContentPos||e.wrap.css("height",e.wH),d("Resize")},updateItemHTML:function(){var i=e.items[e.index];e.contentContainer.detach(),e.content&&e.content.detach(),i.parsed||(i=e.parseEl(e.index));var n=i.type;if(d("BeforeChange",[e.currItem?e.currItem.type:"",n]),e.currItem=i,!e.currTemplate[n]){var r=!!e.st[n]&&e.st[n].markup;d("FirstMarkupParse",r),e.currTemplate[n]=!r||t(r)}o&&o!==i.type&&e.container.removeClass("mfp-"+o+"-holder");var s=e["get"+n.charAt(0).toUpperCase()+n.slice(1)](i,e.currTemplate[n]);e.appendContent(s,n),i.preloaded=!0,d("Change",i),o=i.type,e.container.prepend(e.contentContainer),d("AfterChange")},appendContent:function(t,i){e.content=t,t?e.st.showCloseBtn&&e.st.closeBtnInside&&!0===e.currTemplate[i]?e.content.find(".mfp-close").length||e.content.append(p()):e.content=t:e.content="",d("BeforeAppend"),e.container.addClass("mfp-"+i+"-holder"),e.contentContainer.append(e.content)},parseEl:function(i){var n,o=e.items[i];if(o.tagName?o={el:t(o)}:(n=o.type,o={data:o,src:o.src}),o.el){for(var r=e.types,s=0;s<r.length;s++)if(o.el.hasClass("mfp-"+r[s])){n=r[s];break}o.src=o.el.attr("data-mfp-src"),o.src||(o.src=o.el.attr("href"))}return o.type=n||e.st.type||"inline",o.index=i,o.parsed=!0,e.items[i]=o,d("ElementParse",o),e.items[i]},addGroup:function(t,i){var n=function(n){n.mfpEl=this,e._openClick(n,t,i)};i||(i={});var o="click.magnificPopup";i.mainEl=t,i.items?(i.isObj=!0,t.off(o).on(o,n)):(i.isObj=!1,i.delegate?t.off(o).on(o,i.delegate,n):(i.items=t,t.off(o).on(o,n)))},_openClick:function(i,n,o){if((void 0!==o.midClick?o.midClick:t.magnificPopup.defaults.midClick)||!(2===i.which||i.ctrlKey||i.metaKey||i.altKey||i.shiftKey)){var r=void 0!==o.disableOn?o.disableOn:t.magnificPopup.defaults.disableOn;if(r)if(t.isFunction(r)){if(!r.call(e))return!0}else if(c.width()<r)return!0;i.type&&(i.preventDefault(),e.isOpen&&i.stopPropagation()),o.el=t(i.mfpEl),o.delegate&&(o.items=n.find(o.delegate)),e.open(o)}},updateStatus:function(t,n){if(e.preloader){i!==t&&e.container.removeClass("mfp-s-"+i),n||"loading"!==t||(n=e.st.tLoading);var o={status:t,text:n};d("UpdateStatus",o),t=o.status,n=o.text,e.preloader.html(n),e.preloader.find("a").on("click",(function(t){t.stopImmediatePropagation()})),e.container.addClass("mfp-s-"+t),i=t}},_checkIfClose:function(i){if(!t(i).hasClass("mfp-prevent-close")){var n=e.st.closeOnContentClick,o=e.st.closeOnBgClick;if(n&&o)return!0;if(!e.content||t(i).hasClass("mfp-close")||e.preloader&&i===e.preloader[0])return!0;if(i===e.content[0]||t.contains(e.content[0],i)){if(n)return!0}else if(o&&t.contains(document,i))return!0;return!1}},_addClassToMFP:function(t){e.bgOverlay.addClass(t),e.wrap.addClass(t)},_removeClassFromMFP:function(t){this.bgOverlay.removeClass(t),e.wrap.removeClass(t)},_hasScrollBar:function(t){return(e.isIE7?n.height():document.body.scrollHeight)>(t||c.height())},_setFocus:function(){(e.st.focus?e.content.find(e.st.focus).eq(0):e.wrap).focus()},_onFocusIn:function(i){if(i.target!==e.wrap[0]&&!t.contains(e.wrap[0],i.target))return e._setFocus(),!1},_parseMarkup:function(e,i,n){var o;n.data&&(i=t.extend(n.data,i)),d("MarkupParse",[e,i,n]),t.each(i,(function(i,n){if(void 0===n||!1===n)return!0;if((o=i.split("_")).length>1){var r=e.find(".mfp-"+o[0]);if(r.length>0){var s=o[1];"replaceWith"===s?r[0]!==n[0]&&r.replaceWith(n):"img"===s?r.is("img")?r.attr("src",n):r.replaceWith(t("<img>").attr("src",n).attr("class",r.attr("class"))):r.attr(o[1],n)}}else e.find(".mfp-"+i).html(n)}))},_getScrollbarSize:function(){if(void 0===e.scrollbarSize){var t=document.createElement("div");t.style.cssText="width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;",document.body.appendChild(t),e.scrollbarSize=t.offsetWidth-t.clientWidth,document.body.removeChild(t)}return e.scrollbarSize}},t.magnificPopup={instance:null,proto:a.prototype,modules:[],open:function(e,i){return f(),(e=e?t.extend(!0,{},e):{}).isObj=!0,e.index=i||0,this.instance.open(e)},close:function(){return t.magnificPopup.instance&&t.magnificPopup.instance.close()},registerModule:function(e,i){i.options&&(t.magnificPopup.defaults[e]=i.options),t.extend(this.proto,i.proto),this.modules.push(e)},defaults:{disableOn:0,key:null,midClick:!1,mainClass:"",preloader:!0,focus:"",closeOnContentClick:!1,closeOnBgClick:!0,closeBtnInside:!0,showCloseBtn:!0,enableEscapeKey:!0,modal:!1,alignTop:!1,removalDelay:0,prependTo:null,fixedContentPos:"auto",fixedBgPos:"auto",overflowY:"auto",closeMarkup:'<button title="%title%" type="button" class="mfp-close">&#215;</button>',tClose:"Close (Esc)",tLoading:"Loading...",autoFocusLast:!0}},t.fn.magnificPopup=function(i){f();var n=t(this);if("string"==typeof i)if("open"===i){var o,r=l?n.data("magnificPopup"):n[0].magnificPopup,s=parseInt(arguments[1],10)||0;r.items?o=r.items[s]:(o=n,r.delegate&&(o=o.find(r.delegate)),o=o.eq(s)),e._openClick({mfpEl:o},n,r)}else e.isOpen&&e[i].apply(e,Array.prototype.slice.call(arguments,1));else i=t.extend(!0,{},i),l?n.data("magnificPopup",i):n[0].magnificPopup=i,e.addGroup(n,i);return n};var m,g,v,y=function(){v&&(g.after(v.addClass(m)).detach(),v=null)};t.magnificPopup.registerModule("inline",{options:{hiddenClass:"hide",markup:"",tNotFound:"Content not found"},proto:{initInline:function(){e.types.push("inline"),u("Close.inline",(function(){y()}))},getInline:function(i,n){if(y(),i.src){var o=e.st.inline,r=t(i.src);if(r.length){var s=r[0].parentNode;s&&s.tagName&&(g||(m=o.hiddenClass,g=h(m),m="mfp-"+m),v=r.after(g).detach().removeClass(m)),e.updateStatus("ready")}else e.updateStatus("error",o.tNotFound),r=t("<div>");return i.inlineElement=r,r}return e.updateStatus("ready"),e._parseMarkup(n,{},i),n}}});var b,w=function(){b&&t(document.body).removeClass(b)},x=function(){w(),e.req&&e.req.abort()};t.magnificPopup.registerModule("ajax",{options:{settings:null,cursor:"mfp-ajax-cur",tError:'<a href="%url%">The content</a> could not be loaded.'},proto:{initAjax:function(){e.types.push("ajax"),b=e.st.ajax.cursor,u("Close.ajax",x),u("BeforeChange.ajax",x)},getAjax:function(i){b&&t(document.body).addClass(b),e.updateStatus("loading");var n=t.extend({url:i.src,success:function(n,o,r){var s={data:n,xhr:r};d("ParseAjax",s),e.appendContent(t(s.data),"ajax"),i.finished=!0,w(),e._setFocus(),setTimeout((function(){e.wrap.addClass("mfp-ready")}),16),e.updateStatus("ready"),d("AjaxContentAdded")},error:function(){w(),i.finished=i.loadError=!0,e.updateStatus("error",e.st.ajax.tError.replace("%url%",i.src))}},e.st.ajax.settings);return e.req=t.ajax(n),""}}});var C,k,S=function(i){if(i.data&&void 0!==i.data.title)return i.data.title;var n=e.st.image.titleSrc;if(n){if(t.isFunction(n))return n.call(e,i);if(i.el)return i.el.attr(n)||""}return""};t.magnificPopup.registerModule("image",{options:{markup:'<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',cursor:"mfp-zoom-out-cur",titleSrc:"title",verticalFit:!0,tError:'<a href="%url%">The image</a> could not be loaded.'},proto:{initImage:function(){var i=e.st.image,n=".image";e.types.push("image"),u("Open"+n,(function(){"image"===e.currItem.type&&i.cursor&&t(document.body).addClass(i.cursor)})),u("Close"+n,(function(){i.cursor&&t(document.body).removeClass(i.cursor),c.off("resize.mfp")})),u("Resize"+n,e.resizeImage),e.isLowIE&&u("AfterChange",e.resizeImage)},resizeImage:function(){var t=e.currItem;if(t&&t.img&&e.st.image.verticalFit){var i=0;e.isLowIE&&(i=parseInt(t.img.css("padding-top"),10)+parseInt(t.img.css("padding-bottom"),10)),t.img.css("max-height",e.wH-i)}},_onImageHasSize:function(t){t.img&&(t.hasSize=!0,C&&clearInterval(C),t.isCheckingImgSize=!1,d("ImageHasSize",t),t.imgHidden&&(e.content&&e.content.removeClass("mfp-loading"),t.imgHidden=!1))},findImageSize:function(t){var i=0,n=t.img[0],o=function(r){C&&clearInterval(C),C=setInterval((function(){n.naturalWidth>0?e._onImageHasSize(t):(i>200&&clearInterval(C),3==++i?o(10):40===i?o(50):100===i&&o(500))}),r)};o(1)},getImage:function(i,n){var o=0,r=function(){i&&(i.img[0].complete?(i.img.off(".mfploader"),i===e.currItem&&(e._onImageHasSize(i),e.updateStatus("ready")),i.hasSize=!0,i.loaded=!0,d("ImageLoadComplete")):++o<200?setTimeout(r,100):s())},s=function(){i&&(i.img.off(".mfploader"),i===e.currItem&&(e._onImageHasSize(i),e.updateStatus("error",a.tError.replace("%url%",i.src))),i.hasSize=!0,i.loaded=!0,i.loadError=!0)},a=e.st.image,l=n.find(".mfp-img");if(l.length){var c=document.createElement("img");c.className="mfp-img",i.el&&i.el.find("img").length&&(c.alt=i.el.find("img").attr("alt")),i.img=t(c).on("load.mfploader",r).on("error.mfploader",s),c.src=i.src,l.is("img")&&(i.img=i.img.clone()),(c=i.img[0]).naturalWidth>0?i.hasSize=!0:c.width||(i.hasSize=!1)}return e._parseMarkup(n,{title:S(i),img_replaceWith:i.img},i),e.resizeImage(),i.hasSize?(C&&clearInterval(C),i.loadError?(n.addClass("mfp-loading"),e.updateStatus("error",a.tError.replace("%url%",i.src))):(n.removeClass("mfp-loading"),e.updateStatus("ready")),n):(e.updateStatus("loading"),i.loading=!0,i.hasSize||(i.imgHidden=!0,n.addClass("mfp-loading"),e.findImageSize(i)),n)}}}),t.magnificPopup.registerModule("zoom",{options:{enabled:!1,easing:"ease-in-out",duration:300,opener:function(t){return t.is("img")?t:t.find("img")}},proto:{initZoom:function(){var t,i=e.st.zoom,n=".zoom";if(i.enabled&&e.supportsTransition){var o,r,s=i.duration,a=function(t){var e=t.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),n="all "+i.duration/1e3+"s "+i.easing,o={position:"fixed",zIndex:9999,left:0,top:0,"-webkit-backface-visibility":"hidden"},r="transition";return o["-webkit-"+r]=o["-moz-"+r]=o["-o-"+r]=o[r]=n,e.css(o),e},l=function(){e.content.css("visibility","visible")};u("BuildControls"+n,(function(){if(e._allowZoom()){if(clearTimeout(o),e.content.css("visibility","hidden"),!(t=e._getItemToZoom()))return void l();(r=a(t)).css(e._getOffset()),e.wrap.append(r),o=setTimeout((function(){r.css(e._getOffset(!0)),o=setTimeout((function(){l(),setTimeout((function(){r.remove(),t=r=null,d("ZoomAnimationEnded")}),16)}),s)}),16)}})),u("BeforeClose"+n,(function(){if(e._allowZoom()){if(clearTimeout(o),e.st.removalDelay=s,!t){if(!(t=e._getItemToZoom()))return;r=a(t)}r.css(e._getOffset(!0)),e.wrap.append(r),e.content.css("visibility","hidden"),setTimeout((function(){r.css(e._getOffset())}),16)}})),u("Close"+n,(function(){e._allowZoom()&&(l(),r&&r.remove(),t=null)}))}},_allowZoom:function(){return"image"===e.currItem.type},_getItemToZoom:function(){return!!e.currItem.hasSize&&e.currItem.img},_getOffset:function(i){var n,o=(n=i?e.currItem.img:e.st.zoom.opener(e.currItem.el||e.currItem)).offset(),r=parseInt(n.css("padding-top"),10),s=parseInt(n.css("padding-bottom"),10);o.top-=t(window).scrollTop()-r;var a={width:n.width(),height:(l?n.innerHeight():n[0].offsetHeight)-s-r};return void 0===k&&(k=void 0!==document.createElement("p").style.MozTransform),k?a["-moz-transform"]=a.transform="translate("+o.left+"px,"+o.top+"px)":(a.left=o.left,a.top=o.top),a}}});var E=function(t){if(e.currTemplate.iframe){var i=e.currTemplate.iframe.find("iframe");i.length&&(t||(i[0].src="//about:blank"),e.isIE8&&i.css("display",t?"block":"none"))}};t.magnificPopup.registerModule("iframe",{options:{markup:'<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',srcAction:"iframe_src",patterns:{youtube:{index:"youtube.com",id:"v=",src:"//www.youtube.com/embed/%id%?autoplay=1"},vimeo:{index:"vimeo.com/",id:"/",src:"//player.vimeo.com/video/%id%?autoplay=1"},gmaps:{index:"//maps.google.",src:"%id%&output=embed"}}},proto:{initIframe:function(){e.types.push("iframe"),u("BeforeChange",(function(t,e,i){e!==i&&("iframe"===e?E():"iframe"===i&&E(!0))})),u("Close.iframe",(function(){E()}))},getIframe:function(i,n){var o=i.src,r=e.st.iframe;t.each(r.patterns,(function(){if(o.indexOf(this.index)>-1)return this.id&&(o="string"==typeof this.id?o.substr(o.lastIndexOf(this.id)+this.id.length,o.length):this.id.call(this,o)),o=this.src.replace("%id%",o),!1}));var s={};return r.srcAction&&(s[r.srcAction]=o),e._parseMarkup(n,s,i),e.updateStatus("ready"),n}}});var T=function(t){var i=e.items.length;return t>i-1?t-i:t<0?i+t:t},_=function(t,e,i){return t.replace(/%curr%/gi,e+1).replace(/%total%/gi,i)};t.magnificPopup.registerModule("gallery",{options:{enabled:!1,arrowMarkup:'<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',preload:[0,2],navigateByImgClick:!0,arrows:!0,tPrev:"Previous (Left arrow key)",tNext:"Next (Right arrow key)",tCounter:"%curr% of %total%"},proto:{initGallery:function(){var i=e.st.gallery,o=".mfp-gallery";if(e.direction=!0,!i||!i.enabled)return!1;r+=" mfp-gallery",u("Open"+o,(function(){i.navigateByImgClick&&e.wrap.on("click"+o,".mfp-img",(function(){if(e.items.length>1)return e.next(),!1})),n.on("keydown"+o,(function(t){37===t.keyCode?e.prev():39===t.keyCode&&e.next()}))})),u("UpdateStatus"+o,(function(t,i){i.text&&(i.text=_(i.text,e.currItem.index,e.items.length))})),u("MarkupParse"+o,(function(t,n,o,r){var s=e.items.length;o.counter=s>1?_(i.tCounter,r.index,s):""})),u("BuildControls"+o,(function(){if(e.items.length>1&&i.arrows&&!e.arrowLeft){var n=i.arrowMarkup,o=e.arrowLeft=t(n.replace(/%title%/gi,i.tPrev).replace(/%dir%/gi,"left")).addClass("mfp-prevent-close"),r=e.arrowRight=t(n.replace(/%title%/gi,i.tNext).replace(/%dir%/gi,"right")).addClass("mfp-prevent-close");o.click((function(){e.prev()})),r.click((function(){e.next()})),e.container.append(o.add(r))}})),u("Change"+o,(function(){e._preloadTimeout&&clearTimeout(e._preloadTimeout),e._preloadTimeout=setTimeout((function(){e.preloadNearbyImages(),e._preloadTimeout=null}),16)})),u("Close"+o,(function(){n.off(o),e.wrap.off("click"+o),e.arrowRight=e.arrowLeft=null}))},next:function(){e.direction=!0,e.index=T(e.index+1),e.updateItemHTML()},prev:function(){e.direction=!1,e.index=T(e.index-1),e.updateItemHTML()},goTo:function(t){e.direction=t>=e.index,e.index=t,e.updateItemHTML()},preloadNearbyImages:function(){var t,i=e.st.gallery.preload,n=Math.min(i[0],e.items.length),o=Math.min(i[1],e.items.length);for(t=1;t<=(e.direction?o:n);t++)e._preloadItem(e.index+t);for(t=1;t<=(e.direction?n:o);t++)e._preloadItem(e.index-t)},_preloadItem:function(i){if(i=T(i),!e.items[i].preloaded){var n=e.items[i];n.parsed||(n=e.parseEl(i)),d("LazyLoad",n),"image"===n.type&&(n.img=t('<img class="mfp-img" />').on("load.mfploader",(function(){n.hasSize=!0})).on("error.mfploader",(function(){n.hasSize=!0,n.loadError=!0,d("LazyLoadError",n)})).attr("src",n.src)),n.preloaded=!0}}}}),t.magnificPopup.registerModule("retina",{options:{replaceSrc:function(t){return t.src.replace(/\.\w+$/,(function(t){return"@2x"+t}))},ratio:1},proto:{initRetina:function(){if(window.devicePixelRatio>1){var t=e.st.retina,i=t.ratio;(i=isNaN(i)?i():i)>1&&(u("ImageHasSize.retina",(function(t,e){e.img.css({"max-width":e.img[0].naturalWidth/i,width:"100%"})})),u("ElementParse.retina",(function(e,n){n.src=t.replaceSrc(n,i)})))}}}}),f()})?n.apply(e,o):n)||(t.exports=r)},function(t,e){
/*!
Waypoints - 4.0.1
Copyright © 2011-2016 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blob/master/licenses.txt
*/
!function(){"use strict";var t=0,e={};function i(n){if(!n)throw new Error("No options passed to Waypoint constructor");if(!n.element)throw new Error("No element option passed to Waypoint constructor");if(!n.handler)throw new Error("No handler option passed to Waypoint constructor");this.key="waypoint-"+t,this.options=i.Adapter.extend({},i.defaults,n),this.element=this.options.element,this.adapter=new i.Adapter(this.element),this.callback=n.handler,this.axis=this.options.horizontal?"horizontal":"vertical",this.enabled=this.options.enabled,this.triggerPoint=null,this.group=i.Group.findOrCreate({name:this.options.group,axis:this.axis}),this.context=i.Context.findOrCreateByElement(this.options.context),i.offsetAliases[this.options.offset]&&(this.options.offset=i.offsetAliases[this.options.offset]),this.group.add(this),this.context.add(this),e[this.key]=this,t+=1}i.prototype.queueTrigger=function(t){this.group.queueTrigger(this,t)},i.prototype.trigger=function(t){this.enabled&&this.callback&&this.callback.apply(this,t)},i.prototype.destroy=function(){this.context.remove(this),this.group.remove(this),delete e[this.key]},i.prototype.disable=function(){return this.enabled=!1,this},i.prototype.enable=function(){return this.context.refresh(),this.enabled=!0,this},i.prototype.next=function(){return this.group.next(this)},i.prototype.previous=function(){return this.group.previous(this)},i.invokeAll=function(t){var i=[];for(var n in e)i.push(e[n]);for(var o=0,r=i.length;o<r;o++)i[o][t]()},i.destroyAll=function(){i.invokeAll("destroy")},i.disableAll=function(){i.invokeAll("disable")},i.enableAll=function(){for(var t in i.Context.refreshAll(),e)e[t].enabled=!0;return this},i.refreshAll=function(){i.Context.refreshAll()},i.viewportHeight=function(){return window.innerHeight||document.documentElement.clientHeight},i.viewportWidth=function(){return document.documentElement.clientWidth},i.adapters=[],i.defaults={context:window,continuous:!0,enabled:!0,group:"default",horizontal:!1,offset:0},i.offsetAliases={"bottom-in-view":function(){return this.context.innerHeight()-this.adapter.outerHeight()},"right-in-view":function(){return this.context.innerWidth()-this.adapter.outerWidth()}},window.Waypoint=i}(),function(){"use strict";function t(t){window.setTimeout(t,1e3/60)}var e=0,i={},n=window.Waypoint,o=window.onload;function r(t){this.element=t,this.Adapter=n.Adapter,this.adapter=new this.Adapter(t),this.key="waypoint-context-"+e,this.didScroll=!1,this.didResize=!1,this.oldScroll={x:this.adapter.scrollLeft(),y:this.adapter.scrollTop()},this.waypoints={vertical:{},horizontal:{}},t.waypointContextKey=this.key,i[t.waypointContextKey]=this,e+=1,n.windowContext||(n.windowContext=!0,n.windowContext=new r(window)),this.createThrottledScrollHandler(),this.createThrottledResizeHandler()}r.prototype.add=function(t){var e=t.options.horizontal?"horizontal":"vertical";this.waypoints[e][t.key]=t,this.refresh()},r.prototype.checkEmpty=function(){var t=this.Adapter.isEmptyObject(this.waypoints.horizontal),e=this.Adapter.isEmptyObject(this.waypoints.vertical),n=this.element==this.element.window;t&&e&&!n&&(this.adapter.off(".waypoints"),delete i[this.key])},r.prototype.createThrottledResizeHandler=function(){var t=this;function e(){t.handleResize(),t.didResize=!1}this.adapter.on("resize.waypoints",(function(){t.didResize||(t.didResize=!0,n.requestAnimationFrame(e))}))},r.prototype.createThrottledScrollHandler=function(){var t=this;function e(){t.handleScroll(),t.didScroll=!1}this.adapter.on("scroll.waypoints",(function(){t.didScroll&&!n.isTouch||(t.didScroll=!0,n.requestAnimationFrame(e))}))},r.prototype.handleResize=function(){n.Context.refreshAll()},r.prototype.handleScroll=function(){var t={},e={horizontal:{newScroll:this.adapter.scrollLeft(),oldScroll:this.oldScroll.x,forward:"right",backward:"left"},vertical:{newScroll:this.adapter.scrollTop(),oldScroll:this.oldScroll.y,forward:"down",backward:"up"}};for(var i in e){var n=e[i],o=n.newScroll>n.oldScroll?n.forward:n.backward;for(var r in this.waypoints[i]){var s=this.waypoints[i][r];if(null!==s.triggerPoint){var a=n.oldScroll<s.triggerPoint,l=n.newScroll>=s.triggerPoint;(a&&l||!a&&!l)&&(s.queueTrigger(o),t[s.group.id]=s.group)}}}for(var c in t)t[c].flushTriggers();this.oldScroll={x:e.horizontal.newScroll,y:e.vertical.newScroll}},r.prototype.innerHeight=function(){return this.element==this.element.window?n.viewportHeight():this.adapter.innerHeight()},r.prototype.remove=function(t){delete this.waypoints[t.axis][t.key],this.checkEmpty()},r.prototype.innerWidth=function(){return this.element==this.element.window?n.viewportWidth():this.adapter.innerWidth()},r.prototype.destroy=function(){var t=[];for(var e in this.waypoints)for(var i in this.waypoints[e])t.push(this.waypoints[e][i]);for(var n=0,o=t.length;n<o;n++)t[n].destroy()},r.prototype.refresh=function(){var t,e=this.element==this.element.window,i=e?void 0:this.adapter.offset(),o={};for(var r in this.handleScroll(),t={horizontal:{contextOffset:e?0:i.left,contextScroll:e?0:this.oldScroll.x,contextDimension:this.innerWidth(),oldScroll:this.oldScroll.x,forward:"right",backward:"left",offsetProp:"left"},vertical:{contextOffset:e?0:i.top,contextScroll:e?0:this.oldScroll.y,contextDimension:this.innerHeight(),oldScroll:this.oldScroll.y,forward:"down",backward:"up",offsetProp:"top"}}){var s=t[r];for(var a in this.waypoints[r]){var l,c,u,h,d=this.waypoints[r][a],p=d.options.offset,f=d.triggerPoint,m=0,g=null==f;d.element!==d.element.window&&(m=d.adapter.offset()[s.offsetProp]),"function"==typeof p?p=p.apply(d):"string"==typeof p&&(p=parseFloat(p),d.options.offset.indexOf("%")>-1&&(p=Math.ceil(s.contextDimension*p/100))),l=s.contextScroll-s.contextOffset,d.triggerPoint=Math.floor(m+l-p),c=f<s.oldScroll,u=d.triggerPoint>=s.oldScroll,h=!c&&!u,!g&&(c&&u)?(d.queueTrigger(s.backward),o[d.group.id]=d.group):(!g&&h||g&&s.oldScroll>=d.triggerPoint)&&(d.queueTrigger(s.forward),o[d.group.id]=d.group)}}return n.requestAnimationFrame((function(){for(var t in o)o[t].flushTriggers()})),this},r.findOrCreateByElement=function(t){return r.findByElement(t)||new r(t)},r.refreshAll=function(){for(var t in i)i[t].refresh()},r.findByElement=function(t){return i[t.waypointContextKey]},window.onload=function(){o&&o(),r.refreshAll()},n.requestAnimationFrame=function(e){(window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||t).call(window,e)},n.Context=r}(),function(){"use strict";function t(t,e){return t.triggerPoint-e.triggerPoint}function e(t,e){return e.triggerPoint-t.triggerPoint}var i={vertical:{},horizontal:{}},n=window.Waypoint;function o(t){this.name=t.name,this.axis=t.axis,this.id=this.name+"-"+this.axis,this.waypoints=[],this.clearTriggerQueues(),i[this.axis][this.name]=this}o.prototype.add=function(t){this.waypoints.push(t)},o.prototype.clearTriggerQueues=function(){this.triggerQueues={up:[],down:[],left:[],right:[]}},o.prototype.flushTriggers=function(){for(var i in this.triggerQueues){var n=this.triggerQueues[i],o="up"===i||"left"===i;n.sort(o?e:t);for(var r=0,s=n.length;r<s;r+=1){var a=n[r];(a.options.continuous||r===n.length-1)&&a.trigger([i])}}this.clearTriggerQueues()},o.prototype.next=function(e){this.waypoints.sort(t);var i=n.Adapter.inArray(e,this.waypoints);return i===this.waypoints.length-1?null:this.waypoints[i+1]},o.prototype.previous=function(e){this.waypoints.sort(t);var i=n.Adapter.inArray(e,this.waypoints);return i?this.waypoints[i-1]:null},o.prototype.queueTrigger=function(t,e){this.triggerQueues[e].push(t)},o.prototype.remove=function(t){var e=n.Adapter.inArray(t,this.waypoints);e>-1&&this.waypoints.splice(e,1)},o.prototype.first=function(){return this.waypoints[0]},o.prototype.last=function(){return this.waypoints[this.waypoints.length-1]},o.findOrCreate=function(t){return i[t.axis][t.name]||new o(t)},n.Group=o}(),function(){"use strict";var t=window.jQuery,e=window.Waypoint;function i(e){this.$element=t(e)}t.each(["innerHeight","innerWidth","off","offset","on","outerHeight","outerWidth","scrollLeft","scrollTop"],(function(t,e){i.prototype[e]=function(){var t=Array.prototype.slice.call(arguments);return this.$element[e].apply(this.$element,t)}})),t.each(["extend","inArray","isEmptyObject"],(function(e,n){i[n]=t[n]})),e.adapters.push({name:"jquery",Adapter:i}),e.Adapter=i}(),function(){"use strict";var t=window.Waypoint;function e(e){return function(){var i=[],n=arguments[0];return e.isFunction(arguments[0])&&((n=e.extend({},arguments[1])).handler=arguments[0]),this.each((function(){var o=e.extend({},n,{element:this});"string"==typeof o.context&&(o.context=e(this).closest(o.context)[0]),i.push(new t(o))})),i}}window.jQuery&&(window.jQuery.fn.waypoint=e(window.jQuery)),window.Zepto&&(window.Zepto.fn.waypoint=e(window.Zepto))}()},function(t,e){!function(t,e,i){function n(e,i){this.bodyOverflowX,this.callbacks={hide:[],show:[]},this.checkInterval=null,this.Content,this.$el=t(e),this.$elProxy,this.elProxyPosition,this.enabled=!0,this.options=t.extend({},a,i),this.mouseIsOverProxy=!1,this.namespace="tooltipster-"+Math.round(1e5*Math.random()),this.Status="hidden",this.timerHide=null,this.timerShow=null,this.$tooltip,this.options.iconTheme=this.options.iconTheme.replace(".",""),this.options.theme=this.options.theme.replace(".",""),this._init()}function o(e,i){var n=!0;return t.each(e,(function(t,o){if(void 0===i[t]||e[t]!==i[t])return n=!1,!1})),n}function r(){return!c&&l}function s(){var t=(i.body||i.documentElement).style,e="transition";if("string"==typeof t[e])return!0;v=["Moz","Webkit","Khtml","O","ms"],e=e.charAt(0).toUpperCase()+e.substr(1);for(var n=0;n<v.length;n++)if("string"==typeof t[v[n]+e])return!0;return!1}var a={animation:"fade",arrow:!0,arrowColor:"",autoClose:!0,content:null,contentAsHTML:!1,contentCloning:!0,debug:!0,delay:200,minWidth:0,maxWidth:null,functionInit:function(t,e){},functionBefore:function(t,e){e()},functionReady:function(t,e){},functionAfter:function(t){},hideOnClick:!1,icon:"(?)",iconCloning:!0,iconDesktop:!1,iconTouch:!1,iconTheme:"tooltipster-icon",interactive:!1,interactiveTolerance:350,multiple:!1,offsetX:0,offsetY:0,onlyOne:!1,position:"top",positionTracker:!1,positionTrackerCallback:function(t){"hover"==this.option("trigger")&&this.option("autoClose")&&this.hide()},restoration:"current",speed:350,timer:0,theme:"tooltipster-default",touchDevices:!0,trigger:"hover",updateAnimation:!0};n.prototype={_init:function(){var e=this;if(i.querySelector){var n=null;void 0===e.$el.data("tooltipster-initialTitle")&&(void 0===(n=e.$el.attr("title"))&&(n=null),e.$el.data("tooltipster-initialTitle",n)),null!==e.options.content?e._content_set(e.options.content):e._content_set(n);var o=e.options.functionInit.call(e.$el,e.$el,e.Content);void 0!==o&&e._content_set(o),e.$el.removeAttr("title").addClass("tooltipstered"),!l&&e.options.iconDesktop||l&&e.options.iconTouch?("string"==typeof e.options.icon?(e.$elProxy=t('<span class="'+e.options.iconTheme+'"></span>'),e.$elProxy.text(e.options.icon)):e.options.iconCloning?e.$elProxy=e.options.icon.clone(!0):e.$elProxy=e.options.icon,e.$elProxy.insertAfter(e.$el)):e.$elProxy=e.$el,"hover"==e.options.trigger?(e.$elProxy.on("mouseenter."+e.namespace,(function(){r()&&!e.options.touchDevices||(e.mouseIsOverProxy=!0,e._show())})).on("mouseleave."+e.namespace,(function(){r()&&!e.options.touchDevices||(e.mouseIsOverProxy=!1)})),l&&e.options.touchDevices&&e.$elProxy.on("touchstart."+e.namespace,(function(){e._showNow()}))):"click"==e.options.trigger&&e.$elProxy.on("click."+e.namespace,(function(){r()&&!e.options.touchDevices||e._show()}))}},_show:function(){var t=this;"shown"!=t.Status&&"appearing"!=t.Status&&(t.options.delay?t.timerShow=setTimeout((function(){("click"==t.options.trigger||"hover"==t.options.trigger&&t.mouseIsOverProxy)&&t._showNow()}),t.options.delay):t._showNow())},_showNow:function(i){var n=this;n.options.functionBefore.call(n.$el,n.$el,(function(){if(n.enabled&&null!==n.Content){i&&n.callbacks.show.push(i),n.callbacks.hide=[],clearTimeout(n.timerShow),n.timerShow=null,clearTimeout(n.timerHide),n.timerHide=null,n.options.onlyOne&&t(".tooltipstered").not(n.$el).each((function(e,i){var n=t(i),o=n.data("tooltipster-ns");t.each(o,(function(t,e){var i=n.data(e),o=i.status(),r=i.option("autoClose");"hidden"!==o&&"disappearing"!==o&&r&&i.hide()}))}));var o=function(){n.Status="shown",t.each(n.callbacks.show,(function(t,e){e.call(n.$el)})),n.callbacks.show=[]};if("hidden"!==n.Status){var r=0;"disappearing"===n.Status?(n.Status="appearing",s()?(n.$tooltip.clearQueue().removeClass("tooltipster-dying").addClass("tooltipster-"+n.options.animation+"-show"),n.options.speed>0&&n.$tooltip.delay(n.options.speed),n.$tooltip.queue(o)):n.$tooltip.stop().fadeIn(o)):"shown"===n.Status&&o()}else{n.Status="appearing";r=n.options.speed;n.bodyOverflowX=t("body").css("overflow-x"),t("body").css("overflow-x","hidden");var a="tooltipster-"+n.options.animation,c="-webkit-transition-duration: "+n.options.speed+"ms; -webkit-animation-duration: "+n.options.speed+"ms; -moz-transition-duration: "+n.options.speed+"ms; -moz-animation-duration: "+n.options.speed+"ms; -o-transition-duration: "+n.options.speed+"ms; -o-animation-duration: "+n.options.speed+"ms; -ms-transition-duration: "+n.options.speed+"ms; -ms-animation-duration: "+n.options.speed+"ms; transition-duration: "+n.options.speed+"ms; animation-duration: "+n.options.speed+"ms;",u=n.options.minWidth?"min-width:"+Math.round(n.options.minWidth)+"px;":"",h=n.options.maxWidth?"max-width:"+Math.round(n.options.maxWidth)+"px;":"",d=n.options.interactive?"pointer-events: auto;":"";if(n.$tooltip=t('<div class="tooltipster-base '+n.options.theme+'" style="'+u+" "+h+" "+d+" "+c+'"><div class="tooltipster-content"></div></div>'),s()&&n.$tooltip.addClass(a),n._content_insert(),n.$tooltip.appendTo("body"),n.reposition(),n.options.functionReady.call(n.$el,n.$el,n.$tooltip),s()?(n.$tooltip.addClass(a+"-show"),n.options.speed>0&&n.$tooltip.delay(n.options.speed),n.$tooltip.queue(o)):n.$tooltip.css("display","none").fadeIn(n.options.speed,o),n._interval_set(),t(e).on("scroll."+n.namespace+" resize."+n.namespace,(function(){n.reposition()})),n.options.autoClose)if(t("body").off("."+n.namespace),"hover"==n.options.trigger){if(l&&setTimeout((function(){t("body").on("touchstart."+n.namespace,(function(){n.hide()}))}),0),n.options.interactive){l&&n.$tooltip.on("touchstart."+n.namespace,(function(t){t.stopPropagation()}));var p=null;n.$elProxy.add(n.$tooltip).on("mouseleave."+n.namespace+"-autoClose",(function(){clearTimeout(p),p=setTimeout((function(){n.hide()}),n.options.interactiveTolerance)})).on("mouseenter."+n.namespace+"-autoClose",(function(){clearTimeout(p)}))}else n.$elProxy.on("mouseleave."+n.namespace+"-autoClose",(function(){n.hide()}));n.options.hideOnClick&&n.$elProxy.on("click."+n.namespace+"-autoClose",(function(){n.hide()}))}else"click"==n.options.trigger&&(setTimeout((function(){t("body").on("click."+n.namespace+" touchstart."+n.namespace,(function(){n.hide()}))}),0),n.options.interactive&&n.$tooltip.on("click."+n.namespace+" touchstart."+n.namespace,(function(t){t.stopPropagation()})))}n.options.timer>0&&(n.timerHide=setTimeout((function(){n.timerHide=null,n.hide()}),n.options.timer+r))}}))},_interval_set:function(){var e=this;e.checkInterval=setInterval((function(){if(0===t("body").find(e.$el).length||0===t("body").find(e.$elProxy).length||"hidden"==e.Status||0===t("body").find(e.$tooltip).length)"shown"!=e.Status&&"appearing"!=e.Status||e.hide(),e._interval_cancel();else if(e.options.positionTracker){var i=e._repositionInfo(e.$elProxy),n=!1;o(i.dimension,e.elProxyPosition.dimension)&&("fixed"===e.$elProxy.css("position")?o(i.position,e.elProxyPosition.position)&&(n=!0):o(i.offset,e.elProxyPosition.offset)&&(n=!0)),n||(e.reposition(),e.options.positionTrackerCallback.call(e,e.$el))}}),200)},_interval_cancel:function(){clearInterval(this.checkInterval),this.checkInterval=null},_content_set:function(t){"object"==typeof t&&null!==t&&this.options.contentCloning&&(t=t.clone(!0)),this.Content=t},_content_insert:function(){var t=this,e=this.$tooltip.find(".tooltipster-content");"string"!=typeof t.Content||t.options.contentAsHTML?e.empty().append(t.Content):e.text(t.Content)},_update:function(t){var e=this;e._content_set(t),null!==e.Content?"hidden"!==e.Status&&(e._content_insert(),e.reposition(),e.options.updateAnimation&&(s()?(e.$tooltip.css({width:"","-webkit-transition":"all "+e.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-moz-transition":"all "+e.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-o-transition":"all "+e.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-ms-transition":"all "+e.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms",transition:"all "+e.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms"}).addClass("tooltipster-content-changing"),setTimeout((function(){"hidden"!=e.Status&&(e.$tooltip.removeClass("tooltipster-content-changing"),setTimeout((function(){"hidden"!==e.Status&&e.$tooltip.css({"-webkit-transition":e.options.speed+"ms","-moz-transition":e.options.speed+"ms","-o-transition":e.options.speed+"ms","-ms-transition":e.options.speed+"ms",transition:e.options.speed+"ms"})}),e.options.speed))}),e.options.speed)):e.$tooltip.fadeTo(e.options.speed,.5,(function(){"hidden"!=e.Status&&e.$tooltip.fadeTo(e.options.speed,1)})))):e.hide()},_repositionInfo:function(t){return{dimension:{height:t.outerHeight(!1),width:t.outerWidth(!1)},offset:t.offset(),position:{left:parseInt(t.css("left")),top:parseInt(t.css("top"))}}},hide:function(i){var n=this;i&&n.callbacks.hide.push(i),n.callbacks.show=[],clearTimeout(n.timerShow),n.timerShow=null,clearTimeout(n.timerHide),n.timerHide=null;var o=function(){t.each(n.callbacks.hide,(function(t,e){e.call(n.$el)})),n.callbacks.hide=[]};if("shown"==n.Status||"appearing"==n.Status){n.Status="disappearing";var r=function(){n.Status="hidden","object"==typeof n.Content&&null!==n.Content&&n.Content.detach(),n.$tooltip.remove(),n.$tooltip=null,t(e).off("."+n.namespace),t("body").off("."+n.namespace).css("overflow-x",n.bodyOverflowX),t("body").off("."+n.namespace),n.$elProxy.off("."+n.namespace+"-autoClose"),n.options.functionAfter.call(n.$el,n.$el),o()};s()?(n.$tooltip.clearQueue().removeClass("tooltipster-"+n.options.animation+"-show").addClass("tooltipster-dying"),n.options.speed>0&&n.$tooltip.delay(n.options.speed),n.$tooltip.queue(r)):n.$tooltip.stop().fadeOut(n.options.speed,r)}else"hidden"==n.Status&&o();return n},show:function(t){return this._showNow(t),this},update:function(t){return this.content(t)},content:function(t){return void 0===t?this.Content:(this._update(t),this)},reposition:function(){var i=this;if(0!==t("body").find(i.$tooltip).length){i.$tooltip.css("width",""),i.elProxyPosition=i._repositionInfo(i.$elProxy);var n=null,o=t(e).width(),r=i.elProxyPosition,s=i.$tooltip.outerWidth(!1),a=(i.$tooltip.innerWidth(),i.$tooltip.outerHeight(!1));if(i.$elProxy.is("area")){var l=i.$elProxy.attr("shape"),c=i.$elProxy.parent().attr("name"),u=t('img[usemap="#'+c+'"]'),h=u.offset().left,d=u.offset().top,p=void 0!==i.$elProxy.attr("coords")?i.$elProxy.attr("coords").split(","):void 0;if("circle"==l){var f=parseInt(p[0]),m=parseInt(p[1]),g=parseInt(p[2]);r.dimension.height=2*g,r.dimension.width=2*g,r.offset.top=d+m-g,r.offset.left=h+f-g}else if("rect"==l){f=parseInt(p[0]),m=parseInt(p[1]);var v=parseInt(p[2]),y=parseInt(p[3]);r.dimension.height=y-m,r.dimension.width=v-f,r.offset.top=d+m,r.offset.left=h+f}else if("poly"==l){for(var b=0,w=0,x=0,C=0,k="even",S=0;S<p.length;S++){var E=parseInt(p[S]);"even"==k?(E>x&&(x=E,0===S&&(b=x)),E<b&&(b=E),k="odd"):(E>C&&(C=E,1==S&&(w=C)),E<w&&(w=E),k="even")}r.dimension.height=C-w,r.dimension.width=x-b,r.offset.top=d+w,r.offset.left=h+b}else r.dimension.height=u.outerHeight(!1),r.dimension.width=u.outerWidth(!1),r.offset.top=d,r.offset.left=h}var T=0,_=0,j=0,I=parseInt(i.options.offsetY),P=parseInt(i.options.offsetX),A=i.options.position;function O(){var i=t(e).scrollLeft();T-i<0&&(n=T-i,T=i),T+s-i>o&&(n=T-(o+i-s),T=o+i-s)}function D(i,n){r.offset.top-t(e).scrollTop()-a-I-12<0&&n.indexOf("top")>-1&&(A=i),r.offset.top+r.dimension.height+a+12+I>t(e).scrollTop()+t(e).height()&&n.indexOf("bottom")>-1&&(A=i,j=r.offset.top-a-I-12)}if("top"==A){var L=r.offset.left+s-(r.offset.left+r.dimension.width);T=r.offset.left+P-L/2,j=r.offset.top-a-I-12,O(),D("bottom","top")}if("top-left"==A&&(T=r.offset.left+P,j=r.offset.top-a-I-12,O(),D("bottom-left","top-left")),"top-right"==A&&(T=r.offset.left+r.dimension.width+P-s,j=r.offset.top-a-I-12,O(),D("bottom-right","top-right")),"bottom"==A){L=r.offset.left+s-(r.offset.left+r.dimension.width);T=r.offset.left-L/2+P,j=r.offset.top+r.dimension.height+I+12,O(),D("top","bottom")}if("bottom-left"==A&&(T=r.offset.left+P,j=r.offset.top+r.dimension.height+I+12,O(),D("top-left","bottom-left")),"bottom-right"==A&&(T=r.offset.left+r.dimension.width+P-s,j=r.offset.top+r.dimension.height+I+12,O(),D("top-right","bottom-right")),"left"==A){T=r.offset.left-P-s-12,_=r.offset.left+P+r.dimension.width+12;var Q=r.offset.top+a-(r.offset.top+r.dimension.height);if(j=r.offset.top-Q/2-I,T<0&&_+s>o){var M=2*parseFloat(i.$tooltip.css("border-width")),z=s+T-M;i.$tooltip.css("width",z+"px"),a=i.$tooltip.outerHeight(!1),T=r.offset.left-P-z-12-M,Q=r.offset.top+a-(r.offset.top+r.dimension.height),j=r.offset.top-Q/2-I}else T<0&&(T=r.offset.left+P+r.dimension.width+12,n="left")}if("right"==A){T=r.offset.left+P+r.dimension.width+12,_=r.offset.left-P-s-12;Q=r.offset.top+a-(r.offset.top+r.dimension.height);if(j=r.offset.top-Q/2-I,T+s>o&&_<0){M=2*parseFloat(i.$tooltip.css("border-width")),z=o-T-M;i.$tooltip.css("width",z+"px"),a=i.$tooltip.outerHeight(!1),Q=r.offset.top+a-(r.offset.top+r.dimension.height),j=r.offset.top-Q/2-I}else T+s>o&&(T=r.offset.left-P-s-12,n="right")}if(i.options.arrow){var F="tooltipster-arrow-"+A;if(i.options.arrowColor.length<1)var B=i.$tooltip.css("background-color");else B=i.options.arrowColor;if(n?"left"==n?(F="tooltipster-arrow-right",n=""):"right"==n?(F="tooltipster-arrow-left",n=""):n="left:"+Math.round(n)+"px;":n="","top"==A||"top-left"==A||"top-right"==A)var W=parseFloat(i.$tooltip.css("border-bottom-width")),H=i.$tooltip.css("border-bottom-color");else if("bottom"==A||"bottom-left"==A||"bottom-right"==A)W=parseFloat(i.$tooltip.css("border-top-width")),H=i.$tooltip.css("border-top-color");else if("left"==A)W=parseFloat(i.$tooltip.css("border-right-width")),H=i.$tooltip.css("border-right-color");else if("right"==A)W=parseFloat(i.$tooltip.css("border-left-width")),H=i.$tooltip.css("border-left-color");else W=parseFloat(i.$tooltip.css("border-bottom-width")),H=i.$tooltip.css("border-bottom-color");W>1&&W++;var N="";if(0!==W){var R="",$="border-color: "+H+";";-1!==F.indexOf("bottom")?R="margin-top: -"+Math.round(W)+"px;":-1!==F.indexOf("top")?R="margin-bottom: -"+Math.round(W)+"px;":-1!==F.indexOf("left")?R="margin-right: -"+Math.round(W)+"px;":-1!==F.indexOf("right")&&(R="margin-left: -"+Math.round(W)+"px;"),N='<span class="tooltipster-arrow-border" style="'+R+" "+$+';"></span>'}i.$tooltip.find(".tooltipster-arrow").remove();var V='<div class="'+F+' tooltipster-arrow" style="'+n+'">'+N+'<span style="border-color:'+B+';"></span></div>';i.$tooltip.append(V)}i.$tooltip.css({top:Math.round(j)+"px",left:Math.round(T)+"px"})}return i},enable:function(){return this.enabled=!0,this},disable:function(){return this.hide(),this.enabled=!1,this},destroy:function(){var e=this;e.hide(),e.$el[0]!==e.$elProxy[0]&&e.$elProxy.remove(),e.$el.removeData(e.namespace).off("."+e.namespace);var i=e.$el.data("tooltipster-ns");if(1===i.length){var n=null;"previous"===e.options.restoration?n=e.$el.data("tooltipster-initialTitle"):"current"===e.options.restoration&&(n="string"==typeof e.Content?e.Content:t("<div></div>").append(e.Content).html()),n&&e.$el.attr("title",n),e.$el.removeClass("tooltipstered").removeData("tooltipster-ns").removeData("tooltipster-initialTitle")}else i=t.grep(i,(function(t,i){return t!==e.namespace})),e.$el.data("tooltipster-ns",i);return e},elementIcon:function(){return this.$el[0]!==this.$elProxy[0]?this.$elProxy[0]:void 0},elementTooltip:function(){return this.$tooltip?this.$tooltip[0]:void 0},option:function(t,e){return void 0===e?this.options[t]:(this.options[t]=e,this)},status:function(){return this.Status}},t.fn.tooltipster=function(){var e=arguments;if(0===this.length){if("string"==typeof e[0]){var i=!0;switch(e[0]){case"setDefaults":t.extend(a,e[1]);break;default:i=!1}return!!i||this}return this}if("string"==typeof e[0]){var o="#*$~&";return this.each((function(){var i=t(this).data("tooltipster-ns"),n=i?t(this).data(i[0]):null;if(!n)throw new Error("You called Tooltipster's \""+e[0]+'" method on an uninitialized element');if("function"!=typeof n[e[0]])throw new Error('Unknown method .tooltipster("'+e[0]+'")');var r=n[e[0]](e[1],e[2]);if(r!==n)return o=r,!1})),"#*$~&"!==o?o:this}var r=[],s=e[0]&&void 0!==e[0].multiple,l=s&&e[0].multiple||!s&&a.multiple,c=e[0]&&void 0!==e[0].debug,u=c&&e[0].debug||!c&&a.debug;return this.each((function(){var i=!1,o=t(this).data("tooltipster-ns"),s=null;o?l?i=!0:u&&console.log('Tooltipster: one or more tooltips are already attached to this element: ignoring. Use the "multiple" option to attach more tooltips.'):i=!0,i&&(s=new n(this,e[0]),o||(o=[]),o.push(s.namespace),t(this).data("tooltipster-ns",o),t(this).data(s.namespace,s)),r.push(s)})),l?r:this};var l=!!("ontouchstart"in e),c=!1;t("body").one("mousemove",(function(){c=!0}))}(jQuery,window,document)},function(t,e,i){"use strict";(function(t){var e=i(4),n=i.n(e);t.Flatsome={behaviors:{},plugin:function(t,e,i){i=i||{},jQuery.fn[t]=function(o){if("string"==typeof arguments[0]){var r=null,s=arguments[0],a=Array.prototype.slice.call(arguments,1);return this.each((function(){if(!jQuery.data(this,"plugin_"+t)||"function"!=typeof jQuery.data(this,"plugin_"+t)[s])throw new Error("Method "+s+" does not exist on jQuery."+t);r=jQuery.data(this,"plugin_"+t)[s].apply(this,a)})),"destroy"===s&&this.each((function(){jQuery(this).removeData("plugin_"+t)})),void 0!==r?r:this}if("object"===n()(o)||!o)return this.each((function(){jQuery.data(this,"plugin_"+t)||(o=jQuery.extend({},i,o),jQuery.data(this,"plugin_"+t,new e(this,o)))}))}},behavior:function(t,e){this.behaviors[t]=e,e.arrive&&jQuery(document).arrive(e.arrive.selector,e.arrive.handler||function(){Flatsome.attach(t,this.parentNode)})},attach:function(t){var e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:t;if("string"==typeof t)return this.behaviors.hasOwnProperty(t)&&"function"==typeof this.behaviors[t].attach?this.behaviors[t].attach(e||document):null;for(var i in this.behaviors)"function"==typeof this.behaviors[i].attach&&this.behaviors[i].attach(e||document)},detach:function(t){var e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:t;if("string"==typeof t)return this.behaviors.hasOwnProperty(t)&&"function"==typeof this.behaviors[t].detach?this.behaviors[t].detach(e||document):null;for(var i in this.behaviors)"function"==typeof this.behaviors[i].detach&&this.behaviors[i].detach(e||document)}}}).call(this,i(0))},function(t,e){var i=jQuery("#wrapper"),n=jQuery("#header"),o=-jQuery(".header-wrapper").height()-100,r=-jQuery(".header-top.hide-for-sticky").height()-1,s=n.hasClass("has-sticky"),a=n.hasClass("sticky-hide-on-scroll");if(jQuery(".sticky-shrink .header-wrapper").length){var l=jQuery(".header-top.hide-for-sticky").height();o=-1-(l+=jQuery("#wpadminbar").height()),r=-1-l}if(s&&(n.find(".header-wrapper").waypoint((function(t){var e=jQuery(this.element),i=n.height();"down"===t&&(e.addClass("stuck"),n.height(i),jQuery(".has-transparent").removeClass("transparent"),jQuery(".toggle-nav-dark").removeClass("nav-dark"))}),{offset:o}),i.waypoint((function(t){"up"===t&&(n.height(""),jQuery(".header-wrapper").removeClass("stuck"),jQuery(".has-transparent").addClass("transparent"),jQuery(".toggle-nav-dark").addClass("nav-dark"))}),{offset:r}),a)){var c,u=0;jQuery(window).scroll((function(){clearTimeout(c);var t=jQuery(window).scrollTop(),e=jQuery(".header-wrapper");t>=e.outerHeight()&&(t<=u?(e.addClass("stuck"),n.removeClass("sticky-hide-on-scroll--active")):(e.removeClass("stuck"),n.addClass("sticky-hide-on-scroll--active"))),c=setTimeout((function(){u=jQuery(window).scrollTop()}),100)}))}},function(t,e,i){"use strict";(function(t){var e=i(2),n=i.n(e);i(38);n()(),t.objectFitImages=n.a}).call(this,i(0))},function(t,e){!function(t,e){"use strict";if("IntersectionObserver"in t&&"IntersectionObserverEntry"in t&&"intersectionRatio"in t.IntersectionObserverEntry.prototype)"isIntersecting"in t.IntersectionObserverEntry.prototype||Object.defineProperty(t.IntersectionObserverEntry.prototype,"isIntersecting",{get:function(){return this.intersectionRatio>0}});else{var i=[];o.prototype.THROTTLE_TIMEOUT=100,o.prototype.POLL_INTERVAL=null,o.prototype.USE_MUTATION_OBSERVER=!0,o.prototype.observe=function(t){if(!this._observationTargets.some((function(e){return e.element==t}))){if(!t||1!=t.nodeType)throw new Error("target must be an Element");this._registerInstance(),this._observationTargets.push({element:t,entry:null}),this._monitorIntersections(),this._checkForIntersections()}},o.prototype.unobserve=function(t){this._observationTargets=this._observationTargets.filter((function(e){return e.element!=t})),this._observationTargets.length||(this._unmonitorIntersections(),this._unregisterInstance())},o.prototype.disconnect=function(){this._observationTargets=[],this._unmonitorIntersections(),this._unregisterInstance()},o.prototype.takeRecords=function(){var t=this._queuedEntries.slice();return this._queuedEntries=[],t},o.prototype._initThresholds=function(t){var e=t||[0];return Array.isArray(e)||(e=[e]),e.sort().filter((function(t,e,i){if("number"!=typeof t||isNaN(t)||t<0||t>1)throw new Error("threshold must be a number between 0 and 1 inclusively");return t!==i[e-1]}))},o.prototype._parseRootMargin=function(t){var e=(t||"0px").split(/\s+/).map((function(t){var e=/^(-?\d*\.?\d+)(px|%)$/.exec(t);if(!e)throw new Error("rootMargin must be specified in pixels or percent");return{value:parseFloat(e[1]),unit:e[2]}}));return e[1]=e[1]||e[0],e[2]=e[2]||e[0],e[3]=e[3]||e[1],e},o.prototype._monitorIntersections=function(){this._monitoringIntersections||(this._monitoringIntersections=!0,this.POLL_INTERVAL?this._monitoringInterval=setInterval(this._checkForIntersections,this.POLL_INTERVAL):(r(t,"resize",this._checkForIntersections,!0),r(e,"scroll",this._checkForIntersections,!0),this.USE_MUTATION_OBSERVER&&"MutationObserver"in t&&(this._domObserver=new MutationObserver(this._checkForIntersections),this._domObserver.observe(e,{attributes:!0,childList:!0,characterData:!0,subtree:!0}))))},o.prototype._unmonitorIntersections=function(){this._monitoringIntersections&&(this._monitoringIntersections=!1,clearInterval(this._monitoringInterval),this._monitoringInterval=null,s(t,"resize",this._checkForIntersections,!0),s(e,"scroll",this._checkForIntersections,!0),this._domObserver&&(this._domObserver.disconnect(),this._domObserver=null))},o.prototype._checkForIntersections=function(){var e=this._rootIsInDom(),i=e?this._getRootRect():{top:0,bottom:0,left:0,right:0,width:0,height:0};this._observationTargets.forEach((function(o){var r=o.element,s=a(r),l=this._rootContainsTarget(r),c=o.entry,u=e&&l&&this._computeTargetAndRootIntersection(r,i),h=o.entry=new n({time:t.performance&&performance.now&&performance.now(),target:r,boundingClientRect:s,rootBounds:i,intersectionRect:u});c?e&&l?this._hasCrossedThreshold(c,h)&&this._queuedEntries.push(h):c&&c.isIntersecting&&this._queuedEntries.push(h):this._queuedEntries.push(h)}),this),this._queuedEntries.length&&this._callback(this.takeRecords(),this)},o.prototype._computeTargetAndRootIntersection=function(i,n){if("none"!=t.getComputedStyle(i).display){for(var o,r,s,l,u,h,d,p,f=a(i),m=c(i),g=!1;!g;){var v=null,y=1==m.nodeType?t.getComputedStyle(m):{};if("none"==y.display)return;if(m==this.root||m==e?(g=!0,v=n):m!=e.body&&m!=e.documentElement&&"visible"!=y.overflow&&(v=a(m)),v&&(o=v,r=f,s=void 0,l=void 0,u=void 0,h=void 0,d=void 0,p=void 0,s=Math.max(o.top,r.top),l=Math.min(o.bottom,r.bottom),u=Math.max(o.left,r.left),h=Math.min(o.right,r.right),p=l-s,!(f=(d=h-u)>=0&&p>=0&&{top:s,bottom:l,left:u,right:h,width:d,height:p})))break;m=c(m)}return f}},o.prototype._getRootRect=function(){var t;if(this.root)t=a(this.root);else{var i=e.documentElement,n=e.body;t={top:0,left:0,right:i.clientWidth||n.clientWidth,width:i.clientWidth||n.clientWidth,bottom:i.clientHeight||n.clientHeight,height:i.clientHeight||n.clientHeight}}return this._expandRectByRootMargin(t)},o.prototype._expandRectByRootMargin=function(t){var e=this._rootMarginValues.map((function(e,i){return"px"==e.unit?e.value:e.value*(i%2?t.width:t.height)/100})),i={top:t.top-e[0],right:t.right+e[1],bottom:t.bottom+e[2],left:t.left-e[3]};return i.width=i.right-i.left,i.height=i.bottom-i.top,i},o.prototype._hasCrossedThreshold=function(t,e){var i=t&&t.isIntersecting?t.intersectionRatio||0:-1,n=e.isIntersecting?e.intersectionRatio||0:-1;if(i!==n)for(var o=0;o<this.thresholds.length;o++){var r=this.thresholds[o];if(r==i||r==n||r<i!=r<n)return!0}},o.prototype._rootIsInDom=function(){return!this.root||l(e,this.root)},o.prototype._rootContainsTarget=function(t){return l(this.root||e,t)},o.prototype._registerInstance=function(){i.indexOf(this)<0&&i.push(this)},o.prototype._unregisterInstance=function(){var t=i.indexOf(this);-1!=t&&i.splice(t,1)},t.IntersectionObserver=o,t.IntersectionObserverEntry=n}function n(t){this.time=t.time,this.target=t.target,this.rootBounds=t.rootBounds,this.boundingClientRect=t.boundingClientRect,this.intersectionRect=t.intersectionRect||{top:0,bottom:0,left:0,right:0,width:0,height:0},this.isIntersecting=!!t.intersectionRect;var e=this.boundingClientRect,i=e.width*e.height,n=this.intersectionRect,o=n.width*n.height;this.intersectionRatio=i?Number((o/i).toFixed(4)):this.isIntersecting?1:0}function o(t,e){var i,n,o,r=e||{};if("function"!=typeof t)throw new Error("callback must be a function");if(r.root&&1!=r.root.nodeType)throw new Error("root must be an Element");this._checkForIntersections=(i=this._checkForIntersections.bind(this),n=this.THROTTLE_TIMEOUT,o=null,function(){o||(o=setTimeout((function(){i(),o=null}),n))}),this._callback=t,this._observationTargets=[],this._queuedEntries=[],this._rootMarginValues=this._parseRootMargin(r.rootMargin),this.thresholds=this._initThresholds(r.threshold),this.root=r.root||null,this.rootMargin=this._rootMarginValues.map((function(t){return t.value+t.unit})).join(" ")}function r(t,e,i,n){"function"==typeof t.addEventListener?t.addEventListener(e,i,n||!1):"function"==typeof t.attachEvent&&t.attachEvent("on"+e,i)}function s(t,e,i,n){"function"==typeof t.removeEventListener?t.removeEventListener(e,i,n||!1):"function"==typeof t.detatchEvent&&t.detatchEvent("on"+e,i)}function a(t){var e;try{e=t.getBoundingClientRect()}catch(t){}return e?(e.width&&e.height||(e={top:e.top,right:e.right,bottom:e.bottom,left:e.left,width:e.right-e.left,height:e.bottom-e.top}),e):{top:0,bottom:0,left:0,right:0,width:0,height:0}}function l(t,e){for(var i=e;i;){if(i==t)return!0;i=c(i)}return!1}function c(t){var e=t.parentNode;return e&&11==e.nodeType&&e.host?e.host:e}}(window,document)},function(t,e,i){"use strict";var n=[];function o(){for(var t=0;t<n.length;t++)n[t].element.offsetParent?r(n[t]):n.splice(t,1)}function r(t){!function(t){var e=t.element,i=t.type,n=u(e.dataset.parallax),o=l(e),r=(window.innerHeight-o.offsetHeight)*n;switch(i){case"backgroundImage":e.style.backgroundSize=n?"100% auto":null;break;case"backgroundElement":e.style.height=n?"".concat(o.offsetHeight+r,"px"):null}}(t),function(t){var e=t.element,i=t.type,n=u(e.dataset.parallax||e.dataset.parallaxBackground),o=window.innerHeight,r=l(e),s=e.offsetHeight-r.offsetHeight,c=e.getBoundingClientRect(),h=r!==e?r.getBoundingClientRect():c,d=c.top+e.offsetHeight/2,p=h.top+r.offsetHeight/2,f=o/2-d,m=o/2-p,g=d+a()<o/2?a():f,v=(Math.abs(f),Math.abs(g)/(o/2)),y=0;if(h.top>o||h.top+r.offsetHeight<0)return;switch(i){case"backgroundImage":y=h.top*n,e.style.backgroundPosition=n?"50% ".concat(y.toFixed(0),"px"):null,e.style.backgroundAttachment=n?"fixed":null;break;case"backgroundElement":y=m*n-s/2,e.style.transform=n?"translate3d(0, ".concat(y.toFixed(2),"px, 0)"):null,e.style.backfaceVisibility=n?"hidden":null;break;case"element":y=g*n,e.style.transform=n?"translate3d(0, ".concat(y.toFixed(2),"px, 0)"):null,e.style.backfaceVisibility=n?"hidden":null,void 0!==e.dataset.parallaxFade&&(e.style.opacity=n?(b=1-v,b*(2-b)).toFixed(2):null)}var b}(t)}function s(t){return void 0!==t.dataset.parallaxBackground?"backgroundElement":void 0!==t.dataset.parallaxElemenet?"element":""!==t.style.backgroundImage?"backgroundImage":"element"}function a(){return document.documentElement.scrollTop||document.body.scrollTop}function l(t){return function(t){var e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:null;for(;t&&!c(t).call(t,e);)t=t.parentElement;return t}(t,t.dataset.parallaxContainer||"[data-parallax-container]")||t}function c(t){return t.matches||t.webkitMatchesSelector||t.mozMatchesSelector||t.msMatchesSelector}function u(t){return t/10*-1/(2-Math.abs(t)/10)}window.addEventListener("scroll",(function(){return window.requestAnimationFrame(o)})),window.addEventListener("resize",(function(){return window.requestAnimationFrame(o)})),window.addEventListener("DOMNodeInserted",(function(){return window.requestAnimationFrame(o)})),window.jQuery&&(window.jQuery.fn.flatsomeParallax=function(t){"destroy"!==t&&this.each((function(t,e){return function(t){t.classList.add("parallax-active"),!document.querySelector("body").classList.contains("parallax-mobile")&&/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)||t.classList&&t.dataset&&(n.push({element:t,type:s(t)}),r(n[n.length-1]))}(e)}))})},function(t,e){Flatsome.plugin("resizeselect",(function(t,e){jQuery(t).change((function(){var t=jQuery(this),e=t.find("option:selected").val(),i=t.find("option:selected").text(),n=jQuery('<span class="select-resize-ghost">').html(i);n.appendTo(t.parent());var o=n.width();n.remove(),t.width(o+7),e&&t.parent().parent().find("input.search-field").focus()})).change()}))},function(t,e,i){"use strict";var n=i(5),o=i.n(n);jQuery(".section .loading-spin, .banner .loading-spin, .page-loader").fadeOut(),jQuery("#top-link").click((function(t){jQuery.scrollTo(0,300),t.preventDefault()})),jQuery(".scroll-for-more").click((function(){jQuery.scrollTo(jQuery(this),{duration:300})})),jQuery(".search-dropdown button").click((function(t){jQuery(this).parent().find("input").focus(),t.preventDefault()})),jQuery(".current-cat").addClass("active"),jQuery("html").removeClass("loading-site"),setTimeout((function(){jQuery(".page-loader").remove()}),1e3),jQuery(".resize-select").resizeselect(),flatsomeVars.user.can_edit_pages&&jQuery(".block-edit-link").each((function(){var t=jQuery(this),e=t.data("link"),i=t.data("backend"),n=t.data("title"),o=t.parents('[id^="menu-item-"]');if(o.length&&o.hasClass("menu-item-has-block")){var r=o.attr("id").match(/menu-item-(\d+)/);r&&r[1]&&(e+="&menu_id=".concat(r[1]))}jQuery(this).next().addClass("has-block").tooltipster({animationDuration:100,distance:-15,delay:0,repositionOnScroll:!0,interactive:!0,contentAsHTML:!0,content:n+' <br/> <a class="button edit-block-button edit-block-button-builder" href="'+e+'">UX Builder</a> <a class="button edit-block-button edit-block-button edit-block-button-backend" href="'+i+'">WP Editor</a>'}),jQuery(this).remove()})),document.addEventListener("uxb_app_ready",(function(){var t=new URLSearchParams(window.top.location.search),e=parseInt(t.get("menu_id"));e&&setTimeout((function(){var t=jQuery("#menu-item-".concat(e));t.hasClass("menu-item-has-block has-dropdown")&&!t.hasClass("current-dropdown")&&jQuery("#menu-item-".concat(e," a:first")).click()}),1e3)})),jQuery("#hotspot").click((function(t){t.preventDefault()})),jQuery(".wpcf7-form .wpcf7-submit").on("click",(function(){jQuery(this).parent().parent().addClass("processing")})),jQuery(document).ajaxComplete((function(t,e,i){jQuery(".processing").removeClass("processing")})),jQuery(document).ready((function(){o()()}))},function(t,e){Flatsome.behavior("animate",{attach:function(t){jQuery("[data-animate]",t).each((function(t,e){var i=jQuery(e);if(0===i.data("animate").length)return i.attr("data-animated","true");i.waypoint((function(t){if("down"===t){if("true"==i.data("animated"))return;setTimeout((function(){i.attr("data-animated","true")}),300)}}),{offset:"101%"})}))},detach:function(t){jQuery("[data-animate]",t).each((function(t,e){jQuery(e).attr("data-animated","false")}))}})},function(t,e){Flatsome.behavior("commons",{attach:function(t){jQuery("select.resizeselect").resizeselect(),jQuery("[data-parallax]",t).flatsomeParallax(),jQuery.fn.packery&&(jQuery("[data-packery-options], .has-packery",t).each((function(){var t=jQuery(this);t.packery(),setTimeout((function(){t.imagesLoaded((function(){t.packery("layout")}))}),100)})),jQuery(".banner-grid-wrapper").imagesLoaded((function(){jQuery(this.elements).removeClass("processing")})))},detach:function(t){}})},function(t,e,i){"use strict";var n=i(6),o=i.n(n);Flatsome.behavior("count-up",{attach:function(t){jQuery("span.count-up",t).each((function(t,e){var i=jQuery(e);i.waypoint({handler:function(t){if(!jQuery(this.element).hasClass("active")){var e=parseInt(i.text());new o.a(i.get(0),0,e,0,4).start(),i.addClass("active")}},offset:"100%"})}))}})},function(t,e,i){"use strict";(function(t){var e=i(7),n=i.n(e);function o(t,e){var i=Object.keys(t);if(Object.getOwnPropertySymbols){var n=Object.getOwnPropertySymbols(t);e&&(n=n.filter((function(e){return Object.getOwnPropertyDescriptor(t,e).enumerable}))),i.push.apply(i,n)}return i}function r(t){for(var e=1;e<arguments.length;e++){var i=null!=arguments[e]?arguments[e]:{};e%2?o(Object(i),!0).forEach((function(e){n()(t,e,i[e])})):Object.getOwnPropertyDescriptors?Object.defineProperties(t,Object.getOwnPropertyDescriptors(i)):o(Object(i)).forEach((function(e){Object.defineProperty(t,e,Object.getOwnPropertyDescriptor(i,e))}))}return t}function s(e){e.addClass("current-dropdown"),function(e){var i=e,n=jQuery(".header-inner").width(),o=i.closest("li.menu-item"),s=o.hasClass("menu-item-design-full-width"),a=o.hasClass("menu-item-design-container-width"),l=!s&&!a,c=t.flatsomeVars.rtl;if(l){if(n<750)return!1;var u=i.outerWidth(),h=i.offset(),d=Math.max(document.documentElement.clientWidth,window.innerWidth||0),p=h.left-(d-n)/2;c&&(p=jQuery(window).width()-(h.left+u)-(d-n)/2);var f=i.width(),m=n-(p+f),g=!1;p>m&&p<f&&(g=(p+m)/3),m<0&&(g=-m),g&&c?i.css("margin-right",-g):g&&i.css("margin-left",-g),f>n&&i.addClass("nav-dropdown-full")}if(a){var v=document.querySelector(".header-inner").getBoundingClientRect(),y=i.get(0).getBoundingClientRect();i.css(r(r({width:n},c&&{right:-(v.right-y.right)}),!c&&{left:v.left-y.left}))}if(s){var b=document.body,w=b.getBoundingClientRect(),x=i.get(0).getBoundingClientRect(),C=b.clientWidth;i.css(r(r({width:C},c&&{right:-(w.right-x.right)-15}),!c&&{left:w.left-x.left-15}))}if(a||s){var k=null;if(o.closest("#top-bar").length&&(k=document.querySelector("#top-bar")),o.closest("#masthead").length&&(k=document.querySelector("#masthead")),o.closest("#wide-nav").length&&(k=document.querySelector("#wide-nav")),null!==k){var S=k.getBoundingClientRect(),E=o.get(0).getBoundingClientRect();i.css({top:S.bottom-E.bottom+E.height})}}}(e.find(".nav-dropdown"))}function a(t){t.removeClass("current-dropdown"),t.find(".nav-dropdown").attr("style","")}function l(t){t.each((function(t,e){var i=jQuery(e);i.hasClass("current-dropdown")&&a(i)}))}function c(t,e){t.length&&t.removeClass("ux-body-overlay--".concat(e,"-active"))}Flatsome.behavior("dropdown",{attach:function(t){var e=jQuery(".nav li.has-dropdown",t),i="uxBuilder"===jQuery("html").attr("ng-app"),n=jQuery(".ux-body-overlay"),o="ontouchstart"in window,r=!1,u=null;e.each((function(t,h){var d=jQuery(h),p=d.hasClass("nav-dropdown-toggle")&&!o,f=!1,m=!1;d.on("touchstart click",(function(t){"touchstart"===t.type&&(f=!0),"click"===t.type&&f&&(f&&!m&&t.preventDefault(),m=!0)})),i||p?(r=!0,d.on("click","a:first",(function(t){if(t.preventDefault(),u=d,d.hasClass("current-dropdown"))return a(d),void c(n,"click");l(e),s(d),function(t,e){if(!t.length)return;t.addClass("ux-body-overlay--".concat(e,"-active"))}(n,"click")}))):d.hoverIntent({sensitivity:3,interval:20,timeout:70,over:function(t){l(e),s(d),c(n,"click")},out:function(){m=!1,f=!1,a(d)}})})),!i&&r&&jQuery(document).on("click",(function(t){null===u||u===t.target||u.has(t.target).length||(a(u),c(n,"click"))}))}})}).call(this,i(0))},function(t,e){Flatsome.behavior("lightbox-gallery",{attach:function(t){var e={delegate:"a",type:"image",closeBtnInside:flatsomeVars.lightbox.close_btn_inside,closeMarkup:flatsomeVars.lightbox.close_markup,tLoading:'<div class="loading-spin centered dark"></div>',removalDelay:300,gallery:{enabled:!0,navigateByImgClick:!0,arrowMarkup:'<button class="mfp-arrow mfp-arrow-%dir%" title="%title%"><i class="icon-angle-%dir%"></i></button>',preload:[0,1]},image:{tError:'<a href="%url%">The image #%curr%</a> could not be loaded.',verticalFit:!1}};jQuery('.lightbox .gallery a[href*=".jpg"], .lightbox .gallery a[href*=".jpeg"], .lightbox a.lightbox-gallery',t).parent().magnificPopup(e),jQuery(".lightbox .lightbox-multi-gallery",t).length&&jQuery(".lightbox-multi-gallery",t).each((function(){jQuery(this).magnificPopup(e)}))}})},function(t,e){Flatsome.behavior("lightbox-image",{attach:function(t){jQuery(['.lightbox *[id^="attachment"] a[href*=".jpg"]','.lightbox *[id^="attachment"] a[href*=".jpeg"]','.lightbox .wp-block-image a[href*=".jpg"]:not([target="_blank"])','.lightbox .wp-block-image a[href*=".jpeg"]:not([target="_blank"])',".lightbox a.image-lightbox",'.lightbox .entry-content a[href*=".jpg"]','.lightbox .entry-content a[href*=".jpeg"]'].join(","),t).not([".lightbox a.lightbox-gallery",'.lightbox .gallery a[href*=".jpg"]','.lightbox .gallery a[href*=".jpeg"]','.lightbox .lightbox-multi-gallery a[href*=".jpg"]','.lightbox .lightbox-multi-gallery a[href*=".jpeg"]'].join(",")).magnificPopup({type:"image",tLoading:'<div class="loading-spin centered dark"></div>',closeOnContentClick:!0,closeBtnInside:flatsomeVars.lightbox.close_btn_inside,closeMarkup:flatsomeVars.lightbox.close_markup,removalDelay:300,image:{verticalFit:!1}})}})},function(t,e){Flatsome.behavior("lightboxes-link",{attach:function(t){jQuery(".lightbox-by-id",t).each((function(){var e=jQuery(this).attr("id");jQuery('a[href="#'+e+'"]',t).on("click",(function(t){var e=jQuery(t.currentTarget).attr("href").substring(1),i=jQuery("#".concat(e,".lightbox-by-id"));if(e&&i.length>0){var n=i[0],o=jQuery.magnificPopup.open?300:0;o&&jQuery.magnificPopup.close(),setTimeout((function(){jQuery.magnificPopup.open({removalDelay:300,closeBtnInside:flatsomeVars.lightbox.close_btn_inside,closeMarkup:flatsomeVars.lightbox.close_markup,items:{src:n,type:"inline",tLoading:'<div class="loading-spin dark"></div>'},callbacks:{open:function(){if(Flatsome.attach(this.content),jQuery.fn.flickity){var t=jQuery("[data-flickity-options]",this.content);t&&t.imagesLoaded((function(){t.flickity("resize")}))}if(jQuery.fn.packery){var e=jQuery("[data-packery-options]",this.content);e&&e.imagesLoaded((function(){e.packery("layout")}))}}}})}),o),t.preventDefault()}}))}))}})},function(t,e){Flatsome.behavior("lightbox-video",{attach:function(t){jQuery('a.open-video, a.button[href*="vimeo"], a.button[href*="youtube.com/watch"]',t).magnificPopup({type:"iframe",closeBtnInside:flatsomeVars.lightbox.close_btn_inside,mainClass:"my-mfp-video",closeMarkup:flatsomeVars.lightbox.close_markup,tLoading:'<div class="loading-spin centered dark"></div>',removalDelay:300,preloader:!0,callbacks:{open:function(){jQuery(".slider .is-selected .video").trigger("pause")},close:function(){jQuery(".slider .is-selected .video").trigger("play")}}})}})},function(t,e){Flatsome.behavior("lightboxes",{attach:function(t){jQuery("[data-open]",t).on("click",(function(t){var e=jQuery(t.currentTarget),i=e.data("open"),n=e.data("color"),o=e.data("bg"),r=e.data("pos"),s=e.data("visible-after"),a=e.data("class"),l=e.attr("data-focus");e.offset();e.addClass("current-lightbox-clicked"),jQuery.magnificPopup.open({items:{src:i,type:"inline",tLoading:'<div class="loading-spin dark"></div>'},removalDelay:300,closeBtnInside:flatsomeVars.lightbox.close_btn_inside,closeMarkup:flatsomeVars.lightbox.close_markup,focus:l,callbacks:{beforeOpen:function(){this.st.mainClass="off-canvas ".concat(n," off-canvas-").concat(r)},open:function(){jQuery("html").addClass("has-off-canvas"),jQuery("html").addClass("has-off-canvas-"+r),a&&jQuery(".mfp-content").addClass(a),o&&jQuery(".mfp-bg").addClass(o),jQuery(".mfp-content .resize-select").change(),jQuery.fn.packery&&jQuery("[data-packery-options], .has-packery").packery("layout")},beforeClose:function(){jQuery("html").removeClass("has-off-canvas")},afterClose:function(){jQuery("html").removeClass("has-off-canvas-"+r),jQuery(".current-lightbox-clicked").removeClass("current-lightbox-clicked"),s&&jQuery(i).removeClass("mfp-hide")}}}),t.preventDefault()}))}})},function(t,e){Flatsome.behavior("slider",{attach:function(t){(jQuery(t).data("flickityOptions")?jQuery(t):jQuery("[data-flickity-options]",t)).each((function(t,e){var i=jQuery(e),n=i.closest(".slider-wrapper"),o=i.data("flickity-options");if("undefined"!=typeof UxBuilder&&(o.draggable=!1),!0!==o.watchCSS){i.on("ready.flickity",(function(){i.find(".flickity-slider > :not(.is-selected) .video-bg").trigger("pause"),i.find(".is-selected .video-bg").trigger("play"),"requestAnimationFrame"in window&&(i.removeClass("flickity-enabled"),window.requestAnimationFrame((function(){i.addClass("flickity-enabled")})))}));var r=i.flickity(o);if(i.imagesLoaded((function(){n.find(".loading-spin").fadeOut()})),i.on("change.flickity",(function(){i.find(".flickity-slider > :not(.is-selected) .video-bg").trigger("pause"),i.find(".is-selected .video-bg").trigger("play")})),i.on("dragStart.flickity",(function(){document.ontouchmove=function(t){return t.preventDefault()},i.addClass("is-dragging")})),i.on("dragEnd.flickity",(function(){document.ontouchmove=function(){return!0},i.removeClass("is-dragging")})),o.parallax){var s=r.data("flickity"),a=i.find(".bg, .flickity-slider > .img img");i.addClass("slider-has-parallax"),i.on("scroll.flickity",(function(t,e){s.slides.forEach((function(t,e){var i=a[e],n=-1*(t.target+s.x)/o.parallax;i&&(i.style.transform="translateX( "+n+"px)")}))}))}}}))},detach:function(t){jQuery(t).data("flickityOptions")?jQuery(t).flickity("destroy"):jQuery("[data-flickity-options]",t).flickity("destroy")}})},function(t,e){function i(t,e,i){e.each((function(e,i){return jQuery(i).toggleClass("active",e===t)})),i.each((function(e,i){return jQuery(i).toggleClass("active",e===t)})),jQuery.fn.flickity&&jQuery("[data-flickity-options]",i[t]).flickity("resize"),jQuery.fn.packery&&jQuery("[data-packery-options]",i[t]).packery("layout")}Flatsome.behavior("tabs",{attach:function(t){var e=window.location.hash,n=window.location.href;jQuery(".tabbed-content",t).each((function(t,o){var r=jQuery(o),s=r.find("> .nav > li"),a=r.find("> .tab-panels > .panel"),l=s.filter(".reviews_tab").first();a.removeAttr("style"),s.each((function(t,n){var o=jQuery(n).find("a");o.on("click",(function(e){i(t,s,a),e.preventDefault(),e.stopPropagation()})),e.substr(1).length&&e.substr(1)===o.attr("href").split("#")[1]&&i(t,s,a)})),l.length&&(e.toLowerCase().indexOf("comment-")>=0||"#comments"===e||"#reviews"===e||"#tab-reviews"===e||n.indexOf("comment-page-")>0||n.indexOf("cpage=")>0)&&(l.find("a").click(),jQuery.scrollTo(".reviews_tab",{duration:300,offset:-150}))}))}})},function(t,e){Flatsome.behavior("toggle",{attach:function(t){function e(t){var e=jQuery(t.currentTarget).parent();e.toggleClass("active"),e.attr("aria-expanded","false"===e.attr("aria-expanded")?"true":"false"),t.preventDefault()}jQuery([".widget ul.children",".nav ul.children",".menu .sub-menu",".mobile-sidebar-levels-2 .nav ul.children > li > ul"].join(", "),t).each((function(){var t=jQuery(this).parents(".nav-slide").length?"right":"down";jQuery(this).parent().addClass("has-child").attr("aria-expanded","false"),jQuery(this).before('<button class="toggle"><i class="icon-angle-'.concat(t,'"></i></button>'))})),jQuery(".current-cat-parent",t).addClass("active").attr("aria-expanded","true").removeClass("current-cat-parent"),jQuery(".toggle",t).click(e);var i=jQuery("body").hasClass("mobile-submenu-toggle");jQuery(".sidebar-menu li.menu-item.has-child",t).each((function(){var t=jQuery(this),n=t.find("> a:first");"#"===n.attr("href")?n.click((function(e){e.preventDefault(),t.toggleClass("active"),t.attr("aria-expanded","false"===t.attr("aria-expanded")?"true":"false")})):i&&n.next(".toggle").length&&n.click(e)}))}})},function(t,e){function i(t){t.attr("aria-hidden","true"),t.find("> li > a, > li > button").attr("tabindex","-1")}Flatsome.behavior("sidebar-slider",{attach:function(t){var e=jQuery("body").hasClass("mobile-submenu-toggle");jQuery(".mobile-sidebar-slide",t).each((function(t,n){var o=parseInt(jQuery(n).data("levels"),10)||1,r=jQuery(".sidebar-menu",n),s=jQuery(".nav-sidebar",n);jQuery(["> li > ul.children","> li > .sub-menu",o>1?"> li > ul.children > li > ul":null].filter(Boolean).join(", "),s).each((function(t,n){var o=jQuery(n),s=o.parent(),a=s.parents("ul:first"),l=jQuery(["> .toggle",'> a[href="#"]',e&&"> a"].filter(Boolean).join(","),s),c=s.find("> a").text().trim(),u=o.parents("ul").length,h=Boolean(window.flatsomeVars.rtl),d=jQuery('\n            <li class="nav-slide-header pt-half pb-half">\n              <button class="toggle">\n                <i class="icon-angle-left"></i>\n                '.concat(c||window.flatsomeVars.i18n.mainMenu,"\n              </button>\n            </li>\n          "));o.prepend(d),i(o);var p=null;l.off("click").click((function(t){var e;s.attr("aria-expanded","true"),a.addClass("is-current-parent"),o.addClass("is-current-slide"),r.css("transform","translateX(".concat(h?"":"-").concat(100*u,"%)")),(e=o).attr("aria-hidden","false"),e.find("> li > a, > li > button").attr("tabindex",""),clearTimeout(p),t.preventDefault()})),d.find(".toggle").click((function(){r.css("transform","translateX(".concat(h?"":"-").concat(100*(u-1),"%)")),i(o),p=setTimeout((function(){o.removeClass("is-current-slide"),a.removeClass("is-current-parent")}),300),s.removeClass("active"),s.attr("aria-expanded","false")}))}))}))}})},function(t,e){Flatsome.behavior("nav-hover",{attach:function(t){var e=jQuery(".ux-body-overlay",t);e.length&&jQuery(".nav-prompts-overlay > li.menu-item",t).on({mouseenter:function(){e.addClass("ux-body-overlay--hover-active")},mouseleave:function(){e.removeClass("ux-body-overlay--hover-active")}})}})},function(t,e){Flatsome.behavior("back-to-top",{attach:function(t){jQuery("body",t).waypoint({handler:function(e){jQuery(".back-to-top",t).toggleClass("active")},offset:"-100%"})}})},function(t,e){Flatsome.behavior("scroll-to",{attach:function(){var t=jQuery("span.scroll-to"),e=jQuery(".scroll-to-bullets"),i=flatsomeVars.sticky_height;if(e.length&&(e.children().tooltipster("destroy"),e.remove()),jQuery("li.scroll-to-link").remove(),t.length&&(e=jQuery('<div class="scroll-to-bullets hide-for-medium"/>'),jQuery("body").append(e),t.each((function(t,e){var n=jQuery(e),o=n.data("link"),r=n.data("title"),s=n.data("bullet"),a='a[href*="'.concat(o||"<nolink>",'"]');if(s){var l=jQuery('\n          <a href="'.concat(o,'" data-title="').concat(r,'" title="').concat(r,'">\n          <strong></strong>\n          </a>\n        '));l.tooltipster({position:"left",delay:50,contentAsHTML:!0,touchDevices:!1}),jQuery(".scroll-to-bullets").append(l)}var c=jQuery('\n          <li class="scroll-to-link"><a data-animate="fadeIn" href="'.concat(o,'" data-title="').concat(r,'" title="').concat(r,'">\n          ').concat(r,"\n          </a></li>\n        "));jQuery("li.nav-single-page").before(c),setTimeout((function(){jQuery(".scroll-to-link a").attr("data-animated","true")}),300),n.waypoint((function(t){jQuery(".scroll-to-bullets a, .scroll-to-link").removeClass("active"),jQuery(".scroll-to-bullets").find(a).addClass("active"),jQuery(".nav-single-page").parent().find(a).parent().addClass("active"),"up"===t&&jQuery(".scroll-to-bullets, .nav-single-page").find(a).removeClass("active").prev().addClass("active")}),{offset:i}),jQuery(a).off("click").on("click",(function(t){var e=jQuery(this).attr("href").split("#")[1];if(e){var n="\\#".concat(e),o="span.scroll-to[data-link=".concat(n,"]"),r=jQuery(o).offset().top-i;jQuery.scrollTo(r,{duration:500,axis:"y"}),jQuery.magnificPopup.close(),t.preventDefault()}}))})),location.hash)){var n=location.hash.replace("#","");jQuery.scrollTo("a[name="+n+"]",{duration:500,axis:"y",offset:-i})}},detach:function(){jQuery("span.scroll-to").length&&setTimeout(this.attach,0)}})},function(t,e){Flatsome.behavior("accordion",{attach:function(t){jQuery(".accordion",t).each((function(){var t=jQuery(this).attr("rel");if(t>0){var e=jQuery(this).find(".accordion-item:nth-child("+t+") .accordion-inner");e.show(),e.prev().addClass("active"),jQuery.fn.flickity&&e.find("[data-flickity-options]").flickity("resize"),jQuery.fn.packery&&e.find("[data-packery-options]").packery("layout")}}))}}),Flatsome.behavior("accordion-title",{attach:function(t){jQuery(".accordion-title",t).each((function(){jQuery(this).off("click.flatsome").on("click.flatsome",(function(t){if(jQuery(this).next().is(":hidden")){jQuery(this).parent().parent().find(".accordion-title").removeClass("active").next().slideUp(200),jQuery(this).toggleClass("active").next().slideDown(200,(function(){/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)&&jQuery.scrollTo(jQuery(this).prev(),{duration:300,offset:-100})}));var e=jQuery(this).parent().parent().find("[data-flickity-options]");Flatsome.detach("slider",e),Flatsome.attach("slider",e),jQuery.fn.packery&&jQuery(this).parent().parent().find("[data-packery-options]").packery("layout")}else jQuery(this).parent().parent().find(".accordion-title").removeClass("active").next().slideUp(200);t.preventDefault()}))}))}})},function(t,e){Flatsome.behavior("tooltips",{attach:function(t){jQuery(".tooltip, .has-tooltip, .tip-top, li.chosen a",t).tooltipster(),jQuery(".tooltip-as-html",t).tooltipster({interactive:!0,contentAsHTML:!0})}})},function(t,e){Flatsome.behavior("sticky-section",{attach:function(t){jQuery(".sticky-section",t).each((function(t,e){var i=jQuery(e);i.waypoint((function(t){"down"===t&&(i.addClass("is-sticky-section"),i.after('<div class="sticky-section-helper"></div>')),"up"===t&&(i.removeClass("is-sticky-section"),i.next(".sticky-section-helper").remove())}),{offset:"0.1px"}),i.waypoint((function(t){"down"===t&&(i.removeClass("is-sticky-section"),i.next(".sticky-section-helper").remove()),"up"===t&&(i.addClass("is-sticky-section"),i.after('<div class="sticky-section-helper"></div>'))}),{offset:"-100%"})}))}})},function(t,e){Flatsome.behavior("sticky-sidebar",{attach:function(t){var e=parseInt(flatsomeVars.sticky_height)+15;jQuery(".is-sticky-column",t).each((function(t,i){jQuery(i).stickySidebar({topSpacing:e,bottomSpacing:15,minWidth:850,innerWrapperSelector:".is-sticky-column__inner"}),jQuery(document).on("updated_checkout",(function(){jQuery(i).stickySidebar("updateSticky")}))}))}})},function(t,e){Flatsome.behavior("youtube",{attach:function(t){var e,i,n,o,r,s=jQuery(".ux-youtube",t);0!==s.length&&(window.onYouTubePlayerAPIReady=function(){s.each((function(){var t=jQuery(this),e=t.attr("id"),i=t.data("videoid"),n=t.data("loop"),o=t.data("audio");new YT.Player(e,{height:"100%",width:"100%",playerVars:{html5:1,autoplay:1,controls:0,rel:0,modestbranding:1,playsinline:1,showinfo:0,fs:0,loop:n,el:0,playlist:n?i:void 0},videoId:i,events:{onReady:function(t){0===o&&t.target.mute()}}})}))},e=document,i="script",n="youtube-jssdk",r=e.getElementsByTagName(i)[0],e.getElementById(n)||((o=e.createElement(i)).id=n,o.src="https://www.youtube.com/player_api",r.parentNode.insertBefore(o,r)))}})},,,,,,,,function(t,e,i){"use strict";Flatsome.behavior("lazy-load-bg",{attach:function(t){var e,i=(e=function(t){t.intersectionRatio>0&&(i.unobserve(t.target),jQuery(t.target).addClass("bg-loaded"))},new IntersectionObserver((function(t){for(var i=0;i<t.length;i++)e(t[i])}),{rootMargin:"0px",threshold:.1}));jQuery(".bg",t).each((function(t,e){i.observe(e)}))}})}]);!function(t){var e={};function i(r){if(e[r])return e[r].exports;var o=e[r]={i:r,l:!1,exports:{}};return t[r].call(o.exports,o,o.exports,i),o.l=!0,o.exports}i.m=t,i.c=e,i.d=function(t,e,r){i.o(t,e)||Object.defineProperty(t,e,{enumerable:!0,get:r})},i.r=function(t){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0})},i.t=function(t,e){if(1&e&&(t=i(t)),8&e)return t;if(4&e&&"object"==typeof t&&t&&t.__esModule)return t;var r=Object.create(null);if(i.r(r),Object.defineProperty(r,"default",{enumerable:!0,value:t}),2&e&&"string"!=typeof t)for(var o in t)i.d(r,o,function(e){return t[e]}.bind(null,o));return r},i.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return i.d(e,"a",e),e},i.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},i.p="",i(i.s=63)}({1:function(t,e){t.exports=window.jQuery},63:function(t,e,i){t.exports=i(64)},64:function(t,e,i){"use strict";i.r(e);i(65),i(66),i(67),i(68),i(69);var r=!1;/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)||(r=jQuery(".has-image-zoom .slide").easyZoom({loadingNotice:"",preventClicks:!1})),jQuery("table.my_account_orders").wrap('<div class="touch-scroll-table"/>'),jQuery("a.woocommerce-review-link").click((function(t){jQuery.scrollTo(".reviews_tab",{duration:300,offset:-150})})),jQuery(".single_add_to_cart_button").click((function(){var t=jQuery(this),e=t.closest("form.cart");e?e.on("submit",(function(){t.addClass("loading")})):t.hasClass("disabled")||t.addClass("loading")}));var o=jQuery(".product-thumbnails .first img").attr("data-src")?jQuery(".product-thumbnails .first img").attr("data-src"):jQuery(".product-thumbnails .first img").attr("src"),a=jQuery("form.variations_form"),s=function(){r&&r.length&&r.filter(".has-image-zoom .slide.first").data("easyZoom").swap(jQuery(".has-image-zoom .slide.first img").attr("src"),jQuery(".has-image-zoom .slide.first img").attr("data-large_image"))},n=function(){var t=jQuery(".product-gallery-slider");t.data("flickity")&&t.flickity("select",0)},c=function(){var t=jQuery(".product-gallery-slider");t.data("flickity")&&t.imagesLoaded((function(){t.flickity("resize")}))};function u(t){if(jQuery(".cart-item .nav-dropdown").length)jQuery(".cart-item").addClass("current-dropdown cart-active"),jQuery(".shop-container").click((function(){jQuery(".cart-item").removeClass("current-dropdown cart-active")})),jQuery(".cart-item").hover((function(){jQuery(".cart-active").removeClass("cart-active")})),setTimeout((function(){jQuery(".cart-active").removeClass("current-dropdown")}),t);else{var e=jQuery.magnificPopup.open?0:300;e&&jQuery.magnificPopup.close(),setTimeout((function(){jQuery(".cart-item .off-canvas-toggle").click()}),e)}}a.on("show_variation",(function(t,e){e.hasOwnProperty("image")&&e.image.thumb_src?(jQuery(".product-gallery-slider-old .slide.first img, .sticky-add-to-cart-img, .product-thumbnails .first img, .product-gallery-slider .slide.first .zoomImg").attr("src",e.image.thumb_src).attr("srcset",""),n(),s(),c()):(jQuery(".product-thumbnails .first img").attr("src",o),c())})),a.on("hide_variation",(function(t,e){jQuery(".product-thumbnails .first img, .sticky-add-to-cart-img").attr("src",o),c()})),a.on("click",".reset_variations",(function(){jQuery(".product-thumbnails .first img, .sticky-add-to-cart-img").attr("src",o),n(),s(),c()})),jQuery(document).ready((function(){jQuery(".has-lightbox .product-gallery-slider").each((function(){jQuery(this).magnificPopup({delegate:"a",type:"image",tLoading:'<div class="loading-spin centered dark"></div>',closeMarkup:flatsomeVars.lightbox.close_markup,closeBtnInside:flatsomeVars.lightbox.close_btn_inside,gallery:{enabled:!0,navigateByImgClick:!0,preload:[0,1],arrowMarkup:'<button class="mfp-arrow mfp-arrow-%dir%" title="%title%"><i class="icon-angle-%dir%"></i></button>'},image:{tError:'<a href="%url%">The image #%curr%</a> could not be loaded.',verticalFit:!1}})}))})),jQuery(".zoom-button").click((function(t){jQuery(".product-gallery-slider").find(".is-selected a").click(),t.preventDefault()})),/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)&&jQuery(window).on("load",(function(){c()})),jQuery("body").on("added_to_cart",(function(){u("5000");var t=jQuery("#header"),e=t.hasClass("has-sticky"),i=jQuery(".header-wrapper",t);e&&jQuery(".cart-item.has-dropdown").length&&t.hasClass("sticky-hide-on-scroll--active")&&(i.addClass("stuck"),t.removeClass("sticky-hide-on-scroll--active"))})),jQuery(document.body).on("updated_cart_totals",(function(){jQuery(document).trigger("yith_wcwl_reload_fragments");var t=jQuery(".cart-wrapper");Flatsome.attach("lazy-load-images",t),Flatsome.attach("quick-view",t),Flatsome.attach("wishlist",t),Flatsome.attach("cart-refresh",t),Flatsome.attach("equalize-box",t)})),jQuery(document).ajaxComplete((function(){Flatsome.attach(jQuery(".quantity").parent()),Flatsome.attach("lightboxes-link",jQuery(".woocommerce-checkout .woocommerce-terms-and-conditions-wrapper"))})),jQuery(document).on("yith_infs_adding_elem",(function(t){Flatsome.attach(jQuery(".shop-container"))})),jQuery(document).ready((function(){jQuery("span.added-to-cart").length&&u("5000")})),jQuery(".disable-lightbox a").click((function(t){t.preventDefault()})),jQuery(document).ready((function(){if(jQuery(".custom-product-page").length){var t=jQuery("#respond p.stars");if(t.length>1){var e=t[0].outerHTML;t.remove(),jQuery('select[id="rating"]').hide().before(e)}}})),jQuery(".sticky-add-to-cart-wrapper").waypoint((function(t){var e=jQuery(this.element),i=jQuery(this.element).find(".sticky-add-to-cart");jQuery(".wc-variation-selection-needed").click((function(){jQuery.scrollTo(".sticky-add-to-cart-wrapper",{duration:0,offset:-200})})),"down"===t&&(e.css({height:e.outerHeight()}),i.addClass("sticky-add-to-cart--active"),jQuery("body").addClass("has-sticky-product-cart")),"up"===t&&(i.removeClass("sticky-add-to-cart--active"),e.css({height:"auto"}),jQuery("body").removeClass("has-sticky-product-cart"))})),setTimeout((function(){jQuery(document.body).on("country_to_state_changed",(function(){"undefined"!=typeof floatlabels&&floatlabels.rebuild()}))}),500)},65:function(t,e,i){var r,o;
/*!
 * @name        easyzoom
 * @author       <>
 * @modified    Wednesday, October 3rd, 2018
 * @version     2.5.0
 */!function(a,s){"use strict";r=[i(1)],void 0===(o=function(t){!function(t){var e,i,r,o,a,s,n={loadingNotice:"Loading image",errorNotice:"The image could not be loaded",errorDuration:2500,linkAttribute:"href",preventClicks:!0,beforeShow:t.noop,beforeHide:t.noop,onShow:t.noop,onHide:t.noop,onMove:t.noop};function c(e,i){this.$target=t(e),this.opts=t.extend({},n,i,this.$target.data()),void 0===this.isOpen&&this._init()}c.prototype._init=function(){this.$link=this.$target.find("a"),this.$image=this.$target.find("img"),this.$flyout=t('<div class="easyzoom-flyout" />'),this.$notice=t('<div class="easyzoom-notice" />'),this.$target.on({"mousemove.easyzoom touchmove.easyzoom":t.proxy(this._onMove,this),"mouseleave.easyzoom touchend.easyzoom":t.proxy(this._onLeave,this),"mouseenter.easyzoom touchstart.easyzoom":t.proxy(this._onEnter,this)}),this.opts.preventClicks&&this.$target.on("click.easyzoom",(function(t){t.preventDefault()}))},c.prototype.show=function(t,a){var s=this;if(!1!==this.opts.beforeShow.call(this)){if(!this.isReady)return this._loadImage(this.$link.attr(this.opts.linkAttribute),(function(){!s.isMouseOver&&a||s.show(t)}));this.$target.append(this.$flyout);var n=this.$target.outerWidth(),c=this.$target.outerHeight(),u=this.$flyout.width(),l=this.$flyout.height(),d=this.$zoom.width(),h=this.$zoom.height();(e=d-u)<0&&(e=0),(i=h-l)<0&&(i=0),r=e/n,o=i/c,this.isOpen=!0,this.opts.onShow.call(this),t&&this._move(t)}},c.prototype._onEnter=function(t){var e=t.originalEvent.touches;this.isMouseOver=!0,e&&1!=e.length||(t.preventDefault(),this.show(t,!0))},c.prototype._onMove=function(t){this.isOpen&&(t.preventDefault(),this._move(t))},c.prototype._onLeave=function(){this.isMouseOver=!1,this.isOpen&&this.hide()},c.prototype._onLoad=function(t){t.currentTarget.width&&(this.isReady=!0,this.$notice.detach(),this.$flyout.html(this.$zoom),this.$target.removeClass("is-loading").addClass("is-ready"),t.data.call&&t.data())},c.prototype._onError=function(){var t=this;this.$notice.text(this.opts.errorNotice),this.$target.removeClass("is-loading").addClass("is-error"),this.detachNotice=setTimeout((function(){t.$notice.detach(),t.detachNotice=null}),this.opts.errorDuration)},c.prototype._loadImage=function(e,i){var r=new Image;this.$target.addClass("is-loading").append(this.$notice.text(this.opts.loadingNotice)),this.$zoom=t(r).on("error",t.proxy(this._onError,this)).on("load",i,t.proxy(this._onLoad,this)),r.style.position="absolute",r.src=e},c.prototype._move=function(t){if(0===t.type.indexOf("touch")){var n=t.touches||t.originalEvent.touches;a=n[0].pageX,s=n[0].pageY}else a=t.pageX||a,s=t.pageY||s;var c=this.$target.offset(),u=s-c.top,l=a-c.left,d=Math.ceil(u*o),h=Math.ceil(l*r);if(h<0||d<0||e<h||i<d)this.hide();else{var y=-1*d,m=-1*h;this.$zoom.css({top:y,left:m}),this.opts.onMove.call(this,y,m)}},c.prototype.hide=function(){this.isOpen&&!1!==this.opts.beforeHide.call(this)&&(this.$flyout.detach(),this.isOpen=!1,this.opts.onHide.call(this))},c.prototype.swap=function(e,i,r){this.hide(),this.isReady=!1,this.detachNotice&&clearTimeout(this.detachNotice),this.$notice.parent().length&&this.$notice.detach(),this.$target.removeClass("is-loading is-ready is-error"),this.$image.attr({src:e,srcset:t.isArray(r)?r.join():r}),this.$link.attr(this.opts.linkAttribute,i)},c.prototype.teardown=function(){this.hide(),this.$target.off(".easyzoom").removeClass("is-loading is-ready is-error"),this.detachNotice&&clearTimeout(this.detachNotice),delete this.$link,delete this.$zoom,delete this.$image,delete this.$notice,delete this.$flyout,delete this.isOpen,delete this.isReady},t.fn.easyZoom=function(e){return this.each((function(){var i=t.data(this,"easyZoom");i?void 0===i.isOpen&&i._init():t.data(this,"easyZoom",new c(this,e))}))}}(t)}.apply(e,r))||(t.exports=o)}()},66:function(t,e){Flatsome.plugin("addQty",(function(t,e){jQuery(t).on("click",".plus, .minus",(function(){var t=jQuery(this),e=t.closest(".quantity").find(".qty"),i=parseFloat(e.val()),r=parseFloat(e.attr("max")),o=parseFloat(e.attr("min")),a=e.attr("step");i&&""!==i&&"NaN"!==i||(i=0),""!==r&&"NaN"!==r||(r=""),""!==o&&"NaN"!==o||(o=0),"any"!==a&&""!==a&&void 0!==a&&"NaN"!==parseFloat(a)||(a=1),t.is(".plus")?r&&(r===i||i>r)?e.val(r):e.val(i+parseFloat(a)):o&&(o===i||i<o)?e.val(o):i>0&&e.val(i-parseFloat(a)),e.trigger("change")}))}))},67:function(t,e){Flatsome.behavior("add-qty",{attach:function(t){jQuery(".quantity",t).addQty()}})},68:function(t,e){Flatsome.behavior("equalize-box",{attach:function(t){var e={ScreenSize:{LARGE:1,MEDIUM:2,SMALL:3},equalizeItems:function(t){var e=this;e.maxHeight=0,e.rowEnd=e.disablePerRow?e.boxCount:e.colPerRow,e.$items=[],e.rating={present:!1,height:0,dummy:'<div class="js-star-rating star-rating" style="opacity: 0; visibility: hidden"></div>'},jQuery(t,e.currentElement).each((function(t){var i=jQuery(this);e.$items.push(i),i.height(""),i.height()>e.maxHeight&&(e.maxHeight=i.height()),i.children(".js-star-rating").remove();var r=i.children(".star-rating");r.length&&(e.rating.present=!0,e.rating.height=r.height()),t!==e.rowEnd-1&&t!==e.boxCount-1||(e.$items.forEach((function(t){t.height(e.maxHeight),e.maybeAddDummyRating(t)})),e.rowEnd+=e.colPerRow,e.maxHeight=0,e.$items=[],e.rating.present=!1)}))},getColsPerRow:function(){var t,e=jQuery(this.currentElement).attr("class");switch(this.getScreenSize()){case this.ScreenSize.LARGE:return(t=/large-columns-(\d+)/g.exec(e))?parseInt(t[1]):3;case this.ScreenSize.MEDIUM:return(t=/medium-columns-(\d+)/g.exec(e))?parseInt(t[1]):3;case this.ScreenSize.SMALL:return(t=/small-columns-(\d+)/g.exec(e))?parseInt(t[1]):2}},maybeAddDummyRating:function(t){var e=t;this.rating.present&&e.hasClass("price-wrapper")&&(e.children(".star-rating").length||(e.prepend(this.rating.dummy),e.children(".js-star-rating").height(this.rating.height)))},getScreenSize:function(){return window.matchMedia("(min-width: 850px)").matches?this.ScreenSize.LARGE:window.matchMedia("(min-width: 550px) and (max-width: 849px)").matches?this.ScreenSize.MEDIUM:window.matchMedia("(max-width: 549px)").matches?this.ScreenSize.SMALL:void 0},init:function(){var e=this,i=[".product-title",".price-wrapper",".box-excerpt",".add-to-cart-button"];jQuery(".equalize-box",t).each((function(t,r){e.currentElement=r,e.colPerRow=e.getColsPerRow(),1!==e.colPerRow&&(e.disablePerRow=jQuery(r).hasClass("row-slider")||jQuery(r).hasClass("row-grid"),e.boxCount=jQuery(".box-text",e.currentElement).length,i.forEach((function(t){e.equalizeItems(".box-text "+t)})))}))}};e.init(),jQuery(window).resize((function(){e.init()}))}})},69:function(t,e){Flatsome.behavior("quick-view",{attach:function(t){jQuery(".quick-view",t).each((function(t,e){jQuery(e).hasClass("quick-view-added")||(jQuery(e).click((function(t){if(""!=jQuery(this).attr("data-prod")){jQuery(this).parent().parent().addClass("processing");var e={action:"flatsome_quickview",product:jQuery(this).attr("data-prod")};jQuery.post(flatsomeVars.ajaxurl,e,(function(t){jQuery(".processing").removeClass("processing"),jQuery.magnificPopup.open({removalDelay:300,autoFocusLast:!1,closeMarkup:flatsomeVars.lightbox.close_markup,closeBtnInside:flatsomeVars.lightbox.close_btn_inside,items:{src:'<div class="product-lightbox lightbox-content">'+t+"</div>",type:"inline"}});var e=jQuery(".product-gallery-slider img",t).length>1;setTimeout((function(){jQuery(".product-lightbox").imagesLoaded((function(){jQuery(".product-lightbox .slider").flickity({cellAlign:"left",wrapAround:!0,autoPlay:!1,prevNextButtons:!0,adaptiveHeight:!0,imagesLoaded:!0,dragThreshold:15,pageDots:e,rightToLeft:flatsomeVars.rtl})}))}),300);var i=jQuery(".product-lightbox form.variations_form");jQuery(".product-lightbox form").hasClass("variations_form")&&i.wc_variation_form();var r=jQuery(".product-lightbox .product-gallery-slider"),o=jQuery(".product-lightbox .product-gallery-slider .slide.first img"),a=jQuery(".product-lightbox .product-gallery-slider .slide.first a"),s=o.attr("data-src")?o.attr("data-src"):o.attr("src"),n=function(){r.data("flickity")&&r.flickity("select",0)},c=function(){r.data("flickity")&&r.imagesLoaded((function(){r.flickity("resize")}))};i.on("show_variation",(function(t,e){e.image.src?(o.attr("src",e.image.src).attr("srcset",""),a.attr("href",e.image_link),n(),c()):e.image_src&&(o.attr("src",e.image_src).attr("srcset",""),a.attr("href",e.image_link),n(),c())})),i.on("hide_variation",(function(t,e){o.attr("src",s).attr("srcset",""),c()})),i.on("click",".reset_variations",(function(){o.attr("src",s).attr("srcset",""),n(),c()})),jQuery(".product-lightbox .quantity").addQty()})),t.preventDefault()}})),jQuery(e).addClass("quick-view-added"))}))}})}});(function(window,document){'use strict';var supportedBrowser=!1,loaded=!1;if(document.querySelector){if(window.addEventListener){supportedBrowser=!0}}
window.wp=window.wp||{};if(!!window.wp.receiveEmbedMessage){return}
window.wp.receiveEmbedMessage=function(e){var data=e.data;if(!data){return}
if(!(data.secret||data.message||data.value)){return}
if(/[^a-zA-Z0-9]/.test(data.secret)){return}
var iframes=document.querySelectorAll('iframe[data-secret="'+data.secret+'"]'),blockquotes=document.querySelectorAll('blockquote[data-secret="'+data.secret+'"]'),i,source,height,sourceURL,targetURL;for(i=0;i<blockquotes.length;i++){blockquotes[i].style.display='none'}
for(i=0;i<iframes.length;i++){source=iframes[i];if(e.source!==source.contentWindow){continue}
source.removeAttribute('style');if('height'===data.message){height=parseInt(data.value,10);if(height>1000){height=1000}else if(~~height<200){height=200}
source.height=height}
if('link'===data.message){sourceURL=document.createElement('a');targetURL=document.createElement('a');sourceURL.href=source.getAttribute('src');targetURL.href=data.value;if(targetURL.host===sourceURL.host){if(document.activeElement===source){window.top.location.href=data.value}}}}};function onLoad(){if(loaded){return}
loaded=!0;var isIE10=-1!==navigator.appVersion.indexOf('MSIE 10'),isIE11=!!navigator.userAgent.match(/Trident.*rv:11\./),iframes=document.querySelectorAll('iframe.wp-embedded-content'),iframeClone,i,source,secret;for(i=0;i<iframes.length;i++){source=iframes[i];if(!source.getAttribute('data-secret')){secret=Math.random().toString(36).substr(2,10);source.src+='#?secret='+secret;source.setAttribute('data-secret',secret)}
if((isIE10||isIE11)){iframeClone=source.cloneNode(!0);iframeClone.removeAttribute('security');source.parentNode.replaceChild(iframeClone,source)}}}
if(supportedBrowser){window.addEventListener('message',window.wp.receiveEmbedMessage,!1);document.addEventListener('DOMContentLoaded',onLoad,!1);window.addEventListener('load',onLoad,!1)}})(window,document);(function(factory){var root=typeof self=='object'&&self.self===self&&self||typeof global=='object'&&global.global===global&&global;if(typeof define==='function'&&define.amd){define(['underscore','jquery','exports'],function(_,$,exports){root.Backbone=factory(root,exports,_,$)})}else if(typeof exports!=='undefined'){var _=require('underscore'),$;try{$=require('jquery')}catch(e){}
factory(root,exports,_,$)}else{root.Backbone=factory(root,{},root._,root.jQuery||root.Zepto||root.ender||root.$)}})(function(root,Backbone,_,$){var previousBackbone=root.Backbone;var slice=Array.prototype.slice;Backbone.VERSION='1.4.0';Backbone.$=$;Backbone.noConflict=function(){root.Backbone=previousBackbone;return this};Backbone.emulateHTTP=!1;Backbone.emulateJSON=!1;var Events=Backbone.Events={};var eventSplitter=/\s+/;var _listening;var eventsApi=function(iteratee,events,name,callback,opts){var i=0,names;if(name&&typeof name==='object'){if(callback!==void 0&&'context' in opts&&opts.context===void 0)opts.context=callback;for(names=_.keys(name);i<names.length;i++){events=eventsApi(iteratee,events,names[i],name[names[i]],opts)}}else if(name&&eventSplitter.test(name)){for(names=name.split(eventSplitter);i<names.length;i++){events=iteratee(events,names[i],callback,opts)}}else{events=iteratee(events,name,callback,opts)}
return events};Events.on=function(name,callback,context){this._events=eventsApi(onApi,this._events||{},name,callback,{context:context,ctx:this,listening:_listening});if(_listening){var listeners=this._listeners||(this._listeners={});listeners[_listening.id]=_listening;_listening.interop=!1}
return this};Events.listenTo=function(obj,name,callback){if(!obj)return this;var id=obj._listenId||(obj._listenId=_.uniqueId('l'));var listeningTo=this._listeningTo||(this._listeningTo={});var listening=_listening=listeningTo[id];if(!listening){this._listenId||(this._listenId=_.uniqueId('l'));listening=_listening=listeningTo[id]=new Listening(this,obj)}
var error=tryCatchOn(obj,name,callback,this);_listening=void 0;if(error)throw error;if(listening.interop)listening.on(name,callback);return this};var onApi=function(events,name,callback,options){if(callback){var handlers=events[name]||(events[name]=[]);var context=options.context,ctx=options.ctx,listening=options.listening;if(listening)listening.count++;handlers.push({callback:callback,context:context,ctx:context||ctx,listening:listening})}
return events};var tryCatchOn=function(obj,name,callback,context){try{obj.on(name,callback,context)}catch(e){return e}};Events.off=function(name,callback,context){if(!this._events)return this;this._events=eventsApi(offApi,this._events,name,callback,{context:context,listeners:this._listeners});return this};Events.stopListening=function(obj,name,callback){var listeningTo=this._listeningTo;if(!listeningTo)return this;var ids=obj?[obj._listenId]:_.keys(listeningTo);for(var i=0;i<ids.length;i++){var listening=listeningTo[ids[i]];if(!listening)break;listening.obj.off(name,callback,this);if(listening.interop)listening.off(name,callback)}
if(_.isEmpty(listeningTo))this._listeningTo=void 0;return this};var offApi=function(events,name,callback,options){if(!events)return;var context=options.context,listeners=options.listeners;var i=0,names;if(!name&&!context&&!callback){for(names=_.keys(listeners);i<names.length;i++){listeners[names[i]].cleanup()}
return}
names=name?[name]:_.keys(events);for(;i<names.length;i++){name=names[i];var handlers=events[name];if(!handlers)break;var remaining=[];for(var j=0;j<handlers.length;j++){var handler=handlers[j];if(callback&&callback!==handler.callback&&callback!==handler.callback._callback||context&&context!==handler.context){remaining.push(handler)}else{var listening=handler.listening;if(listening)listening.off(name,callback)}}
if(remaining.length){events[name]=remaining}else{delete events[name]}}
return events};Events.once=function(name,callback,context){var events=eventsApi(onceMap,{},name,callback,this.off.bind(this));if(typeof name==='string'&&context==null)callback=void 0;return this.on(events,callback,context)};Events.listenToOnce=function(obj,name,callback){var events=eventsApi(onceMap,{},name,callback,this.stopListening.bind(this,obj));return this.listenTo(obj,events)};var onceMap=function(map,name,callback,offer){if(callback){var once=map[name]=_.once(function(){offer(name,once);callback.apply(this,arguments)});once._callback=callback}
return map};Events.trigger=function(name){if(!this._events)return this;var length=Math.max(0,arguments.length-1);var args=Array(length);for(var i=0;i<length;i++)args[i]=arguments[i+1];eventsApi(triggerApi,this._events,name,void 0,args);return this};var triggerApi=function(objEvents,name,callback,args){if(objEvents){var events=objEvents[name];var allEvents=objEvents.all;if(events&&allEvents)allEvents=allEvents.slice();if(events)triggerEvents(events,args);if(allEvents)triggerEvents(allEvents,[name].concat(args))}
return objEvents};var triggerEvents=function(events,args){var ev,i=-1,l=events.length,a1=args[0],a2=args[1],a3=args[2];switch(args.length){case 0:while(++i<l)(ev=events[i]).callback.call(ev.ctx);return;case 1:while(++i<l)(ev=events[i]).callback.call(ev.ctx,a1);return;case 2:while(++i<l)(ev=events[i]).callback.call(ev.ctx,a1,a2);return;case 3:while(++i<l)(ev=events[i]).callback.call(ev.ctx,a1,a2,a3);return;default:while(++i<l)(ev=events[i]).callback.apply(ev.ctx,args);return}};var Listening=function(listener,obj){this.id=listener._listenId;this.listener=listener;this.obj=obj;this.interop=!0;this.count=0;this._events=void 0};Listening.prototype.on=Events.on;Listening.prototype.off=function(name,callback){var cleanup;if(this.interop){this._events=eventsApi(offApi,this._events,name,callback,{context:void 0,listeners:void 0});cleanup=!this._events}else{this.count--;cleanup=this.count===0}
if(cleanup)this.cleanup()};Listening.prototype.cleanup=function(){delete this.listener._listeningTo[this.obj._listenId];if(!this.interop)delete this.obj._listeners[this.id]};Events.bind=Events.on;Events.unbind=Events.off;_.extend(Backbone,Events);var Model=Backbone.Model=function(attributes,options){var attrs=attributes||{};options||(options={});this.preinitialize.apply(this,arguments);this.cid=_.uniqueId(this.cidPrefix);this.attributes={};if(options.collection)this.collection=options.collection;if(options.parse)attrs=this.parse(attrs,options)||{};var defaults=_.result(this,'defaults');attrs=_.defaults(_.extend({},defaults,attrs),defaults);this.set(attrs,options);this.changed={};this.initialize.apply(this,arguments)};_.extend(Model.prototype,Events,{changed:null,validationError:null,idAttribute:'id',cidPrefix:'c',preinitialize:function(){},initialize:function(){},toJSON:function(options){return _.clone(this.attributes)},sync:function(){return Backbone.sync.apply(this,arguments)},get:function(attr){return this.attributes[attr]},escape:function(attr){return _.escape(this.get(attr))},has:function(attr){return this.get(attr)!=null},matches:function(attrs){return!!_.iteratee(attrs,this)(this.attributes)},set:function(key,val,options){if(key==null)return this;var attrs;if(typeof key==='object'){attrs=key;options=val}else{(attrs={})[key]=val}
options||(options={});if(!this._validate(attrs,options))return!1;var unset=options.unset;var silent=options.silent;var changes=[];var changing=this._changing;this._changing=!0;if(!changing){this._previousAttributes=_.clone(this.attributes);this.changed={}}
var current=this.attributes;var changed=this.changed;var prev=this._previousAttributes;for(var attr in attrs){val=attrs[attr];if(!_.isEqual(current[attr],val))changes.push(attr);if(!_.isEqual(prev[attr],val)){changed[attr]=val}else{delete changed[attr]}
unset?delete current[attr]:current[attr]=val}
if(this.idAttribute in attrs)this.id=this.get(this.idAttribute);if(!silent){if(changes.length)this._pending=options;for(var i=0;i<changes.length;i++){this.trigger('change:'+changes[i],this,current[changes[i]],options)}}
if(changing)return this;if(!silent){while(this._pending){options=this._pending;this._pending=!1;this.trigger('change',this,options)}}
this._pending=!1;this._changing=!1;return this},unset:function(attr,options){return this.set(attr,void 0,_.extend({},options,{unset:!0}))},clear:function(options){var attrs={};for(var key in this.attributes)attrs[key]=void 0;return this.set(attrs,_.extend({},options,{unset:!0}))},hasChanged:function(attr){if(attr==null)return!_.isEmpty(this.changed);return _.has(this.changed,attr)},changedAttributes:function(diff){if(!diff)return this.hasChanged()?_.clone(this.changed):!1;var old=this._changing?this._previousAttributes:this.attributes;var changed={};var hasChanged;for(var attr in diff){var val=diff[attr];if(_.isEqual(old[attr],val))continue;changed[attr]=val;hasChanged=!0}
return hasChanged?changed:!1},previous:function(attr){if(attr==null||!this._previousAttributes)return null;return this._previousAttributes[attr]},previousAttributes:function(){return _.clone(this._previousAttributes)},fetch:function(options){options=_.extend({parse:!0},options);var model=this;var success=options.success;options.success=function(resp){var serverAttrs=options.parse?model.parse(resp,options):resp;if(!model.set(serverAttrs,options))return!1;if(success)success.call(options.context,model,resp,options);model.trigger('sync',model,resp,options)};wrapError(this,options);return this.sync('read',this,options)},save:function(key,val,options){var attrs;if(key==null||typeof key==='object'){attrs=key;options=val}else{(attrs={})[key]=val}
options=_.extend({validate:!0,parse:!0},options);var wait=options.wait;if(attrs&&!wait){if(!this.set(attrs,options))return!1}else if(!this._validate(attrs,options)){return!1}
var model=this;var success=options.success;var attributes=this.attributes;options.success=function(resp){model.attributes=attributes;var serverAttrs=options.parse?model.parse(resp,options):resp;if(wait)serverAttrs=_.extend({},attrs,serverAttrs);if(serverAttrs&&!model.set(serverAttrs,options))return!1;if(success)success.call(options.context,model,resp,options);model.trigger('sync',model,resp,options)};wrapError(this,options);if(attrs&&wait)this.attributes=_.extend({},attributes,attrs);var method=this.isNew()?'create':options.patch?'patch':'update';if(method==='patch'&&!options.attrs)options.attrs=attrs;var xhr=this.sync(method,this,options);this.attributes=attributes;return xhr},destroy:function(options){options=options?_.clone(options):{};var model=this;var success=options.success;var wait=options.wait;var destroy=function(){model.stopListening();model.trigger('destroy',model,model.collection,options)};options.success=function(resp){if(wait)destroy();if(success)success.call(options.context,model,resp,options);if(!model.isNew())model.trigger('sync',model,resp,options)};var xhr=!1;if(this.isNew()){_.defer(options.success)}else{wrapError(this,options);xhr=this.sync('delete',this,options)}
if(!wait)destroy();return xhr},url:function(){var base=_.result(this,'urlRoot')||_.result(this.collection,'url')||urlError();if(this.isNew())return base;var id=this.get(this.idAttribute);return base.replace(/[^\/]$/,'$&/')+encodeURIComponent(id)},parse:function(resp,options){return resp},clone:function(){return new this.constructor(this.attributes)},isNew:function(){return!this.has(this.idAttribute)},isValid:function(options){return this._validate({},_.extend({},options,{validate:!0}))},_validate:function(attrs,options){if(!options.validate||!this.validate)return!0;attrs=_.extend({},this.attributes,attrs);var error=this.validationError=this.validate(attrs,options)||null;if(!error)return!0;this.trigger('invalid',this,error,_.extend(options,{validationError:error}));return!1}});var Collection=Backbone.Collection=function(models,options){options||(options={});this.preinitialize.apply(this,arguments);if(options.model)this.model=options.model;if(options.comparator!==void 0)this.comparator=options.comparator;this._reset();this.initialize.apply(this,arguments);if(models)this.reset(models,_.extend({silent:!0},options))};var setOptions={add:!0,remove:!0,merge:!0};var addOptions={add:!0,remove:!1};var splice=function(array,insert,at){at=Math.min(Math.max(at,0),array.length);var tail=Array(array.length-at);var length=insert.length;var i;for(i=0;i<tail.length;i++)tail[i]=array[i+at];for(i=0;i<length;i++)array[i+at]=insert[i];for(i=0;i<tail.length;i++)array[i+length+at]=tail[i]};_.extend(Collection.prototype,Events,{model:Model,preinitialize:function(){},initialize:function(){},toJSON:function(options){return this.map(function(model){return model.toJSON(options)})},sync:function(){return Backbone.sync.apply(this,arguments)},add:function(models,options){return this.set(models,_.extend({merge:!1},options,addOptions))},remove:function(models,options){options=_.extend({},options);var singular=!_.isArray(models);models=singular?[models]:models.slice();var removed=this._removeModels(models,options);if(!options.silent&&removed.length){options.changes={added:[],merged:[],removed:removed};this.trigger('update',this,options)}
return singular?removed[0]:removed},set:function(models,options){if(models==null)return;options=_.extend({},setOptions,options);if(options.parse&&!this._isModel(models)){models=this.parse(models,options)||[]}
var singular=!_.isArray(models);models=singular?[models]:models.slice();var at=options.at;if(at!=null)at=+at;if(at>this.length)at=this.length;if(at<0)at+=this.length+1;var set=[];var toAdd=[];var toMerge=[];var toRemove=[];var modelMap={};var add=options.add;var merge=options.merge;var remove=options.remove;var sort=!1;var sortable=this.comparator&&at==null&&options.sort!==!1;var sortAttr=_.isString(this.comparator)?this.comparator:null;var model,i;for(i=0;i<models.length;i++){model=models[i];var existing=this.get(model);if(existing){if(merge&&model!==existing){var attrs=this._isModel(model)?model.attributes:model;if(options.parse)attrs=existing.parse(attrs,options);existing.set(attrs,options);toMerge.push(existing);if(sortable&&!sort)sort=existing.hasChanged(sortAttr)}
if(!modelMap[existing.cid]){modelMap[existing.cid]=!0;set.push(existing)}
models[i]=existing}else if(add){model=models[i]=this._prepareModel(model,options);if(model){toAdd.push(model);this._addReference(model,options);modelMap[model.cid]=!0;set.push(model)}}}
if(remove){for(i=0;i<this.length;i++){model=this.models[i];if(!modelMap[model.cid])toRemove.push(model)}
if(toRemove.length)this._removeModels(toRemove,options)}
var orderChanged=!1;var replace=!sortable&&add&&remove;if(set.length&&replace){orderChanged=this.length!==set.length||_.some(this.models,function(m,index){return m!==set[index]});this.models.length=0;splice(this.models,set,0);this.length=this.models.length}else if(toAdd.length){if(sortable)sort=!0;splice(this.models,toAdd,at==null?this.length:at);this.length=this.models.length}
if(sort)this.sort({silent:!0});if(!options.silent){for(i=0;i<toAdd.length;i++){if(at!=null)options.index=at+i;model=toAdd[i];model.trigger('add',model,this,options)}
if(sort||orderChanged)this.trigger('sort',this,options);if(toAdd.length||toRemove.length||toMerge.length){options.changes={added:toAdd,removed:toRemove,merged:toMerge};this.trigger('update',this,options)}}
return singular?models[0]:models},reset:function(models,options){options=options?_.clone(options):{};for(var i=0;i<this.models.length;i++){this._removeReference(this.models[i],options)}
options.previousModels=this.models;this._reset();models=this.add(models,_.extend({silent:!0},options));if(!options.silent)this.trigger('reset',this,options);return models},push:function(model,options){return this.add(model,_.extend({at:this.length},options))},pop:function(options){var model=this.at(this.length-1);return this.remove(model,options)},unshift:function(model,options){return this.add(model,_.extend({at:0},options))},shift:function(options){var model=this.at(0);return this.remove(model,options)},slice:function(){return slice.apply(this.models,arguments)},get:function(obj){if(obj==null)return void 0;return this._byId[obj]||this._byId[this.modelId(this._isModel(obj)?obj.attributes:obj)]||obj.cid&&this._byId[obj.cid]},has:function(obj){return this.get(obj)!=null},at:function(index){if(index<0)index+=this.length;return this.models[index]},where:function(attrs,first){return this[first?'find':'filter'](attrs)},findWhere:function(attrs){return this.where(attrs,!0)},sort:function(options){var comparator=this.comparator;if(!comparator)throw new Error('Cannot sort a set without a comparator');options||(options={});var length=comparator.length;if(_.isFunction(comparator))comparator=comparator.bind(this);if(length===1||_.isString(comparator)){this.models=this.sortBy(comparator)}else{this.models.sort(comparator)}
if(!options.silent)this.trigger('sort',this,options);return this},pluck:function(attr){return this.map(attr+'')},fetch:function(options){options=_.extend({parse:!0},options);var success=options.success;var collection=this;options.success=function(resp){var method=options.reset?'reset':'set';collection[method](resp,options);if(success)success.call(options.context,collection,resp,options);collection.trigger('sync',collection,resp,options)};wrapError(this,options);return this.sync('read',this,options)},create:function(model,options){options=options?_.clone(options):{};var wait=options.wait;model=this._prepareModel(model,options);if(!model)return!1;if(!wait)this.add(model,options);var collection=this;var success=options.success;options.success=function(m,resp,callbackOpts){if(wait)collection.add(m,callbackOpts);if(success)success.call(callbackOpts.context,m,resp,callbackOpts)};model.save(null,options);return model},parse:function(resp,options){return resp},clone:function(){return new this.constructor(this.models,{model:this.model,comparator:this.comparator})},modelId:function(attrs){return attrs[this.model.prototype.idAttribute||'id']},values:function(){return new CollectionIterator(this,ITERATOR_VALUES)},keys:function(){return new CollectionIterator(this,ITERATOR_KEYS)},entries:function(){return new CollectionIterator(this,ITERATOR_KEYSVALUES)},_reset:function(){this.length=0;this.models=[];this._byId={}},_prepareModel:function(attrs,options){if(this._isModel(attrs)){if(!attrs.collection)attrs.collection=this;return attrs}
options=options?_.clone(options):{};options.collection=this;var model=new this.model(attrs,options);if(!model.validationError)return model;this.trigger('invalid',this,model.validationError,options);return!1},_removeModels:function(models,options){var removed=[];for(var i=0;i<models.length;i++){var model=this.get(models[i]);if(!model)continue;var index=this.indexOf(model);this.models.splice(index,1);this.length--;delete this._byId[model.cid];var id=this.modelId(model.attributes);if(id!=null)delete this._byId[id];if(!options.silent){options.index=index;model.trigger('remove',model,this,options)}
removed.push(model);this._removeReference(model,options)}
return removed},_isModel:function(model){return model instanceof Model},_addReference:function(model,options){this._byId[model.cid]=model;var id=this.modelId(model.attributes);if(id!=null)this._byId[id]=model;model.on('all',this._onModelEvent,this)},_removeReference:function(model,options){delete this._byId[model.cid];var id=this.modelId(model.attributes);if(id!=null)delete this._byId[id];if(this===model.collection)delete model.collection;model.off('all',this._onModelEvent,this)},_onModelEvent:function(event,model,collection,options){if(model){if((event==='add'||event==='remove')&&collection!==this)return;if(event==='destroy')this.remove(model,options);if(event==='change'){var prevId=this.modelId(model.previousAttributes());var id=this.modelId(model.attributes);if(prevId!==id){if(prevId!=null)delete this._byId[prevId];if(id!=null)this._byId[id]=model}}}
this.trigger.apply(this,arguments)}});var $$iterator=typeof Symbol==='function'&&Symbol.iterator;if($$iterator){Collection.prototype[$$iterator]=Collection.prototype.values}
var CollectionIterator=function(collection,kind){this._collection=collection;this._kind=kind;this._index=0};var ITERATOR_VALUES=1;var ITERATOR_KEYS=2;var ITERATOR_KEYSVALUES=3;if($$iterator){CollectionIterator.prototype[$$iterator]=function(){return this}}
CollectionIterator.prototype.next=function(){if(this._collection){if(this._index<this._collection.length){var model=this._collection.at(this._index);this._index++;var value;if(this._kind===ITERATOR_VALUES){value=model}else{var id=this._collection.modelId(model.attributes);if(this._kind===ITERATOR_KEYS){value=id}else{value=[id,model]}}
return{value:value,done:!1}}
this._collection=void 0}
return{value:void 0,done:!0}};var View=Backbone.View=function(options){this.cid=_.uniqueId('view');this.preinitialize.apply(this,arguments);_.extend(this,_.pick(options,viewOptions));this._ensureElement();this.initialize.apply(this,arguments)};var delegateEventSplitter=/^(\S+)\s*(.*)$/;var viewOptions=['model','collection','el','id','attributes','className','tagName','events'];_.extend(View.prototype,Events,{tagName:'div',$:function(selector){return this.$el.find(selector)},preinitialize:function(){},initialize:function(){},render:function(){return this},remove:function(){this._removeElement();this.stopListening();return this},_removeElement:function(){this.$el.remove()},setElement:function(element){this.undelegateEvents();this._setElement(element);this.delegateEvents();return this},_setElement:function(el){this.$el=el instanceof Backbone.$?el:Backbone.$(el);this.el=this.$el[0]},delegateEvents:function(events){events||(events=_.result(this,'events'));if(!events)return this;this.undelegateEvents();for(var key in events){var method=events[key];if(!_.isFunction(method))method=this[method];if(!method)continue;var match=key.match(delegateEventSplitter);this.delegate(match[1],match[2],method.bind(this))}
return this},delegate:function(eventName,selector,listener){this.$el.on(eventName+'.delegateEvents'+this.cid,selector,listener);return this},undelegateEvents:function(){if(this.$el)this.$el.off('.delegateEvents'+this.cid);return this},undelegate:function(eventName,selector,listener){this.$el.off(eventName+'.delegateEvents'+this.cid,selector,listener);return this},_createElement:function(tagName){return document.createElement(tagName)},_ensureElement:function(){if(!this.el){var attrs=_.extend({},_.result(this,'attributes'));if(this.id)attrs.id=_.result(this,'id');if(this.className)attrs['class']=_.result(this,'className');this.setElement(this._createElement(_.result(this,'tagName')));this._setAttributes(attrs)}else{this.setElement(_.result(this,'el'))}},_setAttributes:function(attributes){this.$el.attr(attributes)}});var addMethod=function(base,length,method,attribute){switch(length){case 1:return function(){return base[method](this[attribute])};case 2:return function(value){return base[method](this[attribute],value)};case 3:return function(iteratee,context){return base[method](this[attribute],cb(iteratee,this),context)};case 4:return function(iteratee,defaultVal,context){return base[method](this[attribute],cb(iteratee,this),defaultVal,context)};default:return function(){var args=slice.call(arguments);args.unshift(this[attribute]);return base[method].apply(base,args)}}};var addUnderscoreMethods=function(Class,base,methods,attribute){_.each(methods,function(length,method){if(base[method])Class.prototype[method]=addMethod(base,length,method,attribute)})};var cb=function(iteratee,instance){if(_.isFunction(iteratee))return iteratee;if(_.isObject(iteratee)&&!instance._isModel(iteratee))return modelMatcher(iteratee);if(_.isString(iteratee))return function(model){return model.get(iteratee)};return iteratee};var modelMatcher=function(attrs){var matcher=_.matches(attrs);return function(model){return matcher(model.attributes)}};var collectionMethods={forEach:3,each:3,map:3,collect:3,reduce:0,foldl:0,inject:0,reduceRight:0,foldr:0,find:3,detect:3,filter:3,select:3,reject:3,every:3,all:3,some:3,any:3,include:3,includes:3,contains:3,invoke:0,max:3,min:3,toArray:1,size:1,first:3,head:3,take:3,initial:3,rest:3,tail:3,drop:3,last:3,without:0,difference:0,indexOf:3,shuffle:1,lastIndexOf:3,isEmpty:1,chain:1,sample:3,partition:3,groupBy:3,countBy:3,sortBy:3,indexBy:3,findIndex:3,findLastIndex:3};var modelMethods={keys:1,values:1,pairs:1,invert:1,pick:0,omit:0,chain:1,isEmpty:1};_.each([[Collection,collectionMethods,'models'],[Model,modelMethods,'attributes']],function(config){var Base=config[0],methods=config[1],attribute=config[2];Base.mixin=function(obj){var mappings=_.reduce(_.functions(obj),function(memo,name){memo[name]=0;return memo},{});addUnderscoreMethods(Base,obj,mappings,attribute)};addUnderscoreMethods(Base,_,methods,attribute)});Backbone.sync=function(method,model,options){var type=methodMap[method];_.defaults(options||(options={}),{emulateHTTP:Backbone.emulateHTTP,emulateJSON:Backbone.emulateJSON});var params={type:type,dataType:'json'};if(!options.url){params.url=_.result(model,'url')||urlError()}
if(options.data==null&&model&&(method==='create'||method==='update'||method==='patch')){params.contentType='application/json';params.data=JSON.stringify(options.attrs||model.toJSON(options))}
if(options.emulateJSON){params.contentType='application/x-www-form-urlencoded';params.data=params.data?{model:params.data}:{}}
if(options.emulateHTTP&&(type==='PUT'||type==='DELETE'||type==='PATCH')){params.type='POST';if(options.emulateJSON)params.data._method=type;var beforeSend=options.beforeSend;options.beforeSend=function(xhr){xhr.setRequestHeader('X-HTTP-Method-Override',type);if(beforeSend)return beforeSend.apply(this,arguments)}}
if(params.type!=='GET'&&!options.emulateJSON){params.processData=!1}
var error=options.error;options.error=function(xhr,textStatus,errorThrown){options.textStatus=textStatus;options.errorThrown=errorThrown;if(error)error.call(options.context,xhr,textStatus,errorThrown)};var xhr=options.xhr=Backbone.ajax(_.extend(params,options));model.trigger('request',model,xhr,options);return xhr};var methodMap={create:'POST',update:'PUT',patch:'PATCH',delete:'DELETE',read:'GET'};Backbone.ajax=function(){return Backbone.$.ajax.apply(Backbone.$,arguments)};var Router=Backbone.Router=function(options){options||(options={});this.preinitialize.apply(this,arguments);if(options.routes)this.routes=options.routes;this._bindRoutes();this.initialize.apply(this,arguments)};var optionalParam=/\((.*?)\)/g;var namedParam=/(\(\?)?:\w+/g;var splatParam=/\*\w+/g;var escapeRegExp=/[\-{}\[\]+?.,\\\^$|#\s]/g;_.extend(Router.prototype,Events,{preinitialize:function(){},initialize:function(){},route:function(route,name,callback){if(!_.isRegExp(route))route=this._routeToRegExp(route);if(_.isFunction(name)){callback=name;name=''}
if(!callback)callback=this[name];var router=this;Backbone.history.route(route,function(fragment){var args=router._extractParameters(route,fragment);if(router.execute(callback,args,name)!==!1){router.trigger.apply(router,['route:'+name].concat(args));router.trigger('route',name,args);Backbone.history.trigger('route',router,name,args)}});return this},execute:function(callback,args,name){if(callback)callback.apply(this,args)},navigate:function(fragment,options){Backbone.history.navigate(fragment,options);return this},_bindRoutes:function(){if(!this.routes)return;this.routes=_.result(this,'routes');var route,routes=_.keys(this.routes);while((route=routes.pop())!=null){this.route(route,this.routes[route])}},_routeToRegExp:function(route){route=route.replace(escapeRegExp,'\\$&').replace(optionalParam,'(?:$1)?').replace(namedParam,function(match,optional){return optional?match:'([^/?]+)'}).replace(splatParam,'([^?]*?)');return new RegExp('^'+route+'(?:\\?([\\s\\S]*))?$')},_extractParameters:function(route,fragment){var params=route.exec(fragment).slice(1);return _.map(params,function(param,i){if(i===params.length-1)return param||null;return param?decodeURIComponent(param):null})}});var History=Backbone.History=function(){this.handlers=[];this.checkUrl=this.checkUrl.bind(this);if(typeof window!=='undefined'){this.location=window.location;this.history=window.history}};var routeStripper=/^[#\/]|\s+$/g;var rootStripper=/^\/+|\/+$/g;var pathStripper=/#.*$/;History.started=!1;_.extend(History.prototype,Events,{interval:50,atRoot:function(){var path=this.location.pathname.replace(/[^\/]$/,'$&/');return path===this.root&&!this.getSearch()},matchRoot:function(){var path=this.decodeFragment(this.location.pathname);var rootPath=path.slice(0,this.root.length-1)+'/';return rootPath===this.root},decodeFragment:function(fragment){return decodeURI(fragment.replace(/%25/g,'%2525'))},getSearch:function(){var match=this.location.href.replace(/#.*/,'').match(/\?.+/);return match?match[0]:''},getHash:function(window){var match=(window||this).location.href.match(/#(.*)$/);return match?match[1]:''},getPath:function(){var path=this.decodeFragment(this.location.pathname+this.getSearch()).slice(this.root.length-1);return path.charAt(0)==='/'?path.slice(1):path},getFragment:function(fragment){if(fragment==null){if(this._usePushState||!this._wantsHashChange){fragment=this.getPath()}else{fragment=this.getHash()}}
return fragment.replace(routeStripper,'')},start:function(options){if(History.started)throw new Error('Backbone.history has already been started');History.started=!0;this.options=_.extend({root:'/'},this.options,options);this.root=this.options.root;this._wantsHashChange=this.options.hashChange!==!1;this._hasHashChange='onhashchange' in window&&(document.documentMode===void 0||document.documentMode>7);this._useHashChange=this._wantsHashChange&&this._hasHashChange;this._wantsPushState=!!this.options.pushState;this._hasPushState=!!(this.history&&this.history.pushState);this._usePushState=this._wantsPushState&&this._hasPushState;this.fragment=this.getFragment();this.root=('/'+this.root+'/').replace(rootStripper,'/');if(this._wantsHashChange&&this._wantsPushState){if(!this._hasPushState&&!this.atRoot()){var rootPath=this.root.slice(0,-1)||'/';this.location.replace(rootPath+'#'+this.getPath());return!0}else if(this._hasPushState&&this.atRoot()){this.navigate(this.getHash(),{replace:!0})}}
if(!this._hasHashChange&&this._wantsHashChange&&!this._usePushState){this.iframe=document.createElement('iframe');this.iframe.src='javascript:0';this.iframe.style.display='none';this.iframe.tabIndex=-1;var body=document.body;var iWindow=body.insertBefore(this.iframe,body.firstChild).contentWindow;iWindow.document.open();iWindow.document.close();iWindow.location.hash='#'+this.fragment}
var addEventListener=window.addEventListener||function(eventName,listener){return attachEvent('on'+eventName,listener)};if(this._usePushState){addEventListener('popstate',this.checkUrl,!1)}else if(this._useHashChange&&!this.iframe){addEventListener('hashchange',this.checkUrl,!1)}else if(this._wantsHashChange){this._checkUrlInterval=setInterval(this.checkUrl,this.interval)}
if(!this.options.silent)return this.loadUrl()},stop:function(){var removeEventListener=window.removeEventListener||function(eventName,listener){return detachEvent('on'+eventName,listener)};if(this._usePushState){removeEventListener('popstate',this.checkUrl,!1)}else if(this._useHashChange&&!this.iframe){removeEventListener('hashchange',this.checkUrl,!1)}
if(this.iframe){document.body.removeChild(this.iframe);this.iframe=null}
if(this._checkUrlInterval)clearInterval(this._checkUrlInterval);History.started=!1},route:function(route,callback){this.handlers.unshift({route:route,callback:callback})},checkUrl:function(e){var current=this.getFragment();if(current===this.fragment&&this.iframe){current=this.getHash(this.iframe.contentWindow)}
if(current===this.fragment)return!1;if(this.iframe)this.navigate(current);this.loadUrl()},loadUrl:function(fragment){if(!this.matchRoot())return!1;fragment=this.fragment=this.getFragment(fragment);return _.some(this.handlers,function(handler){if(handler.route.test(fragment)){handler.callback(fragment);return!0}})},navigate:function(fragment,options){if(!History.started)return!1;if(!options||options===!0)options={trigger:!!options};fragment=this.getFragment(fragment||'');var rootPath=this.root;if(fragment===''||fragment.charAt(0)==='?'){rootPath=rootPath.slice(0,-1)||'/'}
var url=rootPath+fragment;fragment=fragment.replace(pathStripper,'');var decodedFragment=this.decodeFragment(fragment);if(this.fragment===decodedFragment)return;this.fragment=decodedFragment;if(this._usePushState){this.history[options.replace?'replaceState':'pushState']({},document.title,url)}else if(this._wantsHashChange){this._updateHash(this.location,fragment,options.replace);if(this.iframe&&fragment!==this.getHash(this.iframe.contentWindow)){var iWindow=this.iframe.contentWindow;if(!options.replace){iWindow.document.open();iWindow.document.close()}
this._updateHash(iWindow.location,fragment,options.replace)}}else{return this.location.assign(url)}
if(options.trigger)return this.loadUrl(fragment)},_updateHash:function(location,fragment,replace){if(replace){var href=location.href.replace(/(javascript:|#).*$/,'');location.replace(href+'#'+fragment)}else{location.hash='#'+fragment}}});Backbone.history=new History;var extend=function(protoProps,staticProps){var parent=this;var child;if(protoProps&&_.has(protoProps,'constructor')){child=protoProps.constructor}else{child=function(){return parent.apply(this,arguments)}}
_.extend(child,parent,staticProps);child.prototype=_.create(parent.prototype,protoProps);child.prototype.constructor=child;child.__super__=parent.prototype;return child};Model.extend=Collection.extend=Router.extend=View.extend=History.extend=extend;var urlError=function(){throw new Error('A "url" property or function must be specified')};var wrapError=function(model,options){var error=options.error;options.error=function(resp){if(error)error.call(options.context,model,resp,options);model.trigger('error',model,resp,options)}};return Backbone})