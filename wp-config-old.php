<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'b1040772_madeinrosia');
/** MySQL database username */
define('DB_USER', 'b1040772_mirm');
/** MySQL database password */
define('DB_PASSWORD', 'm4d3inr0s1am0nt4na');
/** MySQL hostname */
define('DB_HOST', 'localhost');
/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');
/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define ('WPLANG', 'ro_RO');
/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|83L#`2Ev*@}1@wM8L`*hvp)YX^4H{3Dh?`lP b L-wJ1k%f`A@0g&5,t)u:_39o');
define('SECURE_AUTH_KEY',  '|U_mp+9g6Q1(%Ez,p}zXZ+~xP}1qz8ul^|+sd$F%&yfpG-YYVLP5srt1?EUAO/Bb');
define('LOGGED_IN_KEY',    'M+-]RoG+r(#&gh9~zVA#Y -H`,128|ik)S[+`7@j*aaKi`@0W3+r/dSusyGF|pDh');
define('NONCE_KEY',        '-xnc#!&_cIX*I36I68fPfG>2inbzl>I]Wto=G[o-,dU,F2*i~Y73o!vXj>x [SK9');
define('AUTH_SALT',        'I*b!.9-rJqBM]B>Qj eN[`WG|.e*dXYt@o1*-D>`4<l|lTF<;-rvBmJYoW8D3gT^');
define('SECURE_AUTH_SALT', '-cI~ 3-m!|;bW@~6@F,)?+|WedHUx]8vWO@&I1skB,7#Z7[F=-pqWN~88#wXK#|x');
define('LOGGED_IN_SALT',   'F+F6,[a@[Z`)#O.D~??K#L/[lRV24?jz-Ws#REOm* WvoG8em.b:{5AMTjNl&3 v');
define('NONCE_SALT',       '(.mMeq-jl:D-TQEtZBgpW_p?>h2%E)AlsP=+|Gen^TF;w|%s~09kL 2w.Cp!j+]t');
/**#@-*/
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';
/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
/* That's all, stop editing! Happy blogging. */
/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');