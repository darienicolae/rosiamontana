jQuery(function($) {
    jQuery("a.single_add_to_cart_button.eh_paypal_express_link").on('click',function() 
    {
        var qty = $('.qty').val();
        if(!qty){
          qty = $("input[name=quantity]").val();  
        }
        if(!$(this).hasClass('disabled')&&(qty>0))
        {
            if( eh_express_checkout_params.page_name === "product" )
            {
                $('form.cart').block({
                    message: null,
                    overlayCSS: {
                        background: '#fff',
                        opacity: 0.6
                    }
                });
                var code_href = $(this).attr('href');  
                if(!eh_express_checkout_params.is_var_product) {     // Checking product is not a variable product on express checkout from the product page
                    var pid = $("form.cart button").val();
                    var q = qty;
                    code_href=code_href+'&pid='+pid+'&q='+q;
                }
                $('form.cart').attr('action', code_href);
                $(this).attr('disabled', 'disabled');
                $('form.cart').submit();
                return false;
            }else if(eh_express_checkout_params.page_name === "cart")
            {
                $('div.wc-proceed-to-checkout').block({
                    message: null,
                    overlayCSS: {
                        background: '#fff',
                        opacity: 0.6
                    }
                });
            }
        }
    });
    jQuery("span.edit_eh_pe_address").on('click',function() 
    {
        jQuery(".woocommerce-billing-fields p").removeClass("eh_pe_checkout_fields_hide");
        jQuery(".woocommerce-billing-fields p").removeClass("eh_pe_checkout_fields_fill");
        jQuery(".woocommerce-billing-fields .eh_pe_address").hide();
    });
});