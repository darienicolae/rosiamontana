<?php
function wc_smartbill_main()
{
    ?>
    <div class="wrap">
        <h2><?=__('Smart Bill', 'wc_smartbill')?></h2>
    </div>
    <div>
        <img src="<?=plugin_dir_url(__FILE__)?>../assets/images/logo.png" style="width: 590px" />
    </div>
    <p>
        <?=__('Nu aveti cont Smart Bill Cloud?', 'wc_smartbill')?>
        <br />
        <?=__('Inregistrati-va GRATUIT <a href="https://cloud.smartbill.ro/inregistrare-cont/" target="_blank">aici</a>', 'wc_smartbill')?>.
    </p>
    <p>
        <?=__('Mai multe detalii despre Smart Bill Cloud puteti afla <a href="https://www.facturionline.ro/" target="_blank">aici</a>', 'wc_smartbill')?>
    </p>
    <?php
}
