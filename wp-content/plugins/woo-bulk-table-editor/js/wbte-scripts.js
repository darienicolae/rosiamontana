/**
 * Scripts for Bulk Table Editor
 *
 * @package BulkTableEditor/js
 */

var tbl      = '#wbtetable';
var aDefault = '';
var $        = jQuery;
var aColor   = $( 'a' ).css( 'color' );

function checkAll() {

	var radio = $( "input[name='stype']:checked" ).val();
	var view  = $( '#form-gs-view' ).val();
	var table = tbl;

	if ( view === 'ext' ) {
		table = tbl_ext;
	}


	$( table + ' tbody tr' ).each(
		function(){
			var check = this.children[0].children[0]['id'];
			if ( radio === 'rows' ) {
				if ( this.style.display === '' ) {
					$( check ).prop( 'checked', true );
				} else {
					$( check ).prop( 'checked', false );
					if (table === 'prod' ) {
						setDefaultRowValues( this.children[0].children[0] );
					}
				}
			} else {
				$( check ).prop( 'checked', $( '#checkall' ).prop( 'checked' ) );
			}
		}
	);

}

function checkVisible() {
	var view  = $( '#form-gs-view' ).val();
	var table = tbl;

	if ( view === 'ext' ) {
		table = tbl_ext;
	}

	$( table + ' tbody tr' ).each(
		function() {
			var check = this.children[0].children[0]['id'];
			if ( this.style.display === '' ) {
				$( check ).prop( 'checked', $( '#checkall' ).prop('checked') );
			} else {
				$( check ).prop( 'checked', false );
			}
		}
	);
}

function setDefaultRowValues( obj ) {

	obj['stock'].value     = obj['stock'].defaultValue;
	obj['price'].value     = obj['price'].defaultValue;
	obj['saleprice'].value = obj['saleprice'].defaultValue;
	obj['salefrom'].value  = obj['salefrom'].defaultValue;
	obj['saleto'].value    = obj['saleto'].defaultValue;

}

function sort( column, type ) {

	var view  = $( '#form-gs-view' ).val();
	var table = tbl;

	if ( view === 'ext' ) {
		table = tbl_ext;
	}

	$( table ).tableCalc( 'sortColumn', column, type );
	
	if ( $( '#s' + column ).hasClass( 'fa-sort' ) ) {
		$( '#s' + column ).removeClass( 'fa-sort' ).addClass( 'fa-sort-up' );
		resetSortClass( column );
		return;
	}
	if ( $( '#s' + column ).hasClass( 'fa-sort-up' ) ) {
		$( '#s' + column ).removeClass( 'fa-sort-up' ).addClass( 'fa-sort-down' );
		resetSortClass( column );
		return;
	}
	if ( $( '#s' + column ).hasClass( 'fa-sort-down' ) ) {
		$( '#s' + column ).removeClass( 'fa-sort-down' ).addClass( 'fa-sort-up' );
		resetSortClass( column );
		return;
	}

	function resetSortClass( column ){
		for ( var i = 1; i < 10; i++ ) {
			if ( i != column ) {
				$( '#s' + i ).removeClass( 'fa-sort-down' ).removeClass( 'fa-sort-up' ).addClass( 'fa-sort' );
			}
		}
	}

}

function clearSales() {
	
	var is_checked = false;

	$( tbl + ' tbody form' ).each(
		function() {
			if ( this['saleprice'].value.length > 0 || this['salefrom'].value.length > 0 || this['saleto'].value.length > 0 ) {
				this['saleprice'].value   = '';
				this['salefrom'].value    = '';
				this['saleto'].value      = '';
				this['salepercent'].value = '';
				$( this['id'] ).prop( 'checked', true );
				is_checked = true;
			} else {
				this['saleprice'].value   = this['saleprice'].defaultValue;
				this['salefrom'].value    = this['salefrom'].defaultValue;
				this['saleto'].value      = this['saleto'].defaultValue;
				this['salepercent'].value = this['salepercent'].defaultValue;
				$( this['id'] ).prop( 'checked', false );
			}
		}
	)

	if ( is_checked ) {
		$( '#a-clear-sales' ).css( 'color', 'red' );
		$( '#a-delete-rows' ).css( 'color', '' );
		$( '#saveall' ).css( 'color', 'red' );
		$( '#saveall_foot' ).css( 'color', 'red' );
	} else {
		$( '#a-clear-sales' ).css( 'color', '' );
		$( '#a-delete-rows' ).css( 'color', '' );
		$( '#saveall' ).css( 'color', '' );
		$( '#saveall_foot' ).css( 'color', '' );
	}
}


function deleteRows() {
	
	var confirm_delete = confirm( $( '#msg-confirm-delete' ).val() );
	
	if ( confirm_delete === true ) {
		var p_count   = 0;
		var rescount  = 0;
		var count     = 0;
		var nonce_val = $( '#_wpnonce' ).val();
		var json_data = { action: 'wbte_delete_table_rows_data', nonce: nonce_val, rows: [] };

		$( tbl + ' tbody form' ).each(
			function() {
				if ( $( this['id'] ).prop( 'checked' ) ) {
					var frm_data = { id: this['id'].value };
					json_data.rows.push( frm_data );
					count++;
					if ( p_count === 1 ) {
						progressBar('start');
					}
					p_count++;
				}
			}
		)

		$( '#a-clear-sales' ).css( 'color', '' );
		$( '#a-delete-rows' ).css( 'color', 'red' );

		if ( count > 0 ) {
			$.ajax(
				{
					type: 'POST',
					url: ajaxurl,
					datatype: 'json',
					data: json_data,
					success: function( response ) {
						console.log( response );
						rescount++;
						if (rescount > 0) {
							var ms = ( p_count >= 100 ) ? 2000 : 500;
							reloadPage( ms );
						}
					},
					error: function( response ){
						console.log( response );
					}
				}
			);
		
			event.preventDefault();
		}
	} else {
		$( '#checkall' ).prop( 'checked', false );
		checkAll();
	}

}

function saveAll() {

	var count         = 0;
	var p_count       = 0;
	var rescount      = 0;
	var nonce_val     = $( '#_wpnonce' ).val();
	var json_data     = { action: 'wbte_update_table_rows_data', nonce: nonce_val, rows: [] };
	
	$( tbl + ' tbody form' ).each(
		function() {
			if ( $( this['id'] ).prop( 'checked' ) ) {
				var frm_data = { id: this['id'].value,
					stock: this['stock'].value, price: this['price'].value,
					saleprice: this['saleprice'].value, salefrom: this['salefrom'].value, saleto: this['saleto'].value,
					name: this['product_name'].value, name_update: this['name_update'].value
				};
				if ( custom_col_active && show_sku_home === 'no' ) {
					frm_data['customprice'] = this['customprice'].value;
				} else if ( show_sku_home === 'yes' ) {
					frm_data['sku'] = this['sku'].value;
				}
				json_data.rows.push( frm_data );
				count++;
				if ( count === 1 ) {
					jQuery( '#saveall' ).attr( 'disabled', true );
					jQuery( '#saveall_foot' ).attr( 'disabled', true );
				}

				if( count === 80 ) {
					sendJson( ajaxurl, json_data );
					json_data.rows = [];
					count = 0;
				}
				
				if ( p_count === 1 ) {
					progressBar('start');
				}
				p_count++;

			} else {
				this['stock'].value     = this['stock'].defaultValue;
				this['price'].value     = this['price'].defaultValue;
				this['saleprice'].value = this['saleprice'].defaultValue;
				this['salefrom'].value  = this['salefrom'].defaultValue;
				this['saleto'].value    = this['saleto'].defaultValue;
				if ( custom_col_active && show_sku_home === 'no' ) {
					this['customprice'].value = this['customprice'].defaultValue;
				}
			}
		}
	);


	$.ajax(
		{
			type: 'POST',
			url: ajaxurl,
			datatype: 'json',
			data: json_data,
			success: function( response ) {
				console.log( response );
				rescount++;
				if (rescount > 0) {
					jQuery( '#saveall' ).attr( 'disabled', false );
					jQuery( '#saveall_foot' ).attr( 'disabled', false );
					var ms = ( p_count >= 100 ) ? 3000 : 500;
					reloadPage( ms );
				}
			},
			error: function( response ){
				progressBar('stop');
				console.log( response );
			}
		}
	);
	

	event.preventDefault();

};

function saveAllExt() {

	var count         = 0;
	var p_count       = 0;
	var rescount      = 0;
	var nonce_val     = $( '#_wpnonce' ).val();
	var json_data     = { action: 'wbte_update_table_rows_ext_data', nonce: nonce_val, rows: [] };

	
	$( tbl_ext + ' tbody form' ).each(
		function() {
			if ( $( this['id'] ).prop( 'checked' ) ) {
				var frm_data = { id: this['id'].value,
				    featured: this['featured'].value, sku: this['sku'].value,
					backorder: this['backorder'].value, instock: this['instock'].value, weight: this['weight'].value,
					length: this['_length'].value, width: this['width'].value, height: this['height'].value,
					tags: this['tags'].value,
					name: this['product_name'].value, name_update: this['name_update'].value
				};
				
				json_data.rows.push( frm_data );
				count++;
				if ( count === 1 ) {
					jQuery( '#saveallExt' ).attr( 'disabled', true );
					jQuery( '#saveall_foot_ext' ).attr( 'disabled', true );
				}

				if( count === 80 ) {
					sendJson( ajaxurl, json_data );
					json_data.rows = [];
					count = 0;
				}

				if ( p_count === 1 ) {
					progressBar('start');
				}
				p_count++;

			} 
		}
	);

	if ( count > 0 ) {

		//$( '#checkall' ).prop( 'checked', false );
		//checkAll();
	}

	$.ajax(
		{
			type: 'POST',
			url: ajaxurl,
			datatype: 'json',
			data: json_data,
			success: function( response ) {
				console.log( response );
				rescount++;
				if (rescount > 0) {
					jQuery( '#saveallExt' ).attr( 'disabled', false );
					jQuery( '#saveall_foot_ext' ).attr( 'disabled', false );
					var ms = ( p_count >= 100 ) ? 3000 : 500;
					reloadPage( ms );
				}
			},
			error: function( response ){
				progressBar('stop');
				console.log( response );
				jQuery( '#saveallExt' ).attr( 'disabled', false );
				jQuery( '#saveall_foot_ext' ).attr( 'disabled', false );
			}
		}
	);
	

	event.preventDefault();

};

function sendJson( ajaxurl, json_data ) {

	$.ajax(
		{
			type: 'POST',
			url: ajaxurl,
			datatype: 'json',
			data: json_data,
			success: function( response ) {
				console.log( response );
			},
			error: function( response ){
				console.log( response );
			}
		}
	);

}

function reloadPage( msec ) {

	setTimeout(
		function() {
			location.reload();
		},
		msec
	);

}

function setSalesDate( obj ) {

	if ( ! obj ) {
		return;
	}

	$( tbl + ' tbody form' ).each(
		
		function() {
			if ( obj.id === 'datep_from' ) {
				this[ 'salefrom' ].value = obj.value;
			}
			if ( obj.id === 'datep_to' ) {
				this[ 'saleto' ].value = obj.value;
			}
		}
	);

	$( '#saveall' ).css( 'color', aColor );
	$( '#checkall' ).prop( 'checked', true );

	checkAll();

}

function calcChanged( ft ) {

	var tot         = sum_col;
	var is_checkbox = ( this.name === 'id' ) ? true : false;
	var is_inputbox = ( this.type === 'number' || this.type === 'text' ) ? true : false;
	var ftv         = ( ft === true ) ? true : false;

	if ( ! ftv ) {
		if ( is_checkbox ) {
			$( this ).prop( 'checked', this.checked );
		}

		if ( is_inputbox ) {
			var $sbox = $( this ).contents().context.form[0];
			$( $sbox ).prop( 'checked', true );
		}
	} else {
		findSalePricePercent();
	}

	$( '#saveall' ).css( 'color', aColor );

	if ( $( tbl + ' tbody tr' ).length > 0 ) {
		var stock = parseFloat( $( tbl ).tableCalc( 'getSum', 2 ) ).toFixed( 0 );
		$( '#tbl-total-f' ).text( $( tbl ).tableCalc( 'getSum', tot ) );
		$( '#tbl-total-s' ).text( stock );
	}

}

function extChanged() {

	if ( this.name === 'featured' ) {
		if ( $(this).prop('checked') === true ) {
			$( this ).val( 1 );
		} else {
			$( this ).val( '' );
		}
	}

	if ( this.name !== 'id' ) {
		var $sbox = $( this ).contents().context.form[0];
		$( $sbox ).prop( 'checked', true );
	}

}


function changeCalcType( calcType, customCalculation ){

	$( tbl ).tableCalc( 'calculate', calcType, customCalculation );
	calcChanged();

};

function calcSpecial( i, o, id ) {

	var sign    = '+';
	var in_val  = Number( $( '#' + id ).val() );
	var in_type = $( '#' + id + '_type option:selected' ).val();

	if ( in_val === 0 && in_type !== 'round_up' && in_type !== 'round_down' && in_type !== 'round_two' ) {
		$( '#' + id + '_type' ).val( '0' );
		return;
	}

	var calctype = '';

	switch ( in_type ) {
		case '0':
			break;
		case 'up_p':
			sign     = '+';
			calctype = 'percent';
			break;
		case 'up_n':
			sign     = '+';
			calctype = 'number';
			break;
		case 'down_p':
			sign     = '-';
			calctype = 'percent';
			break;
		case 'down_n':
			sign     = '-';
			calctype = 'number';
			break;
		case 'fix_n':
			sign     = '=';
			calctype = 'number';
			break;
		case 'round_up':
			sign     = 'u';
			calctype = 'round';
			break;
		case 'round_down':
			sign     = 'd';
			calctype = 'round';
			break;
		case 'round_two':
			sign     = 't';
			calctype = 'round';
			break;
	}

	$( '#' + id ).val( '' );
	$( '#' + id + '_type' ).val( '0' );

	var percent = 1.0;
	if ( sign === '+' ) {
		percent = percent + in_val / 100;
	} else if ( sign === '-' ) {
		percent = percent - in_val / 100;
	}
	var formula = '(0:' + i + '*' + percent + ')==0:' + o + '';

	if ( calctype === 'number' ) {
		if ( sign === '=' ) {
			var c = o - 2;
			switch(o){
				case 7:
					c = 3;
					break;
				case 8:
					c = 4;
					break;
			}
			formula = '(0:' + c + '*' + 0 + '+' + in_val + ')==0:' + o + '';
		} else {
			formula = '(0:' + i + sign + in_val + ')==0:' + o + '';
		}
	}

	if ( calctype === 'round' ) {
		roundNumbers( o, sign );
	} else {
		$( tbl ).tableCalc( 'calculate', 'c', formula );
	}

	$( tbl ).tableCalc( 'calculate', 'c', '(0:0 * 0:1)' );

	if ( 'sale_price_select' === id ) {
		findSalePricePercent();
	}

	if ( $( tbl + ' tbody tr' ).length > 0 ) {
		$( '#tbl-total-f' ).text( $( tbl ).tableCalc( 'getSum', sum_col ) );
	}

	$( '#saveall' ).css( 'color', aColor );
	$( '#checkall' ).prop( 'checked', true );

	checkAll();

};

function bulkSetValues( id, the_table ) {
	
	var view     = $( '#form-gs-view' ).val();
	var in_val   = $( '#' + id ).val();
	var in_type  = $( '#' + id + '_type option:selected' ).val();
	var table    = tbl;

	$( '#' + id ).val( '' );
	$( '#' + id + '_type' ).val( '0' );

	if ( the_table === 'ext' ) {
		table = tbl_ext;
	}
	var is_changed = false;

	$( table + ' tbody tr' ).each(
		function() {
			var is_visible   = this.style.display;
			var frm          = this.children[1].children[0];
			var frm_elements = frm.form.elements;

			if ( '' === is_visible ) {

				switch ( id ) {
					case 'featured_select':
						if ( in_type === 'yes' ) {
							$( frm_elements['featured'] ).prop( 'checked', true );
							frm_elements['featured'].value = '1';
						} else {
							$( frm_elements['featured'] ).prop( 'checked', false );
							frm_elements['featured'].value = '';
						}
						break;
					case 'sku_select':
						if ( in_type === 'replace' ) {
							frm_elements['sku'].value = in_val;
						} else if ( in_type === 'add-after' ) {
							frm_elements['sku'].value += in_val; 
						} else if ( in_type === 'add-before' ) {
							frm_elements['sku'].value = in_val + frm_elements['sku'].value;
						} else if ( in_type === 'add-id-after' ) {
							frm_elements['sku'].value += '-' + frm_elements['id'].value; 
						} else if ( in_type === 'add-id-before' ) {
							frm_elements['sku'].value = frm_elements['id'].value + '-' + frm_elements['sku'].value;	
						} else if ( in_type === 'clear' ) {
							frm_elements['sku'].value = '';
						} else if ( in_type === 'generate' ) {
							frm_elements['sku'].value = generateSku(frm_elements['product_name'].value);
						} else if ( in_type === 'toupper' ) {
							frm_elements['sku'].value = frm_elements['sku'].value.toUpperCase();
						} else if ( in_type === 'tolower' ) {
							frm_elements['sku'].value = frm_elements['sku'].value.toLowerCase();
						}
						break;
					case 'tags_select':
						if ( in_type === 'add' ) {
							if ( frm_elements['tags'].value.trim() === '' ) {
								frm_elements['tags'].value += in_val;
							} else {
								frm_elements['tags'].value += ', ' + in_val;
							}
						} else if ( in_type === 'toupper' ) {
							frm_elements['tags'].value = frm_elements['tags'].value.toUpperCase(); 
						} else if ( in_type === 'tolower' ) {
							frm_elements['tags'].value = frm_elements['tags'].value.toLowerCase(); 
						} else if ( in_type === 'clear' ) {
							frm_elements['tags'].value = ''; 
						} else if ( in_type === 'replace' ) {
							frm_elements['tags'].value = in_val; 
						}
						break;
					case 'weight_select':
						var current = frm_elements['weight'].value || '0';
						frm_elements['weight'].value = getMeasure( in_type, in_val, current );
						break;
					case 'length_select':
						var current = frm_elements['_length'].value || '0';
						frm_elements['_length'].value = getMeasure( in_type, in_val, current );
						break;
					case 'width_select':
						var current = frm_elements['width'].value || '0';
						frm_elements['width'].value = getMeasure( in_type, in_val, current );
						break;
					case 'height_select':
						var current = frm_elements['height'].value || '0';
						frm_elements['height'].value = getMeasure( in_type, in_val, current );
						break;
					case 'backorder_select':
						$( frm_elements['backorder'] ).val(in_type).change();
						break;
					case 'instock_select':
						$( frm_elements['instock'] ).val(in_type).change();
						break;
				}
				
				function getMeasure( type, value, oldvalue ) {
					var retval = 0;
					switch ( type ) {
						case 'fix_n':
							retval = value;
							break;
						case 'up_n':
							retval = eval( parseFloat( oldvalue ) + parseFloat( value ) );
							break;
						case 'down_n':
							retval = eval( parseFloat( oldvalue ) - parseFloat( value ) );
							break;
						case 'clear':
							retval = '';
					}
					return retval;
				}

				function generateSku( name ) {
					
					if ( name === undefined || name === '' ) {
						return '';
					}

					var isVariation = name.search(',');
					var nameArr     = ( isVariation === -1 ) ? name.split( ' ' ) : name.split(',');
					var retval      = '';
					if ( nameArr.length > 0 ) {
						for ( var i = 0; i < nameArr.length; i++ ) {
							retval += nameArr[i].trim().substring( 0, 3 ) + '-';
						}
						retval = retval.substring( 0, parseInt(retval.length - 1) );
					} else {
						retval = name.substring( 0, 3 );
					}
					return retval;
				}

				//Row changed = true
				$( frm_elements['id'] ).prop( 'checked', true );
				is_changed = true;
			} else {
				//Not changed
				$( frm_elements['id'] ).prop( 'checked', false );
			}
		}
	)
	if (is_changed) {
		$( '#checkall' ).prop( 'checked', true );
		checkAll();
	}
	

}

function roundNumbers( column, sign ) {

	var table = $( tbl + ' tbody tr' );

	table.each(
		function() {
			var rValue = this.children[column].children[0].value;
			var rOut   = 0;
			switch ( sign ) {
				case 'd':
					rOut = Number(Math.floor(rValue)).toFixed(2);
					break;
				case 'u':
					rOut = Number(Math.ceil(rValue)).toFixed(2);
					break;
				case 't':
					rOut = Number(rValue).toFixed(1);
					break;
			}

			var newValue = Number(rOut).toFixed(2);
			if ( Number(rOut) === 0 ) {
				newValue = '';
			}
			
			this.children[column].children[0].value = newValue;
		}
	)
}

function rowSearch(){

	var view  = $( '#form-gs-view' ).val();
	var input = $( '#product_search' ).val();
	var table = $( tbl + ' tbody tr' );
	var radio = $( "input[name='stype']:checked" ).val();
	var arr   = [];

	if ( view === 'ext' ) {
		table = $( tbl_ext + ' tbody tr' );
	}

	if ( radio === 'rows' ) {
		if ( input.length > 0 ) {
			input = input.toLowerCase().replace( ',', '' );
			arr   = input.split( ' ' );
		}
		table.each(
			function() {
				var textValue     = this.children[1].children[0].value;
				var skuPriceValue = this.children[3].children[0].value;
				if (textValue){
					var txt   = textValue.toLowerCase();
					var count = 0;
					for( var i = 0; i < arr.length; i++ ) {
						if ( txt.match( arr[i] ) || skuPriceValue.toLowerCase().match( arr[i] ) ) {
							count++;
						} 
					}
					if ( count === arr.length ) {
						this.style.display = '';
					} else {
						this.style.display = 'none';
					}
				}
			}
		)
	} 
}

function radioChange() {

	var radio = $( "input[name='stype']:checked" ).val();

	if ( radio === 'rows' ) {
		$( 'form #btnsearch' ).attr( 'disabled', true );
	} else {
		$( 'form #btnsearch' ).attr( 'disabled', false );
	}

}

function rowSearchChange( obj ){

	var searchVal = $(obj).closest('form').find( "input[name='product_search']" ).val();
	var radio     = $(obj).closest('form').find( "input[name='stype']:checked" ).val();

	if ( 'rows' === radio ) {
		$('#wbte-pages').find('a').each(function(){
			var url    = $(this).attr('href');
			var newUrl = url.replace( /\&row_search\=.*/i, '' );
			$(this).attr('href', newUrl + '&row_search='+searchVal);
		});
	}

}

function sortTable( table, order ) {
	
	var sort  = 'asc';
	var tbody = table.find( 'tbody' );
	if ( order !== 'asc' ) {
		sort = '';
	}

    tbody.find('tr').sort(function(a, b) {
		var a1 = a.children[1].children[0].value;
		var b1 = b.children[1].children[0].value;
	
        if ( sort === 'asc' ) {
			return a1.localeCompare(b1);
        } else {
			return b1.localeCompare(a1);
        }
	}).appendTo(tbody);
	
}

function findSalePricePercent() {
	
	var table = $( tbl + ' tbody form' );

	table.each(
		function() {
			calcPercent( this, true );
		}
	);
}

function calcPercent( obj, isForm = false ) {

	var form;

	if ( isForm ) {
		form = obj;
	} else {
		form = obj.form;
	}

	var price      = form['price'].value;
	var sale_price = form['saleprice'].value;

	if ( parseFloat(price) > 0 && parseFloat(sale_price) > 0 ) {
		var percent = eval( ( price - sale_price ) * 100 / price );
		form['salepercent'].value = String( Number( percent ).toFixed(0) ) + '% ';
	}

}

function progressBar( option ) {
	
	if (option === 'start'){
		$('#wbte-saving').removeClass('wbte-saving').addClass('wbte-saving-show');
		var v = 0;
		
		setInterval(
			function() {
				if ( v > 100) {
					v = 0;
				}
				$('#pbar-saving').val(v);
				v += 5;
			},
			50
		);

	} else {
		$('#wbte-saving').removeClass('wbte-saving-show').addClass('wbte-saving');
	}

}
