<?php
/**
 * Bulk Table Editor templates
 *
 * @package BulkTableEditor/includes
 */

defined( 'ABSPATH' ) || exit;

require_once __DIR__ . '/class-wbte-functions.php';
require_once __DIR__ . '/class-wbte-extended.php';

/**
 * Class for templates
 */
class WbteTemplates {

	/**
	 * WbteFunctions
	 *
	 * @var var $wbtefunctions.
	 */
	public $wbtefunctions;

	/**
	 * WbteExtended
	 *
	 * @var var $extended.
	 */
	public $extended;

	/**
	 * WbteOptions
	 *
	 * @var var $wbte_options.
	 */
	public $wbte_options;


	/**
	 * Constructor
	 */
	public function __construct() {
		$this->wbtefunctions = new WbteFunctions();
		$this->wbte_options  = get_option( 'wbte_options' );
		$this->extended      = new WbteExtended();
	}

	/**
	 * Load the page and contents
	 */
	public function wbte_load() {

		$view = filter_input( 1, 'view', FILTER_SANITIZE_STRING );

		if ( ! isset( $view ) ) {
			$view = 'prod';
		}
		
		$this->wbte_get_bte_header( $view );
		$this->wbte_get_table_paging_top( $view );

		if ( 'prod' === $view ) {
			$this->wbte_get_table();
		} else {
			$this->extended->wbte_get_table();
		}

		$this->wbte_get_mobile_view();
	}

	/**
	 * Check for autofocus
	 */
	public function wbte_get_autofocus_value( $type ) {
		
		$mouseover_value = 'this.focus();';

		if ( 'date' === $type ) {
			if ( 'yes' !== $this->wbte_options['wbte_date_format_autofocus'] ) {
				$mouseover_value = '';
			}
		} else {
			if ( 'yes' === $this->wbte_options['wbte_no_autofocus'] ) {
				$mouseover_value = '';
			}
		}

		return $mouseover_value;
	
	}

	/**
	 * Get header and search
	 *
	 * @param var $view page view.
	 */
	public function wbte_get_bte_header( $view ) {

		$get_prod_search     = filter_input( 1, 'product_search', FILTER_SANITIZE_STRING );
		$get_row_search      = filter_input( 1, 'row_search', FILTER_SANITIZE_STRING );
		$option_no_sale_date = get_option( 'wbte_options[wbte_error_sale_no_date]' );
		$stype               = filter_input( 1, 'stype', FILTER_SANITIZE_STRING );
		$prod_cat            = filter_input( 1, 'product_cat', FILTER_SANITIZE_STRING );
		$sales_filter        = filter_input( 1, 'sales_filter', FILTER_SANITIZE_STRING );
		$url                 = admin_url( 'edit.php?post_type=product&page=wbte-products&sales_filter=' . $sales_filter . '&product_cat=' . $prod_cat . '&view=' );
		
		$auto_date  = 'date';
		$auto_focus = '';
		
		?>
		<div class="wrap">
			<h1 class="wp-heading-inline"><?php esc_html_e( 'Bulk Table Editor', 'woo-bulk-table-editor' ); ?></h1>
			<?php
			if ( 'true' === $option_no_sale_date ) {
				do_action( 'wbte_admin_notice_err_infinity_sale' );
			} 
			?>
		</div>

		<div class="row-container">
			<div style="display:inline-flex;padding-top:10px;">
			<?php
			if ( 'prod' === $view ) {
				?>
					<a type="button" href="#0" name="products" class="button-secondary disabled" style="margin-right:5px;">
					<i class="fas fa-home"></i> <?php esc_attr_e( 'Editor home', 'woo-bulk-table-editor' ); ?>
					</a>
					<a type="button" href="<?php echo esc_url( $url . 'ext' ); ?>" name="extend" class="button-primary" style="margin-right:5px;">
					<i class="fas fa-arrows-alt"></i> <?php esc_attr_e( 'Other values', 'woo-bulk-table-editor' ); ?>
					</a>
				<?php
			} else {
				?>
					<a type="button" href="<?php echo esc_url( $url . 'prod' ); ?>" name="products" class="button-primary" style="margin-right:5px;">
					<i class="fas fa-home"></i> <?php esc_attr_e( 'Editor home', 'woo-bulk-table-editor' ); ?>
					</a>
					<a type="button" href="#0" name="extended" class="button-secondary disabled" style="margin-right:5px;">
					<i class="fas fa-arrows-alt"></i> <?php esc_attr_e( 'Other values', 'woo-bulk-table-editor' ); ?>
					</a>
				<?php
			}
			?>
			<form method="post" id="download_products" action="<?php esc_attr_e( admin_url( 'admin-post.php?csv=true&product_cat=' . $prod_cat ) ); ?>">
					<input type="hidden" name="action" value="return_products_csv_file">
					<button type="submit" name="download_products" class="button">
						<i class="fas fa-cloud-download-alt"></i> 
					</button>
			</form>
			</div>
			<div class="col-search display-desktop">
				<div>   
				<form name="gs" type="get" action="edit.php">  
					<input type="hidden" name="post_type" value="product">  
					<input type="hidden" name="page" value="wbte-products"> 
					<input type="hidden" name="view" id="form-gs-view" value="<?php echo esc_attr( $view ); ?>"> 
					<input type="search" value="<?php esc_attr_e( $get_row_search ); ?>" class="input-txt-search" onchange="rowSearchChange(this);" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_focus ) ); ?>" placeholder="<?php esc_html_e( 'search', 'woo-bulk-table-editor' ); ?><?php echo ' ' . esc_attr( $get_prod_search ); ?>" id="product_search" name="product_search" >
					<button type="submit" id="btnsearch" class="button"><i class="fas fa-search"></i></button><br/>
					<div class="frm-radio">
					<?php esc_attr_e( 'Search in', 'woo-bulk-table-editor' ); ?>
					<input type="radio" name="stype" value="s" <?php echo ( 's' === $stype ) ? 'checked="checked"' : ''; ?>><?php esc_attr_e( 'Text', 'woo-bulk-table-editor' ); ?>
					<input type="radio" name="stype" value="sku" <?php echo ( 'sku' === $stype ) ? 'checked="checked"' : ''; ?>><?php esc_attr_e( 'SKU', 'woo-bulk-table-editor' ); ?>
					<input type="radio" name="stype" value="rows" <?php echo ( 'rows' === $stype || !isset( $stype ) ) ? 'checked="checked"' : ''; ?>><?php esc_attr_e( 'Rows', 'woo-bulk-table-editor' ); ?>
					</div>           
				</form>
				</div>
			</div>
		</div>
		<?php
	}

	/**
	 * Get table head info
	 */
	public function wbte_get_table_head_info() {

		$custom_price    = $this->wbtefunctions->wbte_get_custom_price_info();
		$calculate_field = 4;
		$options         = get_option( 'wbte_options' );
		$show_sku        = ( strlen( $options[ 'wbte_use_sku_main_page' ] ) > 0 ) ? $options[ 'wbte_use_sku_main_page' ] : 'no';

		if ( 'yes' === $custom_price['normal_calc'] ) {
			$calculate_field = 1;
		}

		$auto_date  = 'date';
		$auto_focus = '';
		?>
			<tr class="th-info">
				<th></th>
				<th style="vertical-align: middle;"><strong><?php esc_html_e( 'Bulk Editor', 'woo-bulk-table-editor' ); ?></strong><br /><span class="th-desc"><?php esc_html_e( 'Fill in numbers and dates here to apply to all visible products', 'woo-bulk-table-editor' ); ?></span></th>
				<th style="vertical-align: top;"><input type="number" name="stock_select" id="stock_select" class="input-txt" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_focus ) ); ?>"><br/>
					<select name="stock_select_type" id="stock_select_type" class="input-select" onchange="calcSpecial(0,2,'stock_select');">
						<?php $this->wbtefunctions->wbte_get_bulk_options( 'stock' ); ?>
					</select>
				</th>
				<th style="vertical-align: top;"><input type="number" name="price_select" id="price_select" class="input-txt" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_focus ) ); ?>">
				<br/>
					<select name="price_select_type" id="price_select_type" class="input-select" onchange="calcSpecial(1,3,'price_select');">
						<?php $this->wbtefunctions->wbte_get_bulk_options( '' ); ?>
					</select>
				</th>
				<th style="vertical-align: top;"><input type="number" name="sale_price_select" id="sale_price_select" class="input-txt" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_focus ) ); ?>">
				<br/>
					<select name="sale_price_select_type" id="sale_price_select_type" class="input-select" onchange="calcSpecial(1,4,'sale_price_select');">
						<?php $this->wbtefunctions->wbte_get_bulk_options( '' ); ?>
					</select>
				</th>
				<th style="vertical-align: top;"><input type="text" class="input-date" id="datep_from" onchange="setSalesDate(this);" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_date ) ); ?>;"></th>
				<th style="vertical-align: top;"><input type="text" class="input-date" id="datep_to" onchange="setSalesDate(this);" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_date ) ); ?>;"></th>
				<th><!-- Sale in percent --></th>
				<?php
				if ( 'yes' === $custom_price['active'] && 'no' === $show_sku ) {
					?>
					<th style="vertical-align: top;"><input type="number" name="custom_price_select" id="custom_price_select" class="input-txt" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_focus ) ); ?>">
					<br/>
						<select name="custom_price_select_type" id="custom_price_select_type" class="input-select" onchange="calcSpecial(<?php echo esc_js( $calculate_field ); ?>,8,'custom_price_select');">
							<?php $this->wbtefunctions->wbte_get_bulk_options( '' ); ?>
						</select>
					</th>
					<?php
				} elseif ( 'yes' === $show_sku ) {
					?>
					<th style="vertical-align: top;"><input type="text" name="sku_select" id="sku_select" class="input-txt" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_focus ) ); ?>">
					<br/>
						<select name="sku_select_type" id="sku_select_type" class="input-select" onchange="bulkSetValues('sku_select','');">
							<?php $this->wbtefunctions->wbte_get_bulk_options( 'sku' ); ?>
						</select>
					</th>
					<?php
				}
				?>
				<th style="vertical-align: top;">
					<button type="button" id="saveall" class="button action" onclick='saveAll();' style="width:100%;"><i class="fas fa-database"></i> <?php esc_html_e( 'Save all', 'woo-bulk-table-editor' ); ?></button>
					<div class="wbte-saving" id="wbte-saving">
						<progress id="pbar-saving" class="pbar-saving" value="0" max="100"></progress>
					</div>
				</th>
			</tr>

		<?php
	}


	/**
	 * Get table row
	 *
	 * @param var $product object.
	 * @param var $has_attributes bool.
	 * @param var $parent_id int.
	 */
	public function wbte_get_table_row( $product, $has_attributes, $parent_id ) {

		$product_name       = $product->get_title();
		$product_id         = $product->get_id();
		$custom_price       = $this->wbtefunctions->wbte_get_custom_price_info();
		$date_format        = get_option( 'date_format' );
		$options            = get_option( 'wbte_options' );
		$show_sku           = ( strlen( $options[ 'wbte_use_sku_main_page' ] ) > 0 ) ? $options[ 'wbte_use_sku_main_page' ] : 'no';
		$custom_price_value = '';
	
		if ( $has_attributes ) {

			$attributes = $product->get_attributes();

			if ( isset( $attributes ) && is_array( $attributes ) ) {
				foreach ( $attributes as $key => $val ) {
					if ( isset( $val ) && is_string( $val ) && strlen( $val ) > 0 ) {
						$product_name .= ', ' . ucfirst( $val );
					}
				}
			}

			$product_id = $product->get_parent_id();
			
			if ( 0 === $product_id ) {
				$product_id = $product->get_id();
			}
		}

		if ( 'yes' === $custom_price['active'] ) {
			$custom_price_value = get_post_meta( $product->get_id(), $custom_price['price'], true );
		}

		$auto_date  = 'date';
		$auto_focus = '';

		?>
		<tr class="lozad">
			<td id="<?php echo esc_attr( $product->get_id() ); ?>">
				<form id="frm_<?php echo esc_attr( $product->get_id() ); ?>_<?php echo esc_attr( wp_rand( 1, 50000 ) ); ?>" method="post">
				<input name="id" value="<?php echo esc_attr( $product->get_id() ); ?>" type="checkbox">
			</td>
			<td>
				<?php
				if ( ! $has_attributes ) {
					?>
					<input type="text" class="input-txt-name" name="product_name" value="<?php echo esc_html( $product_name ); ?>" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_focus ) ); ?>;">
					<input type="hidden" name="name_update" value="true">
					<?php
				} else {
					?>
					<input type="hidden" name="product_name" value="<?php echo esc_html( $product_name ); ?>">
					<input type="hidden" name="name_update" value="false">
					<?php echo esc_html( $product_name ); ?>
					<?php
				}
				?>
				<a href="<?php echo esc_url( get_admin_url() . 'post.php?post=' . $product_id . '&action=edit' ); ?>">
					<i class="fas fa-edit" style="float:right;"></i>
				</a>
			</td>
			<td>
				<input type="number" name="stock" class="input-txt" value="<?php echo esc_attr( $product->get_stock_quantity() ); ?>" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_focus ) ); ?>;"> 
			</td>
			<td>
				<input type="number" step="any" name="price" class="input-txt" value="<?php echo esc_attr( $product->get_regular_price() ); ?>" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_focus ) ); ?>"> 
			</td> 
			<td>
				<input type="number" step="any" name="saleprice" class="input-txt" value="<?php echo esc_attr( $product->get_sale_price() ); ?>" onchange="calcPercent(this);" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_focus ) ); ?>"> 
			</td>   
			<td>
			<?php
			$sale_from_value = '';
			if ( $product->get_date_on_sale_from() ) {
				$sale_from_value = date_format( $product->get_date_on_sale_from(), $date_format );
			};
			?>
				<input type="text" name="salefrom" autocomplete="off" class="input-date" value="<?php echo esc_attr( $sale_from_value ); ?>" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_date ) ); ?>;"> 
			</td>
			<td>
			<?php
			$sale_to_value = '';
			if ( $product->get_date_on_sale_to() ) {
				$sale_to_value = date_format( $product->get_date_on_sale_to(), $date_format );
			}
			?>
				<input type="text" name="saleto" autocomplete="off" class="input-date" value="<?php echo esc_attr( $sale_to_value ); ?>" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_date ) ); ?>;"> 
			</td>
			<td><input type="text" name="salepercent" class="input-txt" value="" readonly="readonly" style="direction:rtl;"></td>
			<?php
			if ( 'yes' === $custom_price['active'] && 'no' === $show_sku ) {
				?>
				<td>
				<input type="number" step="any" name="customprice" class="input-txt" value="<?php echo esc_attr( $custom_price_value ); ?>" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_focus ) ); ?>"> 
				</td>
				<?php
			} elseif ( 'yes' === $show_sku ) {
				?>
				<td>
					<input type="text" class="input-txt" name="sku" value="<?php echo esc_attr( $this->wbtefunctions->wbte_get_product_sku( $product ) ); ?>" onmouseover="<?php echo esc_js( $this->wbte_get_autofocus_value( $auto_focus ) ); ?>">
				</td>
				<?php
			}
			?>
			<td>
				<input type="number" name="totcol" class="input-txt" value="" readonly="readonly" style="direction:rtl;"> 
				</form>
			</td>
		</tr>
		<?php
	}

	/**
	 * No row found
	 */
	public function wbte_get_no_row_found() {
		
		$custom_price = $this->wbtefunctions->wbte_get_custom_price_info();
		$colspan_val  = 8;
		
		if ( 'yes' === $custom_price['active'] ) {
			$colspan_val = 9;
		}
		?>
			<tr><td colspan="<?php echo esc_attr( $colspan_val ); ?>"><strong><?php esc_html_e( 'No products found', 'woo-bulk-table-editor' ); ?></strong></td></tr>
		<?php

	}

	/**
	 * Get table footer
	 */
	public function wbte_get_table_footer() {
		
		$custom_price = $this->wbtefunctions->wbte_get_custom_price_info();
		$colspan_val  = 8;
		
		if ( 'yes' === $custom_price['active'] ) {
			$colspan_val = 9;
		}
		?>
		<tfoot>
			<tr>
				<td><?php wp_nonce_field( 'footer_id' ); ?></td>
				<td><input type="hidden" id="msg-confirm-delete" value="<?php esc_attr_e( 'Move checked items to trash?', 'woo-bulk-table-editor' ); ?>"></td>
				<td><span id="tbl-total-s" class="span-txt-center"></span></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			<?php
			if ( 'yes' === $custom_price['active'] ) {
				?>
				<td></td>
				<?php
			}
			?>
				<td></td>
				<td><span id="tbl-total-f" class="span-txt"></span></td>
			</tr>
			<tr class="th-info">
				<td colspan="<?php echo esc_attr( $colspan_val ); ?>"></td>
				<td>
				<button type="button" id="saveall_foot" class="button action" onclick='saveAll();' style="width:100%;"><i class="fas fa-database"></i> <?php esc_html_e( 'Save all', 'woo-bulk-table-editor' ); ?></button>
				</td>
			</tr>
		</tfoot>
		<?php
	}

	/**
	 * Get paging above table
	 *
	 * @param var $view the view.
	 */
	public function wbte_get_table_paging_top( $view ) {

		$get_prod_cat = ( strlen( filter_input( 1, 'product_cat', FILTER_SANITIZE_STRING ) ) > 0 ) ? filter_input( 1, 'product_cat', FILTER_SANITIZE_STRING ) : get_option( 'wbte_options' )['wbte_product_cat'];
		$prod_search  = filter_input( 1, 'product_search', FILTER_SANITIZE_STRING );

		if ( strlen( $prod_search ) > 0 ) {
			$get_prod_cat = '';
		}
		?>
		<div class="row-cat-paging display-desktop">
			<div class="col-category"> <!-- Select category -->
				<?php $this->wbtefunctions->wbte_get_categories_select( $get_prod_cat ); ?>
			<div>
				<?php $this->wbtefunctions->wbte_get_sales_products_select(); ?>
			</div>
			<div class="col-clear">
				<a href="<?php echo esc_url( admin_url( 'admin.php?page=wc-settings&tab=wbte' ) ); ?>" class="a-clear" id="a-settings"><i class="fas fa-cog"></i> <?php esc_attr_e( 'Settings', 'woo-bulk-table-editor' ); ?></a> 
			<?php
			if ( 'prod' === $view ) {
				?>
				| <a href="#" class="a-clear" id="a-delete-rows" onclick="deleteRows();"><i class="fas fa-trash-alt"></i> <?php esc_attr_e( 'Delete rows', 'woo-bulk-table-editor' ); ?></a>
				| <a href="#" class="a-clear" id="a-clear-sales" onclick="clearSales();"><i class="fas fa-eraser"></i> <?php esc_attr_e( 'Remove sales', 'woo-bulk-table-editor' ); ?></a> 
				<?php
			}
			?>
				
				<?php do_action( 'wbte_action_cyp_create_link_to_cyp'); ?>
				</div>
			</div>
			<div id="wbte-paging-top" class="div-paging">
			
			</div>
		</div>
		<?php
	}


	/**
	 * Get table head
	 */
	public function wbte_get_table_head() {

		$custom_price = $this->wbtefunctions->wbte_get_custom_price_info();
		$options      = get_option( 'wbte_options' );
		$show_sku     = ( strlen( $options[ 'wbte_use_sku_main_page' ] ) > 0 ) ? $options[ 'wbte_use_sku_main_page' ] : 'no';
		
		?>
		<tr>
			<th class="th-first"><input type="checkbox" id="checkall" onclick="checkVisible();"></th>
			<th style="width:20%;"><a href="#" onclick="sort(1,'text');"><?php esc_html_e( 'Name', 'woo-bulk-table-editor' ); ?></a> <i id="s1" class="fas fa-sort"></i></th>
			<th><a href="#" onclick="sort(2,'number');"><?php esc_html_e( 'Stock', 'woo-bulk-table-editor' ); ?></a> <i id="s2" class="fas fa-sort"></i></th>
			<th><a href="#" onclick="sort(3,'number');"><?php esc_html_e( 'Price', 'woo-bulk-table-editor' ); ?></a> <i id="s3" class="fas fa-sort"></i></th>
			<th><a href="#" onclick="sort(4,'number');"><?php esc_html_e( 'Sale price', 'woo-bulk-table-editor' ); ?></a> <i id="s4" class="fas fa-sort"></i></th>
			<th class="th-center"><a href="#" onclick="sort(5,'date');"><?php esc_html_e( 'Sale start date', 'woo-bulk-table-editor' ); ?></a> <i id="s5" class="fas fa-sort"></i></th>
			<th class="th-center"><a href="#" onclick="sort(6,'date');"><?php esc_html_e( 'Sale end date', 'woo-bulk-table-editor' ); ?></a> <i id="s6" class="fas fa-sort"></i></th>
			<th style="width:70px;" class="th-center"><a href="#" onclick="sort(7,'percent');"><?php esc_html_e( 'Sale %', 'woo-bulk-table-editor' ); ?></a> <i id="s7" class="fas fa-sort"></i></th>
		<?php
		if ( 'yes' === $custom_price['active'] && 'no' === $show_sku ) {	
			?>
			<th class="th-center"><a href="#" onclick="sort(8,'number');"><?php echo esc_html( $custom_price['name'] ); ?></a> <i id="s8" class="fas fa-sort"></i> </th>
			<th class="th-center"><a href="#" onclick="sort(9,'number');"><?php esc_html_e( 'Stock value', 'woo-bulk-table-editor' ); ?></a> <i id="s9" class="fas fa-sort"></i> </th>
			<?php
		} elseif ( 'yes' === $show_sku ) {
			?>
			<th class="th-center"><a href="#" onclick="sort(8,'text');"><?php esc_html_e( 'SKU', 'woo-bulk-table-editor' ); ?></a> <i id="s8" class="fas fa-sort"></i> </th>
			<th class="th-center"><a href="#" onclick="sort(9,'number');"><?php esc_html_e( 'Stock value', 'woo-bulk-table-editor' ); ?></a> <i id="s9" class="fas fa-sort"></i> </th>
			<?php
		} else {
			?>
			<th class="th-center"><a href="#" onclick="sort(8,'number');"><?php esc_html_e( 'Stock value', 'woo-bulk-table-editor' ); ?></a> <i id="s8" class="fas fa-sort"></i> </th>
			<?php
		}
		?>
		</tr>

		<?php
	}

	/**
	 * Get table
	 *
	 */
	public function wbte_get_table() {
		?>
		<div class="display-desktop"> <!-- Table -->
			<table class="wp-list-table widefat fixed striped posts" id="wbtetable" style="width:100%;">
				<thead>
					<?php $this->wbte_get_table_head(); ?>
					<?php $this->wbte_get_table_head_info(); ?>
				</thead>
				<tbody id="the-list"> 
					<?php $this->wbtefunctions->wbte_loop_products(); ?>
				</tbody>
					<?php $this->wbte_get_table_footer(); ?>
			</table>
			<div id="wbte-paging-bottom" class="div-paging" style="margin-top: 6px;">
			</div>

			</div>
		<div>
		<?php
	}

	/**
	 * Get mobile view
	 */
	public function wbte_get_mobile_view() {

		?>
		<div class="display-mobile">
			<div class="updated woocommerce-info display-mobile" style="width: 98%;">
				<?php esc_html_e( 'This plugin currently does not support mobile views. ', 'woo-bulk-table-editor' ); ?>
				<?php esc_html_e( 'Please use screen size larger than 700px', 'woo-bulk-table-editor' ); ?>
			</div>
		</div>
		<?php

	}


}
