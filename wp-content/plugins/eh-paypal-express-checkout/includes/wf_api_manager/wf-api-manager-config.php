<?php

$product_name = 'paypalexpresspaymentgateway'; // name should match with 'Software Title' configured in server, and it should not contains white space
$product_version = '1.2.6';
$product_slug = 'eh-paypal-express-checkout/eh-paypal-express-checkout.php'; //product base_path/file_name
$serve_url = 'https://www.webtoffee.com/';
$plugin_settings_url = admin_url('admin.php?page=wc-settings&tab=checkout&section=eh_paypal_express');

//include api manager
include_once ( 'wf_api_manager.php' );
new WF_API_Manager($product_name, $product_version, $product_slug, $serve_url, $plugin_settings_url);
?>