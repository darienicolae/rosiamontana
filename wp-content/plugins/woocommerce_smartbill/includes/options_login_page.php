<?php
function wc_smartbill_login()
{
    ?>
    <div class="wrap">
        <h2><?=__('Smart Bill - Autentificare', 'wc_smartbill')?></h2>
        <?php settings_errors(); ?>
        <form action="options.php" method="post">
            <?php settings_fields('wc_smartbill_plugin_options'); ?>
            <?php do_settings_sections('wc_smartbill_plugin'); ?>

            <input name="Submit" type="submit" value="<?=__('Autentificare', 'wc_smartbill')?>" />
        </form>
    </div>
    <?php
}

function wc_smartbill_plugin_login_section_text()
{
    echo '
        <div>
            <img src="'.plugin_dir_url(__FILE__).'../assets/images/logo.png" style="width: 590px" />
        </div>
    ';
}

function wc_smartbill_settings_display_username()
{
    $options = get_option('wc_smartbill_plugin_options');
    if (!empty($options) && is_array($options) && isset($options['username'])) {
        $username = $options['username'];
    } else {
        $username = '';
    }
    echo '
    <input id="wc_smartbill_settings_username" name="wc_smartbill_plugin_options[username]" type="text" value="'.$username.'" style="width: 400px;" />';
}

function wc_smartbill_settings_display_password()
{
    echo '
    <input id="wc_smartbill_settings_password" name="wc_smartbill_plugin_options[password]" type="password" value="" style="width: 400px;" />';
}

function wc_smartbill_settings_display_token()
{
    $options = get_option('wc_smartbill_plugin_options');
    if (!empty($options) && is_array($options) && isset($options['token'])) {
        $token = $options['token'];
    } else {
        $token = '';
    }

    if (!empty($token)) {
        echo '<div style="color: green"><strong>'.__('Conectare cu succes la Smart Bill Cloud', 'wc_smartbill').'</strong></div>';
    }

    echo '
    <input id="wc_smartbill_settings_token" name="wc_smartbill_plugin_options[token]" type="hidden" value="'.$token.'" style="width: 400px;" />';
}

function wc_smartbill_plugin_options_validate($input)
{
    // get token
    $options = get_option('wc_smartbill_plugin_options');
    if (!empty($options) && is_array($options) && isset($options['token'])) {
        $token = $options['token'];
    } else {
        $token = '';
    }
    $loginResponse = SmartBillHelper::curl(
        $token,
        SmartBillHelper::LOGIN_URL,
        SmartBillHelper::prepareLoginData(
            $input,
            array(
                'url' => SmartBillHelper::getStoreURL(get_site_url()),
                'callback' => SmartBillHelper::getCallbackURL(),
                'name' => get_bloginfo('name')
            )
        )
    );

    $loginResponse = json_decode($loginResponse, true);
    if (isset($loginResponse['error'])) {
        $input['token'] = '';
        add_settings_error('wc_smartbill_settings_token', '', $loginResponse['error'], 'error');
    } else {
        $input['token'] = $loginResponse['token'];
        add_settings_error('wc_smartbill_settings_token', '', __('Autentificare realizata cu succes', 'wc_smartbill'), 'updated');
    }

    $input['password'] = '';

    return $input;
}
