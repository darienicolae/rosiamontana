// source --> https://wordpress5fc51df9a3eaf.cloud.bunnyroute.com/wp-content/plugins/sitepress-multilingual-cms/res/js/cookies/language-cookie.js?ver=4.4.6 
jQuery( 'document' ).ready(function(){
	jQuery.each( wpml_cookies, function( cookieName, cookieData ) {
		jQuery.cookie(cookieName, cookieData.value, {
			'expires': cookieData.expires,
			'path': cookieData.path
		});
	});
});