<?php
function wc_smartbill_help()
{
    ?>
    <div class="wrap">
        <h2><?=__('Smart Bill - Ajutor', 'wc_smartbill')?></h2>
    </div>

    <h3><?=__('Cum facturez comenzile WooCommerce in Smart Bill', 'wc_smartbill')?></h3>


    <p>
        <strong><?=__('Pentru a factura comenzile online in Smart Bill trebuie sa aveti pachetul Smart Bill Cloud.')?>
        </strong>
    </p>
    <ol>
        <li>
            <?=__('In meniul Smart Bill > Autentificare se salveaza configurarile cu numele si parola utilizatorului din Smart Bill. Daca se folosesc date valide, se afiseaza mesaj de succes.', 'wc_smartbill')?>
        </li>
        <li>
            <?=__('In meniul Smart Bill > Setari se salveaza configurarile dorite: compania pe care se vor emite documentele (in cazul in care contul Smart Bill e legat la mai multe companii), setarile legate de TVA daca firma e platitoare de TVA, tipul de document si seria implicita pe care se vor emite documentele si celelalte setari. Atentie si la indicatiile de sub fiecare selector.', 'wc_smartbill')?>
        </li>
        <li>
            <?=__('Optiunile din sectiunea de Setari emitere documente se vor regasi in documentele emise in Smart Bill si trebuie sa fie in concordanta cu restul setarilor din magazinul online.', 'wc_smartbill')?>
        </li>
    </ol>
    <hr />
    <h3><?=__('Intrebari frecvente', 'wc_smartbill')?></h3>
    <ol>
        <li>
            <strong><?=__('Ce drepturi trebuie sa aiba utilizatorul Smart Bill cu care voi emite documente din magazinul online?', 'wc_smartbill')?></strong>
            <br />
            <?=__('Pentru o functionare corespunzatoare, utilizatorul Smart Bill folosit la autentificare in extensia de facturare trebuie sa aiba drepturi de vizualizare, emitere si editare pe tipul de documente pe care se va face emiterea.', 'wc_smartbill')?>
        </li>
        <li>
            <strong><?=__('Daca am mai multe firme in Smart Bill, pe care dintre ele se vor emite documentele mele?', 'wc_smartbill')?></strong>
            <br />
            <?=__('In sectiunea de setari se poate alege compania pe care se vor emite documentele. Alegand compania, restul setarilor se vor prelua de pe respectiva companie (serii, unitati de masura etc), functie si de restrictiile pe serii ale utilizatorului conectat. Pentru functionarea corespunzatoare, contul de Smart Bill trebuie sa fie conectat pe aceeasi firma cu cea selectata in magazinul online.', 'wc_smartbill')?>
        </li>
        <li>
            <strong><?=__('De unde si in ce stadiu al comenzii online pot emite documentul in Smart Bill?', 'wc_smartbill')?></strong>
            <br />
            <?=__('Documentul Smart Bill poate fi emis indiferent de starea comenzii online. In ecranul de vizualizare a detaliilor unei comenzi apare butonul de Emitere in Smart Bill, in momentul in care extensia e corect instalata si conexiunea cu Smart Bill e realizata cu succes.', 'wc_smartbill')?>
        </li>
        <li>
            <strong><?=__('De ce suma din comanda WooCommerce e diferita de cea de pe documentul Smart Bill?', 'wc_smartbill')?></strong>
            <br />
            <?=__('Foarte probabil nu coincid setarile magazinului cu cele din extensia Smart Bill. Trebuie verificate setarile de cota TVA/tax, includerea transportului in documentul Smart Bill, locul de unde sunt preluate preturile produselor la emiterea documentului Smart Bill.', 'wc_smartbill')?>
        </li>
        <li>
            <strong><?=__('Ce se intampla cu optiunile de facturare din WooCommerce daca modific configurarile din Smart Bill?', 'wc_smartbill')?></strong>
            <br />
            <?=__('Dupa ce realizati o modificare in configurarile programului de facturare, trebuie improspatata conexiunea WooCommerce-Smart Bill prin salvarea configurarilor din sectiunea Smart Bill > Autentificare si Smart Bill > Setari.', 'wc_smartbill')?>
        </li>
        <li>
            <strong><?=__('Cum stiu care comenzi sunt facturate in Smart Bill?', 'wc_smartbill')?></strong>
            <br />
            <?=__('In tabelul de comenzi din sectiunea administrativa a magazinului online, langa coloana cu numarul comenzilor apare o coloana noua, cu link spre documentul Smart Bill emis pentru respectiva comanda. Linkul deschide documentul Smart Bill care poate fi rapid vizualizat/modificat/trimis prin email.', 'wc_smartbill')?>
        </li>
        <li>
            <strong><?=__('Nu apare linkul spre documentul Smart Bill, desi stiu ca am intocmit in Smart Bill', 'wc_smartbill')?></strong>
            <br />
            <?=__('Daca linkul spre documentul Smart Bill nu apare in tabelul comenzilor, probabil ca documentul emis pentru acea comanda nu a fost finalizat si a ramas in stadiul de ciorna. In acest caz, se poate emite din nou documentul, iar programul va suprascrie ciorna precedenta, ori se poate accesa programul de facturare si se poate finaliza documentul din raportul de documente emise.', 'wc_smartbill')?>
        </li>
        <li>
            <strong><?=__('Am o eroare pe un document Smart Bill si vreau sa o raportez', 'wc_smartbill')?></strong>
            <br />
            <?=__('In tabelul de comenzi din sectiunea administrativa a magazinului online, se bifeaza casuta in dreptul comenzii cu documentul problematic, iar din lista de Actiuni din dreapta sus se alege optiunea Trimite erori Smart Bill si se apasa Submit/Trimitere.', 'wc_smartbill')?>
        </li>
        <li>
            <strong><?=__('Contact Smart Bill', 'wc_smartbill')?></strong>
            <br />
            <?=__('Informatii legate de programul de facturare Smart Bill se pot obtine de pe <a href="https://www.facturionline.ro" target="_blank">www.facturionline.ro</a> sau la <a href="mailto:cloud@smartbill.ro">cloud@smartbill.ro</a>', 'wc_smartbill')?>
        </li>
    </ol>
    <?php
}
